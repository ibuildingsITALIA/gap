<?php

/**
 * Implements hook_preprocess_field().
 */
function gpunto_preprocess_field(&$variables) {
//  dpm($variables);
  if ($variables['element']['#field_name'] == 'field_list_price') {
    $variables['label'] = t('Price');
  }
}
