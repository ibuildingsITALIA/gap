<?php

/**
 * Implements hook_preprocess_page().
 */
function gpunto_preprocess_page(&$variables) {
  $variables['theme_path'] = '/' . drupal_get_path('theme', 'gpunto') . '/';

  // Add a template suggestion for iframe pages.
  if (drupal_match_path(current_path(), "renew-login\ncas")) {
    $variables['theme_hook_suggestions'][] = 'page__nomarkup';
  }

  // Check if is a page were unset url_back_destination from session.
  $path = current_path();
  if (!empty($_SESSION['url_back_destination']) && ($path != 'signup' && $path != 'register' && $path != 'giunti-card/bind')) {
    unset($_SESSION['url_back_destination']);
  }

  // Add a template suggestion if viewing the site from app.
  if (isset($_GET['source']) && 'app' === $_GET['source']) {
    $variables['theme_hook_suggestions'][] = 'page__app';
  }

  // Add the Giunti special css if possible.
  $uri = file_default_scheme() . '://uploads/giunti.css';
  // We don't do file_exists() call anymore to don't block page rendering.
  drupal_add_css($uri, array('preprocess' => FALSE));

  // Add Colorbox on all pages.
  drupal_add_js(drupal_get_path('theme', 'gpunto') . '/libraries/colorbox/jquery.colorbox-min.js');
}
