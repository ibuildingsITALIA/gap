<?php

/**
 * @file
 * Contains a pre-process hook for 'gp_signup_page'.
 */

/**
 * Implements hook_preprocess_gp_signup_page().
 */
function gpunto_preprocess_gp_signup_page(&$variables) {
  // If possible, show points and purchase link
  // if a cart or an item is available.
  $variables['points'] = $variables['purchase_url'] = FALSE;
  if ($context = gp_card_points_context_info()) {
    $variables['points'] = theme('gp_card_earn_points', array('points' => $context['points']));
    $variables['purchase_url'] = $context['purchase_url'];
  }

  // Get the return url from the destination parameter, if valid.
  $query = array();
  if (isset($_GET['destination']) && !url_is_external($_GET['destination'])) {
    $query['destination'] = $_GET['destination'];
  }

  // Add a link to the create card page.
  $query['card'] = 'create';
  $variables['register_create_card'] = array(
    '#markup' => l(t('Create your GiuntiCard', array('context' => 'giunti-signup')), 'register', array(
      'query' => $query,
      'attributes' => array(
        'class' => array('btn'),
      ),
    )),
  );

  // Add a link to the bind card page.
  $query['card'] = 'bind';
  $variables['register_bind_card'] = array(
    '#markup' => l(t('Bind your GiuntiCard', array('context' => 'giunti-signup')), 'register', array(
      'query' => $query,
      'attributes' => array(
        'class' => array('btn'),
      ),
    )),
  );
}
