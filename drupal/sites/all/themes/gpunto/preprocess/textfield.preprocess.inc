<?php

/**
 * Implements hook_preprocess_textfield().
 */
function gpunto_preprocess_textfield(&$variables) {
  if (strpos($variables['element']['#name'], 'search_api_views_fulltext') !== FALSE && !isset($variables['element']['#attributes']['placeholder'])) {
    $placeholder = 'Cerca la libreria';
    $variables['element']['#attributes']['placeholder'] = $placeholder;
  }
}
