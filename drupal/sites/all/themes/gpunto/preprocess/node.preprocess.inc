<?php

/**
 * @file
 * Contains a pre-process hook for 'node'.
 */

/**
 * Implements hook_preprocess_node().
 */
function gpunto_preprocess_node(&$variables) {
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  // Only book in detail view mode.
  if (('book' === $variables['type']) && ('detail' === $variables['view_mode'])) {
    // Field image is empty or is set default image.
    if (empty($variables['field_image']) || !empty($variables['field_image'][0]['is_default'])) {
      // Module gp_amazone is active.
      if (module_exists('gp_amazon')) {
        $asin = $variables['field_asin']['und'][0]['value'];
        // Get AmazonItem via API.
        $amazon_item = gp_amazon_get_single_item($asin);
        // Set the new field with the image from amazon.
        $image = array(
          'uri' => $amazon_item->getAttributes()['MediumImage']['URL'],
          'height' => $amazon_item->getAttributes()['MediumImage']['Height'],
          'width' => $amazon_item->getAttributes()['MediumImage']['Width'],
        );
        // Add the new field in the node content.
        $variables['content']['field_image_amazon'] = array($image);
      }
    }
  }
}
