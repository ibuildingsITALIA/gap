<?php

/**
 * Implements hook_preprocess_maintenance_page().
 */
function gpunto_preprocess_maintenance_page(&$variables) {
  $variables['theme_path'] = '/' . drupal_get_path('theme', 'gpunto') . '/';
}
