<?php

/**
 * @file
 * Contains a pre-process hook for 'user_profile_category'.
 */

/**
 * Implements hook_preprocess_user_profile_category().
 */
function gpunto_preprocess_user_profile_category(&$variables) {
  // Use classes added in code.
  if (!empty($variables['element']['#classes_array'])) {
    $variables['classes_array'] = array_unique(array_merge($variables['classes_array'], $variables['element']['#classes_array']));
  }
}
