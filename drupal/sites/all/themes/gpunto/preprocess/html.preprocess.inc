<?php

/**
 * Implements hook_preprocess_html().
 */
function gpunto_preprocess_html(&$variables) {
  // Use Product name only for product page title.
  if (!($variables['is_front']) && (in_array('page-product', $variables['classes_array']))) {
    /* @var AmazonItem $item */
    $item = gp_amazon_load_item_from_url();
    $product_type = $item->ItemAttributes['ProductTypeName'];
    if (($product_type == 'ABIS_BOOK')) {
      $product_authorship = parse_authorship($item, 'Author', 'Autore');
      $authors = '';
      foreach ($product_authorship as $author) {
        $authors .= $author['name'] . ', ';
      }
      if (!empty($authors)) {
        $authors = substr($authors, 0, -2);
      }
      $placeholder = variable_get('amazon_book_title', 'Libro %%title%% di %%author%%');
      $replaced = str_replace('%%title%%', $item->getTitle(), $placeholder);
      $replaced = str_replace('%%author%%', $authors, $replaced);
      $variables['head_title'] = $replaced;
    }
    else {
      if (($product_type == 'ABIS_EBOOKS')) {
        $product_authorship = parse_authorship($item, 'Author', 'Autore');
        $product_authorship = array_merge($product_authorship, parse_authorship($item, 'Creator', 'a cura di'));
        $authors = '';
        foreach ($product_authorship as $author) {
          $authors .= $author['name'] . ', ';
        }
        if (!empty($authors)) {
          $authors = substr($authors, 0, -2);
        }
        $placeholder = variable_get('amazon_book_title', 'eBook %%title%% di %%author%%');
        $replaced = str_replace('%%title%%', $item->getTitle(), $placeholder);
        $replaced = str_replace('%%author%%', $authors, $replaced);
        $variables['head_title'] = $replaced;
      }
      else {
        $variables['head_title'] = $variables['head_title_array']['title'];
      }
    }
  }

  // Add conditional stylesheets for IE.
  drupal_add_css(
    drupal_get_path('theme', 'gpunto') . '/css/gpunto.ie.css',
    array(
      'group' => CSS_THEME,
      'browsers' => array(
        'IE' => 'lte IE 10',
        '!IE' => FALSE,
      ),
      'weight' => 999,
      'every_page' => TRUE,
    )
  );
}
