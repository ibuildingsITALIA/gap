<?php

/**
 * Implements hook_preprocess_region().
 */
function gpunto_preprocess_region(&$variables) {
  // Simple variable to retrieve path for the theme.
  $variables['theme_path'] = '/' . drupal_get_path('theme', 'gpunto') . '/';
}
