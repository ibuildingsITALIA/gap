<?php

/**
 * Implements hook_preprocess_gp_amazon_item().
 */
function gpunto_preprocess_gp_amazon_item(&$variables) {
  /* @var AmazonItem $item */
  $item = $variables['item'];

  // Normalize imagesets to an array.
  $images = array();

  if (isset($item->ImageSets)) {
    if (array_key_exists('SmallImage', $item->ImageSets['ImageSet'])) {
      // There is only one ImageSet, normalize.
      $images = array($item->ImageSets['ImageSet']);
    }
    else {
      $images = $item->ImageSets['ImageSet'];
    }
  }

  $variables['images'] = $images;

  // Wrap OfferListing.
  if (!empty($item->Offers['TotalOffers']) && !empty($item->Offers['Offer']['OfferListing'])) {
    $offer_listing = $item->Offers['Offer']['OfferListing'];

    $variables['offer_listing'] = $offer_listing;

    $price = _gpunto_amazon_item_get_price($offer_listing);

    // Set prices.
    $variables['actual_price'] = $price['actual_price'];
    $variables['list_price'] = $price['list_price'];
    $variables['amount_saved'] = $price['amount_saved'];
    $variables['list_price'] = $price['list_price'];

    // Format the discount percentage.
    $variables['saved_percentage'] = gp_amazon_discount_percentage($price['actual_price'], $price['amount_saved']);
  }

  // Mi prendo il l'indice principale della ricerca corrente (se presente).
  if ($item->getBrowseNode()) {
    $variables['search_index'] = $item->getBrowseNode()->getSearchIndex();
  }

  // Controlla che il prodotto corrente sia un libro.
  $variables['is_book'] = check_product_type(
    $item,
    array('ABIS_BOOK', 'ABIS_EBOOKS', 'BOOKS_1973_AND_LATER')
  );
  $variables['is_ebook'] = check_product_type(
    $item,
    array('ABIS_EBOOKS')
  );
  $variables['is_video'] = check_product_type(
    $item,
    array('ABIS_DVD')
  );
  $variables['is_music'] = check_product_type(
    $item,
    array('ABIS_MUSIC', 'DOWNLOADABLE_MUSIC_ALBUM', 'DOWNLOADABLE_MUSIC_TRACK')
  );
  $variables['is_digital_music'] = check_product_type(
    $item,
    array('DOWNLOADABLE_MUSIC_ALBUM', 'DOWNLOADABLE_MUSIC_TRACK')
  );
  $variables['is_app'] = check_product_type(
    $item,
    array('MOBILE_APPLICATION')
  );

  // Parso le info degli autori.
  $product_authorship = array();
  if ($variables['is_book']) {
    $product_authorship = array_merge($product_authorship, parse_authorship($item, 'Author', 'Autore'));
    $product_authorship = array_merge($product_authorship, parse_authorship($item, 'Creator', 'a cura di'));
  }
  if ($variables['is_video']) {
    $product_authorship = array_merge($product_authorship, parse_authorship($item, 'Director', 'Regista'));
    $product_authorship = array_merge($product_authorship, parse_authorship($item, 'Creator', 'Attore'));
  }
  if ($variables['is_music']) {
    $product_authorship = array_merge($product_authorship, parse_authorship($item, 'Artist', 'Artista'));
  }
  if ($variables['is_digital_music']) {
    $product_authorship = array_merge($product_authorship, parse_authorship($item, 'Creator', 'Artista'));
  }

  foreach ($product_authorship as $key => $author) {
    // Build search link.
    $parameters = array(
      'bt' => t('Author') . ': ' . $author['name'],
      'Author' => $author['name'],
      'Sort' => 'relevancerank',
      'BrowseNode' => '411663031',
    );
    $search_url = url('search', array(
      'query' => gp_amazon_search_get_search_query($parameters),
    ));

    // Add search-url to array.
    $product_authorship[$key]['search-url'] = $search_url;
  }
  $variables['product_authorship'] = $product_authorship;

  // Prendo solo i nomi degli autori (solo in view_mode detail).
  $product_authors = array();
  if ($variables['view_mode'] != 'full') {
    foreach ($product_authorship as $authorship) {
      if ($authorship['role_id'] == 'Author' || $authorship['role_id'] == 'Director' || $authorship['role_id'] == 'Artist') {
        $product_authors[] = $authorship['name'];
      }
      elseif ($variables['is_digital_music'] && $authorship['role_id'] == 'Creator') {
        $product_authors[] = $authorship['name'];
      }
    }
  }
  $variables['product_authors'] = $product_authors;

  // Controllo che "Prime" sia attivo.
  $variables['prime'] = isset($variables['offer_listing']['IsEligibleForSuperSaverShipping']) && $variables['offer_listing']['IsEligibleForSuperSaverShipping'] == 1;

  // Product date.
  $product_date = FALSE;
  if (isset($item->ItemAttributes['ReleaseDate'])) {
    $product_date['amazon_value'] = $item->ItemAttributes['ReleaseDate'];
  }

  // Build markup to render.
  if ($product_date) {
    $today = new DateTime(date('Y-m-d H:i:s'));
    $next_months = new DateTime(date('Y-m-d H:i:s'));
    $next_months->add(new DateInterval('P3M'));
    $prev_month = new DateTime(date('Y-m-d H:i:s'));
    $prev_month->sub(new DateInterval('P1M'));

    $publication_date = new DateTime($product_date['amazon_value']);

    $markup_date = '<span class="date-display-single">';
    $markup_date .= date_format($publication_date, 'd/m/Y');
    $markup_date .= '</span>';
    $markup_label = '';

    // From today to next three months.
    if (($publication_date > $today) && ($publication_date < $next_months)) {
      $markup_label = t('New expeted release from');
    }
    // From past month to today.
    elseif (($publication_date <= $today) && ($publication_date > $prev_month)) {
      $markup_label = t('New release from');
    }
    // Other cases.
    else {
      $product_date['#access'] = FALSE;
    }

    $markup = '<p class="exp-release-date-label">';
    $markup .= $markup_label . '&nbsp;';
    $markup .= $markup_date;
    $markup .= '</p>';
    $product_date['#markup'] = $markup;
  }

  $variables['product_date'] = $product_date;

  $variables['availability'] = FALSE;
  if (isset($variables['offer_listing'])) {

    $availability_attributes = $variables['offer_listing']['AvailabilityAttributes'];

    // Availability type.
    $availability_type = $availability_attributes['AvailabilityType'];

    // Is preorder?
    $is_preorder = isset($availability_attributes['IsPreorder']) && $availability_attributes['IsPreorder'] == '1';

    // Avaiability text.
    $availability_text = FALSE;
    $availability_moretext = FALSE;
    if ($availability_type == 'now' && !$is_preorder) {
      $availability_text = 'Disponibilità immediata.';
    }
    if ($availability_type == 'futureDate') {
      $availability_text = 'Disponibile a breve.';
    }
    if ($availability_type == 'unknown') {
      $availability_text = 'Attualmente non disponibile.';
    }
    if ($availability_type == 'now' && $is_preorder) {
      $publication_date = new DateTime($product_date['amazon_value']);
      $availability_text = 'Disponibile dal ' . date_format($publication_date, 'd/m/Y') . '.';
    }

    if (array_key_exists('Availability', $variables['offer_listing'])) {
      $availability_moretext = $variables['offer_listing']['Availability'];
    }

    $variables['availability'] = array(
      'type' => $availability_type,
      'text' => $availability_text,
      'more_text' => $availability_moretext,
      'is_preorder' => $is_preorder,
    );
  }

  // kindle.
  if (isset($item->ItemAttributes['Format']) && $item->ItemAttributes['Format'] == 'eBook Kindle') {
    $variables['availability'] = array(
      'type' => 'kindle',
      'text' => 'Disponibile su Kindle Store',
      'is_preorder' => FALSE,
    );
  }

  if (!empty($variables['view_mode'])) {
    $function = __FUNCTION__ . '_' . $variables['view_mode'];
    if (function_exists($function)) {
      $function($variables);
    }
  }
}

/**
 * Implement hook_preprocess_gp_amazon_item_image().
 */
function gpunto_preprocess_gp_amazon_item_image(&$variables) {
  $variables['title'] = truncate_utf8($variables['title'], 37, TRUE, TRUE);
}

/**
 * Implement hook_preprocess_gp_amazon_item_full().
 */
function gpunto_preprocess_gp_amazon_item_full(&$variables) {
  /* @var AmazonItem $item */
  $item = $variables['item'];

  // Descrizione prodotto (molto variabile).
  $variables['description'] = FALSE;
  if (isset($item->ItemAttributes['Feature'])) {
    // Se è un array ciclo e costruisco una lista HTML.
    if (is_array($item->ItemAttributes['Feature'])) {
      $variables['description'] = '<ul>';
      foreach ($item->ItemAttributes['Feature'] as $feature) {
        $variables['description'] .= '<li>' . $feature[0] . '&nbsp;</li>';
      }
      $variables['description'] .= '</ul>';
    }
    else {
      $variables['description'] = $item->ItemAttributes['Feature'];
    }
  }
  if ($variables['is_ebook']) {
    $placeholder = variable_get('amazon_book_description', "Compra l'eBook %%title%% di %%author%%; lo trovi in offerta a prezzi scontati su Giuntialpunto.it");

    $product_authorship = parse_authorship($item, 'Author', 'Autore');
    $product_authorship = array_merge($product_authorship, parse_authorship($item, 'Creator', 'a cura di'));
    $authors = '';
    foreach ($product_authorship as $author) {
      $authors .= $author['name'] . ', ';
    }
    if (!empty($authors)) {
      $authors = substr($authors, 0, -2);
    }

    $replaced = str_replace('%%title%%', $item->getTitle(), $placeholder);
    $replaced = str_replace('%%author%%', $authors, $replaced);

    $meta_description = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'description',
        'content' => $replaced,
      ),
    );
    drupal_add_html_head($meta_description, 'description');
  }
  // Add meta description only if is a book item.
  else {
    if ($variables['is_book']) {
      $placeholder = variable_get('amazon_book_description', 'Compra il libro %%title%% di %%author%%; lo trovi in offerta a prezzi scontati su Giuntialpunto.it');

      $product_authorship = parse_authorship($item, 'Author', 'Autore');
      $authors = '';
      foreach ($product_authorship as $author) {
        $authors .= $author['name'] . ', ';
      }
      if (!empty($authors)) {
        $authors = substr($authors, 0, -2);
      }

      $replaced = str_replace('%%title%%', $item->getTitle(), $placeholder);
      $replaced = str_replace('%%author%%', $authors, $replaced);

      $meta_description = array(
        '#type' => 'html_tag',
        '#tag' => 'meta',
        '#attributes' => array(
          'name' => 'description',
          'content' => $replaced,
        ),
      );
      drupal_add_html_head($meta_description, 'description');
    }
  }
  $variables['editorial_reviews'] = FALSE;
  if (isset($item->EditorialReviews)) {
    foreach ($item->EditorialReviews as $editorial_review) {
      // Ci può essere il caso in cui esiste un ulteriore livello di array.
      if (array_key_exists('Content', $editorial_review)) {
        $variables['editorial_reviews'][$editorial_review['Source']] = $editorial_review['Content'];
      }
      else {
        foreach ($editorial_review as $editorial_review_second) {
          $variables['editorial_reviews'][$editorial_review_second['Source']] = $editorial_review_second['Content'];
        }
      }
    }
    $variables['editorial_reviews'] = array_unique($variables['editorial_reviews']);
  }

  $product_details = process_product_details($item);
  $product_type = $item->ItemAttributes['ProductTypeName'];

  if (($product_type == 'ABIS_BOOK') || ($product_type == 'ABIS_EBOOKS')) {

    /*
     * Rimuovo "Binding" e "NumberOfPages" per creare un attributo
     * personalizzato "Binding => NumberOfPages"
     */
    if (array_key_exists('_NumberOfPages', $product_details)) {
      $new_binding = array($product_details['_Binding'] => $product_details['_NumberOfPages'] . ' pagine');
      unset($product_details['_NumberOfPages']);
    }
    else {
      $new_binding = array($product_details['_Binding'] => '&nbsp;');
    }
    unset($product_details['_Binding']);
    $product_details = array_merge($new_binding, $product_details);

    // Formatting Release Date.
    if (!empty($product_details[t('Release Date')])) {
      $publication_date = new DateTime($product_details[t('Release Date')]);
      unset($product_details[t('Release Date')]);
      $product_details[t('Release Date')] = date_format($publication_date, 'd/m/Y');
    }

    // Check $content['node']['field_series_description'].
    if (!empty($variables['content']['node'])) {
      $node = $variables['content']['node'];
      if (!empty($node['field_series_description'])) {

        // Build search link.
        $series = $node['field_series_description']['#items'][0]['value'];
        $publisher_term = taxonomy_term_load($node['field_publisher']['#items'][0]['tid']);
        $parameters = array(
          'bt' => t('Series') . ': ' . $node['field_series_description'][0]['#markup'],
          'sort' => 'field_amazon_rank',
          'order' => 'asc',
          'editore' => $publisher_term->name,
          'collana' => $series,
        );

        $search_url = url('book_search', array('query' => $parameters));
        $prefix = '<a href="' . $search_url . '">';
        $suffix = '</a>';
        $markup = $node['field_series_description'][0]['#markup'];

        $variables['content']['node']['field_series_description'][0]['#markup'] = $prefix . $markup . $suffix;
      }
    }
  }

  if ($product_type == 'ABIS_MUSIC') {

    /*
     * Rimuovo "Binding" per creare un attributo
     * personalizzato "Binding => ProductDate"
     */
    $new_binding = array($product_details['_Binding'] => $variables['product_date']);
    unset($product_details['_Binding']);
    $product_details = array_merge($new_binding, $product_details);
  }

  $variables['product_details'] = $product_details;

  $variables['alternate_versions'] = _gpunto_amazon_item_alternate_versions($item);

  $variables['product_warnings'] = array(
    '#prefix' => '<span class="product-warnings"><span class="icon-info"></span>',
    '#markup' => t('See <a href="@url">warnings and details of this product</a> on Amazon.it', array('@url' => $item->DetailPageURL . '#productDetails')),
    '#suffix' => '</span>',
  );

  // Set GiuntiCard points and value.
  if (($variables['is_book']) && (!empty($variables['actual_price']))) {
    $variables['giunticard_points'] = gp_card_calculate_points($variables['actual_price']);
    $variables['giunticard_points_value'] = gp_card_calculate_points_value($variables['giunticard_points']);
  }
}


/**
 * Implement hook_preprocess_gp_amazon_item_detail().
 */
function gpunto_preprocess_gp_amazon_item_detail(&$variables) {
  /* @var AmazonItem $item */
  $item = $variables['item'];

  // Descrizione prodotto (molto variabile).
  $variables['description'] = FALSE;
  if (isset($item->ItemAttributes['Feature'])) {
    // Se è un array ciclo e costruisco una lista HTML.
    if (is_array($item->ItemAttributes['Feature'])) {
      $variables['description'] = '<ul>';
      foreach ($item->ItemAttributes['Feature'] as $feature) {
        $variables['description'] .= '<li>' . strip_tags($feature[0]) . '</li>';
      }
      $variables['description'] .= '</ul>';
    }
    else {
      $variables['description'] = $item->ItemAttributes['Feature'];
    }
  }

  $variables['editorial_reviews'] = FALSE;
  if (isset($item->EditorialReviews)) {
    foreach ($item->EditorialReviews as $editorial_review) {
      // Ci può essere il caso in cui esiste un ulteriore livello di array.
      if (array_key_exists('Content', $editorial_review)) {
        $variables['editorial_reviews'][$editorial_review['Source']] = truncate_utf8(strip_tags($editorial_review['Content']), 100, TRUE, TRUE);
      }
      else {
        foreach ($editorial_review as $editorial_review_second) {
          $variables['editorial_reviews'][$editorial_review_second['Source']] = truncate_utf8(($editorial_review_second['Content']), 100, TRUE, TRUE);
        }
      }
    }
    $variables['editorial_reviews'] = array_unique($variables['editorial_reviews']);
  }

  $product_details = process_product_details($item);
  $product_type = $item->ItemAttributes['ProductTypeName'];

  if (($product_type == 'ABIS_BOOK') || ($product_type == 'ABIS_EBOOKS')) {
    // Formatting Release Date.
    if (!empty($product_details[t('Release Date')])) {
      $publication_date = new DateTime($product_details[t('Release Date')]);
      unset($product_details[t('Release Date')]);
      $product_details[t('Release Date')] = format_date($publication_date->getTimestamp(), 'custom', 'j M Y');
    }
  }
  $variables['product_details'] = $product_details;
}

/**
 * Parse the authorship information.
 *
 * @param AmazonItem $item
 *   The Amazon item
 *
 * @param string $type
 *   The type of attribute
 *
 * @param string $name
 *   The role of the author
 *
 * @return array
 *   book_authorship
 */
function parse_authorship(AmazonItem &$item, $type, $name) {
  $book_authorship = array();

  if (!isset($item->ItemAttributes[$type])) {
    return array();
  }

  if (is_array($item->ItemAttributes[$type])) {
    foreach ($item->ItemAttributes[$type] as $author) {
      $book_authorship[] = array(
        'name' => $author[0],
        'role' => $name,
        'role_id' => $type,
      );
    }
  }
  else {
    $book_authorship[] = array(
      'name' => $item->ItemAttributes[$type],
      'role' => $name,
      'role_id' => $type,
    );
  }

  return $book_authorship;
}

/**
 * Check the product type
 *
 * @param AmazonItem $item
 * @param array $types List of ProductTypeName
 * @return bool Return TRUE or FALSE if the product $item belongs types $types
 */
function check_product_type(AmazonItem $item, $types) {
  return isset($item->ItemAttributes['ProductTypeName']) && in_array($item->ItemAttributes['ProductTypeName'], $types);
}

/**
 * Extract and normalize the product attributes (or product details)
 *
 * @param AmazonItem $item
 * @return array
 */
function process_product_details(AmazonItem $item) {

  /*
   * Attributi con chiave con "_" davanti vengono estratti a mano,
   * quelli senza rispecchiano l'attributo originale dell'AmazonItem.
   *
   * Valori con "_" davanti non vengono tradotti perché verranno
   * processati successivamente prima di essere inviati al template.
   *
   */

  $attribute_templates = array(
    'ABIS_DVD' => array(
      'Actor' => t('Actors'),
      'Director' => t('Director'),
      'Format' => t('Format'),
      '_AudioFormat' => t('Audio'),
      '_Language' => t('Language'),
      '_Subtitle' => t('Subtitles'),
      '_DeafSubtitle' => t('Deaf subtitles'),
      '_RegionCode' => t('Region'),
      'AspectRatio' => t('Aspect ratio'),
      'NumberOfDiscs' => t('Number of discs'),
      'Publisher' => t('Studios'),
      'ReleaseDate' => t('DVD version date'),
      'RunningTime' => t('Running time'),
      '_ASIN' => t('ASIN'),
    ),
    'ABIS_BOOK' => array(
      'Binding' => '_Binding',
      'NumberOfPages' => '_NumberOfPages',
      '_Publisher' => t('Publisher'),
      'BookSeries' => t('BookSeries'), // guess
      '_Language' => t('Language'),
      'ISBN' => t('ISBN-10'),
      'EAN' => t('ISBN-13'),
      '_Dimensions' => t('Dimensions'),
      '_Weight' => t('Shipping weight'),
      'ReleaseDate' => t('Release Date'),
    ),
    'ABIS_EBOOKS' => array(
      'Binding' => '_Binding',
      'NumberOfPages' => '_NumberOfPages',
      '_Publisher' => t('Publisher'),
      'BookSeries' => t('BookSeries'), // guess
      '_Language' => t('Language'),
      'ISBN' => t('ISBN-10'),
      'EAN' => t('ISBN-13'),
    ),
    'ABIS_MUSIC' => array(
      'Binding' => '_Binding',
      'NumberOfDiscs' => t('Number of discs'),
      'Label' => t('Label'),
      '_ASIN' => t('ASIN'),
    ),
    'NOTEBOOK_COMPUTER' => array(
      'Brand' => t('Brand'),
      'Model' => t('Model'),
      'Color' => t('Color'),
      'Warranty' => t('Warranty'),
      '_Dimensions' => t('Dimensions'),
      '_Weight' => t('Shipping weight'),
      'OperatingSystem' => t('Operating system'),
      '_ASIN' => t('ASIN'),
    ),
    'ABIS_ELECTRONICS' => array(
      'Brand' => t('Brand'),
      'Model' => t('Model'),
      '_Dimensions' => t('Dimensions'),
      '_Weight' => t('Shipping weight'),
      '_ASIN' => t('ASIN'),
    ),
    'DOWNLOADABLE_MUSIC_ALBUM' => array(
      'Label' => t('Label'),
      'ReleaseDate' => t('Release date'),
      '_RunningTime' => t('Total running time'),
      '_ASIN' => t('ASIN'),
    ),
    'DOWNLOADABLE_MUSIC_TRACK' => array(
      'Label' => t('Label'),
      'ReleaseDate' => t('Release date'),
      '_RunningTime' => t('Running time'),
      '_ASIN' => t('ASIN'),
    ),
  );

  $region_codes_bluray = array(
    '1' => t('Region @code', array('@code' => 'A')),
    '2' => t('Region @code', array('@code' => 'B')),
    '3' => t('Region @code', array('@code' => 'C')),
  );

  // Non c'è il ProductTypeName o non ho un template? esco.
  if (!isset($item->ItemAttributes['ProductTypeName']) && !array_key_exists($item->ItemAttributes['ProductTypeName'], $attribute_templates)) {
    return array();
  }

  $product_type = $item->ItemAttributes['ProductTypeName'];
  $product_details = array();

  $languages = array();
  $audio_formats = array();
  $subtitles = array();
  $deaf_subtitles = array();

  // Languages parsing (unique cycle).
  if (isset($item->ItemAttributes['Languages'])) {

    if (array_key_exists('Name', $item->ItemAttributes['Languages']['Language'])) {
      parse_languages($item->ItemAttributes['Languages']['Language'], $languages, $audio_formats, $subtitles, $deaf_subtitles);
    }

    foreach ($item->ItemAttributes['Languages']['Language'] as $language) {
      if (!is_array($language)) {
        continue;
      }
      parse_languages($language, $languages, $audio_formats, $subtitles, $deaf_subtitles);
    }

    $languages = array_unique($languages);
    $audio_formats = array_unique($audio_formats);
    $subtitles = array_unique($subtitles);
  }

  if (isset($attribute_templates[$product_type])) {
    foreach ($attribute_templates[$product_type] as $attribute_key => $attribute) {

      if (array_key_exists($attribute_key, $item->ItemAttributes)) {
        // Standard attributes.
        $attributes = parse_generic($item, $attribute_key);
        $product_details[$attribute] = implode(', ', $attributes);
      }
      else {
        // Custom parsing.
        switch ($attribute_key) {

          case '_ASIN':
            $product_details[$attribute] = $item->ASIN;
            break;

          case '_Weight';
            if (!empty($item->ItemAttributes['PackageDimensions'])) {
              $weight = $item->ItemAttributes['PackageDimensions']['Weight'];

              // Assumo il peso in centesimi di libbre, converto in grammi.
              $weight = round($weight * 4.5359237);

              if ($weight >= 1000) {
                $weight = ($weight / 1000) . ' kg';
              }
              else {
                $weight .= ' g';
              }

              $product_details[$attribute] = $weight;
            }
            break;

          case '_Dimensions';
            if (!empty($item->ItemAttributes['PackageDimensions'])) {
              $dim = $item->ItemAttributes['PackageDimensions'];

              // Assumo le dimensioni in centesimi di pollici, converto in centrimetri.
              $dim['Length'] = round($dim['Length'] * 0.0254);
              $dim['Width'] = round($dim['Width'] * 0.0254);
              $dim['Height'] = round($dim['Height'] * 0.0254);

              $dimensions = sprintf('<span title="Lunghezza">%s</span> x <span title="Larghezza">%s</span> x <span title="Altezza">%s</span> cm', $dim['Length'], $dim['Width'], $dim['Height']);
              $product_details[$attribute] = $dimensions;
            }
            break;

          case '_Publisher':
            if (!empty($item->ItemAttributes['Publisher'])) {
              $product_details[$attribute] = $item->ItemAttributes['Publisher'];
              if (isset($item->ItemAttributes['PublicationDate'])) {
                $product_details[$attribute] .= ' (' . $item->ItemAttributes['PublicationDate'] . ')';
              }
            }
            break;

          case '_RunningTime':
            if (!empty($item->ItemAttributes['RunningTime'])) {
              // Assumo la durata in secondi, converto in minuti.
              $seconds = intval($item->ItemAttributes['RunningTime']);
              if ($seconds >= 3600) {
                $format = 'H:i:s';
              }
              else {
                $format = 'i:s';
              }
              $product_details[$attribute] = gmdate($format, $seconds);
            }
            break;

          case '_RegionCode':
            if (isset($item->ItemAttributes['RegionCode'])) {
              $region_code = $item->ItemAttributes['RegionCode'];

              if (isset($item->ItemAttributes['Binding']) && $item->ItemAttributes['Binding'] == 'Blu-ray') {
                $product_details[$attribute] = !empty($region_codes_bluray[$region_code]) ? $region_codes_bluray[$region_code] : '';
              }
              else {
                $product_details[$attribute] = t('Region @code', array('@code' => $region_code));
              }
            }
            break;

          case '_Language':
            if (count($languages)) {
              $product_details[$attribute] = implode(', ', $languages);
            }
            break;

          case '_AudioFormat':
            if (count($audio_formats)) {
              $product_details[$attribute] = implode(', ', $audio_formats);
            }
            break;

          case '_Subtitle':
            if (count($subtitles)) {
              $product_details[$attribute] = implode(', ', $subtitles);
            }
            break;

          case '_DeafSubtitle':
            if (count($deaf_subtitles)) {
              $product_details[$attribute] = implode(', ', $deaf_subtitles);
            }
            break;

          case 'ReleaseDate':
            if (!empty($item->ItemAttributes['ReleaseDate'])) {
              $publication_date = new DateTime($item->ItemAttributes['ReleaseDate']);
              $product_details[$attribute] = date_format($publication_date, 'd/m/Y');
            }
            break;
        }
      }
    }
  }

  return $product_details;
}

/**
 * Generic product attribute parsing (single or array)
 *
 * @param AmazonItem $item
 * @param $attribute
 * @return array
 */
function parse_generic(AmazonItem $item, $attribute) {
  $values = array();
  if (is_array($item->ItemAttributes[$attribute])) {
    foreach ($item->ItemAttributes[$attribute] as $value) {
      $values[] = reset($value);
    }
    $values = array_unique($values);
  }
  elseif (!empty($item->ItemAttributes[$attribute])) {
    $values[] = $item->ItemAttributes[$attribute];
  }
  return $values;
}

/**
 * Parse product languages and update arrays provided by params.
 *
 * @param array $language
 * @param array $languages
 * @param array $audio_formats
 * @param array $subtitles
 * @param array $deaf_subtitles
 */
function parse_languages(array $language, array &$languages, array &$audio_formats, array &$subtitles, array &$deaf_subtitles) {
  // Subtitles.
  if ($language['Type'] == 'Sottotitolato') {
    $subtitles[] = $language['Name'];
  }
  elseif ($language['Type'] == 'Sottotitoli per non udenti') {
    $deaf_subtitles[] = $language['Name'];
  }
  else {
    // Generic languages.
    $languages[] = $language['Name'];
  }

  if (array_key_exists('AudioFormat', $language) || $language['Type'] == 'Lingua originale' || ($language['Type'] == 'Sottotitolato' && array_key_exists('AudioFormat', $language))) {
    $audio_format = $language['Name'];
    // Audio formats.
    if (array_key_exists('AudioFormat', $language)) {
      $audio_format .= ' (' . $language['AudioFormat'] . ')';
    }
    $audio_formats[] = $audio_format;
  }
}

/**
 * Create link to Alternate Versions
 *
 * @param AmazonItem $item
 * @return array alternate_version_links
 */
function _gpunto_amazon_item_alternate_versions(AmazonItem &$item) {
  $alternate_version_items = array();

  // No alternate versions.
  if (!isset($item->AlternateVersions)) {
    return array();
  }

  // Array of AlternateVersions ASIN.
  $alternate_items = array();
  foreach ($item->AlternateVersions as $version) {
    $alternate_items[] = $version['ASIN'];
  }

  $items = gp_amazon_get_items($alternate_items);

  foreach ($items as $item) {
    if (!empty($item->Offers['Offer']['OfferListing'])) {
      $offer_listing = $item->Offers['Offer']['OfferListing'];
      $price = _gpunto_amazon_item_get_price($offer_listing);

      $alternate_version_items[] = array(
        'binding' => $item->ItemAttributes['Binding'],
        'link' => "product/{$item->ASIN}",
        'price' => gp_amazon_currency_format($price['actual_price']),
      );
    }
  }

  return $alternate_version_items;
}

/**
 * Function to return prices values from the Offer Listing.
 * @param $offer_listing
 * @return array
 */
function _gpunto_amazon_item_get_price(&$offer_listing) {
  $price = array();

  // Actual price.
  if (!empty($offer_listing['SalePrice']['Amount'])) {
    $price['actual_price'] = $offer_listing['SalePrice']['Amount'];
  }
  else {
    $price['actual_price'] = $offer_listing['Price']['Amount'];
  }

  // Check if discount are applied and calculate list price.
  $price['amount_saved'] = 0;
  if (!empty($offer_listing['AmountSaved']['Amount'])) {
    $price['amount_saved'] = $offer_listing['AmountSaved']['Amount'];
  }
  $price['list_price'] = $price['actual_price'] + $price['amount_saved'];

  return $price;
}
