<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * Giunti al Punto theme.
 */

/**
 * Implements hook_page_alter().
 */
function gpunto_page_alter(&$page) {
  // Force some regions to be rendered.
  // @see omega/template.php
  $regions = system_region_list($GLOBALS['theme_key'], REGIONS_VISIBLE);
  $force = array('header_top', 'footer_first');
  foreach (array_intersect_key($regions, array_flip($force)) as $region => $name) {
    if (empty($page[$region])) {
      $page[$region]['#theme_wrappers'] = array('region');
      $page[$region]['#region'] = $region;
    }
  }
}
