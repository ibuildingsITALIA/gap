<?php

/**
 * @file
 * Contains the theme function override for 'breadcrumb'.
 */

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @ingroup themeable
 */
function gpunto_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $output = '';

  if (!empty($breadcrumb)) {
    $total_breads = count($breadcrumb);

    // If the breadcrumb length is of only one item,
    // don't render anything.
    if (1 === $total_breads) {
      return $output;
    }

    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    // Override Drupal core breadcrumb item imploding (no arrow).
    $output .= '<ul class="breadcrumb">';

    foreach ($breadcrumb as $key => $bread) {
      $output .= '<li>' . $bread;
      if ($total_breads != $key + 1) {
        $output .= '&nbsp;>&nbsp;';
      }
      $output .= '</li>';
    }

    $output .= '</ul>';

    return $output;
  }
}
