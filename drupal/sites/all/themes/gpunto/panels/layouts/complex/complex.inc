<?php

/**
 * @file
 * Defines a full one-column layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Complex'),
  'category' => t('Giunti al Punto content layouts'),
  'icon' => 'complex.png',
  'theme' => 'complex',
  'regions' => array(
    'preface' => t('Preface'),
    'highlight_left' => t('Highlight left'),
    'highlight_right' => t('Highlight right'),
    'box_first' => t('First box'),
    'box_second' => t('Second box'),
    'box_third' => t('Third box'),
    'content' => t('Content'),
    'footer_first' => t('First footer'),
    'footer_second' => t('Second footer'),
    'footer_third' => t('Third footer'),
  ),
);

/**
 * Implements hook_preprocess_left_sidebar().
 */
function template_preprocess_complex(&$variables) {
  $variables['attributes_array']['class'][] = 'panel-complex';
  $variables['attributes_array']['class'][] = 'panel-display--complex';

  foreach ($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region';
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region--' . drupal_clean_css_identifier($name);
  }
}
