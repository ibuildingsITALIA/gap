<?php
/**
 * @file
 * Plain (naked) layout template, simply printing the content area.
 */
?>
<div<?php print $attributes ?>>
  <?php if (!empty($content['content'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['content'])?>>
      <?php print $content['content'] ?>
    </div>
  <?php endif; ?>
</div>
