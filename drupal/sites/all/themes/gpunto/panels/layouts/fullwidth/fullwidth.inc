<?php

/**
 * @file
 * Defines a full one-column layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Full width'),
  'category' => t('Giunti al Punto content layouts'),
  'icon' => 'fullwidth.png',
  'theme' => 'fullwidth',
  'regions' => array(
    'content' => t('Content'),
  ),
);

/**
 * Implements hook_preprocess_fullwidth().
 */
function template_preprocess_fullwidth(&$variables) {
  $variables['attributes_array']['class'][] = 'panel-fullwidth';
  $variables['attributes_array']['class'][] = 'panel-display--fullwidth';

  foreach ($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region';
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region--' . drupal_clean_css_identifier($name);
  }
}
