<?php
/**
 * @file
 * Layout template with one sidebar.
 */
?>
<div<?php print $attributes ?>>
  <?php if (!empty($content['sidebar-left'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['sidebar-left'])?>>
      <?php print $content['sidebar-left'] ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($content['content'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['content'])?>>
      <?php print $content['content'] ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($content['sidebar-right'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['sidebar-right'])?>>
      <?php print $content['sidebar-right'] ?>
    </div>
  <?php endif; ?>
</div>
