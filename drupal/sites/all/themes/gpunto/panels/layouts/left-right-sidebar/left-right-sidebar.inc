<?php

/**
 * @file
 * Defines a full one-column layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Left Right sidebar'),
  'category' => t('Giunti al Punto content layouts'),
  'icon' => 'left-right-sidebar.png',
  'theme' => 'left_right_sidebar',
  'regions' => array(
    'sidebar-left' => t('Left sidebar'),
    'content' => t('Content'),
    'sidebar-right' => t('Right sidebar'),
  ),
);

/**
 * Implements hook_preprocess_left_right_sidebar().
 */
function template_preprocess_left_right_sidebar(&$variables) {
  $variables['attributes_array']['class'][] = 'panel-left-right-sidebar';
  $variables['attributes_array']['class'][] = 'panel-display--left-right-sidebar';

  foreach ($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region';
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region--' . drupal_clean_css_identifier($name);
  }
}
