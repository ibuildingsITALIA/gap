<?php
/**
 * @file
 * Layout template with one sidebar.
 */
?>
<div<?php print $attributes ?>>
  <?php if (!empty($content['content'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['content'])?>>
      <?php print $content['content'] ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($content['sidebar'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['sidebar'])?>>
      <?php print $content['sidebar'] ?>
    </div>
  <?php endif; ?>
</div>
