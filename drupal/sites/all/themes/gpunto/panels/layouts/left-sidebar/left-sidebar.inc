<?php

/**
 * @file
 * Defines a full one-column layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Left sidebar'),
  'category' => t('Giunti al Punto content layouts'),
  'icon' => 'left-sidebar.png',
  'theme' => 'left_sidebar',
  'regions' => array(
    'sidebar' => t('Left sidebar'),
    'content' => t('Content'),
  ),
);

/**
 * Implements hook_preprocess_left_sidebar().
 */
function template_preprocess_left_sidebar(&$variables) {
  $variables['attributes_array']['class'][] = 'panel-left-sidebar';
  $variables['attributes_array']['class'][] = 'panel-display--left-sidebar';

  foreach ($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region';
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region--' . drupal_clean_css_identifier($name);
  }
}
