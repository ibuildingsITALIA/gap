<?php

/**
 * @file
 * Defines a full one-column layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Right sidebar'),
  'category' => t('Giunti al Punto content layouts'),
  'icon' => 'right-sidebar.png',
  'theme' => 'right_sidebar',
  'regions' => array(
    'sidebar' => t('Right sidebar'),
    'content' => t('Content'),
  ),
);

/**
 * Implements hook_preprocess_right_sidebar().
 */
function template_preprocess_right_sidebar(&$variables) {
  $variables['attributes_array']['class'][] = 'panel-right-sidebar';
  $variables['attributes_array']['class'][] = 'panel-display--right-sidebar';

  foreach ($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region';
    $variables['region_attributes_array'][$name]['class'][] = 'panel-region--' . drupal_clean_css_identifier($name);
  }
}
