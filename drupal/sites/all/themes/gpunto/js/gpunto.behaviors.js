(function ($) {
  var ua = navigator.userAgent,
    event = (ua.match(/iPad/i)) ? "touchend" : "click";

  Drupal.behaviors.gpuntoGalleryBehavior = {
    attach: function (context, settings) {
      $('.amazon-item--full .images-area-large a', context).once('gpunto', function () {
        $(this).click(function() {
          $('.images-area-thumbs a').colorbox({
            open: true
          });
        });
      });
      $('.amazon-item--full .images-area-thumbs a', context).once('gpunto', function () {
        $(this).colorbox({
          rel:'product-gallery-thumb',
          photo: true,
          maxWidth: "90%",
          maxHeight: "90%",
          current: "Immagine {current} di {total}",
          previous: "Precedente",
          next: "Successiva",
          close: "Chiudi",
          xhrError: "Errore nel caricamento del contenuto.",
          imgError: "Errore nel caricamento dell'immagine."
        });
      });
      $('.view-events .field--name-field-event-image a', context).once('gpunto', function () {
        $(this).colorbox({
          photo: true,
          maxWidth: "90%",
          maxHeight: "90%",
          current: "Immagine {current} di {total}",
          previous: "Precedente",
          next: "Successiva",
          close: "Chiudi",
          xhrError: "Errore nel caricamento del contenuto.",
          imgError: "Errore nel caricamento dell'immagine."
        });
      });
    }
  };

  Drupal.behaviors.gpuntoColoboxBehavior = {
    attach: function (context, settings) {
      $('a.gap_colorbox', context).once('gpunto', function () {
        $(this).click(function() {
          $(this).colorbox({
            photo: true
          });
        });
      });
    }
  };

  Drupal.behaviors.gpuntoSearchOrderBy = {
    attach: function (context, settings) {
      $('.search-header-bar .sort-button, .alice-search-header-bar .sort-button', context).once('gpunto', function () {
        var $block = $(this);
        var $delay=1000;

        $block.addClass('closed');

        $block.on('click', function (e) {
          $block.toggleClass('opened closed');

          e.stopPropagation();
        });

        $(document).on(event, function () {
          $block.addClass('closed')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('opened');
              $(this).dequeue();
            });
        });

        $block.on('click', 'li.active-sort', function(e) {
          // Element is the active sort, do not refresh page.
          e.preventDefault();
        });

        $block.on('click', 'a.sort-arrow', function (e) {
          e.preventDefault();
        });
      });

      $('.alice-search-header-bar .sort-button', context).once('gpunto-searchapi-sorts', function () {
        // At document ready we switch the active sort at first position.
        var $this = $(this);
        var $active_sort = $this.find('li > .search-api-sort-active').parent();
        $active_sort.detach();
        $this.find('li').first().before($active_sort);

        // After 500ms we switch the items order and set the clicked item at first position.
        $this.find('.sort-item').on('click', function (e) {
          var $clicked_link = $(this);
          setTimeout(function() {
            var $clicked_sort = $clicked_link.closest('li');
            $clicked_sort.detach();
            $this.find('li').first().before($clicked_sort);
          }, 500);
        });
      });
    }
  };

  Drupal.behaviors.gpuntoCategoriesDropdown = {
    attach: function (context, settings) {
      $('#block-menu-menu-categories').once('dropdown', function () {
        var $block = $(this);
        var $delay=1000;
        $block.addClass('closed');

        $block.on('click', function (e) {
          $block.toggleClass('open closed');
          e.stopPropagation();
        });

        $(document).on(event, function () {
          $block.addClass('closed')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('open');
              $(this).dequeue();
            });
        });

        $block.on(event, function (e) {
          e.stopPropagation();
        });
      });
    }
  };

  Drupal.behaviors.gpuntoMobileCategoriesToggle = {
    attach: function (context, settings) {
      $('.pane-browsenode-nav .pane-title').once('collapse', function () {
        var $block = $(this);
        $block.addClass('closed');

        $block.on('click', function (e) {
          $block.toggleClass('open closed');
          e.stopPropagation();
        });
      });
    }
  };

  Drupal.behaviors.gpuntoMobileMenuToggle = {
    attach: function (context, settings) {
      $('.main-navigation h2').once('collapse', function () {
        var $block = $(this);
        $block.addClass('closed');

        $block.on('click', function (e) {
          $block.toggleClass('open closed');
          e.stopPropagation();
        });
      });
    }
  };
/*
    // Fix percentage widths for slick containers.
  Drupal.behaviors.gpuntoSlickWidth = {
    attach: function (context, settings) {
      $('div.render-mode-slick', context).once('width', function () {
        var $this = $(this),
          real_width = window.getComputedStyle($this.get(0)).width;

        $this.width( Math.floor(parseInt(real_width, 10)) );
      });
    }
  };
*/

  Drupal.behaviors.gpuntoPanelsSlick = {
    attach: function (context, settings) {
      $('.panel-left-right-sidebar div.render-mode-slick', context).not('.big-images-style').once('slick', function () {
        $(this).slick({
          dots: false,
          infinite: false,
          slidesToShow: 5,
          slidesToScroll: 5,
          responsive: [
            {
              breakpoint: 1800,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4
              }
            },
            {
              breakpoint: 1500,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 1240,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 1078,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 700,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                arrows: false
              }
            }
          ]
        });
      });

      $('div.render-mode-slick', context).not('.big-images-style').once('slick', function () {
        $(this).slick({
          dots: false,
          infinite: false,
          slidesToShow: 10,
          slidesToScroll: 10,
          responsive: [
            {
              breakpoint: 2100,
              settings: {
                slidesToShow: 8,
                slidesToScroll: 8
              }
            },
            {
              breakpoint: 1800,
              settings: {
                slidesToShow: 7,
                slidesToScroll: 7
              }
            },
            {
              breakpoint: 1500,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 5
              }
            },
            {
              breakpoint: 1240,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4
              }
            },
            {
              breakpoint: 700,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 580,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                arrows: false
              }
            }
          ]
        });
      });

    }
  };

  Drupal.behaviors.gpuntoPanelsSlickBig = {
    attach: function (context, settings) {
      $('div.render-mode-slick.big-images-style', context).once('slick', function () {
        $(this).slick({
          dots: false,
          infinite: false,
          slidesToShow: 8,
          slidesToScroll: 8,
          responsive: [
            {
              breakpoint: 1800,
              settings: {
                slidesToShow: 6,
                slidesToScroll: 6
              }
            },
            {
              breakpoint: 1500,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4
              }
            },
            {
              breakpoint: 1240,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 700,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
              }
            }
          ]
        });
      });
    }
  };

  Drupal.behaviors.gpuntoPanelsSlickBlurb = {
    attach: function (context, settings) {
      $('div.view-blurb-block .view-content', context).once('slick', function () {
        $(this).slick({
          dots: false,
          infinite: false,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [
            {
              breakpoint: 1240,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 700,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
              }
            }
          ]
        });
      });
    }
  };

  Drupal.behaviors.gpuntoPanelsSlickAliceSearchBox = {
    attach: function (context, settings) {
      $('div.view-alice-search-block .view-content', context).once('slick', function () {
        $(this).slick({
          dots: false,
          infinite: false,
          slidesToShow: 10,
          slidesToScroll: 10,
          responsive: [
            {
              breakpoint: 2100,
              settings: {
                slidesToShow: 8,
                slidesToScroll: 8
              }
            },
            {
              breakpoint: 1800,
              settings: {
                slidesToShow: 7,
                slidesToScroll: 7
              }
            },
            {
              breakpoint: 1500,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 5
              }
            },
            {
              breakpoint: 1240,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4
              }
            },
            {
              breakpoint: 700,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 580,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            }
          ]
        });
      });
    }
  };

  Drupal.behaviors.gpuntoSlideshow = {
    attach: function (context, settings) {
      $('div.view-slideshow', context).once('slideshow', function () {
        var $this = $(this),
            $content = $this.children('div.view-content'),
            $pager = $this.children('div.attachment'),
            active_class = 'slick-active';

        $content.slick({
          dots: false,
          infinite: true,
          autoplay: true,
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplaySpeed: 7000,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                arrows: false
              }
            }
          ],
          onAfterChange: function (slick, index) {
            $pager
              .find('li')
              .removeClass(active_class)
              .eq(index)
              .addClass(active_class);
          }
        });

        $pager.find('li').first().addClass(active_class);

        $pager.on('click', 'li', function () {
          $content.slickGoTo( $(this).index() );
        });
      });
    }
  };

  Drupal.behaviors.gpuntoHerobox = {
    attach: function (context, settings) {
      $('div.view-hero-block', context).once('slideshow', function () {
        var $this = $(this),
          $content = $this.children('div.view-content'),
          $pager = $this.children('div.attachment'),
          active_class = 'slick-active';

        $content.slick({
          vertical: true,
          draggable: false,
          dots: false,
          infinite: true,
          autoplay: true,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplaySpeed: 7000,
          onAfterChange: function (slick, index) {
            $pager
              .find('li')
              .removeClass(active_class)
              .eq(index)
              .addClass(active_class);
          }
        });

        $pager.find('li').first().addClass(active_class);
        var pagerTimer = 0;

        $pager.on('mouseenter', 'li', function () {

          var $this = $(this);
          $content.slickPause();
          $this.siblings().removeClass(active_class);
          $this.addClass(active_class);

          pagerTimer = setTimeout(function(){
            $content.slickGoTo($this.index());
          }, 500);

        }).on('mouseleave', 'li', function () {
          $content.slickPlay();
          clearTimeout(pagerTimer);
        });
      });
    }
  };

  Drupal.behaviors.gpuntoStoreSlideshow = {
    attach: function (context, settings) {
      $('div.field--name-field-store-images', context).once('slideshow', function () {
        var $this = $(this),
          $content = $this.children('div.field__items');

        $content.slick({
          dots: true,
          infinite: true,
          autoplay: true,
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplaySpeed: 7000,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                arrows: false
              }
            }
          ]
        });
      });
    }
  };

  Drupal.behaviors.gpuntoAddToCart = {
    attach: function (context, settings) {
      $('.add-to-cart-button').once('gpunto', function () {
        $(this).click(function() {
          alert("Coming soon...");
        });
      });
    }
  };

  Drupal.theme.prototype.gpuntoSearchSelect = function ($formItem) {
    $formItem.append('<span class="nav-search-in-content"></span><span class="nav-down-arrow"></span>');
    $formItem.find('.nav-search-in-content').text($formItem.find('select option:selected').text());
  };

  Drupal.behaviors.gpuntoSearchSelect = {
    attach: function (context) {
      $('.form-item-bnid', context).once('gpunto', function () {
        var $formItem = $(this);
        Drupal.theme('gpuntoSearchSelect', $formItem);

        $formItem.find('select').on({
          change: function() {
            $formItem.find('.nav-search-in-content').text($(this).find('option:selected').text());
            $formItem.removeClass('nav-focus');
          }, focus: function() {
            $formItem.addClass('nav-focus');
          }, blur: function() {
            $formItem.removeClass('nav-focus');
          }
        });
      });
    }
  };

  Drupal.behaviors.gpuntoRetinaImages = {
    attach: function (context) {
      if(window.devicePixelRatio >= 1.2){
        var images = document.getElementsByTagName('img');
        for(var i=0;i < images.length;i++){
          var attr = images[i].getAttribute('data-2x');
          if(attr){
            images[i].src = attr;
          }
        }
      }
    }
  };

  Drupal.behaviors.gpuntoUserDropdown = {
    attach: function (context, settings) {
      $('#block-gp-blocks-user').once('dropdown', function () {
        var $block = $(this);
        $block.addClass('closed');

        var $delay=1000;
        $block.hover(function (e) {
          $block.addClass('open')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('closed');
              $(this).dequeue();
            });
        }, function(e){
          $block.addClass('closed')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('open');
              $(this).dequeue();
            });
        });
      });
    }
  };

  Drupal.behaviors.gpuntoMenuBarDropdown = {
    attach: function (context, settings) {
      $('#block-gp-menu-menu-bar .expanded').once('dropdown', function () {
        var $block = $(this);
        $block.addClass('closed');

        var $delay=1000;
        $block.hover(function (e) {
          $block.addClass('open')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('closed');
              $(this).dequeue();
            });
        }, function(e){
          $block.addClass('closed')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('open');
              $(this).dequeue();
            });
        });
      });
    }
  };

  Drupal.behaviors.gpuntoMenuBarHomeDropdown = {
    attach: function (context, settings) {
      $('#block-menu-menu-gp-menu-bar-home .expanded').once('dropdown', function () {
        var $block = $(this);
        $block.addClass('closed');

        // If in book browsenode page.
        if (!($('.page-browse-411663031').length == 0)) {
          $block.addClass('page-browse');
        }

        var $delay=1000;
        $block.hover(function (e) {
          $block.addClass('open')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('closed');
              $(this).dequeue();
            });
        }, function(e){
          $block.addClass('closed')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('open');
              $(this).dequeue();
            });
        });

        $(document).on(event, function () {
          $block.addClass('closed')
            .stop(true)
            .delay($delay)
            .queue(function() {
              $(this).removeClass('open');
              $(this).dequeue();
            });
        });

        $block.on(event, '.menu a', function (e) {
          e.stopPropagation();
        });
      });
    }
  };

  Drupal.behaviors.gpuntoDescriptionReadMore = {
    attach: function (context) {
      $('.product-description').once('readmore', function () {
        $(this).readmore({
          speed: 75,
          maxHeight: 92,
          moreLink: '<a href="#">Leggi tutto</a>',
          lessLink: '<a href="#">Chiudi</a>'
        });
      })
    }
  };

  Drupal.behaviors.gpuntoChosenFix = {
    attach: function (context) {
      $('.chosen-container .chosen-results').on('touchend', function(event) {
        event.stopPropagation();
        event.preventDefault();
      });
    }
  };

  Drupal.behaviors.gpuntoConfirmUserUpdate = {
    attach: function (context, settings) {
      $('#gp-user-profile-form').once('userUpdate', function () {
        var $parent = $(this);
        var $oldMail = $parent.find('#edit-email').val();

        $(this).find('#edit-submit').on('click', function(event) {
          var $newMail = $parent.find('#edit-email').val();

          if ($oldMail !== $newMail) {
            if(!confirm(Drupal.t('Stai attento! ' + $newMail + ' diventerà la tua e-mail con cui effettuare l\'accesso a www.giuntialpunto.it')))
              event.preventDefault();
          }
        });
      });
    }
  };

  Drupal.behaviors.gpuntoSearchForm = {
    attach: function (context) {
      $('#gp-amazon-search-form', context).once('gpunto', function () {
        $parent = $(this);
        $parent.find('#edit-submit').click(function() {
          if ('' === $parent.find('#edit-keywords').val()) {
            $SearchIndex = $parent.find('#edit-bnid').val();
            $url = ('all' === $SearchIndex)? '/' : '/browse/' + $SearchIndex + '/';
            window.location = $url;
            return false;
          }
        });
        $parent.find('#edit-submit--2').click(function() {
          if ('' === $parent.find('#edit-keywords').val()) {
            $SearchIndex = $parent.find('#edit-bnid').val();
            $url = ('all' === $SearchIndex)? '/' : '/browse/' + $SearchIndex + '/';
            window.location = $url;
            return false;
          }
        });
      });
    }
  };

  $.fn.addTimedText = function(wrapper_class, child_class, text_class, text) {

    $this = this;
    $($this).addClass(wrapper_class);
    $($this).find('.' + child_class).append('<p class="' + text_class + '">' + text + '</p>');

    setTimeout(function() {
      $($this).find('.' + text_class).fadeOut('slow', function(){
        $(this).remove();
      });
    }, 2000);
  };

  Drupal.behaviors.gpuntoAutocomplete = {
    attach: function (context, settings) {

      if (typeof Drupal.jsAC == 'undefined') {
        return;
      }

      Drupal.jsAC.prototype.onkeyup = function (input, e) {
        var temp_string = $('#autocomplete > ul > li.selected').text();

        if (!e) {
          e = window.event;
        }
        // Fire standard function.
        $.proxy(Drupal.jsAC.defaultOnkeyup, this)(input, e);

        if ((38 == e.keyCode || 40 == e.keyCode) && $(input).hasClass('auto_submit')) {
          var $string1 = $('#autocomplete > ul > li.selected').text();

          // Remove previous search channel.
          $('#edit-bnid').removeAttr('search_channel');

          if (($string1.substr($string1.length - 18).indexOf(" in All categories") >= 0) || ($string1.substr($string1.length - 22).indexOf(" in Tutte le categorie") >= 0)){
            var $string2 = $string1.replace(' in All categories','');
            $string1 = $string2;
            $string2 = $string1.replace(' in Tutte le categorie','');
            $('#edit-keywords').val($string2);
            $('#edit-bnid').attr('search_channel', 'all');
          }
          if (($string1.substr($string1.length - 9).indexOf(" in Books") >= 0) || ($string1.substr($string1.length - 9).indexOf(" in Libri") >= 0)) {
            var $string2 = $string1.replace(' in Books','');
            $string1 = $string2;
            $string2 = $string1.replace(' in Libri','');
            $('#edit-keywords').val($string2);
            $('#edit-bnid').attr('search_channel', '411663031');
          }

          if ($string1.substr($string1.length - 16).indexOf(" in Kindle Store") >= 0) {
            var $string2 = $string1.replace(' in Kindle store','');
            $('#edit-keywords').val($string2);
            $('#edit-bnid').attr('search_channel', '818937031');
          }
        }
        var $text = $('#autocomplete > ul > li.selected').text();
        $text = $text.replace(' in All categories','');
        $text = $text.replace(' in Tutte le categorie','');
        $text = $text.replace(' in Books','');
        $text = $text.replace(' in Libri','');
        $text = $text.replace(' in Kindle store','');

        if ($text) {
          $('#edit-keywords').val($text);
        }

        if (13 == e.keyCode && $(input).hasClass('auto_submit')) {
          // Prepare to search in all categories and books case.
          var $string1 = $('#edit-keywords').val();

          if ($('#edit-bnid').attr('search_channel')) {
            $('#edit-bnid').val($('#edit-bnid').attr('search_channel'));
          }
          if ($string1.substr($string1.length - 4).indexOf("_all") >= 0) {
            var $string2 = $string1.replace('_all','');
            $('#edit-keywords').val($string2);
            $('#edit-bnid').val('all');
          }
          if ($string1.substr($string1.length - 5).indexOf("_book") >= 0) {
            var $string2 = $string1.replace('_book','');
            $('#edit-keywords').val($string2);
            $('#edit-bnid').val('411663031');
          }
          if ($string1.substr($string1.length - 6).indexOf("_ebook") >= 0) {
            var $string2 = $string1.replace('_ebook','');
            $('#edit-keywords').val($string2);
            $('#edit-bnid').val('818937031');
          }

          $(':submit', input.form).trigger('click');
        }
      };

      Drupal.jsAC.prototype.select = function(node) {
        this.input.value = $(node).data('autocompleteValue');
        if ($(this.input).hasClass('auto_submit')) {
          if (typeof Drupal.search_api_ajax != 'undefined') {
            // Use Search API Ajax to submit
            Drupal.search_api_ajax.navigateQuery($(this.input).val());
          } else {
            var $string1 = $(this.input).val();
            var $string2 = $(this.input).val();
            if ($string1.substr($string1.length - 4).indexOf("_all") >= 0) {
              $string2 = $string1.replace('_all','');
              $(this.input).val($string2);
              $('#edit-bnid').val('all');
            }
            if ($string1.substr($string1.length - 5).indexOf("_book") >= 0) {
              $string2 = $string1.replace('_book','');
              $(this.input).val($string2);
              $('#edit-bnid').val('411663031');
            }
            if ($string1.substr($string1.length - 6).indexOf("_ebook") >= 0) {
              $string2 = $string1.replace('_ebook','');
              $(this.input).val($string2);
              $('#edit-bnid').val('818937031');
            }

            var self = this;
            setTimeout(function() {
              $(self.input).val($string2);

              // Do submit after 0.5 seconds
              $(':submit', self.input.form).trigger('click');
            }, 200);


          }
          return true;
        }
      };

    }
  }

})(jQuery);
