<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html>
<?php if (omega_extension_enabled('compatibility') && omega_theme_get_setting('omega_conditional_classes_html', TRUE)): ?>
<!--[if IEMobile 7]><html class="ie iem7" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"><![endif]-->
<!--[if lte IE 6]><html class="ie lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="ie lt-ie10 lt-ie9 lt-ie8" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"><![endif]-->
<!--[if IE 8]><html class="ie lt-ie10 lt-ie9" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"><![endif]-->
<!--[if IE 9]><html class="no-js ie lt-ie10 lt-ie9" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><html class="ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"><![endif]-->
<![if !IE]><html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"><![endif]>
<?php else: ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<?php endif; ?>
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body<?php print $attributes;?>>
<div class="l-page">
  <header class="l-header" role="banner">
    <div class="content">
      <div class="l-branding">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" data-2x="<?php print $theme_path; ?>images/logo2x.png" width="259" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <span class="amazon-logo">
          <img src="<?php print $theme_path; ?>images/powered-by-amazon.png" data-2x="<?php print $theme_path; ?>images/powered-by-amazon2x.png" width="86" height="39" alt="Powered by Amazon" />
        </span>

        <?php print render($page['branding']); ?>
      </div>

      <?php print render($page['header']); ?>
      <?php print render($page['navigation']); ?>
    </div>
  </header>

  <div class="l-main">
    <div class="l-content" role="main">
      <?php if ($title): ?><h1><?php print $title; ?></h1><?php endif; ?>
      <?php print $messages; ?>
      <?php print $content; ?>
    </div>
  </div>

  <footer class="l-footer" role="contentinfo">
    <div class="content">
      <?php print render($page['footer_first']); ?>
      <?php print render($page['footer_second']); ?>
    </div>
  </footer>
</div>
</body>
</html>
