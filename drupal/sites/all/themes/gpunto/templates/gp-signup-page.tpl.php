<?php
/**
 * @file
 * Template for signup page wrapper.
 */
?>
<div<?php print $attributes; ?>>
  <?php if ($points): ?>
    <?php print $points; ?>

    <p class="notice-card">
      Per accumulare i punti maturati con questo acquisto devi essere titolare di una GiuntiCard.
    </p>
  <?php else: ?>
    <div class="signup-notice">
      <a href="/content/giunticard">
        <img src="<?php print base_path(); ?>sites/all/themes/gpunto/images/giunticard-shadow.png">
      </a>
      <div class="notice-wrapper">
        <h3>GIUNTIcard</h3>
        <p class="notice-card">La carta fedeltà </br>delle librerie Giunti al Punto</p>
        <p class="notice-register">Registrati o esegui l'accesso per avere </br>subito tutti i vantaggi di GiuntiCard.</p>
      </div>
    </div>
  <?php endif; ?>

  <div class="signup-wrapper">
    <p class="signup-title"><?php print 'Hai una GiuntiCard e sei già registrato?'; ?></p>
    <?php print $iframe; ?>
    <a class="change-pwd" href="/changepwd">Hai dimenticato la password?</a>
  </div>
  
  <div class="binder-wrapper">
    <p class="binder-title"><?php print 'Hai già una GiuntiCard ma non sei registrato?'; ?></p>
    <p class="binder-action">
      <?php print render($register_bind_card); ?>
    </p>
  </div>
  <div class="register-wrapper">
    <p class="register-title"><?php print 'Non hai una GiuntiCard?'; ?></p>
    <p class="register-action">
      <?php print render($register_create_card); ?>
    </p>
  </div>
  <?php if ($purchase_url): ?>
    <div class="skip-card">
      <p>Se invece non vuoi accumulare punti e sconti sulla tua GiuntiCard, procedi direttamente con l'acquisto su Amazon.it.</p>
      <a class="btn" id="gp-amazon-cart-redirect" href="<?php print $purchase_url; ?>" target="_blank"><?php print t('Buy on Amazon.it'); ?></a>
    </div>
    <div class="disclaimer-wrapper disclaimer-wrapper-purchase">
      <p>Registrandoti o accedendo a www.giuntialpunto.it attraverso questa pagina e procedendo con l’ordine, verrai trasferito sul sito www.amazon.it per completare il tuo acquisto e acconsenti che al completamento di tale acquisto Amazon possa condividere il tuo nome e indirizzo email con Giunti al Punto S.p.A. Giunti al Punto potrà utilizzare il tuo nome e indirizzo email in qualità di autonomo titolare del trattamento per fornirti i suoi servizi e per finalità di marketing. Per maggiori informazioni in merito all’informativa privacy di Giunti al Punto (e alla possibilità di opporti al ricevimento di comunicazioni commerciali da parte di Giunti al Punto) clicca <a href="content/informativa-sulla-privacy">qui</a>.</p>
    </div>
  <?php else: ?>
    <div class="disclaimer-wrapper disclaimer-wrapper-signup">
      <p>Registrandoti o accedendo a www.giuntialpunto.it attraverso questa pagina e procedendo con l’ordine, verrai trasferito sul sito www.amazon.it per completare il tuo acquisto e acconsenti che al completamento di tale acquisto Amazon possa condividere il tuo nome e indirizzo email con Giunti al Punto S.p.A. Giunti al Punto potrà utilizzare il tuo nome e indirizzo email in qualità di autonomo titolare del trattamento per fornirti i suoi servizi e per finalità di marketing. Per maggiori informazioni in merito all’informativa privacy di Giunti al Punto (e alla possibilità di opporti al ricevimento di comunicazioni commerciali da parte di Giunti al Punto) clicca <a href="content/informativa-sulla-privacy">qui</a>.</p>
    </div>
  <?php endif; ?>

</div>
