<?php
/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region with underscores replaced with
 *     dashes. For example, the page_top region would have a region-page-top
 *     class.
 * - $region: The name of the region variable as defined in the theme's .info
 *   file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see template_process()
 */
?>
<div<?php print $attributes; ?>>
  <div class="footer-logo">
    <img src="<?php print $theme_path; ?>images/logo-footer.png" width="255" data-2x="<?php print $theme_path; ?>images/logo-footer@2x.png" alt="Giunti al Punto e-store" />
  </div><!-- /.footer-logo -->

  <div class="colophon">
    <p>Giunti al Punto S.p.A. Sede operativa, Via Bolognese 165, 50139 Firenze - Sede legale, Piazza Virgilio 4, 20123 Milano </p>
    <p>Codice fiscale e numero d’iscrizione al Registro Imprese di Milano e Partita Iva 00977690239 REA Milano 1542308</p>
  </div>

  <?php if ($content): ?>
    <?php print $content; ?>
  <?php endif; ?>
</div>
