<?php
/**
 * @file
 * Template to render an amazon item in detail mode.
 *
 * @var string $attributes
 *   List of HTML attributes.
 *
 * @var string $title
 *   The title of the product.
 *
 * @var string $url
 *   The url of the product.
 *
 * @var array $images
 *   List of product images.
 *
 * @var bool $is_book
 *   If the current product represents a book
 *
 * @var bool $is_video
 *   If the current product represents a video (DVD, Blu-Ray, ecc)
 *
 * @var bool $is_music
 *   If the current product represents a music (CD, LP, p3, etc)
 *
 * @var bool $is_digital_music
 *   If the current product represents a downloadable music (MP3 Store)
 *
 * @var bool $prime
 *   If the current product support the "Prime" shipping
 *
 * @var array $product_authorship
 *   List of authors and creators (only on books).
 *
 * @var array $product_authors
 *   List of authors (lite) (only on books).
 *
 * @var array $product_date
 *   Product date: publishing or release date.
 *
 * @var array $availability
 *   Product availability.
 *
 * @var AmazonItem $item
 *   An AmazonItem object.
 */
?>
<article<?php print $attributes; ?>>

  <div class="product-image">
    <?php if (isset($images) && count($images)): ?>
      <a href="<?php print $url; ?>"><img src="<?php print $images[0]['MediumImage']['URL']; ?>" alt=""/></a>
    <?php else: ?>
      <a href="<?php print $url; ?>"><img src="<?php print base_path(); ?>sites/all/themes/gpunto/images/no-img-placeholder.png" alt=""/></a>
    <?php endif; ?>
  </div>

  <div class="product-content">
    <h2 class="product_title">
      <a href="<?php print $url; ?>" itemprop="name"><?php print $title; ?></a>
      <?php if(!empty($product_details[t('Release Date')])): ?>
        <span itemprop="datePublished" class="product-date-published"><?php print ($product_details[t('Release Date')]); ?></span>
      <?php endif; ?>
    </h2>
    <?php if (($is_book || $is_video || $is_music) && !empty($product_authors)): ?>
      <span class="author_name"><?php print implode(' e ', $product_authors); ?></span>
    <?php endif; ?>

    <?php if (isset($offer_listing)): ?>

      <div class="offer-box" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

        <span class="offer-price"><span class="price-label"><?php print t('Price:'); ?>&nbsp;</span>
          <?php if (!empty($amount_saved)): ?>
            <span class="offer-list-price"><?php print gp_amazon_currency_format($list_price); ?></span>&nbsp;
          <?php endif; ?>
          <span class="price-content" itemprop="price"><?php print gp_amazon_currency_format($actual_price); ?></span>
        </span>
        <meta itemprop="priceCurrency" content="<?php print $offer_listing['Price']['CurrencyCode']; ?>" />
        <link itemprop="availability" href="http://schema.org/InStock">

        <?php if (isset($item->ItemAttributes['Binding'])): ?>
          <span class="product-binding"><?php print $item->ItemAttributes['Binding']; ?></span>
        <?php endif; ?>
        <?php if ($prime): ?>
          <div class="prime-available"><img src="<?php print base_path(); ?>sites/all/themes/gpunto/images/prime_icon.png" alt="Spedizione gratuita con Amazon Prime." title="Spedizione gratuita con Amazon Prime."></div>
        <?php endif; ?>
      </div>
      <?php if(isset($item->Offers['Offer']['Merchant'])): ?>
        <div class="offer-merchant">Venduto da: <span class="merchant"><?php print $item->Offers['Offer']['Merchant']['Name']; ?></span>  </div>
      <?php endif; ?>
    <div class="shipping-box">
      <?php if (!$is_digital_music): ?>
        <div class="shipping-availability availability-<?php print $availability['type']; ?>">
          <?php if ($availability['type'] == 'now' && !$availability['is_preorder']): ?>
            <?php print $availability['more_text']; ?>
          <?php else: ?>
            <?php print $availability['text']; ?>
            <?php if (!empty($availability['more_text'])): ?>
              <div class="availability-moretext">
              <?php print $availability['more_text']; ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>

    <?php else: ?>
      <div class="product-unavailable">
        <?php if ($availability && $availability['type'] == 'kindle'): ?>
          <?php print t('Disponibile solo su Kindle Store.'); ?>
        <?php else: ?>
          <?php print t('Currently unavailable.'); ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if (!empty($description)): ?>
      <div class="product-description">
        <?php print $description; ?>
      </div>
    <?php elseif (!empty($content) && !empty($content['node']['body'])): ?>
      <div class="product-description">
        <?php print render($content['node']['body']); ?>
      </div>
    <?php elseif (!empty($editorial_reviews)): ?>
      <div class="product-description">
        <?php foreach ($editorial_reviews as $editorial_review_type => $editorial_review): ?>
          <?php if (strlen($editorial_review) < 50): ?>
            <?php continue; ?>
          <?php endif; ?>
          <p><?php print str_replace('. ', '. <br/>', $editorial_review); ?></p>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>

    <?php if (!empty($content) && !empty($content['node']['field_series_description'])): ?>
      <?php print render($content['node']['field_series_description']); ?>
    <?php endif; ?>

    <p class="product-buy-link">
      <a href="<?php print $url; ?>" itemprop="name"><?php print t('Buy the product'); ?></a>
    </p>

    <?php if ((isset($add_to_cart)) && ($availability)): ?>
      <div class="add-to-cart-box">
        <?php print render($add_to_cart); ?>
      </div>
    <?php endif; ?>

    <?php if (isset($wishlist) && $wishlist): ?>
      <?php print render($wishlist); ?>
    <?php endif; ?>

  </div>
</article>
