<?php
/**
 * @file
 * Template to render an amazon item in detail mode.
 *
 * @var string $attributes
 *   List of HTML attributes.
 *
 * @var string $title
 *   The title of the product.
 *
 * @var string $url
 *   The url of the product.
 *
 * @var array $images
 *   List of product images.
 *
 * @var bool $is_book
 *   If the current product represents a book
 *
 * @var bool $is_video
 *   If the current product represents a video (DVD, Blu-Ray, etc)
 *
 * @var bool $is_music
 *   If the current product represents a music (CD, LP, Mp3, etc)
 *
 * @var bool $is_digital_music
 *   If the current product represents a downloadable music (MP3 Store)
 *
 * @var bool $prime
 *   If the current product support the "Prime" shipping
 *
 * @var string $description
 *   The product description.
 *
 * @var array $product_authorship
 *   List of authors and creators (only on books).
 *
 * @var array $product_authors
 *   List of authors (lite) (only on books).
 *
 * @var array $availability
 *   Product availability.
 *
 * @var array $editorial_reviews
 *   Editorial reviews.
 *
 * @var array $similarities
 *   Rendarable array of similar products block.
 *
 * @var AmazonItem $item
 *   An AmazonItem object.
 */
?>
<?php if ($item->isBook()): ?>
<article<?php print $attributes; ?> itemscope itemtype="http://schema.org/Book">
<?php else: ?>
<article<?php print $attributes; ?>>
<?php endif; ?>
  <div class="images-area">
    <div class="images-area-large">
      <?php if(!empty($content) && !empty($content['node']['field_image']['#items'][0]) && ($content['node']['field_image']['#items'][0]['filename'] !== "no-img-placeholder.png")): ?>
        <a href="#" data-image-large="http://http://giunti-cloud-prod.s3.amazonaws.com/alice/<?php print $content['node']['field_image']['#items'][0]['filename']; ?>">
          <img itemprop="image" src="http://giunti-cloud-prod.s3.amazonaws.com/alice/<?php print $content['node']['field_image']['#items'][0]['filename']; ?>" alt=""/>
        </a>
      <?php elseif (isset($images[0])): ?>
        <a href="#" data-image-large="<?php print $images[0]['LargeImage']['URL']; ?>">
          <img itemprop="image" src="<?php print $images[0]['LargeImage']['URL']; ?>" alt=""/>
        </a>
      <?php else: ?>
        <img src="<?php print base_path(); ?>sites/all/themes/gpunto/images/no-img-placeholder.png" alt=""/>
      <?php endif; ?>
    </div>
    <div class="images-area-thumbs">
      <?php if(!empty($content) && !empty($content['node']['field_image']['#items'][0]) && ($content['node']['field_image']['#items'][0]['filename'] !== "no-img-placeholder.png")): ?>
        <a href="http://giunti-cloud-prod.s3.amazonaws.com/alice/<?php print $content['node']['field_image']['#items'][0]['filename']; ?>" data-image-large="http://http://giunti-cloud-prod.s3.amazonaws.com/alice/<?php print $content['node']['field_image']['#items'][0]['filename']; ?>">
          <img src="http://giunti-cloud-prod.s3.amazonaws.com/alice/<?php print $content['node']['field_image']['#items'][0]['filename']; ?>" alt=""/>
        </a>
        <?php foreach ($images as $key => $image): ?>
          <?php if ($key === 0) : continue; endif ?>
          <?php if ($key >= 4) : break; endif ?>
          <a href="<?php print $image['LargeImage']['URL']; ?>" data-image-large="<?php print $image['LargeImage']['URL']; ?>"><img src="<?php print $image['SmallImage']['URL']; ?>" alt=""/></a>
        <?php endforeach; ?>
      <?php else: ?>
        <?php foreach ($images as $key => $image): ?>
          <?php if ($key >= 5) : break; endif ?>
          <a href="<?php print $image['LargeImage']['URL']; ?>" data-image-large="<?php print $image['LargeImage']['URL']; ?>"><img src="<?php print $image['SmallImage']['URL']; ?>" alt=""/></a>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>

  <div class="product-right-side">
    <div class="product-offer-wrapper">
    <div class="product-offer">
      <?php if (isset($offer_listing)): ?>
        <p class="price offer-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
          <span class="price-label">Prezzo:</span>
          <span class="price-content" itemprop="price"><?php print gp_amazon_currency_format($actual_price); ?></span>
          <meta itemprop="priceCurrency" content="<?php print $offer_listing['Price']['CurrencyCode']; ?>" />
          <link itemprop="availability" href="http://schema.org/InStock">
        </p>

        <?php if (!empty($amount_saved)): ?>
          <p class="price list-price">
            <span class="price-label">Prezzo di listino:</span>
            <span class="price-content"><?php print gp_amazon_currency_format($list_price); ?></span>
          </p>
          <p class="price saved-amount">
            <span class="price-label">Risparmi:</span>
            <span class="price-content">
              <?php print gp_amazon_currency_format($amount_saved); ?> <span class="offer-saved-perc">(<?php print $saved_percentage; ?>)</span>
            </span>
          </p>
        <?php endif; ?>

        <?php if(!empty($product_details['Formato Kindle'])): ?>
          <p><?php print t('Formato Kindle'); ?></p>
        <?php endif; ?>


        <?php if (($availability['text']) && !($item->isKindle())): ?>
          <p class="shipping-price">Spedizione gratuita <?php if ($prime): ?>
            con <span class="amazon-prime">Amazon <img src="<?php print base_path(); ?>sites/all/themes/gpunto/images/prime_icon.png" alt="Spedizione gratuita con Amazon Prime." title="Spedizione gratuita con Amazon Prime."></span> e
          <?php endif; ?> per ordini superiori a € 19 spediti da Amazon.it.<br />Se il prodotto non è spedito da Amazon verifica i costi su Amazon.it.</p>

          <div class="product-availability availability-<?php print $availability['type']; ?>">
            <?php print $availability['text']; ?>
            <?php if ("Attualmente non disponibile." === ($availability['text'])): ?>
              <?php $availability['more_text'] = "Attualmente non disponibile. Questo articolo può essere ordinato. Amazon ti invierà un'e-mail con una data di consegna prevista quando l'articolo sarà disponibile. L'importo sarà addebitato solo al momento della spedizione da parte di Amazon." ?>
            <?php endif; ?>

            <?php if ((!empty($availability['more_text'])) && !$is_digital_music): ?>
              <div class="availability-moretext">
                <?php print $availability['more_text']; ?>
              </div>
            <?php endif; ?>
          </div>
          <div class="product-availability">Confezione regalo disponibile</div>
        <?php endif; ?>

        <?php if ((isset($add_to_cart)) && ($availability)): ?>
          <div class="add-to-cart-box">
            <?php print render($add_to_cart); ?>
          </div>
        <?php endif; ?>
      <?php else: ?>
        <div class="product-unavailable">
          <?php if (!($availability && $availability['type'] == 'kindle')): ?>
            <?php print t('Currently unavailable.'); ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
      <?php if (isset($wishlist) && $wishlist): ?>
        <?php print render($wishlist); ?>
      <?php endif; ?>

      <?php if (($availability && $availability['type'] == 'kindle')): ?>
        <form name="buyItem" action="https://www.amazon.it/gp/digital/fiona/buy.html/ref=kics_dp_buybox_oneclick_sample" method="POST"  target="_blank">
          <input type="hidden" name="ASIN.0" value="<?php print $item->getAttributes()['ASIN']; ?>">
          <input type="hidden" name="offerListingID.0" value="<?php print $item->getAttributes()['Offers']['Offer']['OfferListing']['OfferListingId']; ?>">
          <input type="hidden" name="t" value="fiona">
          <input type="hidden" name="subtype.0" value="FREE_CHAPTER">
          <input type="hidden" name="itemCount" value="1">
          <input type="hidden" name="cor.0" value="IT">
          <table style="text-align:center;width:100%;">
            <tbody>
              <tr><td>
                <div class="button">
                  <input type="image" src="http://g-ecx.images-amazon.com/images/G/29/digital/fiona/general/button-send_sample_now-sec._V163464046_.gif" value="buy.digitalfeed" border="0" alt="Inviare campione ora">
                </div>
              </td></tr>
            </tbody>
          </table>
        </form>
      <?php endif; ?>
    </div>

    <div class="product-info">
      <h1 class="product_title" itemprop="name"><?php print $title; ?></h1>

      <?php if (!empty($product_date)): ?>
        <?php print render($product_date); ?>
      <?php endif; ?>
      <br />
      <?php if (!empty($content) && !empty($content['node']['field_publisher'])): ?>
        <?php print render($content['node']['field_publisher']); ?>
      <?php endif; ?>

      <?php if (!empty($content) && !empty($content['node']['field_series_description'])): ?>
        <?php print render($content['node']['field_series_description']); ?>
      <?php endif; ?>

      <?php if (!empty($content) && !empty($content['node']['field_authors']) && ($content['node']['field_authors']['#access'])): ?>
        <?php print render($content['node']['field_authors']); ?>
      <?php elseif ($product_authorship): ?>
        <div class="product_authorship">
          <?php foreach($product_authorship as $author): ?>
            <?php if ($item->isBook()): ?>
              <span itemprop="author" class="author_name">
            <?php else: ?>
              <span class="author_name">
            <?php endif; ?>
              <a href="<?php print $author['search-url']; ?>">
                <?php print $author['name']; ?>
              </a>
            </span>&nbsp;
            <span class="author_role">
              (<?php print $author['role']; ?>)
            </span>&emsp;
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>

    <?php if (!empty($giunticard_points)): ?>
      <div class="block-giunti-card">
        <p class="block-content">
          <span class="text1">Acquista e ottieni subito <strong><?php print $giunticard_points; ?> punti</strong></span><br />
          <span class="text2">pari a <strong><?php print $giunticard_points_value; ?> di sconto</strong> in libreria</span>
        </p>
      </div>
    <?php endif; ?>

    <div class="product-alternate-versions">
      <h2>Formati disponibili</h2>
        <div class="version-name">
          <strong>
            <?php
            if (!empty($item->getAttributes()['ItemAttributes']['Binding'])):
              print $item->getAttributes()['ItemAttributes']['Binding'];
            endif;
            if (!empty($actual_price)): ?>
              <span class="price"><?php print gp_amazon_currency_format($actual_price); ?></span>
            <?php endif; ?>
          </strong>
        </div>

      <?php if (($is_book) && (!empty($alternate_versions))): ?>
        <?php foreach($alternate_versions as $version): ?>
          <div class="version-name">
            <a href="<?php print base_path(); ?><?php print $version['link']; ?>">
              <?php print $version['binding']; ?>
              <span class="price"><?php print $version['price']; ?></span>
            </a>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>

    <?php if (!empty($description)): ?>
      <div itemprop="description" class="product-description description-from-amazon">
        <?php print $description; ?>
      </div>
    <?php elseif (!empty($content) && !empty($content['node']['body'])): ?>
      <div itemprop="description" class="product-description description-from-alice">
        <?php print render($content['node']['body']); ?>
      </div>
    <?php elseif (!empty($editorial_reviews)): ?>
      <div itemprop="description" class="product-description editorial-from-amazon">
        <?php foreach ($editorial_reviews as $editorial_review_type => $editorial_review): ?>
          <?php if (strlen($editorial_review) < 50): ?>
            <?php continue; ?>
          <?php endif; ?>
          <p><?php print str_replace('. ', '. <br/>', $editorial_review); ?></p>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>

    <?php if (!empty($item_attributes) && !($item->isBook())): ?>
      <div class="product-details">
        <h2>Dettagli prodotto</h2>
        <ul>
          <?php foreach($item_attributes as $key => $item_attribute): ?>
            <?php if ($item_attribute['#access']): ?>
            <li>
              <?php print render($item_attribute); ?>
            </li>
              <?php endif; ?>
          <?php endforeach; ?>

          <?php if (!empty($product_warnings) && ($item->needProductWarnings())): ?>
            <li>
              <?php print render($product_warnings); ?>
            </li>
          <?php endif; ?>
        </ul>
      </div>
    <?php endif; ?>

    <?php if (!empty($product_details) && ($item->isBook())): ?>
      <div class="product-details">
        <h2>Dettagli prodotto</h2>
        <ul>
          <?php if(!empty($product_details['Copertina flessibile'])): ?>
            <li><?php print t('Copertina flessibile'); ?></li>
          <?php endif; ?>

          <?php if(!empty($product_details['Copertina rigida'])): ?>
            <li><?php print t('Copertina rigida'); ?></li>
          <?php endif; ?>

          <?php if(!empty($product_details['Formato Kindle'])): ?>
            <li><?php print t('Formato Kindle'); ?></li>
          <?php endif; ?>

          <?php if(!empty($content['node']['field_pages'])): ?>
            <li><?php print $content['node']['field_pages']['#title']; ?>:&nbsp;<span itemprop="numberOfPages"><?php print $content['node']['field_pages']['#items'][0]['value']; ?></span></li>
          <?php endif; ?>

          <?php if(!empty($product_details[t('Dimensions')])): ?>
            <li><?php print t('Dimensions'); ?>:&nbsp;<span><?php print ($product_details[t('Dimensions')]); ?></span></li>
          <?php endif; ?>

          <?php if(!empty($product_details[t('Release Date')])): ?>
            <li><?php print t('Release Date'); ?>:&nbsp;<span itemprop="datePublished"><?php print ($product_details[t('Release Date')]); ?></span></li>
          <?php endif; ?>

          <?php if (!empty($product_details['ISBN-13'])): ?>
            <li><?php print t('EAN'); ?>:&nbsp;<span itemprop="isbn"><?php print render($product_details['ISBN-13']); ?></span></li>
          <?php elseif (!empty($content['node']['field_isbn13'])): ?>
            <li><?php print $content['node']['field_isbn13']['#title']; ?>:&nbsp;<span itemprop="isbn"><?php print $content['node']['field_isbn13']['#items'][0]['value']; ?></span></li>
          <?php endif; ?>
        </ul>
      </div>
    <?php endif; ?>
  </div>

  <?php if ($similarities): ?>
    <div class="block-similarities">
      <h3 class="similarities-title">Suggeriti per te</h3>
      <?php print render($similarities); ?>
    </div>
  <?php endif; ?>

  <div class="product-bottom">

    <div class="product-bottom-content">
      <?php if (isset($item->CustomerReviews) && $item->CustomerReviews['HasReviews'] == 'true'): ?>
        <iframe class="product-customer-reviews" src="<?php print $item->CustomerReviews['IFrameURL']; ?>"></iframe>
      <?php endif; ?>
    </div>

  </div>
</article>
