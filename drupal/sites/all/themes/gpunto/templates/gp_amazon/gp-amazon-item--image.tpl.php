<?php
/**
 * @file
 * Template to render an amazon item in detail mode.
 *
 * @var string $attributes
 *   List of HTML attributes.
 *
 * @var string $title
 *   The title of the product.
 *
 * @var string $url
 *   The url of the product.
 *
 * @var array $images
 *   List of product images.
 *
 * @var bool $is_book
 *   If the current product represents a book
 *
 * @var bool $is_video
 *   If the current product represents a video (DVD, Blu-Ray, ecc)
 *
 * @var bool $is_music
 *   If the current product represents an audio CD or LP
 *
 * @var array $product_authorship
 *   List of authors and creators (only on books).
 *
 * @var array $product_authors
 *   List of authors (lite) (only on books).
 *
 * @var array $availability
 *   Product availability.
 *
 * @var AmazonItem $item
 *   An AmazonItem object.
 */
?>
<article<?php print $attributes; ?>>
  <?php if (isset($extra['delta'])): ?>
    <span class="rank"><?php print ($extra['delta'] + 1); ?></span>
  <?php endif; ?>
  <div class="product-image">
    <?php if (isset($images) && count($images)): ?>
      <a href="<?php print $url; ?>"><img src="<?php print $images[0]['LargeImage']['URL']; ?>" alt=""/></a>
    <?php else: ?>
      <a href="<?php print $url; ?>"><img src="<?php print base_path(); ?>sites/all/themes/gpunto/images/no-img-placeholder.png" alt=""/></a>
    <?php endif; ?>
  </div>

  <div class="product-content">
    <h2 class="product_title"><a href="<?php print $url; ?>" itemprop="name"><?php print $title; ?></a></h2>

    <?php if ($product_authors): ?>
      <div class="product_authorship">
        <span class="author_name"><?php print implode(', ', array_slice($product_authors, 0, 3)) ?></span>
      </div>
    <?php endif; ?>

    <?php if ($is_book || $is_music || $is_video): ?>
      <div class="product-binding"><?php print $item->ItemAttributes['Binding']; ?></div>
    <?php endif; ?>

    <?php if (isset($offer_listing)): ?>
      <div class="offer-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <?php if (!empty($amount_saved)): ?>
          <span class="offer-list-price"><?php print gp_amazon_currency_format($list_price); ?></span>&nbsp;
        <?php endif; ?>
        <span itemprop="price"><?php print gp_amazon_currency_format($offer_listing['Price']['Amount']); ?></span>
        <meta itemprop="priceCurrency" content="<?php print $offer_listing['Price']['CurrencyCode']; ?>" />
        <link itemprop="availability" href="http://schema.org/InStock">
      </div>

    <?php else: ?>
      <div class="product-unavailable">
        <?php if ($availability && $availability['type'] == 'kindle'): ?>
          <?php print t('Disponibile solo su Kindle Store.'); ?>
        <?php elseif ($item->isApp()): ?>
          <?php print t('Available on Amazon.it'); ?>
        <?php else: ?>
          <?php print t('Currently unavailable.'); ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>
  </div>
</article>
