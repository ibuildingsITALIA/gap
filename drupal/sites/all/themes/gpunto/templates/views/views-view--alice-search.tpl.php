<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
<?php print render($title_prefix); ?>
<?php if ($title): ?>
  <?php print $title; ?>
<?php endif; ?>

<?php if ($exposed): ?>
  <div class="view-filters">
    <?php print $exposed; ?>
  </div>
<?php endif; ?>

<?php $search_sort_block = module_invoke('search_api_sorts', 'block_view', 'search-sorts'); ?>

<?php if ($search_sort_block && $alice_search_items): ?>
  <div class="alice-search-header-bar">
    <?php if ($alice_search_items): ?>
      <div class="result-count">
        <?php print $alice_search_items; ?>
      </div>
    <?php endif; ?>

    <?php if ($search_sort_block): ?>
    <div class="sorts">
      <span class="sort-label"><?php print t('Order by'); ?></span>
      <div class="sort-button">
        <?php print render($search_sort_block['content']); ?>
        <a href="#" class="sort-arrow"><span>▼</span></a>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($alice_search_items_per_page): ?>
      <div class="items-per-page">
        <span class="items-per-page-label"><?php print t('Items per page'); ?></span>
        <div class="items-per-page-list">
          <?php print render($alice_search_items_per_page); ?>
        </div>
      </div>
    <?php endif; ?>

  </div>
<?php endif; ?>

<?php print render($title_suffix); ?>
<?php if ($header): ?>
  <div class="view-header">
    <?php print $header; ?>
  </div>
<?php endif; ?>

<?php if ($attachment_before): ?>
  <div class="attachment attachment-before">
    <?php print $attachment_before; ?>
  </div>
<?php endif; ?>

<?php if ($rows): ?>
  <div class="view-content">
    <?php print $rows; ?>
  </div>
<?php elseif ($empty): ?>
  <div class="view-empty">
    <?php print $empty; ?>
  </div>
<?php endif; ?>

<?php if ($pager): ?>
  <?php print $pager; ?>
<?php endif; ?>

<?php if ($attachment_after): ?>
  <div class="attachment attachment-after">
    <?php print $attachment_after; ?>
  </div>
<?php endif; ?>

<?php if ($more): ?>
  <?php print $more; ?>
<?php endif; ?>

<?php if ($footer): ?>
  <div class="view-footer">
    <?php print $footer; ?>
  </div>
<?php endif; ?>

<?php if ($feed_icon): ?>
  <div class="feed-icon">
    <?php print $feed_icon; ?>
  </div>
<?php endif; ?>

</div><?php /* class view */ ?>