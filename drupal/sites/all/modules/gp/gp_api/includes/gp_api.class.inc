<?php
/**
 * @file
 * Classes to handle API calls.
 */

class RemoteApi {
  /**
   * The request method (GET or POST).
   *
   * @var string
   */
  protected $method = 'GET';

  /**
   * Headers to send with the request.
   *
   * @var array
   */
  protected $headers = array();

  /**
   * The base url of the API.
   *
   * @var string
   */
  protected $baseUrl = '';

  /**
   * The endpoint of the request.
   *
   * @var string
   */
  protected $endpoint = '';

  /**
   * The parameters to send with the request.
   *
   * @var array
   */
  protected $parameters = array();

  /**
   * The timeout for the request.
   *
   * @var int
   */
  protected $timeout = 30;

  /**
   * Holds the executed status.
   *
   * @var bool
   */
  protected $executed = FALSE;

  /**
   * The raw response object returned from the request.
   *
   * @var mixed
   */
  protected $rawResponse;

  /**
   * The data returned.
   *
   * @var mixed
   */
  protected $data;

  /**
   * The response code (200, 404, 500, etc).
   *
   * @var int
   */
  protected $code = 0;

  /**
   * The response code as status message.
   *
   * @var string
   */
  protected $statusMessage;

  /**
   * The error that may have been generated.
   *
   * @var string
   */
  protected $error = NULL;

  /**
   * Constructor for the class.
   */
  public function __construct($method = 'GET', $base_url = '', $endpoint = '', $parameters = array(), $timeout = 30, $headers = array()) {
    $this->method = $method;
    $this->baseUrl = $base_url;
    $this->endpoint = $endpoint;
    $this->parameters = $parameters;
    $this->timeout = $timeout;
    $this->headers = $headers;
  }

  /**
   * Execute the API call.
   *
   * @return bool
   *   Wheter or not the API call has been executed.
   */
  public function execute() {
    $url = $this->buildUrl();

    // Execute the request if both base url and endpoint are filled.
    if (empty($url)) {
      return FALSE;
    }

    $this->rawResponse  = drupal_http_request($url, array(
      'method' => $this->method,
      'headers' => $this->buildHeaders(),
      'data' => $this->buildParameters(),
      'timeout' => $this->timeout,
    ));

    // Post process hooks.
    $this->processRawResponse();
    $this->processResponse();

    // Mark the API call as executed.
    $this->executed = TRUE;

    return TRUE;
  }

  /**
   * Build the url for the request.
   *
   * @return string
   *   The url ready.
   */
  protected function buildUrl() {
    return rtrim($this->baseUrl, '/') . '/' . ltrim($this->endpoint, '/');
  }

  /**
   * Build the headers for the request.
   *
   * @return array
   *   An array of headers.
   */
  protected function buildHeaders() {
    $headers = $this->headers;

    if ('POST' === $this->method) {
      $headers += array('Content-type' => 'application/x-www-form-urlencoded');
    }

    return $headers;
  }

  /**
   * Build the parameters for the request.
   *
   * @return mixed
   *   The parameters ready for usage.
   */
  protected function buildParameters() {
    return drupal_http_build_query($this->parameters);
  }

  /**
   * Process the data from the raw response.
   */
  protected function processRawResponse() {
    if (isset($this->rawResponse->error)) {
      $this->error = $this->rawResponse->error;
    }

    $this->code = $this->rawResponse->code;
    $this->statusMessage = $this->rawResponse->status_message;
    $this->data = $this->rawResponse->data;
  }

  /**
   * Further processing after HTTP response has been processed.
   */
  protected function processResponse() {
    // Always try to decode the data as sometimes more info are in it.
    $decoded_data = FALSE;
    if (!is_null($this->data) && $decoded_data = drupal_json_decode($this->data)) {
      $this->data = $decoded_data;
    }

    // Quit if the raw response isn't already valid.
    if (!$this->isValid()) {
      // If there's a data with an error_message, use it.
      if (!empty($decoded_data['error_message'])) {
        $this->error = $decoded_data['error_message'];
      }

      return;
    }

    if (!$decoded_data || !isset($decoded_data['status']) || ('OK' === $decoded_data['status'] && !isset($decoded_data['data']))) {
      $this->code = 1000;
      $this->statusMessage = 'KO';
      $this->error = 'Invalid response data';
      return;
    }

    if ('OK' !== $decoded_data['status']) {
      $this->code = 1001;
      $this->statusMessage = 'KO';
      $this->error = isset($decoded_data['error']) ? $decoded_data['error'] : 'Request failed';
      return;
    }

    // Bring the real data outside.
    $this->data = $this->data['data'];
  }

  /**
   * Returns if the request is valid.
   *
   * @return bool
   *   The validity itself.
   */
  public function isValid() {
    return (!isset($this->error) && 200 == $this->code && !is_null($this->data));
  }

  /**
   * Returns the method (GET or POST).
   *
   * @return string
   *   The current method.
   */
  public function getMethod() {
    return $this->method;
  }

  /**
   * Set the method for the request.
   *
   * The method can only be changed before the request has run.
   *
   * @param string $method
   *   Either GET or POST.
   *
   * @throws RemoteApiException
   */
  public function setMethod($method) {
    if ($this->executed) {
      throw new RemoteApiException('The request has been already executed');
    }

    $this->method = $method;
  }

  /**
   * Returns the current headers.
   *
   * @return array
   *   The headers.
   */
  public function getHeaders() {
    return $this->headers;
  }

  /**
   * Set the headers for the request.
   *
   * @param array $headers
   *   An array of headers.
   *
   * @throws RemoteApiException
   */
  public function setHeaders($headers) {
    if ($this->executed) {
      throw new RemoteApiException('The request has been already executed');
    }

    $this->headers = $headers;
  }

  /**
   * Returns the base url.
   *
   * @return string
   *   The base url.
   */
  public function getBaseUrl() {
    return $this->baseUrl;
  }

  /**
   * Set the base url for the request.
   *
   * @param string $base_url
   *   A valid base url.
   *
   * @throws RemoteApiException
   */
  public function setBaseUrl($base_url) {
    if ($this->executed) {
      throw new RemoteApiException('The request has been already executed');
    }

    $this->baseUrl = $base_url;
  }

  /**
   * Set the base url using a Drupal variable.
   *
   * @param string $variable_name
   *   The name of the variable.
   */
  public function setBaseUrlFromVariable($variable_name) {
    if (!$this->executed) {
      $this->baseUrl = trim(variable_get($variable_name, ''));
    }
  }

  /**
   * Returns the endpoint.
   *
   * @return string
   *   The url of the endpoint.
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

  /**
   * Set the endpoint for the request.
   *
   * @param string $endpoint
   *   The url for the endpoint.
   *
   * @throws RemoteApiException
   */
  public function setEndpoint($endpoint) {
    if ($this->executed) {
      throw new RemoteApiException('The request has been already executed');
    }

    $this->endpoint = $endpoint;
  }

  /**
   * Returns the parameters set.
   *
   * @return array
   *   Key-value array of parameters.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Retrieve a single parameter by name.
   *
   * @param string $name
   *   The name of the parameter.
   *
   * @return null|string
   *   NULL if not found, the value of the parameter otherwise.
   */
  public function getParameter($name) {
    return isset($this->parameters[$name]) ? $this->parameters[$name] : NULL;
  }

  /**
   * Set an array as parameters for the request.
   *
   * @param array $parameters
   *   The array containing all the parameters.
   *
   * @throws RemoteApiException
   */
  public function setParameters($parameters) {
    if ($this->executed) {
      throw new RemoteApiException('The request has been already executed');
    }

    $this->parameters = $parameters;
  }

  /**
   * Set a single parameter value.
   *
   * @param string $name
   *   The name of the parameter to set.
   *
   * @param mixed $value
   *   The value of the parameter.
   */
  public function setParameter($name, $value) {
    if (!$this->executed) {
      $this->parameters[$name] = $value;
    }
  }

  /**
   * Add parameters to the current ones.
   *
   * @param array $parameters
   *   The parameters to add.
   *
   * @param bool $replace
   *   TRUE to replace parameters with the same key.
   */
  public function addParameters(array $parameters, $replace = TRUE) {
    if ($replace) {
      $this->parameters = array_merge($this->parameters, $parameters);
    }
    else {
      $this->parameters += $parameters;
    }
  }

  /**
   * Return the timeout set.
   *
   * @return int
   *   Timeout in seconds.
   */
  public function getTimeout() {
    return $this->timeout;
  }

  /**
   * Set the timeout for the response.
   *
   * @param int $timeout
   *   Number of seconds.
   *
   * @throws RemoteApiException
   */
  public function setTimeout($timeout) {
    if ($this->executed) {
      throw new RemoteApiException('The request has been already executed');
    }

    $this->timeout = $timeout;
  }

  /**
   * Returns the executed status.
   *
   * @return bool
   *   If the request has already been executed.
   */
  public function isExecuted() {
    return $this->executed;
  }

  /**
   * Returns the raw response obtained from the api.
   *
   * @return mixed
   *   The raw response.
   */
  public function getRawResponse() {
    return $this->rawResponse;
  }

  /**
   * Returns the response data.
   *
   * @return mixed
   *   The content of the response obtained.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Returns the response code of the request.
   *
   * @return int
   *   An HTTP response code (200, 400, etc).
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * Returns the status message for the request.
   *
   * @return string
   *   The response code as status message.
   */
  public function getStatusMessage() {
    return $this->statusMessage;
  }

  /**
   * Returns the error message, if available.
   *
   * @return bool/string
   *   FALSE if no error, a string representing the error otherwise.
   */
  public function getError() {
    return $this->error;
  }
}

/**
 * Class JsonRemoteApi
 *
 * Handle remote API services that work with JSON.
 */
class JsonRemoteApi extends RemoteApi {
  /**
   * Override buildHeaders.
   */
  protected function buildHeaders() {
    $headers = $this->headers;
    $headers += array('Content-type' => 'application/json');
    return $headers;
  }

  /**
   * Override buildParameters.
   */
  protected function buildParameters() {
    return drupal_json_encode($this->parameters);
  }
}

/**
 * Class UglyGetRemoteApi
 *
 * Ugly remote API bleh.
 */
class UglyGetRemoteApi extends RemoteApi {
  /**
   * Customize the URL for this shit.
   */
  protected function buildUrl() {
    // This ugly shit uses parameters as GET parameters.
    $url = url(parent::buildUrl(), array(
      'query' => $this->parameters,
      'absolute' => TRUE,
    ));

    return $url;
  }

  /**
   * Parameters are used in the url, ffs.
   */
  protected function buildParameters() {
    return NULL;
  }
}

/**
 * Class RemoteApiException.
 */
class RemoteApiException extends Exception {}

class ReportApi {
  /**
   * Downloads FTP File.
   */
  public static function downloadFTPFile($ftp, $folder, $file, $close_connection = TRUE, $delete_file = FALSE, $local_file_name_prefix = "") {
    while (TRUE) {
      if ($ftp === FALSE) {
        break;
      }
      try {
        if (empty($folder)) {
          $folder = 'amazon';
        }
        watchdog('ReportApi', 'trying to change ftp dir to %dir', array('%dir' => $folder), WATCHDOG_NOTICE);
        $result = $ftp->chdir($folder);
        if (!$result) {
          break;
        }
        $ftp_folder = $ftp->pwd();
        watchdog('ReportApi', 'current ftp dir = %dir', array('%dir' => $ftp_folder), WATCHDOG_NOTICE);
        $local_dir = getcwd() . '/' . trim('sites/default/files/ftp_import', '/');
        $local_file = $local_dir . '/' . $local_file_name_prefix . $file;
        watchdog('ReportApi', 'creating handler for file', WATCHDOG_INFO);
        $local_file_h = fopen($local_file, 'w');
        if ($local_file_h === FALSE) {
          break;
        }

        watchdog('ReportApi', 'downloading file %file', array('%file' => $file), WATCHDOG_INFO);
        $result = $ftp->fget($local_file_h, ltrim($file,'/'), Ftp::BINARY);
        if (!$result) {
          break;
        }

        watchdog('closing file handler', WATCHDOG_INFO);
        fclose($local_file_h);

        if ($delete_file) {
          exec('rm -f ' . $local_file);
        }
      } catch (Exception $exc) {
      }
      break;
    }

    if ($close_connection) {
      try {
        watchdog('ReportApi', 'closing ftp connection', WATCHDOG_INFO);
        $ftp->close();
      } catch (Exception $exc) {
        // Do nothing.
      }
    }
  }

  /**
   * Uploads FTP File.
   */
  public static function uploadFTPFile($ftp, $folder, $file, $close_connection = TRUE, $delete_file = FALSE, $local_file_name_prefix = "") {
    while (TRUE) {
      if ($ftp === FALSE) {
        break;
      }
      try {
        if (empty($folder)) {
          $folder = 'amazon';
        }
        watchdog('ReportApi', 'trying to change ftp dir to %dir', array('%dir' => $folder));
        $result = $ftp->chdir($folder);
        if (!$result) {
          break;
        }
        $ftp_folder = $ftp->pwd();
        watchdog('ReportApi', 'current ftp dir = %dir', array('%dir' => $ftp_folder), WATCHDOG_NOTICE);
        $local_dir = getcwd() . '/' . trim('../private/ftp_export', '/');
        $local_file = $local_dir . '/' . $file;
        watchdog('ReportApi', 'creating handler for file');
        $local_file_h = fopen($local_file, 'r');
        if ($local_file_h === FALSE) {
          break;
        }

        watchdog('ReportApi', 'uploading file %file', array('%file' => $file), WATCHDOG_NOTICE);
        $result = $ftp->fput(ltrim($file,'/'), $local_file_h, Ftp::BINARY);

        if (!$result) {
          break;
        }

        watchdog('ReportApi', 'closing file handler');
        fclose($local_file_h);

        if ($delete_file) {
          exec('rm -f ' . $local_file);
        }
      } catch (Exception $exc) {
      }
      break;
    }

    if ($close_connection) {
      try {
        watchdog('ReportApi', 'closing ftp connection');
        if ($ftp) {
          $ftp->close();
        }
      } catch (Exception $exc) {
        // Do nothing.
      }
    }
  }

  /**
   * Connects to FTP host and return ftp resource.
   */
  public static function connectToFTP($url, $user, $password, $port, $timeout, $max_attempt) {
    $attempt = $max_attempt;
    while ($attempt > 0) {
      try {
        $ftp = new Ftp();
        $ftp->connect($url, $port, $timeout);
        watchdog('ReportApi', 'Connected to %url, using %user & %pwd [%code]', array(
          '%url' => $url,
          '%user' => $user,
          '%pwd' => $password,
        ), WATCHDOG_INFO);

        $max_log_attempts = $max_attempt;
        for ($i = 0; $i < $max_log_attempts; $i++) {
          $result = $ftp->login($user, $password);
          if (!$result) {
            watchdog('ReportApi', 'NOT logged to %url, using %user & %pwd, attempt %a of %max [%code]', array(
              '%url' => $url,
              '%user' => $user,
              '%pwd' => $password,
              '%a' => $i,
              '%max' => $max_log_attempts,
            ), WATCHDOG_CRITICAL);
          }
          else {
            watchdog('ReportApi', 'Logged to %url, using %user & %pwd [%code]', array(
              '%url' => $url,
              '%user' => $user,
              '%pwd' => $password,
            ), WATCHDOG_INFO);
            break;
          }
        }
        $ftp->pasv(TRUE);
        if ($result) {
          return $ftp;
        }
      } catch (FtpException $exc) {
        watchdog('ReportApi', 'FTP Error to %url, using %user & %pwd. [%code] (%msg)', array(
          '%url' => $url,
          '%user' => $user,
          '%pwd' => $password,
          '%code' => $exc->getCode(),
          '%msg' => $exc->getMessage(),
        ), WATCHDOG_ERROR);
      }
      try {
        $ftp->close();
      } catch (Exception $exc) {
        // Do nothing.
      }
      $attempt--;
    }
    return FALSE;
  }
}
