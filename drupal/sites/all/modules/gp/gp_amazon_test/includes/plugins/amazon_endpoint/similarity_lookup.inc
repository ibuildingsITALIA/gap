<?php
/**
 * @file
 * SimilarityLookup endpoint plugin.
 */

$plugin = array(
  'title' => 'Similarities',
);

/**
 * Form callback for the endpoint.
 */
function gp_amazon_test_similarity_lookup_endpoint_form($form, &$form_state) {
  $form['config']['asin'] = array(
    '#type' => 'textfield',
    '#title' => t('ASIN'),
    '#required' => TRUE,
  );

  $form['config']['amazon_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only from Amazon merchant'),
  );

  return $form;
}

/**
 * API callback.
 */
function gp_amazon_test_similarity_lookup_endpoint_api($values) {
  $parameters = array(
    'ItemId' => strtoupper($values['asin']),
    'IdType' => 'ASIN',
    'ResponseGroup' => 'Small,ItemAttributes,OfferFull,Images,BrowseNodes',
    'SimilarityType' => 'Random',
  );

  if (!empty($values['amazon_only'])) {
    $parameters['MerchantId'] = 'Amazon';
  }

  $result = gp_amazon_test_request('SimilarityLookup', $parameters);
  return $result;
}