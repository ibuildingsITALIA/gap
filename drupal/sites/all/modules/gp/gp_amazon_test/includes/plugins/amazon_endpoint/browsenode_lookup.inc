<?php
/**
 * @file
 * BrowseNodeLookup endpoint plugin.
 */

$plugin = array(
  'title' => 'Browsenode lookup',
);

/**
 * Form callback for the endpoint.
 */
function gp_amazon_test_browsenode_lookup_endpoint_form($form, &$form_state) {
  $form['config']['browsenode_id'] = array(
    '#type' => 'textfield',
    '#title' => t('BrowseNode Id'),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * API callback.
 */
function gp_amazon_test_browsenode_lookup_endpoint_api($values) {
  $result = gp_amazon_test_request('BrowseNodeLookup', array(
    'BrowseNodeId' => $values['browsenode_id'],
    'ResponseGroup' => 'BrowseNodeInfo,MostGifted,NewReleases,MostWishedFor,TopSellers',
  ));

  return $result;
}
