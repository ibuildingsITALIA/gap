<?php
/**
 * @file
 * ItemLookup endpoint plugin.
 */

$plugin = array(
  'title' => 'Item lookup',
);

/**
 * Form callback for the endpoint.
 */
function gp_amazon_test_item_lookup_endpoint_form($form, &$form_state) {
  $form['config']['asin'] = array(
    '#type' => 'textfield',
    '#title' => t('ASIN'),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * API callback.
 */
function gp_amazon_test_item_lookup_endpoint_api($values) {
  $result = gp_amazon_test_request('ItemLookup', array(
    'ItemId' => strtoupper($values['asin']),
    'IdType' => 'ASIN',
    'ResponseGroup' => 'BrowseNodes,Images,ItemAttributes,OfferFull,PromotionSummary,Reviews,EditorialReview,SalesRank,AlternateVersions',
  ));

  return $result;
}
