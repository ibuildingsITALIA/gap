<?php
/**
 * @file
 * ItemSearch endpoint plugin.
 */

$plugin = array(
  'title' => 'Item search',
);

/**
 * Form callback for the endpoint.
 */
function gp_amazon_test_item_search_endpoint_form($form, &$form_state) {
  // Prepare search indexes options.
  $search_indexes = array();
  foreach (gp_amazon_search_indexes() as $key => $index) {
    $search_indexes[$key] = $index['label'];
  }

  $form['config']['search_index'] = array(
    '#type' => 'select',
    '#title' => t('Search index'),
    '#options' => $search_indexes,
    '#required' => TRUE,
  );

  $form['config']['search_string'] = array(
    '#type' => 'textfield',
    '#title' => t('Search query string'),
    '#description' => t('Enter a valid query string for the ItemLookup API endpoint.'),
    '#required' => TRUE,
    '#size' => 150,
  );

  $form['config']['amazon_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only from Amazon merchant'),
  );

  $form['config']['availability'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only available products'),
  );

  $form['config']['min_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum price'),
    '#default_value' => 10,
  );

  return $form;
}

/**
 * API callback.
 */
function gp_amazon_test_item_search_endpoint_api($values) {
  // Prepare search string.
  // @see gp_amazon_search_amazon_search_block_content_type_render().
  $search_string = gp_amazon_search_replace_tokens($values['search_string']);
  $search_parameters = explode('&', $search_string);

  // Explode parameters.
  $parameters = array();
  foreach ($search_parameters as $single) {
    list($key, $value) = explode('=', $single);
    $parameters[$key] = $value;
  }

  // Add (or overwrite) search index.
  $parameters += array(
    'SearchIndex' => $values['search_index'],
    'ResponseGroup' => 'Large,Images,ItemAttributes,OfferFull,PromotionSummary',
  );

  // Add default filters for the selected search id.
  $search_indexes = gp_amazon_search_indexes();
  $index_data = $search_indexes[$parameters['SearchIndex']];
  if (isset($index_data['filters'])) {
    $parameters += $index_data['filters'];
  }

  // Add some configuration.
  if (!empty($values['amazon_only'])) {
    $parameters['MerchantId'] = 'Amazon';
  }
  if (!empty($values['availability'])) {
    $parameters['Availability'] = 'Available';
  }
  if (!empty($values['min_price'])) {
    $parameters['MinimumPrice'] = $values['min_price'];
  }

  $result = gp_amazon_test_request('ItemSearch', $parameters);

  return $result;
}
