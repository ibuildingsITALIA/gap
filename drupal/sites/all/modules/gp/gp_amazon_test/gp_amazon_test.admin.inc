<?php
/**
 * @file
 * Form callbacks.
 */

/**
 * Form for testing purposes of Amazon API endpoints.
 */
function gp_amazon_test_form($form, &$form_state) {
  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_amazon_test', 'gp_amazon_test.admin');

  // Retrieve all endpoint handlers.
  ctools_include('plugins');
  $plugins = ctools_get_plugins('gp_amazon_test', 'amazon_endpoint');

  // Expose endpoints.
  $options = array();
  foreach ($plugins as $id => $info) {
    $options[$id] = $info['title'];
  }
  $form['endpoint'] = array(
    '#type' => 'select',
    '#title' => t('Endpoint'),
    '#options' => $options,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'gp_amazon_test_form_change_callback',
      'wrapper' => 'gp-endpoint-wrapper',
    ),
  );

  // Add a wrapper for every configuration field returned by the plugin.
  $form['config'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'gp-endpoint-wrapper'),
  );

  // Add the submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test'),
  );

  $candidate_plugin = FALSE;
  if (!empty($form_state['values']['endpoint'])) {
    $candidate_plugin = $form_state['values']['endpoint'];

    // If the plugin exists, use it.
    if (isset($plugins[$candidate_plugin]) && $function = ctools_plugin_get_function($plugins[$candidate_plugin], 'form callback')) {
      $form = $function($form, $form_state);
    }
  }

  if ($form_state['executed'] && $candidate_plugin && $function = ctools_plugin_get_function($plugins[$candidate_plugin], 'api callback')) {
    // Pass a clean form_state.
    $clean_form_state = $form_state;
    form_state_values_clean($clean_form_state);
    // Run the api request.
    $result = $function($clean_form_state['values']);

    $form['result'] = array(
      '#type' => 'textarea',
      '#title' => t('XML'),
      '#value' => $result,
      '#rows' => 15,
      '#weight' => 200,
    );
  }

  return $form;
}

/**
 * Ajax callback to change the endpoint form.
 */
function gp_amazon_test_form_change_callback($form, &$form_state) {
  return $form['config'];
}

/**
 * Submit callback for gp_amazon_test_form.
 */
function gp_amazon_test_form_submit($form, &$form_state) {
  // Just rebuild the form.
  $form_state['rebuild'] = TRUE;
}
