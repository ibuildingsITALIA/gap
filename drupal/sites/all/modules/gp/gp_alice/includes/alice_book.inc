<?php

/**
 * Book node migration.
 */
class AliceBooksMigration extends Migration {

  protected function beginProcess($newStatus) {
    if (function_exists('drush_log') && $this->status) {
      drush_log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
      drush_log("+ DRUPAL Start Alice Migration", "status");
    }
    parent::beginProcess($newStatus);
  }

  public function endProcess() {
    if (function_exists('drush_log') && $this->status) {
      drush_log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
      drush_log("+ DRUPAL End Alice Migration at time " . date('d-m-Y H:i'), "status");
      drush_log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
    }
      parent::endProcess();
  }
  public function __construct(array $arguments) {
    //$group = MigrateGroup::getInstance($arguments['group_name']);

    parent::__construct($arguments);



    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'ISBN-13' => array(
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );


    // FILE INVIATO DA ALICE.

    $this->source = new MigrateSourceCSV('private://migrate.csv', $this->columns(), array(
      'header_rows' => 0,
      'embedded_newlines' => 1,
      'delimiter' => "\t"
    ), $this->fields());

    // DESTINAZIONE SU DB GSAMAZON.
    $this->destination = new MigrateDestinationNode('book');

    // Update, not create
    // $this->systemOfRecord = Migration::DESTINATION;


    /*$this->addFieldMapping('nid', 'key')
      ->sourceMigration('book');
    */
    $this->addFieldMapping('title', 'titolo_composto')->callbacks('utf8_encode');
    $this->addFieldMapping('body', 'abstract')->callbacks('utf8_encode')->arguments(array('format' => 'filtered_html'));
    $this->addFieldMapping('field_isbn13', 'ISBN-13');
    $this->addFieldMapping('field_authors', 'autori')->separator(',');
//    $this->addFieldMapping("field_authors:ignore_case")
//      ->defaultValue(TRUE);
    $this->addFieldMapping('field_authors:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_vol_number', 'numero_volume');
    $this->addFieldMapping('field_edition_date', 'data_edizione');
    $this->addFieldMapping('field_edition_number', 'numero_edizione');
    $this->addFieldMapping('field_exp_release_date', 'data_prevista_pubblicazione')->callbacks(array(
      $this,
      'adaptDate',
    ));
    $this->addFieldMapping('field_release_date', 'data_prima_commercializzazione')->callbacks(array(
      $this,
      'adaptDate',
    ));
    $this->addFieldMapping('field_publisher', 'editore')->callbacks('utf8_encode');
//    $this->addFieldMapping("field_publisher:ignore_case")
//      ->defaultValue(TRUE);
    $this->addFieldMapping('field_publisher:create_term')
      ->defaultValue(TRUE);
    //$this->addFieldMapping('field_pages', 'pagine')->callbacks('utf8_encode');

    // IMMAGINE COPERTINA.
    $this->addFieldMapping('field_image', 'url_copertina_locale');

    // ASSOCIAZIONE BIC-CCE.
    $this->addFieldMapping('field_bic_subjects', 'soggetti_bic_cce')->separator(',');
//    $this->addFieldMapping("field_bic_subjects:ignore_case")
//      ->defaultValue(TRUE);
    $this->addFieldMapping('field_bic_subjects:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_bic_subjects:source_type')
      ->defaultValue('tid');

    // ASSOCIAZIONE QUALIFICATORE CCE.
    $this->addFieldMapping('field_bic_qualifiers', 'qualificatori_bic_cce')->separator(',');
//    $this->addFieldMapping("field_bic_qualifiers:ignore_case")
//      ->defaultValue(TRUE);
    $this->addFieldMapping('field_bic_qualifiers:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_bic_qualifiers:source_type')
      ->defaultValue('tid');

    // NUOVI CAMPI DA RICHIESTE PINZAUTI.
    $this->addFieldMapping('field_series_code', 'codice_collana')->callbacks('utf8_encode');
    $this->addFieldMapping('field_series_description', 'descrizione_collana')->callbacks('utf8_encode');
    $this->addFieldMapping('field_series_number', 'numerazione_collana')->callbacks('utf8_encode');
    $this->addFieldMapping('field_pages', 'pagine')->callbacks('utf8_encode');
    $this->addFieldMapping('field_publisher_code', 'codice_editore')->callbacks('utf8_encode');
    $this->addFieldMapping('field_is_available', 'disponibilita')->callbacks('utf8_encode');
    $this->addFieldMapping('field_list_price', 'prezzo')->callbacks('utf8_encode');

    $this->addFieldMapping('uid')
      ->defaultValue(1);
  }

  /**
   * Devo definirmi le colonne a mano, perchè non ho header.
   *
   * per fare ciò mi definisco un array la cui chiave è il num di colonna 0-XX
   * mentre il valore è un ulteriore array con nome_colonna + descrizione
   */
  public function columns() {
    return array(
      0 => array('ISBN-13', 'ISBN-13'),
      1 => array('tipo_prodotto', 'Tipo prodotto'),
      3 => array('autore1', 'Autore 1'),
      4 => array('autore2', 'Autore 2'),
      5 => array('autore3', 'Autore 3'),
      14 => array('titolo_composto', 'Titolo composto'),
      17 => array('numero_volume', 'Numero volume'),
      19 => array('data_edizione', 'Data edizione'),
      20 => array('numero_edizione', 'Numero edizione'),
      22 => array('data_prevista_pubblicazione', 'Data prevista pubblicazione'),
      23 => array('data_prima_commercializzazione', 'Data prima commercializzazione'),
      24 => array('codice_editore', 'Codice Editore'),
      25 => array('editore', 'Editore'),
      26 => array('disponibilita', 'Disponibilità'),
      27 => array('prezzo', 'Prezzo in euro'),
      29 => array('codice_collana', 'Codice collana'),
      30 => array('descrizione_collana', 'Descrizione collana'),
      31 => array('numerazione_collana', 'Numerazione collana'),
      33 => array('formato', 'Formato'),
      35 => array('pagine', 'Pagine'),
      37 => array('bic_cce1', 'Codice Categoria CCE 1'),
      38 => array('bic_cce2', 'Codice Categoria CCE 2'),
      39 => array('bic_cce3', 'Codice Categoria CCE 3'),
      40 => array('bic_cce4', 'Codice Categoria CCE 4'),
      41 => array('bic_cce_desc1', 'Descrizione Categoria CCE 1'),
      42 => array('bic_cce_desc2', 'Descrizione Categoria CCE 2'),
      43 => array('bic_cce_desc3', 'Descrizione Categoria CCE 3'),
      44 => array('bic_cce_desc4', 'Descrizione Categoria CCE 4'),
      45 => array('qual_cce1', 'Codice Qualificatore CCE 1'),
      46 => array('qual_cce2', 'Codice Qualificatore CCE 2'),
      47 => array('qual_cce3', 'Codice Qualificatore CCE 3'),
      48 => array('qual_cce4', 'Codice Qualificatore CCE 4'),
      49 => array('qual_cce_desc1', 'Descrizione Qualificatore CCE 1'),
      50 => array('qual_cce_desc2', 'Descrizione Qualificatore CCE 2'),
      51 => array('qual_cce_desc3', 'Descrizione Qualificatore CCE 3'),
      52 => array('qual_cce_desc4', 'Descrizione Qualificatore CCE 4'),
      64 => array('abstract', 'Abstract'),
      65 => array('url_copertina_alt', 'Url copertina alt'),
      67 => array('url_copertina_alt_2', 'Url copertina alt 2'),
      73 => array('url_copertina', 'Url copertina'),
    );
  }

  protected function adaptDate($value) {
    if (empty($value)) {
      return $value;
    }
    return date('Y-m-d 12:00:00', strtotime($value));
  }

  public function fields() {
    return array(
      'autori' => 'Gli autori',
      'url_copertina_locale' => 'Url locale per la copertina',
      'soggetti_bic_cce' => 'Soggetti BIC CCE',
      'qualificatori_bic_cce' => 'Qualificatori BIC CCE',
    );
  }

  public function prepareRow($row) {
    $isbn = 'ISBN-13';
    if (function_exists('drush_log')) {
      drush_log("+ DRUPAL Updating " . $row->$isbn . " title " . $row->titolo_composto, "status");
    }

    if ($row->disponibilita == 'E' || $row->disponibilita == 'A') {
      $row->disponibilita = FALSE;
    }
    else {
      $row->disponibilita = TRUE;
    }

    $row->autori = implode(",", array_filter(array(
      utf8_encode($row->autore1),
      utf8_encode($row->autore2),
      utf8_encode($row->autore3),
    )));

    // Carico prima i bic-cce.
    $bic_cce = array_filter(array(
      $row->bic_cce1,
      $row->bic_cce2,
      $row->bic_cce3,
      $row->bic_cce4,
      ));
    $qual_bic_cce = array_filter(array(
      $row->bic_cce1,
      $row->bic_cce2,
      $row->bic_cce3,
      $row->bic_cce4,
      ));

    if (count($bic_cce) > 0) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'taxonomy_term')
        ->fieldCondition('field_id', 'value', $bic_cce, 'IN');
      $result_bic_cce = $query->execute();
      if (isset($result_bic_cce['taxonomy_term'])) {
        $row->soggetti_bic_cce = implode(",", array_keys($result_bic_cce['taxonomy_term']));
      }
    }
    if (count($qual_bic_cce) > 0) {
      // Poi tutti i qualificatori associati.
      $query2 = new EntityFieldQuery();
      $query2->entityCondition('entity_type', 'taxonomy_term')
        ->fieldCondition('field_id', 'value', $qual_bic_cce, 'IN');
      $result_bic_cce_qual = $query->execute();
      // Infine le divido con le virgole.
      if (isset($result_bic_cce_qual['taxonomy_term'])) {
        $row->qualificatori_bic_cce = implode(",", array_keys($result_bic_cce_qual['taxonomy_term']));
      }
    }
    $copertina = trim($row->url_copertina);
    $copertina_alt = trim($row->url_copertina_alt);
    $copertina_alt2 = trim($row->url_copertina_alt_2);

    // MECCANISMO DI FALLBACK SU UNA DELLE COPERTINA.
    if (!empty($copertina)) {
      $filename = drupal_basename($copertina);
      $copertina_download = system_retrieve_file($copertina, 's3://alice/' . $filename);
      if ($copertina_download) {
        $row->url_copertina_locale = $copertina_download;
      }
    }
    else {
      if (!empty($copertina_alt)) {
        $filename = drupal_basename($copertina_alt);
        $copertina_download = system_retrieve_file($copertina_alt, 's3://alice/' . $filename);
        if ($copertina_download) {
          $row->url_copertina_locale = $copertina_download;
        }
      }
      else {
        if (!empty($copertina_alt2)) {
          $filename = drupal_basename($copertina_alt2);
          $copertina_download = system_retrieve_file($copertina_alt2, 's3://alice/' . $filename);
          if ($copertina_download) {
            $row->url_copertina_locale = $copertina_download;
          }
        }
      }
    }
    return parent::prepareRow($row);
  }
}

if (!function_exists('drush_log')) {
  function drush_log() {
  }
}
