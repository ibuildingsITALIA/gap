<?php

/**
 * Book node migration.
 */
class CCEQualificatoriMigration extends Migration {

  public function __construct(array $arguments) {
    //$group = MigrateGroup::getInstance($arguments['group_name']);
    parent::__construct($arguments);

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'codicealfa' => array(
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );


    //FILE BIC_CCE
    $this->source = new MigrateSourceCSV('private://TreeBIC2.csv', array(), array(
      'header_rows' => 1,
      'embedded_newlines' => 1,
      'delimiter' => ";"
    ));

    //DESTINAZIONE SU DB GSAMAZON
    $this->destination = new MigrateDestinationTerm('bic_qualifier');

    $this->addFieldMapping('field_id', 'codicealfa');
    $this->addFieldMapping('name', 'soggettocce')->callbacks('utf8_encode');
    $this->addFieldMapping('parent', 'parent');

  }


  /**
   * Implements Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    //E' un padre, non faccio nulla
    if (sizeof($row->codicealfa) <= 1) {
      $row->parent = 0;
    }
    //ALTRIMENTI, vado a prendere il padre.
    $codice_padre = substr($row->codicealfa, 0, -1);
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'taxonomy_term')
      ->fieldCondition('field_id', 'value', $codice_padre, '=');
    $result = $query->execute();
    if (isset($result['taxonomy_term'])) {
      //In pratica il tid è la chiave che risulta dall'execute della query...un po' tricky
      $row->parent = array_keys($result['taxonomy_term'])[0];
    }
  }

}