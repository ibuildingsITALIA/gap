<?php

/**
 * Implements hook_migrate_api().
 */
function gp_alice_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'giunti' => array(
        'title' => t('Giunti'),
      ),
    ),
    'migrations' => array(
      'AliceBooks' => array(
        'class_name' => 'AliceBooksMigration',
        'group_name' => 'giunti',
      ),
      'AliceBooksUpdate' => array(
        'class_name' => 'AliceBooksUpdateFieldsMigration',
        'group_name' => 'giunti',
      ),
      'BicCCE' => array(
        'class_name' => 'BicCCEMigration',
        'group_name' => 'giunti',
      ),
      'QualCCE' => array(
        'class_name' => 'CCEQualificatoriMigration',
        'group_name' => 'giunti',
      ),
    ),
  );

  return $api;
}