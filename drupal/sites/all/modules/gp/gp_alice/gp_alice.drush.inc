<?php
/**
 * @file
 * Drush tasks.
 */

/**
 * Implements hook_drush_help().
 */

define ('alert_example' , 'drush delete-isbn-db /abs_path/to/file');
define ('task_done' , 'Task done.');

/**
 * Implements hook_drush_help().
 */
function gp_alice_drush_help($command) {
  switch ($command) {
    case 'drush:delete-isbn-db':
      return dt('Remove record from DB taking isbn codes from a file');
  }
}

/**
 * Implements hook_drush_command().
 */
function gp_alice_drush_command() {
  $items = array();
  $items['delete-isbn-db'] = array(
    'description' => dt('Remove record from DB taking isbn codes from a file'),
    'arguments'   => array(
      'arg1'    => dt('Mandatory file path'),
    ),
    'examples' => array(
      'Standard example' => alert_example,
    ),
  );

  return $items;
}

/**
 * Callback function for drush in order to delete from DB.
 */
function drush_gp_alice_delete_isbn_db($arg1 = NULL) {
  // Cancella ogni record sul DB in base al codice isbn.
  drush_log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
  drush_log("+ DRUPAL: delete-isbn-db on $arg1\n", "status");

  drush_log("Staring to delete node", 'status');
  if (!isset($arg1)) {
    drush_log('You must enter an absolute file path! exiting...', 'error');
    drush_log(alert_example, 'notice');
    return;
  }

  if (!file_exists($arg1)) {
    drush_log('File not exists! exiting...', 'error');
    drush_log(alert_example, 'notice');
    return;
  }

  $data = file_get_contents($arg1);
  $isbn_codes_array = explode(PHP_EOL, $data);

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
        ->fieldCondition('field_isbn13', 'value', $isbn_codes_array, 'IN');
  $nodes_to_remove = $query->execute();

  if (isset($nodes_to_remove['node']) && $nodes_to_remove['node'] != NULL) {
    $nids = array();
    foreach ($nodes_to_remove['node'] as $row) {
      drush_log("Node: " . $row->nid . " type: " . $row->type . " to be removed");
      $nids[] = $row->nid;
    }
    node_delete_multiple($nids);
  }
  else {
    drush_log('Nothing to delete!', 'warning');
  }

  drush_log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
  drush_log(task_done, 'notice');
  drush_log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
}

