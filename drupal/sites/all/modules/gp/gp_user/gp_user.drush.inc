<?php
/**
 * @file
 * Drush tasks.
 */

/**
 * Implements hook_drush_help().
 */

define('ALERT_EXAMPLE', 'drush check-user-db /abs_path/to/file');
define('TASK_START', 'Task start.');
define('TASK_DONE', 'Task done.');

function gp_user_drush_help($command) {
  switch ($command) {
    case 'drush:check-user-db':
      return dt('Check user on DB');

  }
}

/**
 * Implements hook_drush_command().
 */
function gp_user_drush_command() {


  $items = array();
  $items['check-user-db'] = array(
    'description' => dt('Check user email'),
    'arguments'   => array(
      'arg1'    => dt('Mandatory file path'),
    ),
    'examples' => array(
      'Standard example' => ALERT_EXAMPLE,
    ),
  );

  return $items;
}

/**
 * Callback function for drush in order to delete from DB.
 */
function drush_gp_user_check_user_db($arg1 = NULL) {
  drush_log(TASK_START);
  // Get all Drupal users.
  $users = entity_load('user');
  $disambiguation = array();
  $count_total = 0;
  $count_cas = 0;
  $count_im = 0;

  foreach ($users as $user) {
    $user_id = $user->uid;
    drush_log($user_id);

    $ssouid = !empty($user->data['sso_uid']) ? $user->data['sso_uid'] : FALSE;
    $drupal_mail = $user->mail;
    $cas_mail = $user->cas_name;
    // Get IM mail from API.
    if (!empty($ssouid)) {
      $data = gp_user_im_user_data_get_by_sso_uid($ssouid);
      $im_mail = !empty($data['email']) ? $data['email'] : FALSE;
    }
    else {
      $im_mail = FALSE;
    }

    // Check emails.
    $match = TRUE;
    // Cas name is not equal.
    if ($drupal_mail != $cas_mail) {
      $match = FALSE;
      $count_cas++;
    }
    // IM is not egual.
    if ($drupal_mail != $im_mail) {
      $match = FALSE;
      $count_im++;
    }

    // If same mail not match.
    if (!$match) {
      $count_total++;
      $disambiguation[] = array(
        'uid' => $user_id,
        'mail_drupal' => $drupal_mail,
        'mail_cas' => $cas_mail,
        'mail_im' => $im_mail,
        'ssouid' => $ssouid,
      );
    }
  }

  // Writing report in file.
  $output = fopen($arg1, 'w') or die("Can't open file");
  header("Content-Type:application/csv");
  header("Content-Disposition:attachment;filename=pressurecsv.csv");
  fputcsv($output, array(
    'uid',
    'mail_drupal',
    'mail_cas',
    'mail_im',
    'ssouid',
  ));
  foreach ($disambiguation as $user) {
    fputcsv($output, $user);
  }
  fclose($output) or die("Can't close file");

  drush_log('CAS: ' . $count_cas);
  drush_log('IM: ' . $count_im);
  drush_log('Total: ' . $count_total);

  drush_log(TASK_DONE);
}

