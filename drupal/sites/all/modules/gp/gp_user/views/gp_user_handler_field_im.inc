<?php
/**
 * @file
 * Definition of gp_user_handler_field_im.
 */

class gp_user_handler_field_im extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();

    $options['im_field'] = array('default' => 'nome');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['im_field'] = array(
      '#type' => 'select',
      '#title' => t('Field to show'),
      '#options' => array(
        'nome' => t('First Name'),
        'cognome' => t('Last Name'),
      ),
      '#default_value' => $this->options['im_field'],
      '#description' => t('Which field of the Identity Manager must be shown.'),
    );

    parent::options_form($form, $form_state);
  }

  function render($values) {
    $uid = $this->get_value($values);

    if (empty($uid)) {
      return '';
    }

    $account = user_load($uid);
    if (empty($account)) {
      return '';
    }

    $field = $this->options['im_field'];
    $value = token_replace("[user:im:$field]", array('user' => $account), array('clear' => TRUE));

    return $value;
  }
}