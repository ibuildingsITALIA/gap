<?php
/**
 * @file
 * Definition of gp_user_handler_field_im_uid.
 */

class gp_user_handler_field_im_uid extends views_handler_field {
  function render($values) {
    $uid = $this->get_value($values);

    if (empty($uid)) {
      return '';
    }

    $account = user_load($uid);
    if (empty($account)) {
      return '';
    }

    $value = gp_user_get_sso_uid($account);

    return $value;
  }
}