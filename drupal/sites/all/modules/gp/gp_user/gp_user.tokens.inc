<?php
/**
 * @file
 * Token module integration.
 */

/**
 * Implements hook_token_info().
 */
function gp_user_token_info() {
  $types = array();
  $types['im'] = array(
    'name' => t('Giunti identity manager'),
    'description' => t('Tokens related to Giunti identity manager users. These tokens are saved locally.'),
    'needs-data' => 'user',
  );

  $im = array();
  $im['nome'] = array(
    'name' => t('Nome field'),
    'description' => t('Nome field from identity manager.'),
  );
  $im['cognome'] = array(
    'name' => t('Cognome field'),
    'description' => t('Cognome field from identity manager.'),
  );
  $im['fullname'] = array(
    'name' => t('Full user name'),
    'description' => t('Name and username of the user.'),
  );
  $im['frontend-name'] = array(
    'name' => t('Frontend name'),
    'description' => t('User name if set, email otherwise. For frontend use.'),
  );

  return array(
    'types' => $types,
    'tokens' => array('im' => $im),
  );
}

/**
 * Implements hook_token_info_alter().
 */
function gp_user_token_info_alter(&$data) {
  $data['tokens']['user']['im'] = array(
    'name' => t('Identity manager'),
    'description' => t('The identity manager sso uid.'),
    'type' => 'im',
  );
}

/**
 * Implements hook_tokens().
 */
function gp_user_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'user' && !empty($data['user'])) {
    $account = $data['user'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'im':
          // If sso_uid is not yet retrieved, set a default.
          $sso_uid = !empty($account->data['sso_uid']) ? $account->data['sso_uid'] : t('not yet assigned');
          $replacements[$original] = $sanitize ? check_plain($sso_uid) : $sso_uid;
          break;
      }
    }

    if (!empty($account->data['gp_user']) && $im_tokens = token_find_with_prefix($tokens, 'im')) {
      $replacements += token_generate('im', $im_tokens, array('im' => $account), $options);
    }
  }

  if ($type == 'im' && !empty($data['im'])) {
    $account = $data['im'];
    $values = $account->data['gp_user'];
    foreach ($tokens as $name => $original) {
      if (isset($values[$name])) {
        // If value is directly set, use it.
        $replacements[$original] = $sanitize ? check_plain($values[$name]) : $values[$name];
      }
      else {
        // Custom values.
        switch ($name) {
          case 'fullname':
            $name = !empty($values['nome']) ? $values['nome'] : '';
            $surname = !empty($values['cognome']) ? $values['cognome'] : '';
            $fullname = $name . ' ' . $surname;
            // If both values are empty, use the mail.
            if (' ' === $fullname) {
              $fullname = $account->mail;
            }
            $replacements[$original] = $sanitize ? check_plain($fullname) : $fullname;
            break;

          case 'frontend-name':
            $name = !empty($values['nome']) ? $values['nome'] : $account->mail;
            $replacements[$original] = $sanitize ? check_plain($name) : $name;
            break;
        }
      }
    }
  }

  return $replacements;
}
