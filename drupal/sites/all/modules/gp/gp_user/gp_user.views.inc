<?php
/**
 * @file
 * Views related hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function gp_user_views_data_alter(&$data) {
  if (isset($data['users'])) {
    $data['users']['im_uid'] = array(
      'title' => t('IdentityManager uid'),
      'help' => t('Identity Manager uid of the user.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_user_handler_field_im_uid',
      ),
    );
    $data['users']['im'] = array(
      'title' => t('IdentityManager data'),
      'help' => t('Data about the Identity Manager of the user.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_user_handler_field_im',
      ),
    );
  }
}
