<?php
/**
 * @file
 * Form declarations for gp_user module.
 */

/**
 * User registration form.
 */
function gp_user_register_form($form, &$form_state) {
  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_user', 'gp_user.forms');

  // Add some text.
  $form['initial_text'] = array(
    '#markup' => '<p class="initial-text">' . t('Fill the underlying field to register to the site:') . '</p>',
  );

  // Attach some profile fields.
  $fields = array('nome', 'cognome', 'email');
  gp_user_field_schema_attach_form_fields($form, $form_state, $fields);

  // Those fields are required.
  foreach ($fields as $key) {
    $form[$key]['#required'] = TRUE;
  }

  // Turn off autocomplete for email field.
  $form['email']['#attributes'] = array('autocomplete' => 'off');

  // Set correct weight for email and add a confirmation field for it.
  $form['email_confirm'] = array(
    '#type' => 'textfield',
    '#title' => t('Confirm e-mail'),
    '#required' => TRUE,
    '#attributes' => array('autocomplete' => 'off'),
  );

  // Add the password field.
  $form['pass'] = array(
    '#type' => 'password_confirm',
    '#size' => 25,
    '#description' => t('Provide a password for the new account in both fields.'),
    '#required' => TRUE,
    '#attributes' => array('autocomplete' => 'off'),
  );

  // The privacy options to display.
  $terms_and_conditions = array(
    'terms_of_use' => variable_get('terms_and_conditions_terms_of_use'),
    'privacy' => variable_get('terms_and_conditions_privacy'),
  );

  // Add privacy checkbox;
  $form['terms_and_conditions'] = array(
    '#type' => 'checkboxes',
    '#options' => $terms_and_conditions,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register'),
  );

  // Set explicit validate and submit callbacks.
  $form['#validate'] = array('gp_user_register_form_validate');
  $form['#submit'] = array('gp_user_register_form_submit');

  return $form;
}

/**
 * Validate handler for gp_user_register_form.
 *
 * Handles creation of user into the identity manager.
 */
function gp_user_register_form_validate($form, &$form_state) {
  if (empty($form_state['values'])) {
    // The elements validation will kick in.
    return;
  }

  // Clean submitted values.
  form_state_values_clean($form_state);
  // Shortcut to values.
  $values = $form_state['values'];

  // Validate mails.
  foreach (array('email', 'email_confirm') as $key) {
    if (isset($values[$key]) && $error = user_validate_mail($values[$key])) {
      form_set_error($key, $error);
    }
  }

  // Validate the mail confirm field.
  if (isset($values['email']) && isset($values['email_confirm'])) {
    if (strcmp($values['email'], $values['email_confirm'])) {
      form_set_error('email_confirm', t('The specified e-mails do not match.'));
    }
  }

  // Remove our custom fields.
  $values['password'] = $values['pass'];
  unset($values['email_confirm'], $values['pass']);

  // Validate data with the identity manager.
  $result = gp_user_im_user_data_validate($values);

  // If result is an array, it means that errors have occurred.
  // Set them into the form.
  if (is_array($result)) {
    foreach ($result as $name => $message) {
      form_set_error($name, $message);
    }
  }

  // Validate terms and conditions.
  if (isset($values['terms_and_conditions'])) {
    foreach ($values['terms_and_conditions'] as $term => $value) {
      if (!$value) {
        form_set_error($term, t('You have to check terms and conditions.'));
        break;
      }
    }
  }

  // If the form has any errors, quit.
  $errors = form_get_errors();
  if (!empty($errors)) {
    return;
  }

  // As the create user api can return errors (even if we validated everything),
  // call it here so the results can be used.
  $result = gp_user_im_user_create($values['email'], $values['password']);
  // Same. Old. Story.
  if (is_array($result)) {
    foreach ($result as $name => $message) {
      form_set_error($name, $message);
    }
    // Now we quit.
    return;
  }

  // Save sso_uid to form_state in case somebody needs it (cough cough gp_card).
  $form_state['sso_uid'] = $result;

  // Save the profile data now.
  // If an error is thrown, who cares. User will edit profile again.
  gp_user_im_user_data_save($result, $values);
}

/**
 * Submit handler for gp_user_register_form.
 */
function gp_user_register_form_submit($form, &$form_state) {
  // Automatically register the user.
  $cas_user = array(
    'name' => $form_state['values']['email'],
    'login' => TRUE,
    'register' => TRUE,
    'attributes' => array(),
  );
  gp_user_handle_cas_user($cas_user);

  // Redirect and show messages based on success of registration and login.
  if (user_is_logged_in()) {
    drupal_set_message(t('Thanks for your registration. Welcome to the site Giunti al Punto.'));
    $form_state['redirect'] = 'user';
  }
  else {
    drupal_set_message(t('Thanks for your registration. You can now login with your credentials.'));
    $form_state['redirect'] = 'signup';
  }
}

/**
 * Profile edit submit form.
 */
function gp_user_profile_form($form, &$form_state, $account = NULL) {
  global $user;

  // If a sso_uid is not set in form_state, get one from account passed
  // or from current user.
  if (empty($form_state['sso_uid'])) {
    if (empty($account)) {
      $account = user_load($user->uid);

      if (!$account || !$account->uid) {
        drupal_access_denied();
        drupal_exit();
      }
    }

    $form_state['sso_uid'] = gp_user_get_sso_uid($account);

    // Persist also account to form state, as we need to save data
    // if we are in a edit profile form.
    $form_state['account'] = $account;
  }

  // Get user data and merge with values retrieved before.
  $user_data = gp_user_im_user_data_get_by_sso_uid($form_state['sso_uid']);
  if (!$user_data) {
    // API error, return a message and quit.
    drupal_set_message(t('An error has occurred. Please try again later.'), 'error');
    return array();
  }

  // Pass the user data into the form state.
  $form_state['user_data'] = $user_data;

  // Get field schema fields.
  gp_user_field_schema_attach_form_fields($form, $form_state);

  // Remove some unwanted fields.
  unset($form['codiceFiscale']);
  unset($form['partitaIva']);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  // Set custom handlers to be sure that the correct ones are called.
  $form['#validate'][] = 'gp_user_profile_form_validate';
  $form['#submit'][] = 'gp_user_profile_form_submit';

  return $form;
}

/**
 * Validate handler for gp_user_profile_form.
 *
 * Saves user data to identity manager.
 */
function gp_user_profile_form_validate($form, &$form_state) {
  global $user;

  // Remove unneeded values.
  form_state_values_clean($form_state);

  $email = strtolower($form_state['values']['email']);
  $previous_email = strtolower($user->mail);

  if ($previous_email != $email) {
    $account = user_load_by_name($form_state['values']['email']);
    if (!empty($account)) {
      form_set_error('email', t('This email is already in use on the system.'));
      return;
    }
  }

  // Check if there are values left.
  if (!empty($form_state['values'])) {
    $values = $form_state['values'];
    $result = gp_user_im_user_data_save($form_state['sso_uid'], $values);

    // If result is an array, it means that errors have occurred.
    // Set them into the form.
    if (is_array($result)) {
      foreach ($result as $name => $message) {
        form_set_error($name, $message);
      }
      return;
    }
  }
}

/**
 * Submit handler for gp_user_profile_form.
 *
 * Redirects to correct page.
 */
function gp_user_profile_form_submit($form, &$form_state) {
  global $user;

  // Behave differently if this form is multistep or not.
  if (empty($form_state['is_multistep'])) {
    // Update user data if we have an non anonymous account.
    if (isset($form_state['account']) && $form_state['account']->uid) {
      gp_user_save_data($form_state['account'], $form_state['values']);

      $email = strtolower($form_state['values']['email']);
      $previous_email = strtolower($user->mail);
      if ($previous_email != $email) {
        // Update drupal user.
        $account = $user;
        user_save($account, array(
          'name' => $email,
          'mail' => $email,
          'cas_name' => $email,
        ));
      }
    }

    drupal_set_message(t('Profile updated.'));
  }
  else {
    drupal_set_message(t('Thanks for your registration. You can now <a href="@login">login with your credentials</a>.', array(
      '@login' => url('signup'),
    )));
    drupal_goto();
  }
}

/**
 * User password change form.
 *
 * This form, despite the ugly name chosen by the client, doesn't change
 * anything. It only sends the mail to the user with a reset password link.
 */
function gp_user_change_pass_form($form, &$form_state) {
  global $user;

  // This endpoint is used by other platforms in the Giunti Network.
  // We need parameters but we can't handle them like Drupal does.
  // They will be passed in the query parameters.
  $source = !empty($_GET['source']) ? check_plain($_GET['source']) : 'web';

  // Save web app.
  $form_state['source'] = $source;

  // This code is basically the same of user_pass(). Since we need to change
  // some labels, defaults and handlers, instead of including the
  // user.pages.inc file we write again the small amount of code.
  // @see user_pass()
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#size' => 60,
    '#required' => TRUE,
  );
  // Allow logged in users to request this also.
  if ($user->uid > 0) {
    $form['mail']['#type'] = 'value';
    $form['mail']['#value'] = $user->mail;
    $form['text'] = array(
      '#prefix' => '<p>',
      '#markup' => t('Password reset instructions will be mailed to %email. You must log out to use the password reset link in the e-mail.', array(
        '%email' => $user->mail,
      )),
      '#suffix' => '</p>',
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('E-mail reset link'));

  return $form;
}

/**
 * Validate handler for gp_user_change_pass_form.
 */
function gp_user_change_pass_form_validate($form, &$form_state) {
  if ($error = user_validate_mail($form_state['values']['mail'])) {
    form_set_error('mail', $error);
  }
}

/**
 * Submit handler for gp_user_change_pass_form.
 */
function gp_user_change_pass_form_submit($form, &$form_state) {
  global $language;

  $mail = $form_state['values']['mail'];

  // Get the sso uid to see if the user exists.
  $sso_uid = gp_user_im_user_ssouid_get_by_identifier($mail);

  // If we have found the user, send the mail.
  if (FALSE !== $sso_uid) {
    // Get the user remote data.
    $data = gp_user_im_user_data_get_by_sso_uid($sso_uid);

    // If we failed retrieving the full profile, ask the user to retry.
    if (FALSE === $data) {
      drupal_set_message(t('An error has occurred retrieving information about the profile, try again in a few seconds.'), 'warning');
      $form_state['rebuild'] = TRUE;
      return;
    }

    $params = array(
      'reset_pwd_url' => gp_user_reset_pass_url($sso_uid, $data),
      'source' => _gp_user_change_pass_source_web_app($form_state['source']),
    );
    drupal_mail('gp_user', 'changepwd', $mail, $language, $params);

    watchdog('user', 'Password reset instructions mailed to %email.', array('%email' => $mail));

    drupal_set_message(t('Further instructions have been sent to your e-mail address.'));
    $form_state['redirect'] = user_is_logged_in() ? 'user' : 'signup';
  }
  else {
    $query = array(
      'card' => 'bind',
    );
    $url = url('register', array('query' => $query));

    $string = 'The e-mail address with which you are trying to recover the password is not present in our archives.<br />';
    $string .= 'Check therefore if you entered correctly or if you have used a different one.<br />';
    $string .= 'If the recovery password is not successful end we invite you to make a new record click <a href="@signin">here</a>.';

    drupal_set_message(t($string, array('@signin' => $url)), 'warning');

    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Helper function to convert an id of a source web app into a string.
 *
 * @param int $source
 *   The id of the app.
 *
 * @return string
 *   The string to use in the mail.
 */
function _gp_user_change_pass_source_web_app($source) {
  switch ($source) {
    case 'app':
      $string = 'della App GiuntiCard - Giunti al Punto.';
      break;

    case 'web':
    default:
      $string = 'del sito Giuntialpunto.it .';
  }

  return $string;
}

/**
 * User password reset form.
 *
 * This form allow the user to insert a new password.
 */
function gp_user_reset_pass_form($form, &$form_state, $sso_uid) {
  // Save the sso_uid to form_state.
  $form_state['sso_uid'] = $sso_uid;

  $form['pass'] = array(
    '#type' => 'password_confirm',
    '#size' => 25,
    '#description' => t('Provide a new password for the account in both fields.'),
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Change password'),
  );

  return $form;
}

/**
 * Submit handler for gp_user_reset_pass_form().
 */
function gp_user_reset_pass_form_submit($form, &$form_state) {
  $sso_uid = $form_state['sso_uid'];

  // Save the new password.
  $success = gp_user_im_user_password_save($sso_uid, $form_state['values']['pass']);

  // If result is an array, it means that errors have occurred.
  if (!$success) {
    watchdog('gp_card', 'Failed to reset password for sso_uid %sso_uid.', array('%sso_uid' => $sso_uid));
    drupal_set_message(t('An error has occurred saving your new password. Please try again later.'), 'warning');
    $form_state['rebuild'] = TRUE;
  }
  else {
    // Show a success message and redirect to signup page.
    drupal_set_message(t('You have successfully reset your password. You can now login with your new credentials.'));
    drupal_goto('signup');
  }
}
