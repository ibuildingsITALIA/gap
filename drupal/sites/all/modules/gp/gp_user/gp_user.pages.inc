<?php
/**
 * @file
 * Page callbacks for user module.
 */

/**
 * Page callback for a login page wrapper around cas.
 */
function gp_user_signup_page() {
  global $user;

  if (isset($_GET['url_back_destination']) && url_is_external($_GET['url_back_destination'])) {
    $_SESSION += array(
      'url_back_destination' => $_GET['url_back_destination'],
    );
  }

  // If the user is logged in, redirect to account.
  if ($user->uid) {
    if (!empty($_SESSION['url_back_destination'])) {
      $options['query'] = array('user_uid' => $user->data['sso_uid']);
      $url = url($_SESSION['url_back_destination'], $options);
      unset($_SESSION['url_back_destination']);

      drupal_goto($url);
    }

    drupal_goto('user');
  }

  // Add query parameters.
  $query = drupal_get_query_parameters();

  $build = array(
    '#theme_wrappers' => array('gp_signup_page'),
  );

  $build['iframe'] = array(
    '#theme' => 'cas_iframe',
    '#url' => url('cas', array('absolute' => TRUE, 'query' => $query)),
  );

  // If the user is logged in, we don't have to render the register link.
  if (user_is_logged_in()) {
    return $build;
  }

  $login_url = _gp_user_get_signup_url();

  // Add a link to the register page.
  $build['register'] = array(
    '#markup' => l(t('Register', array('context' => 'giunti-signup')), 'register', array(
      'query' => array('destination' => ltrim($login_url, '/')),
      'attributes' => array(
        'class' => array('btn'),
      ),
    )),
  );

  return $build;
}

/**
 * Page callback to allow login on CAS server.
 */
function gp_user_cas_login_page($cas_first_login = FALSE, $renew = FALSE) {
  // Path defaults.
  $path = '';
  $options = array('absolute' => TRUE);

  // Retrieve destination from url if present.
  // We do not allow absolute URLs to be passed via $_GET,
  // as this can be an attack vector.
  // @see drupal_goto()
  if (isset($_GET['destination']) && !url_is_external($_GET['destination'])) {
    $destination = drupal_parse_url($_GET['destination']);
    $path = $destination['path'];
    $options['query'] = $destination['query'];
    $options['fragment'] = $destination['fragment'];
  }

  // Prepare url.
  $url = url($path, $options);

  if (!empty($_SESSION['url_back_destination'])) {
    $ssouid = gp_user_im_user_ssouid_get_by_identifier($_SESSION['cas_name']);
    $options['query'] = array('user_uid' => $ssouid);
    $url = url($_SESSION['url_back_destination'], $options);
    unset($_SESSION['url_back_destination']);
  }

  // Add javascript needed for redirect.
  drupal_add_js("window.parent.location = '{$url}'", 'inline');
  return 'Redirect...';
}

/**
 * Page callback for the user reset password form.
 *
 * @param string $sso_uid
 *   The sso_uid of the profile we are going to reset the password for.
 *
 * @param string $timestamp
 *   The timestamp of when the link was generated.
 *
 * @param string $hashed_pass
 *   The hashed timestamp and password to allow validation of the url.
 *
 * @return array
 *   Render array of the form.
 */
function gp_user_reset_pass_page($sso_uid, $timestamp, $hashed_pass) {
  global $user;

  // If the user is logged in, and the reset link is suited for another user,
  // show a message and redirect.
  if ($user->uid && $user->uid != gp_user_get_sso_uid()) {
    drupal_set_message(t('You are already logged into the site, but you tried to reset the password for another account. Please <a href="!logout">logout</a> and try using the link again.', array(
      '!logout' => url('user/logout'),
    )), 'warning');
    drupal_goto();
  }

  // Time out, in seconds, until login URL expires. Defaults to 24 hours =
  // 86400 seconds.
  $timeout = variable_get('user_password_reset_timeout', 86400);
  $current = REQUEST_TIME;
  // Check if the link has expired.
  if ($current - $timestamp > $timeout) {
    drupal_set_message(t('You have tried to use a reset link that has expired. Please request a new one using the form below.'), 'warning');
    drupal_goto('changepwd');
  }

  // Get the user profile requested, allowing us to validate the request.
  $data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
  // If we failed retrieving data, send user to the change password form again.
  // @todo If the API failed, what we do?
  if (FALSE === $data || $hashed_pass != gp_user_reset_hash($timestamp, $data)) {
    drupal_set_message(t('You have tried to use a reset link that is no longer valid. Please request a new one using the form below.'), 'warning');
    drupal_goto('changepwd');
  }

  // Include and display the form.
  module_load_include('inc', 'gp_user', 'gp_user.forms');
  return drupal_get_form('gp_user_reset_pass_form', $sso_uid);
}
