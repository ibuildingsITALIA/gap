<?php
/**
 * @file
 * Base class for field handlers.
 */

/**
 * Class IdentityManagerFieldHandler
 */
abstract class IdentityManagerFieldHandler {
  protected $name;
  protected $mandatory;
  protected $hidden;
  protected $data;
  protected $value;
  protected $title;
  protected $description;

  /**
   * Class constructor.
   *
   * @param object $info
   *   Info about the field.
   *
   * @param string $value
   *   An initial value (optional).
   */
  public function __construct($info, $value = '') {
    // Set information from info.
    $this->name = $info->name;
    $this->mandatory = (bool) $info->mandatory;
    $this->hidden = (bool) (isset($info->hidden) ? $info->hidden : (isset($info->system) ? $info->system : 0));
    $this->data = $info->data;

    // Retrieve title and description from variable.
    $strings = variable_get('gp_user_im_fields_strings', array());
    if (isset($strings[$this->name])) {
      $this->title = $strings[$this->name]['title'];
      $this->description = $strings[$this->name]['description'];
    }

    // Set default value.
    $this->value = $value;
  }

  /**
   * Returns the name of the field.
   *
   * @return string
   *   The name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Returns the translated title of the field.
   *
   * @return string
   *   The title.
   */
  public function getTitle() {
    return t($this->title, array(), array('context' => 'gp_user_im_field'));
  }

  /**
   * Returns the translated description of the field.
   *
   * @return string
   *   The description.
   */
  public function getDescription() {
    return t($this->description, array(), array('context' => 'gp_user_im_field'));
  }

  /**
   * Returns the value of the field.
   *
   * @return string
   *   The current value.
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Validate form.
   *
   * @param array $form
   *   The full form array.
   *
   * @param array $form_state
   *   The form_state array.
   *
   * @return bool
   *   Returns the success of validation.
   */
  public function validateForm($form, &$form_state) {
    return TRUE;
  }

  /**
   * Return form elements for this field.
   */
  abstract public function getForm($form, &$form_state);
}
