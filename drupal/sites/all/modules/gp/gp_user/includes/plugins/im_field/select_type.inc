<?php
/**
 * @file
 * Select type field plugin.
 */

$plugin = array(
  'title' => 'Select type handler',
  'handler' => 'SelectTypeFieldHandler',
);
