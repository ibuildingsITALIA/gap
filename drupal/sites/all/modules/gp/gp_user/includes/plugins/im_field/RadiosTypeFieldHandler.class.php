<?php
/**
 * @file
 * Handler for radios plugin.
 */

/**
 * Class RadiosFieldHandler
 */
class RadiosTypeFieldHandler extends SelectTypeFieldHandler {
  /**
   * Return form elements for this field.
   */
  public function getForm($form, &$form_state) {
    $form = parent::getForm($form, $form_state);

    if (!empty($form[$this->name])) {
      $form[$this->name]['#type'] = 'radios';
    }

    return $form;
  }
}
