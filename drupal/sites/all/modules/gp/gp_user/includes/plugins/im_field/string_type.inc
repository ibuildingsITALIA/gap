<?php
/**
 * @file
 * String type field plugin.
 */

$plugin = array(
  'title' => 'String type handler',
  'handler' => 'StringTypeFieldHandler',
);
