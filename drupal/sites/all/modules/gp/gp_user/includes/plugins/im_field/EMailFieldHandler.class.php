<?php
/**
 * @file
 * Handler for e-mail field.
 */

/**
 * Class EMailFieldHandler
 */
class EMailFieldHandler extends StringTypeFieldHandler {
  /**
   * Return form elements for this field.
   */
  public function getForm($form, &$form_state) {
    $form = parent::getForm($form, $form_state);

    // $form[$this->name]['#required'] = TRUE;

    return $form;
  }

  /**
   * Validate callback.
   */
  public function validateForm($form, &$form_state) {
    $value = $form_state['values'][$this->name];

    if ($error = user_validate_mail($value)) {
      form_set_error($this->name, $error);
    }
  }
}
