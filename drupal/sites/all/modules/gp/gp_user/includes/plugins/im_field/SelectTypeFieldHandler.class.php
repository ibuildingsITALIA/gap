<?php
/**
 * @file
 * Handler for select plugin.
 */

/**
 * Class SelectTypeFieldHandler
 */
class SelectTypeFieldHandler extends StringTypeFieldHandler {
  /**
   * Return form elements for this field.
   */
  public function getForm($form, &$form_state) {
    $form = parent::getForm($form, $form_state);

    // If the form is already empty, quit.
    if (empty($form)) {
      return $form;
    }

    // Retrieve options from data field.
    $options = array();
    if (isset($this->data['function']) && function_exists($this->data['function'])) {
      $options = call_user_func($this->data['function']);
    }

    $form[$this->name]['#options'] = $options;
    $form[$this->name]['#type'] = 'select';

    return $form;
  }
}
