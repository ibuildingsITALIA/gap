<?php
/**
 * @file
 * Radios type field plugin.
 */

$plugin = array(
  'title' => 'Radios type handler',
  'handler' => 'RadiosTypeFieldHandler',
);
