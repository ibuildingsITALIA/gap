<?php
/**
 * @file
 * Handler for date plugin.
 */

/**
 * Class DateTypeFieldHandler
 */
class DateTypeFieldHandler extends StringTypeFieldHandler {
  /**
   * Return form elements for this field.
   *
   * @todo Date module isn't active now, implement this if needed.
   */
  public function getForm($form, &$form_state) {
    $form = parent::getForm($form, $form_state);

    if (!empty($form[$this->name])) {
      $form[$this->name]['#type'] = 'date';

      // Default date component values.
      $form[$this->name]['#default_value'] = array(
        'year' => '',
        'month' => '',
        'day' => '',
      );

      if (!empty($this->value)) {
        // Fill default values from current date.
        $date = new DateTime($this->value);
        $form[$this->name]['#default_value']['year'] = $date->format('Y');
        $form[$this->name]['#default_value']['month'] = $date->format('n');
        $form[$this->name]['#default_value']['day'] = $date->format('j');
      }
    }

    return $form;
  }

  /**
   * Validate callback.
   *
   * Implode value into a single string. It is safe to do this now
   * because element_validate has already been done.
   */
  public function validateForm($form, &$form_state) {
    $components = & $form_state['values'][$this->name];
    if (isset($components['year'], $components['month'], $components['day'])) {
      // Pad string to always have zero.
      $month = str_pad($components['month'], 2, '0', STR_PAD_LEFT);
      $day = str_pad($components['day'], 2, '0', STR_PAD_LEFT);
      $components = "{$components['year']}-{$month}-{$day}";
    }
  }
}
