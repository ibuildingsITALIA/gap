<?php
/**
 * @file
 * Handler for textfield plugin.
 */

/**
 * Class StringTypeFieldHandler
 */
class StringTypeFieldHandler extends IdentityManagerFieldHandler {
  /**
   * Return form elements for this field.
   */
  public function getForm($form, &$form_state) {
    // If this field is hidden, do not render.
    if ($this->hidden) {
      return $form;
    }

    $form[$this->name] = array(
      '#title' => $this->getTitle(),
      '#description' => $this->getDescription(),
      '#type' => 'textfield',
      '#required' => $this->mandatory,
      '#default_value' => $this->value,
    );

    return $form;
  }
}
