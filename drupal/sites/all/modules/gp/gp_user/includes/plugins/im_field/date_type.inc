<?php
/**
 * @file
 * Date field plugin.
 */

$plugin = array(
  'title' => 'Date type handler',
  'handler' => 'DateTypeFieldHandler',
);
