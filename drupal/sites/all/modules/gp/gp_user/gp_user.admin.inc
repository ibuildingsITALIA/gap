<?php
/**
 * @file
 * Administration forms.
 */

/**
 * Admin settings form.
 */
function gp_user_admin_settings_form() {
  $form = array();

  $form['im_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Identity manager API settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['im_api']['gp_user_im_api_url'] = array(
    '#title' => t('Identity manager API url'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('gp_user_im_api_url', ''),
    '#size' => 100,
  );

  $form['im_api']['gp_user_im_api_key'] = array(
    '#title' => t('Identity manager API key'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('gp_user_im_api_key', ''),
    '#size' => 100,
  );

  $form['im_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Identity manager fields'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $schema = gp_user_field_schema_load();
  $defaults = variable_get('gp_user_im_fields_strings', array());
  // Add label and description for each field of the schema.
  foreach ($schema as $name => $data) {
    if (!empty($data->hidden) || !empty($data->system)) {
      // Skip system/hidden fields.
      continue;
    }

    $form['im_fields'][$name] = array();
    $form['im_fields'][$name]['title'] = array(
      '#title' => t('Title for field "@name"', array('@name' => $name)),
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => empty($defaults[$name]['title']) ? '' : $defaults[$name]['title'],
    );
    $form['im_fields'][$name]['description'] = array(
      '#title' => t('Description for field "@name"', array('@name' => $name)),
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => empty($defaults[$name]['description']) ? '' : $defaults[$name]['description'],
    );
  }

  $form['terms_and_conditions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Privacy terms'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $terms_of_use = variable_get('terms_and_conditions_terms_of_use');
  $form['terms_and_conditions']['terms_of_use'] = array(
    '#title' => t('Description for field "terms of use".'),
    '#type' => 'textarea',
    '#required' => FALSE,
    '#default_value' => empty($terms_of_use) ? '' : $terms_of_use,
  );

  $privacy = variable_get('terms_and_conditions_privacy');
  $form['terms_and_conditions']['privacy'] = array(
    '#title' => t('Description for field "privacy".'),
    '#type' => 'textarea',
    '#required' => FALSE,
    '#default_value' => empty($privacy) ? '' : $privacy,
  );

  $form['#submit'] = array('gp_user_admin_settings_form_submit');

  // Add a submit button to refresh schema.
  $form['actions']['refresh'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh schema'),
    '#submit' => array('gp_user_admin_settings_form_refresh'),
    '#weight' => 99,
  );

  return system_settings_form($form);
}

/**
 * Submit handler for gp_user_admin_settings_form.
 */
function gp_user_admin_settings_form_submit($form, &$form_state) {
  // Loop fields strings to prepare an array of strings.
  $data = array();
  foreach ($form_state['values']['im_fields'] as $name => $strings) {
    // Always save description as empty one is ok.
    $data[$name]['description'] = $strings['description'];
    // Use submitted label or the name if no label is submitted.
    $data[$name]['title'] = $strings['title'] ? $strings['title'] : $name;
  }

  // Save to variables.
  variable_set('gp_user_im_fields_strings', $data);

  // Save to variables.
  variable_set('terms_and_conditions_terms_of_use', $form_state['values']['terms_and_conditions']['terms_of_use']);
  variable_set('terms_and_conditions_privacy', $form_state['values']['terms_and_conditions']['privacy']);

  // Remove from form_state to prevent further save.
  unset($form_state['values']['im_fields']);
}

/**
 * Submit handler to trigger schema refresh.
 */
function gp_user_admin_settings_form_refresh($form, &$form_state) {
  if (gp_user_field_schema_refresh()) {
    drupal_set_message(t('Schema refreshed successfully.'));
  }
}
