<?php
/**
 * @file
 * Hooks provided by the gp_user module.
 */

/**
 * Act on identity manager data save.
 *
 * @param string $sso_uid
 *   The sso uid of the user.
 *
 * @param array $data
 *   The data sent to the im.
 */
function hook_im_user_save($sso_uid, $data) {
  // Update something.
}
