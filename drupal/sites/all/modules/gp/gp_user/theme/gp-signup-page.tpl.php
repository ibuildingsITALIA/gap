<?php
/**
 * @file
 * Template for signup page wrapper.
 */
?>
<div<?php print $attributes; ?>>
  <div class="signup-wrapper">
    <?php print $iframe; ?>
    <a class="change-pwd" href="/changepwd">Hai dimenticato la password?</a>
  </div>
  <div class="register-wrapper">
    <p class="register-title"><?php print t('Not a member yet?'); ?></p>
    <p class="register-descr"><?php print t('Come and join us to be part of the largest community of Italian teachers on the web!'); ?></p>
    <p class="register-action">
      <?php print $register; ?>
    </p>
  </div>
</div>
