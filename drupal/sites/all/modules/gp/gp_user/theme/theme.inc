<?php
/**
 * @file
 * Theme functions and template preprocess.
 */

/**
 * Render a html iframe tag.
 */
function theme_cas_iframe($variables) {
  $output = '<iframe id="signup-iframe" name="signup-iframe" src="' . $variables['url'];
  $output .= '" width="100%" height="400"></iframe>';
  return $output;
}

/**
 * Preprocess function for the wrapper around signup page contents.
 */
function template_preprocess_gp_signup_page(&$variables) {
  $element = $variables['element'];
  $variables['iframe'] = render($element['iframe']);
  // Register is not present for logged users.
  $variables['register'] = isset($element['register']) ? render($element['register']) : FALSE;

  // Add class based on logged status.
  $variables['classes_array'][] = user_is_logged_in() ? 'logged-user' : 'anonymous-user';
}
