<?php
/**
 * @file
 * Views related hooks.
 */

/**
 * Implements hook_views_data().
 */
function gp_promo_views_data() {
  $data = array();

  // If date_views module is enabled, use its filter implementation
  // for date fields.
  $date_filter_handler = module_exists('date_views') ? 'date_views_filter_handler_simple' : 'views_handler_filter_date';

  $data['gp_promo_code']['table']['group'] = t('Promo code');

  // Advertise this table as possible base table.
  $data['gp_promo_code']['table']['base'] = array(
    'field' => 'tid',
    'title' => t('Giunti promotion codes'),
  );

  // Expose cid field.
  $data['gp_promo_code']['cid'] = array(
    'title' => t('ID'),
    'help' => t('The promo code ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose code field.
  $data['gp_promo_code']['code'] = array(
    'title' => t('Code'),
    'help' => t('The code string.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose promo field.
  $data['gp_promo_code']['promo'] = array(
    'title' => t('Promo identifier'),
    'help' => t('The identifier of the promo the code belongs to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'gp_promo_get_all',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose value field.
  $data['gp_promo_code']['value'] = array(
    'title' => t('Value'),
    'help' => t('The money value of the code.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose uid field as user.
  $data['gp_promo_code']['user'] = array(
    'title' => t('Owner'),
    'help' => t('The user the code belongs to.'),
    'real field' => 'uid',
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'title' => t('Owner'),
      'help' => t('Relate promo code with the owner.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'relationship field' => 'uid',
      'label' => t('Owner'),
    ),
  );

  // Expose created field.
  $data['gp_promo_code']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the promo code was created in the system.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  // Expose changed field.
  $data['gp_promo_code']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the promo code was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  return $data;
}
