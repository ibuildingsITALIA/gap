<?php
/**
 * @file
 * Rules integration for the gp_promo module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function gp_promo_rules_condition_info() {
  $conditions = array();

  $conditions['gp_promo_user_has_promo'] = array(
    'label' => t('User has a promo code associated'),
    'parameter' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'promo' => array(
        'type' => 'text',
        'label' => t('Promo name'),
        'options list' => 'gp_promo_get_all',
        'restriction' => 'input',
      ),
    ),
    'group' => t('Giunti al Punto promo'),
    'callbacks' => array(
      'execute' => 'gp_promo_rules_user_has_promo',
    ),
  );

  $conditions['gp_promo_promo_has_codes_left'] = array(
    'label' => t('Promo has codes left'),
    'parameter' => array(
      'promo' => array(
        'type' => 'text',
        'label' => t('Promo name'),
        'options list' => 'gp_promo_get_all',
        'restriction' => 'input',
      ),
    ),
    'group' => t('Giunti al Punto promo'),
    'callbacks' => array(
      'execute' => 'gp_promo_rules_promo_has_codes_left',
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function gp_promo_rules_action_info() {
  $actions = array();

  $actions['gp_promo_get_code'] = array(
    'label' => t('Retrieve a promo code'),
    'description' => t('Retrieve a promo code and binds it to an user.'),
    'parameter' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'promo' => array(
        'type' => 'text',
        'label' => t('Promo name'),
        'options list' => 'gp_promo_get_all',
        'restriction' => 'input',
      ),
    ),
    'provides' => array(
      'code' => array(
        'type' => 'text',
        'label' => t('The code retrieved'),
      ),
    ),
    'group' => t('Giunti al Punto promo'),
    'callbacks' => array(
      'execute' => 'gp_promo_rules_get_code',
    ),
  );

  return $actions;
}

/**
 * Rules condition: checks if an user has a code of a promo associated.
 */
function gp_promo_rules_user_has_promo($account, $promo) {
  return gp_promo_code_find_by_uid($promo, $account->uid);
}

/**
 * Rules condition: checks if a promo has codes left.
 */
function gp_promo_rules_promo_has_codes_left($promo) {
  return gp_promo_get_code($promo);
}

/**
 * Rules action: retrieve (and bind) a code to an user.
 */
function gp_promo_rules_get_code($account, $promo) {
  $code = gp_promo_bind_code($promo, $account->uid);

  return array('code' => $code->code);
}
