<?php
/**
 * @file
 * Drush commands for gp_promo.
 */

/**
 * Implements hook_drush_command().
 */
function gp_promo_drush_command() {
  $items = array();

  $items['promo-import'] = array(
    'description' => 'Import promo codes from a csv file.',
    'arguments' => array(
      'csv' => 'The file to import.',
    ),
  );

  $items['promo-resend'] = array(
    'description' => 'Resend the gift message.',
    'arguments' => array(
      'mid' => 'The message id.',
    ),
  );

  return $items;
}

/**
 * Drush callback: import a csv promo file.
 *
 * @param string $csv
 *   The path to the csv.
 */
function drush_gp_promo_promo_import($csv) {
  if (!file_exists($csv)) {
    drush_print('You must specify a full path to a valid csv file.');
    return;
  }

  $handle = fopen($csv, 'r');
  if (FALSE === $handle) {
    drush_print('You must specify a full path to a valid csv file.');
    return;
  }

  // Get the headers from the first line.
  $headers = fgetcsv($handle, 1024, ';');

  $transaction = db_transaction();

  try {
    $query = db_insert('gp_promo_code')->fields(array(
      'code',
      'promo',
      'value',
      'created',
      'changed',
    ));

    while (($row = fgetcsv($handle, 1024, ';')) !== FALSE) {
      $columns = array_combine($headers, $row);
      $query->values(array(
        'code' => $columns['code'],
        'promo' => $columns['promo'],
        'value' => intval($columns['value']),
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
      ));
    }

    $result = $query->execute();

    if (!$result) {
      throw new Exception('Failed importing codes.');
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drush_print('Error importing codes. Rolled back.');
    return;
  }

  drush_print('Codes imported with success.');
}

/**
 * Drush callback: retrieve code and resend an existing message.
 */
function drush_gp_promo_promo_resend($mid) {
  $message = entity_load_single('message', $mid);

  if (!$message) {
    drush_print('Message not found.');
    return;
  }

  $wrapper = entity_metadata_wrapper('message', $message);

  try {
    $uid = $wrapper->user->getIdentifier();

    if (!$uid) {
      drush_print('No user found;');
      return;
    }

    drush_print("User uid is {$uid}...");
    $code = gp_promo_bind_code('Z1M7CZ', $uid);

    if (FALSE === $code) {
      drush_print('Failed binding a code.');
      return;
    }

    drush_print("Promo code is {$code->code}, {$code->cid}");
    $wrapper->message_promo_code->set($code->code);
    $wrapper->save();

    $options = array(
      'save on fail' => FALSE,
      'save on success' => FALSE,
    );
    drush_print('Sending email...');
    message_notify_send_message($message, $options, 'email');
    drush_print('Email sent.');
  }
  catch (Exception $e) {
    drush_print($e->getMessage());
    drush_print('Exception while processing.');
  }
}
