<?php
/**
 * @file
 * Views related hooks.
 */

/**
 * Implements hook_views_data().
 */
function gp_data_views_data() {
  $data = array();

  // If date_views module is enabled, use its filter implementation
  // for date fields.
  $date_filter_handler = module_exists('date_views') ? 'date_views_filter_handler_simple' : 'views_handler_filter_date';

  $data['gp_data_amazon_txn']['table']['group'] = t('Amazon transaction');

  // Advertise this table as possible base table.
  $data['gp_data_amazon_txn']['table']['base'] = array(
    'field' => 'tid',
    'title' => t('Amazon transactions'),
  );

  // Expose tid field.
  $data['gp_data_amazon_txn']['tid'] = array(
    'title' => t('ID'),
    'help' => t('The transaction ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose subtag field.
  $data['gp_data_amazon_txn']['subtag'] = array(
    'title' => t('Subtag'),
    'help' => t('The subtag this transaction refers to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'title' => t('Cart'),
      'help' => t('Relate transaction with the cart it belongs.'),
      'handler' => 'views_handler_relationship',
      'base' => 'gp_amazon_cart',
      'base field' => 'subtag',
      'relationship field' => 'subtag',
      'label' => t('Cart'),
    ),
  );

  // Expose quantity field.
  $data['gp_data_amazon_txn']['quantity'] = array(
    'title' => t('Quantity'),
    'help' => t('The quantity of items of this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose price field.
  $data['gp_data_amazon_txn']['price'] = array(
    'title' => t('Price'),
    'help' => t('The price of a single item.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose total field.
  $data['gp_data_amazon_txn']['total'] = array(
    'title' => t('Total'),
    'help' => t('The total price of the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose earnings field.
  $data['gp_data_amazon_txn']['earnings'] = array(
    'title' => t('Earnings'),
    'help' => t('The amount earned with this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose refund field.
  $data['gp_data_amazon_txn']['refund'] = array(
    'title' => t('Refund'),
    'help' => t('Whether or not this transaction is a refund.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'refund-norefund' => array(t('Refund'), t('Not refund')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Refund'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose category field.
  $data['gp_data_amazon_txn']['category'] = array(
    'title' => t('Category'),
    'help' => t('The category of the item.'),
    'field' => array(
      'handler' => 'gp_data_handler_field_transaction_category',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'gp_data_earnings_categories',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose processed field.
  $data['gp_data_amazon_txn']['processed'] = array(
    'title' => t('Processed date'),
    'help' => t('The date this transaction was processed by Amazon.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  // Expose created field.
  $data['gp_data_amazon_txn']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date this transaction was inserted in the system.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  // Add a points earned field.
  // Expose created field.
  $data['gp_data_amazon_txn']['earned'] = array(
    'real field' => 'total',
    'title' => t('Points earned'),
    'help' => t('The points earned with this transaction.'),
    'field' => array(
      'handler' => 'gp_data_handler_field_points_earned',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Customer feeds table.
  $data['gp_data_customer_feed_entry']['table']['group'] = t('Amazon customer feed');

  // Advertise this table as possible base table.
  $data['gp_data_customer_feed_entry']['table']['base'] = array(
    'field' => 'eid',
    'title' => t('Amazon customer feed entries'),
  );

  // Expose eid field.
  $data['gp_data_customer_feed_entry']['eid'] = array(
    'title' => t('ID'),
    'help' => t('The entry ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose fid field.
  $data['gp_data_customer_feed_entry']['fid'] = array(
    'title' => t('File'),
    'help' => t('The file that created the entry.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose customer_id field.
  $data['gp_data_customer_feed_entry']['customer_id'] = array(
    'title' => t('Customer ID'),
    'help' => t('The encrypted customer identifier.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose name field.
  $data['gp_data_customer_feed_entry']['name'] = array(
    'title' => t('Clear name'),
    'help' => t('The customer clear name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose mail field.
  $data['gp_data_customer_feed_entry']['mail'] = array(
    'title' => t('Mail'),
    'help' => t('The customer mail.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose subtag field.
  $data['gp_data_customer_feed_entry']['subtag'] = array(
    'title' => t('Subtag'),
    'help' => t('The subtag this entry refers to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'title' => t('Cart'),
      'help' => t('Relate transaction with the cart it belongs.'),
      'handler' => 'views_handler_relationship',
      'base' => 'gp_amazon_cart',
      'base field' => 'subtag',
      'relationship field' => 'subtag',
      'label' => t('Cart'),
    ),
  );

  // Expose order_id field.
  $data['gp_data_customer_feed_entry']['order_id'] = array(
    'title' => t('Order ID'),
    'help' => t('The Amazon order ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose processed field.
  $data['gp_data_customer_feed_entry']['processed'] = array(
    'title' => t('Processed date'),
    'help' => t('The date this entry was processed by Amazon.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  // Expose created field.
  $data['gp_data_customer_feed_entry']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date this entry was inserted in the system.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  $data['gp_data_item']['table']['group'] = t('Amazon items transaction');

  // Advertise this table as possible base table.
  $data['gp_data_item']['table']['base'] = array(
    'field' => 'tid',
    'title' => t('Amazon items transactions'),
  );

  // Expose tid field.
  $data['gp_data_item']['tid'] = array(
    'title' => t('ID'),
    'help' => t('The item transaction ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose bid field.
  $data['gp_data_item']['bid'] = array(
    'title' => t('Book'),
    'help' => t('The related Book id.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose asin field.
  $data['gp_data_item']['asin'] = array(
    'title' => t('ASIN'),
    'help' => t('The ASIN this transaction refers to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose ean field.
  $data['gp_data_item']['ean'] = array(
    'title' => t('EAN'),
    'help' => t('The EAN of the item this transaction refers to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose title field.
  $data['gp_data_item']['title'] = array(
    'title' => t('Title'),
    'help' => t('The item Title this transaction refers to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose publisher field.
  $data['gp_data_item']['publisher'] = array(
    'title' => t('Publisher'),
    'help' => t('The Publisher of the item this transaction refers to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose authors field.
  $data['gp_data_item']['authors'] = array(
    'title' => t('Authors'),
    'help' => t('The Authors of the item this transaction refers to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose quantity field.
  $data['gp_data_item']['quantity'] = array(
    'title' => t('Quantity'),
    'help' => t('The quantity of items of this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose price field.
  $data['gp_data_item']['price'] = array(
    'title' => t('Price'),
    'help' => t('The price of a single item.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose rate field.
  $data['gp_data_item']['rate'] = array(
    'title' => t('Rate'),
    'help' => t('The earnings rate of the item.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose total field.
  $data['gp_data_item']['total'] = array(
    'title' => t('Total'),
    'help' => t('The total price of the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose retail price field.
  $data['gp_data_item']['retail_price'] = array(
    'title' => t('Retail price'),
    'help' => t('The retail price of the item in this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose earnings field.
  $data['gp_data_item']['earnings'] = array(
    'title' => t('Earnings'),
    'help' => t('The amount earned with this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose refund field.
  $data['gp_data_item']['refund'] = array(
    'title' => t('Refund'),
    'help' => t('Whether or not this transaction is a refund.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'refund-norefund' => array(t('Refund'), t('Not refund')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Refund'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose category field.
  $data['gp_data_item']['category'] = array(
    'title' => t('Category'),
    'help' => t('The category of the item.'),
    'field' => array(
      'handler' => 'gp_data_handler_field_transaction_category',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'gp_data_earnings_categories',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose seller field.
  $data['gp_data_item']['seller'] = array(
    'title' => t('Seller'),
    'help' => t('The seller in this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose binding field.
  $data['gp_data_item']['binding'] = array(
    'title' => t('Binding'),
    'help' => t('The binding of the item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose device type field.
  $data['gp_data_item']['device_type'] = array(
    'title' => t('DeviceType'),
    'help' => t('The DeviceType in this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose tag field.
  $data['gp_data_item']['tag'] = array(
    'title' => t('Tag'),
    'help' => t('The Tag in this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose date field.
  $data['gp_data_item']['date'] = array(
    'title' => t('Date'),
    'help' => t('The string field Date saved in this transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose processed field.
  $data['gp_data_item']['processed'] = array(
    'title' => t('Processed date'),
    'help' => t('The date this transaction was processed by Amazon.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  // Expose created field.
  $data['gp_data_item']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date this transaction was inserted in the system.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function gp_data_views_data_alter(&$data) {
  if (isset($data['gp_amazon_cart'])) {
    $data['gp_amazon_cart']['transaction'] = array(
      'relationship' => array(
        'title' => t('Transactions'),
        'help' => t('Relate cart with the transactions.'),
        'handler' => 'views_handler_relationship',
        'base' => 'gp_data_amazon_txn',
        'base field' => 'subtag',
        'relationship field' => 'subtag',
        'label' => t('Transactions'),
      ),
    );

    // Expose a new handler with the total items found in the transaction logs.
    $data['gp_amazon_cart']['txn_total_items'] = array(
      'title' => t('Total transactions items'),
      'help' => t('Total items found in the transaction logs.'),
      'real field' => 'subtag',
      'field' => array(
        'handler' => 'gp_data_handler_field_txn_total_items',
      ),
    );

    // Expose a new handler with the total items found in the transaction logs.
    $data['gp_amazon_cart']['txn_subtotal'] = array(
      'title' => t('Transactions subtotal'),
      'help' => t('Subtotal of the transaction entries for the cart.'),
      'real field' => 'subtag',
      'field' => array(
        'handler' => 'gp_data_handler_field_txn_subtotal',
      ),
    );
  }

  if (isset($data['gp_card_history'])) {
    $data['gp_card_history']['transaction'] = array(
      'relationship' => array(
        'title' => t('Transactions'),
        'help' => t('Relate history entry with the transactions.'),
        'handler' => 'views_handler_relationship',
        'base' => 'gp_data_amazon_txn',
        'base field' => 'subtag',
        'relationship field' => 'import_id',
        'label' => t('Transactions'),
      ),
    );
  }
}
