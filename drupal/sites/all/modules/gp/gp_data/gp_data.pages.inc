<?php
/**
 * @file
 * Page callbacks for data module.
 */

// Time in seconds between sequence import.
define('GP_DATA_SEQUENCE_INTERVAL', 6 * 60 * 60);

function gp_data_check_sequence() {
  variable_set('gp_data_sequence_items_txn_start', time());

  $sequence_items_txn_start = variable_get('gp_data_sequence_items_txn_start', 0);
  $sequence_items_txn_end = variable_get('gp_data_sequence_items_txn_start', 0);

//  print 'START: ' . date('Y-m-d h:i', $sequence_items_txn_start) . ' |';
//  print 'END: ' . date('Y-m-d h:i', $sequence_items_txn_end) . ' |';

  if ($sequence_items_txn_start > $sequence_items_txn_end) {
    // In progress.
    if (time() >= ($sequence_items_txn_start + GP_DATA_SEQUENCE_INTERVAL)) {
      print 'ERROR: import in progress but too long time from sequence start - ' . date('Y-m-d h:i', $sequence_items_txn_start);
      exit;
    }
    print 'WARNING - Import in progress started ' . date('Y-m-d h:i', $sequence_items_txn_start);
    exit;
  }
  else if (time() >= ($sequence_items_txn_start + (2*GP_DATA_SEQUENCE_INTERVAL+(1*60*60)))) {
    print 'ERROR: too long time from last sequence import start - ' . date('Y-m-d h:i', $sequence_items_txn_start);
    exit;
  }
  print 'OK';
  exit;
}

function gp_data_ftp_test_page() {
  $params = array(
    GP_API_FTP_URL => variable_get(GP_API_VAR_REPORT_FTP_URL, ''),
    GP_API_FTP_PORT => variable_get(GP_API_VAR_REPORT_FTP_PORT, ''),
    GP_API_FTP_USER => variable_get(GP_API_VAR_REPORT_FTP_USER, ''),
    GP_API_FTP_PASSWORD => variable_get(GP_API_VAR_REPORT_FTP_PASSWORD, ''),
    GP_API_FTP_MAX_LOGIN_ATTEMPT => variable_get(GP_API_VAR_REPORT_FTP_MAX_LOG_ATTEMPTS, ''),
    GP_API_FTP_CONNECTION_TIMEOUT => 15,
    'path' => variable_get(GP_API_VAR_REPORT_FTP_PATH, ''),
  );
  gp_data_export_content($params);

}