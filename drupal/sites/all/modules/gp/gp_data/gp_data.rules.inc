<?php
/**
 * @file
 * Rules integration for the gp_data module.
 */

/**
 * Implements hook_rules_event_info().
 */
function gp_data_rules_event_info() {
  return array(
    'gp_data_points_inserted' => array(
      'label' => t('Points has been inserted after earnings process'),
      'group' => t('Giunti al Punto data'),
      'variables' => array(
        'cart_entity' => array(
          'type' => 'cart',
          'label' => t('Cart entity'),
        ),
        'points' => array(
          'type' => 'integer',
          'label' => t('Points inserted with this transaction'),
        ),
        'total_points' => array(
          'type' => 'integer',
          'label' => t('Total points earned by transactions with the same subtag'),
        ),
        'total_earned' => array(
          'type' => 'decimal',
          'label' => t('Total amount earned with all the transactions of the same subtag'),
        ),
      ),
    ),
  );
}
