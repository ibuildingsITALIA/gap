<?php

/**
 * Implements hook_drush_help().
 */
function gp_data_drush_help($command) {
  switch ($command) {
    case 'drush:gpdata-export-report':
      return dt('Export items transactions report.');

  }
}

/**
 * Implements hook_drush_command().
 */
function gp_data_drush_command() {
  $items = array();
  $items['gpdata-export-report'] = array(
    'description' => dt('Export report'),
    'arguments' => array(
      'filename' => 'The name of the file to export.'
    ),
  );
  return $items;
}

/**
 * Callback function for drush to export report to ftp.
 */
function drush_gp_data_gpdata_export_report($filename = '') {
  $start_time = REQUEST_TIME;
  watchdog('[gpdata_export_report]', 'Start time: %time', array('%time' => $start_time), WATCHDOG_NOTICE);
  $path = variable_get(GP_API_VAR_REPORT_FTP_PATH, '') . $filename;

  $params = array(
    GP_API_FTP_URL => variable_get(GP_API_VAR_REPORT_FTP_URL, ''),
    GP_API_FTP_PORT => variable_get(GP_API_VAR_REPORT_FTP_PORT, ''),
    GP_API_FTP_USER => variable_get(GP_API_VAR_REPORT_FTP_USER, ''),
    GP_API_FTP_PASSWORD => variable_get(GP_API_VAR_REPORT_FTP_PASSWORD, ''),
    GP_API_FTP_MAX_LOGIN_ATTEMPT => variable_get(GP_API_VAR_REPORT_FTP_MAX_LOG_ATTEMPTS, ''),
    GP_API_FTP_CONNECTION_TIMEOUT => 15,
    'path' => $path,
  );
  gp_data_export_content($params);

  $end_time = REQUEST_TIME;
  watchdog('[gpdata_export_report]', 'End time: %time', array('%time' => $end_time), WATCHDOG_NOTICE);
}