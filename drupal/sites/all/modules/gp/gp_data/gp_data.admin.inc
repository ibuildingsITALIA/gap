<?php
/**
 * @file
 * Administration forms.
 */


/**
 * Sequence mass import form.
 */
function gp_data_sequence_mass_import_form($form, &$form_state) {
  // Filter all sequences that work with a date variable.
  $sequences = array();
  foreach (gp_data_data_sequences() as $name => $info) {
    if (!empty($info['fetcher']['settings']['options']['date_variable'])) {
      $sequences[$name] = $name;
    }
  }

  $form['sequence'] = array(
    '#type' => 'select',
    '#title' => t('Sequence to mass import'),
    '#options' => $sequences,
    '#required' => TRUE,
  );

  $form['since'] = array(
    '#type' => 'textfield',
    '#title' => t('Start date'),
    '#description' => t('Enter a date in the format YYYYMMDD'),
    '#required' => TRUE,
    '#default_value' => 20140511,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Start import'),
  );

  return $form;
}

/**
 * Sequence items transactions import form.
 */
function gp_data_sequence_items_txn_import_form($form, &$form_state) {
  // Filter all sequences that work with a date variable.
  $sequences = array();
  foreach (gp_data_data_sequences() as $name => $info) {
    if (!empty($info['fetcher']['settings']['options']['date_variable'])) {
      $sequences[$name] = $name;
    }
  }

  $form['xmlfile'] = array(
    '#title' => t('File'),
    '#type' => 'managed_file',
    '#description' => t('Please upload the new file!'),
    '#progress_indicator' => 'bar',
    '#upload_location' => 'private://items-txn/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('xml'),
    ),
  );

  $form['since'] = array(
    '#type' => 'textfield',
    '#title' => t('Date'),
    '#description' => t('Enter a date in the format YYYYMMDD'),
    '#required' => TRUE,
    '#default_value' => date('Ymd', time()),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Start import'),
  );

  return $form;
}

/**
 * Report export form.
 */
function gp_data_report_export_form() {
  $form[GP_API_VAR_REPORT_FTP] = array(
    '#type' => 'fieldset',
    '#title' => t('Report FTP settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form[GP_API_VAR_REPORT_FTP][GP_API_VAR_REPORT_FTP_URL] = array(
    '#type' => 'textfield',
    '#title' => t('Report ftp url'),
    '#description' => t('The uri for Report FTP resource.'),
    '#default_value' => variable_get(GP_API_VAR_REPORT_FTP_URL, ''),
    '#size' => 100,
    '#maxlenght' => 255,
  );

  $form[GP_API_VAR_REPORT_FTP][GP_API_VAR_REPORT_FTP_PORT] = array(
    '#type' => 'textfield',
    '#title' => t('Report ftp port'),
    '#description' => t('The port for Report FTP resource.'),
    '#default_value' => variable_get(GP_API_VAR_REPORT_FTP_PORT, '21'),
    '#size' => 100,
    '#maxlenght' => 255,
  );

  $form[GP_API_VAR_REPORT_FTP][GP_API_VAR_REPORT_FTP_USER] = array(
    '#type' => 'textfield',
    '#title' => t('Report ftp login username'),
    '#description' => t('The username for Report FTP resource.'),
    '#default_value' => variable_get(GP_API_VAR_REPORT_FTP_USER, ''),
    '#size' => 30,
    '#maxlenght' => 255,
  );

  $form[GP_API_VAR_REPORT_FTP][GP_API_VAR_REPORT_FTP_PASSWORD] = array(
    '#type' => 'textfield',
    '#title' => t('Report ftp login password'),
    '#description' => t('The password for Report FTP resource.'),
    '#default_value' => variable_get(GP_API_VAR_REPORT_FTP_PASSWORD, ''),
    '#size' => 30,
    '#maxlenght' => 255,
  );

  $form[GP_API_VAR_REPORT_FTP][GP_API_VAR_REPORT_FTP_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Report ftp file path'),
    '#description' => t('The path for Report FTP resource.'),
    '#default_value' => variable_get(GP_API_VAR_REPORT_FTP_PATH, ''),
    '#size' => 30,
    '#maxlenght' => 255,
  );

  $val = variable_get(GP_API_VAR_REPORT_FTP_MAX_LOG_ATTEMPTS, '5');
  $val = is_numeric($val) ? $val : 5;
  variable_set(GP_API_VAR_REPORT_FTP_MAX_LOG_ATTEMPTS, $val);
  $form[GP_API_VAR_REPORT_FTP][GP_API_VAR_REPORT_FTP_MAX_LOG_ATTEMPTS] = array(
    '#type' => 'textfield',
    '#title' => t('Report ftp max login attempts'),
    '#description' => t('The number of max login attempts.'),
    '#default_value' => $val,
    '#size' => 5,
    '#maxlenght' => 255,
  );

  return system_settings_form($form);
}

/**
 * Submit handler for sequence mass import form.
 */
function gp_data_sequence_mass_import_form_submit($form, &$form_state) {
  $sequence_name = $form_state['values']['sequence'];
  $since = strtotime($form_state['values']['since']);

  // Get the variable associated with this sequence.
  $sequences = gp_data_data_sequences();
  $date_variable = $sequences[$sequence_name]['fetcher']['settings']['options']['date_variable'];

  // Compute days.
  $day_seconds = 60 * 60 * 24;
  $datediff = REQUEST_TIME - $since;
  $days = floor($datediff / $day_seconds);

  $operations = array();
  for ($i = 0; $i < $days; $i++) {
    $timestamp = $since + ($i * $day_seconds);
    $date = date('Ymd', $timestamp);
    $operations[] = array(
      'gp_data_sequence_mass_import_batch_op',
      array(
        $sequence_name,
        $date_variable,
        $date,
      ),
    );
  }

  $batch = array(
    'title' => t('Mass import of sequence files'),
    'operations' => $operations,
    'finished' => 'gp_data_sequence_mass_import_batch_finished',
    'file' => drupal_get_path('module', 'gp_data') . '/gp_data.admin.inc',
  );

  batch_set($batch);
}

/**
 * Submit handler for sequence items transactions import form.
 */
function gp_data_sequence_items_txn_import_form_submit($form, &$form_state) {
  $sequence_name = 'items';
  $since = strtotime($form_state['values']['since']);

  if ($form['xmlfile']['#file']) {

    $filename = 'moodlestore-21-items-' . $form_state['values']['since'] . '.xml';
    $path = $form['xmlfile']['#upload_location'];
    $form['xmlfile']['#file']->filename = $filename;
    $file = file_move($form['xmlfile']['#file'], $path . $filename, 2);
    if (!$file) {
      $form_state['rebuild'] = TRUE;
      unset($form['xmlfile']['#file']);
      return;
    }

    // Update the status of the file to permanent.
    $file->status = FILE_STATUS_PERMANENT;
    // Save the updated file information to the database.
    file_save($file);
  }

  variable_set('gp_data_sequence_items_txn_start', time());
  watchdog('giunti_amazon', 'Start Import.', array(), WATCHDOG_NOTICE);

  // Get the variable associated with this sequence.
  $sequences = gp_data_data_sequences();
  $date_variable = $sequences[$sequence_name]['fetcher']['settings']['options']['date_variable'];

  // Compute days.
  $day_seconds = 60 * 60 * 24;
  $datediff = REQUEST_TIME - $since;
  $days = floor($datediff / $day_seconds);

  $operations = array();
  for ($i = 0; $i < $days; $i++) {
    $timestamp = $since + ($i * $day_seconds);
    $date = date('Ymd', $timestamp);
    $operations[] = array(
      'gp_data_sequence_mass_import_batch_op',
      array(
        $sequence_name,
        $date_variable,
        $date,
      ),
    );
  }

  $batch = array(
    'title' => t('Mass import of sequence files'),
    'operations' => $operations,
    'finished' => 'gp_data_sequence_items_import_batch_finished',
    'file' => drupal_get_path('module', 'gp_data') . '/gp_data.admin.inc',
  );

  batch_set($batch);
}

/*function gp_data_report_export_form_submit($form, &$form_state) {
  // Save to variables.
  variable_set('GP_API_VAR_REPORT_FTP_URL', $form_state['values'][GP_API_VAR_REPORT_FTP_URL]);
  variable_set('GP_API_VAR_REPORT_FTP_PORT', $form_state['values'][GP_API_VAR_REPORT_FTP_PORT]);
  variable_set('GP_API_VAR_REPORT_FTP_USER', $form_state['values'][GP_API_VAR_REPORT_FTP_USER]);
  variable_set('GP_API_VAR_REPORT_FTP_PASSWORD', $form_state['values'][GP_API_VAR_REPORT_FTP_PASSWORD]);
  variable_set('GP_API_VAR_REPORT_FTP_MAX_LOG_ATTEMPTS', $form_state['values'][GP_API_VAR_REPORT_FTP_MAX_LOG_ATTEMPTS]);
  variable_set('GP_API_VAR_REPORT_FTP_PATH', $form_state['values'][GP_API_VAR_REPORT_FTP_PATH]);
}*/

/**
 * Earnings mass import batch operation.
 */
function gp_data_sequence_mass_import_batch_op($sequence_name, $data_variable, $date, &$context) {
  variable_set($data_variable, $date);

  // Get sequence needed.
  $sequences = gp_data_data_sequences();

  // Execute single earning sequence.
  watchdog('gp_data', 'Executing @name.', array(
    '@name' => $sequence_name,
  ), WATCHDOG_NOTICE);

  gp_data_execute_sequence($sequence_name, $sequences[$sequence_name]);
}

/**
 * Earnings mass import batch finished callback.
 */
function gp_data_sequence_mass_import_batch_finished($success, $results, $operations) {
  drupal_set_message(t('Mass import finished.'));
}

/**
 * Earnings mass import batch finished callback.
 */
function gp_data_sequence_items_import_batch_finished($success, $results, $operations) {
  variable_set('gp_data_sequence_items_txn_end', time());
  drupal_set_message(t('Items import finished.'));
}
