<?php
/**
 * @file
 * Definition of gp_data_handler_field_transaction_category.
 */

/**
 * Field handler for transaction category.
 */
class gp_data_handler_field_transaction_category extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);
    $categories = gp_data_earnings_categories();
    return isset($categories[$value]) ? $categories[$value] : t('Unknown');
  }
}
