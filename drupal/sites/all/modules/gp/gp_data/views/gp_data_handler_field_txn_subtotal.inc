<?php
/**
 * @file
 * Definition of gp_data_handler_field_txn_subtotal.
 */

class gp_data_handler_field_txn_subtotal extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);

    $result = db_query('SELECT total, refund FROM {gp_data_amazon_txn} WHERE subtag = :subtag', array(
      ':subtag' => $value,
    ));
    $subtotal = 0;
    foreach ($result as $record) {
      $subtotal += $record->refund ? -$record->total : $record->total;
    }

    return gp_amazon_currency_format($subtotal * 100);
  }
}
