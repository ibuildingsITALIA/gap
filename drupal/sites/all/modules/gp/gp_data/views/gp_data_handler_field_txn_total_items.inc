<?php
/**
 * @file
 * Definition of gp_data_handler_field_txn_total_items.
 */

class gp_data_handler_field_txn_total_items extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);

    $result = db_query('SELECT quantity, refund FROM {gp_data_amazon_txn} WHERE subtag = :subtag', array(
      ':subtag' => $value,
    ));
    $count = 0;
    foreach ($result as $record) {
      $count += $record->refund ? -$record->quantity : $record->quantity;
    }

    return $count;
  }
}
