<?php
/**
 * @file
 * Definition of gp_data_handler_field_points_earned.
 *
 * This handler calculate points for *everything* you pass on it.
 * It doesn't handle any item category.
 */

class gp_data_handler_field_points_earned extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);
    return gp_card_calculate_points($value * 100);
  }
}
