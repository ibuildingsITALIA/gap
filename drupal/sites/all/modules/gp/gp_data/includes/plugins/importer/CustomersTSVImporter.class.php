<?php
/**
 * @file
 * Handler for customers TSV importer plugin.
 */

class CustomersTSVImporter extends DataCSVImporter {
  /**
   * Handle the import of data from the source.
   */
  public function execute() {
    // If source is already imported, skip.
    if ($this->sourceIsAlreadyImported()) {
      return TRUE;
    }

    if (!$this->openFile()) {
      // Failed opening file.
      return FALSE;
    }

    // Start db transaction as we do an out out insert.
    // If any error occurs, we quit everything so we can re-process later.
    $transaction = db_transaction();

    try {
      // Prepare query.
      $query = db_insert('gp_data_customer_feed_entry')
        ->fields(array(
          'fid',
          'customer_id',
          'name',
          'mail',
          'subtag',
          'order_id',
          'processed',
          'created',
        ));

      $date = DateTime::createFromFormat('Ymd', variable_get($this->options['date_variable'], 20140511));

      $last_subtag = '';

      while ($line = $this->nextLine()) {
        // Duplicate line, skip.
        if ($last_subtag == $line['subtag']) {
          continue;
        }
        // Not subtag, skip.
        if (strlen($line['subtag']) < 20) {
          continue;
        }
        $last_subtag = $line['subtag'];

        // Prepare values for insert.
        $values = $line + array(
          'fid' => $this->source->fid,
          'processed' => $date->getTimestamp(),
          'created' => REQUEST_TIME,
        );

        $query->values($values);
      }

      $query->execute();

      // Mark the file as used from this module.
      file_usage_add($this->source, 'gp_data', get_class($this), $date->format('Ymd'));

      // Increment date variable.
      $date->add(new DateInterval('P1D'));
      variable_set($this->options['date_variable'], $date->format('Ymd'));
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('gp_data', $e);
      $this->closeFile();
      return FALSE;
    }

    $this->closeFile();
    return TRUE;
  }

  /**
   * Check if a source is already imported.
   *
   * @return bool
   *   If the source is already imported.
   */
  protected function sourceIsAlreadyImported() {
    $usage = file_usage_list($this->source);
    return isset($usage['gp_data'][get_class($this)]);
  }
}
