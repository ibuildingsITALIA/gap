<?php
/**
 * @file
 * Handler for earnings XML importer plugin.
 */

class EarningsXmlImporter extends DataImporter {
  /**
   * Import data from the source.
   *
   * @return array|bool
   *   A boolean value or a list of transaction ids.
   */
  public function execute() {
    // If source is already imported, skip.
    if ($this->sourceIsAlreadyImported()) {
      return TRUE;
    }

    // If file doesn't exists or is not a file, quit.
    if (!is_file($this->source)) {
      return FALSE;
    }

    $xml = simplexml_load_file($this->source);
    // If loading failed, quit.
    if (!$xml) {
      return FALSE;
    }

    if (!isset($xml->Items) || !isset($xml->Items->Item)) {
      // Items are empty, this doesn't mean an error.
      return TRUE;
    }

    // Start db transaction as we do an out out insert.
    // If any error occurs, we quit everything so we can re-process later.
    $transaction = db_transaction();

    try {
      // Prepare query.
      $query = db_insert('gp_data_amazon_txn')
        ->fields(array(
          'subtag',
          'quantity',
          'price',
          'total',
          'earnings',
          'refund',
          'category',
          'processed',
          'created',
        ));

      foreach ($xml->Items->Item as $item) {
        // Prepare some values for insert.
        $category = (string) $item['Category'];
        $multiplier = $this->getVatMultiplier($category);
        $price = $multiplier * $this->stringToPrice((string) $item['Price']);
        $total = $multiplier * $this->stringToPrice((string) $item['Revenue']);
        $earnings = $multiplier * $this->stringToPrice((string) $item['Earnings']);

        $query->values(array(
          'subtag' => (string) $item['SubTag'],
          'quantity' => (int) $item['Qty'],
          'price' => $price,
          'total' => $total,
          'earnings' => abs($earnings),
          'refund' => $earnings < 0 ? 1 : 0,
          'category' => $category,
          'processed' => (string) $item['EDate'],
          'created' => REQUEST_TIME,
        ));
      }

      $query->execute();

      // Insert current file into database to prevent later processing.
      db_insert('gp_data_earnings_files')
        ->fields(array(
          'filename' => drupal_basename($this->source),
          'created' => REQUEST_TIME,
        ))
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('gp_data', $e);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Check if a source is already imported.
   *
   * @return bool
   *   If the source is already imported.
   */
  protected function sourceIsAlreadyImported() {
    $basename = drupal_basename($this->source);
    return db_query('SELECT filename FROM gp_data_earnings_files WHERE filename = :source', array(':source' => $basename))->fetchField();
  }

  /**
   * Helper function to convert a string into a float number.
   *
   * This is faster than Number class.
   *
   * @param string $string
   *   The string to convert.
   *
   * @return float
   *   The converted value.
   */
  protected function stringToPrice($string) {
    return floatval(str_replace(array('.', ','), array('', '.'), $string));
  }

  /**
   * Returns the multiplier to calculate Vat on transactions.
   *
   * @param string $category
   *   The category of the entry.
   *
   * @return float|int
   *   The multiplier.
   */
  protected function getVatMultiplier($category) {
    switch ($category) {
      case '351':
        $multiplier = 1.03;
        break;

      case '14':
        $multiplier = 1;
        break;

      default:
        $multiplier = 1.15;
    }

    return $multiplier;
  }
}
