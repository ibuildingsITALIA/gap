<?php
/**
 * @file
 * S3 Amazon data feed fetcher plugin.
 */

$plugin = array(
  'title' => 'S3 Amazon data feed fetcher',
  'handler' => 'S3FeedFetcher',
);
