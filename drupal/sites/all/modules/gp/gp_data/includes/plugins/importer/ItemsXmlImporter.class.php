<?php
/**
 * @file
 * Handler for earnings XML importer plugin.
 */

class ItemsXmlImporter extends DataImporter {
  /**
   * @var array
   *   Options for the plugin.
   */
  protected $options;

  /**
   * Import data from the source.
   *
   * @return array|bool
   *   A boolean value or a list of transaction ids.
   */
  public function execute() {
    // If source is already imported, skip.
    if ($this->sourceIsAlreadyImported()) {
      return TRUE;
    }

    // If file doesn't exists or is not a file, quit.
    if (!is_file($this->source->uri)) {
      return FALSE;
    }

    $xml = simplexml_load_file($this->source->uri);
    // If loading failed, quit.
    if (!$xml) {
      return FALSE;
    }

    if (!isset($xml->Items) || !isset($xml->Items->Item)) {
      // Items are empty, this doesn't mean an error.
      return TRUE;
    }

    // Start db transaction as we do an out out insert.
    // If any error occurs, we quit everything so we can re-process later.
    $transaction = db_transaction();

    try {
      $id = $this->source->timestamp;

      foreach ($xml->Items->Item as $item) {
        // Prepare query.
        $query = db_insert('gp_data_item')
          ->fields(array(
            'asin',
            'quantity',
            'price',
            'rate',
            'total',
            'earnings',
            'refund',
            'category',
            'title',
            'binding',
            'device_type',
            'seller',
            'link_type',
            'ean',
            'publisher',
            'authors',
            'retail_price',
            'tag',
            'date',
            'processed',
            'created',
          ));

        // Prepare some values for insert.
        $category = (string) $item['Category'];
        $multiplier = 1;
        $price = $multiplier * $this->stringToPrice((string) $item['Price']);
        $rate = $this->stringToPrice((string) $item['Rate']);
        $total = $multiplier * $this->stringToPrice((string) $item['Revenue']);
        $earnings = $multiplier * $this->stringToPrice((string) $item['Earnings']);

        $query->values(array(
          'asin' => strtoupper((string) $item['ASIN']),
          'quantity' => (int) $item['Qty'],
          'price' => $price,
          'rate' => $rate,
          'total' => $total,
          'earnings' => $earnings,
          'refund' => $earnings < 0 ? 1 : 0,
          'category' => $category,
          'title' => (string) $item['Title'],
          'binding' => (string) $item['Binding'],
          'device_type' => (string) $item['DeviceType'],
          'seller' => (string) $item['Seller'],
          'link_type' => (string) $item['LinkType'],
          'ean' => '',
          'publisher' => '',
          'authors' => '',
          'retail_price' => 0,
          'tag' => (string) $item['Tag'],
          'date' => (string) $item['Date'],
          'processed' => (string) $item['EDate'],
          'created' => REQUEST_TIME,
        ));

        $query->execute();
      }

      // Mark the file as used from this module.
      file_usage_add($this->source, 'gp_data', get_class($this), $id);
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('gp_data', $e);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Check if a source is already imported.
   *
   * @return bool
   *   If the source is already imported.
   */
  protected function sourceIsAlreadyImported() {
    $usage = file_usage_list($this->source);
    return isset($usage['gp_data'][get_class($this)]);
  }

  /**
   * Helper function to convert a string into a float number.
   *
   * This is faster than Number class.
   *
   * @param string $string
   *   The string to convert.
   *
   * @return float
   *   The converted value.
   */
  protected function stringToPrice($string) {
    return floatval(str_replace(array('.', ','), array('', '.'), $string));
  }

  /**
   * Returns the multiplier to calculate Vat on transactions.
   *
   * @param string $category
   *   The category of the entry.
   *
   * @return float|int
   *   The multiplier.
   */
  protected function getVatMultiplier($category) {
    switch ($category) {
      case '351':
        $multiplier = 1.03;
        break;

      case '14':
        $multiplier = 1;
        break;

      default:
        $multiplier = 1.15;
    }

    return $multiplier;
  }
}
