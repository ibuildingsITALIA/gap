<?php
/**
 * @file
 * Earnings xml importer plugin.
 */

$plugin = array(
  'title' => 'Earnings XML importer',
  'handler' => 'EarningsXmlImporter',
);
