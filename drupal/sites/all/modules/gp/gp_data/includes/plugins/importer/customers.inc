<?php
/**
 * @file
 * Customers tsv importer plugin.
 */

$plugin = array(
  'title' => 'Customers TSV importer',
  'handler' => 'CustomersTSVImporter',
);
