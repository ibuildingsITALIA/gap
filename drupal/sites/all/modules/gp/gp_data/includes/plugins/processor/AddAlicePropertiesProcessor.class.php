<?php
/**
 * @file
 * Handler for points processor plugin.
 */

class AddAlicePropertiesProcessor extends DataProcessor {
  /**
   * Class constructor.
   */
  public function __construct(array $options = array()) {
    parent::__construct($options);
  }

  /**
   * Process data.
   */
  public function execute() {
    // Get all books to process.
    $books = $this->getBooks();
    $count = count($books);

    watchdog('giunti_amazon', 'Start to add Alice fields. [%count books to process]', array(
      '%count' => $count,
    ), WATCHDOG_NOTICE);

    foreach ($books as $book) {
      $entity_id = $book->entity_id;
      $asin = $book->field_asin_value;
      $isbn13 = $book->field_isbn13_value;
      $publisher = empty($book->field_publisher_name) ? '' : $book->field_publisher_name;
      $retail_price = ((int) $book->field_list_price_value) / 100;
      $authors = empty($book->field_author_name) ? '' : $book->field_author_name;

      db_update('gp_data_item')
        ->fields(array(
          'created' => REQUEST_TIME,
          'bid' => $entity_id,
          'ean' => $isbn13,
          'publisher' => $publisher,
          'authors' => $authors,
          'retail_price' => $retail_price,
        ))
        ->condition('asin', $asin, 'LIKE')
        ->execute();
    }

    // Get all products to process.
    $products = $this->getProducts();
    $count = count($products);

    watchdog('giunti_amazon', 'Start to add extra fields via Amazon API. [%count products to process]', array(
      '%count' => $count,
    ), WATCHDOG_NOTICE);

    // Cache products to performe one API call to each ASIN value.
    $products_cached = array();
    foreach ($products as $product) {
      if (empty($products_cached[$product->asin])) {
        $item = amazon_item_load($product->asin);

        if (empty($item)) {
          watchdog('giunti_amazon', '[ASIN: %Asin] - No item found.', array(
            '%Asin' => $product->asin,
          ), WATCHDOG_NOTICE);
          continue;
        }

        $products_cached[$product->asin] = array();
        $products_cached[$product->asin]['asin'] = $item->getAttributes()['ASIN'];
        $products_cached[$product->asin]['entity_id'] = 0;

        if (!empty($item->getAttributes()['ItemAttributes']['EAN'])) {
          $products_cached[$product->asin]['isbn13'] = $item->getAttributes()['ItemAttributes']['EAN'];
        }
        elseif (!empty($item->getAttributes()['ItemAttributes']['EISBN'])) {
          $products_cached[$product->asin]['isbn13'] = $item->getAttributes()['ItemAttributes']['EISBN'];
        }
        elseif (!empty($item->getAttributes()['ItemAttributes']['ISBN'])) {
          $products_cached[$product->asin]['isbn13'] = $item->getAttributes()['ItemAttributes']['ISBN'];
        }
        else {
          $products_cached[$product->asin]['isbn13'] = '';
        }

        $products_cached[$product->asin]['publisher'] = empty($item->getAttributes()['ItemAttributes']['Publisher']) ? '' : $item->getAttributes()['ItemAttributes']['Publisher'];
        $products_cached[$product->asin]['retail_price'] = ((int) $item->getAttributes()['ItemAttributes']['ListPrice']['Amount']) / 100;

        if (empty($item->getAttributes()['ItemAttributes']['Author'])) {
          $products_cached[$product->asin]['authors'] = '';
        }
        elseif (is_array($item->getAttributes()['ItemAttributes']['Author'])) {
          $products_cached[$product->asin]['authors'] = '';
          $separator = '';
          foreach ($item->getAttributes()['ItemAttributes']['Author'] as $author) {
            $products_cached[$product->asin]['authors'] .= $separator . $author[0];
            $separator = ', ';
          }
        }
        else {
          $products_cached[$product->asin]['authors'] = $item->getAttributes()['ItemAttributes']['Author'];
        }
      }

      db_update('gp_data_item')
        ->fields(array(
        'created' => REQUEST_TIME,
        'bid' => $products_cached[$product->asin]['entity_id'],
        'ean' => $products_cached[$product->asin]['isbn13'],
        'publisher' => $products_cached[$product->asin]['publisher'],
        'authors' => $products_cached[$product->asin]['authors'],
        'retail_price' => $products_cached[$product->asin]['retail_price'],
        ))
        ->condition('asin', $products_cached[$product->asin]['asin'], 'LIKE')
        ->execute();

      watchdog('giunti_amazon', '[ASIN: %Asin] - processed.', array(
        '%Asin' => $products_cached[$product->asin]['asin'],
      ), WATCHDOG_NOTICE);
    }

    watchdog('giunti_amazon', 'Extra fields added.', array(), WATCHDOG_NOTICE);

    return TRUE;
  }

  /**
   * Retrieve the books of the transactions in Alice DataBase.
   */
  protected function getBooks() {
    $query = 'SELECT fa.entity_id, fa.field_asin_value, ib.field_isbn13_value,';
    $query .= ' p.field_publisher_tid, t.name AS field_publisher_name,';
    $query .= ' a.field_authors_tid, ta.name AS field_author_name,';
    $query .= '  lp.field_list_price_value';
    $query .= ' FROM field_data_field_asin fa';
    $query .= ' left join field_data_field_isbn13 ib';
    $query .= ' on ib.entity_type = :type and ib.entity_id = fa.entity_id';
    $query .= ' left join field_data_field_publisher p';
    $query .= ' on p.entity_type = :type and p.entity_id = fa.entity_id';
    $query .= ' left join taxonomy_term_data t';
    $query .= ' on t.tid = p.field_publisher_tid';
    $query .= ' left join field_data_field_authors a';
    $query .= ' on a.entity_type = :type and a.entity_id = fa.entity_id';
    $query .= ' left join taxonomy_term_data ta';
    $query .= ' on ta.tid = a.field_authors_tid';
    $query .= ' left join field_data_field_list_price lp';
    $query .= ' on lp.entity_type = :type and lp.entity_id = fa.entity_id';
    $query .= ' where UPPER(field_asin_value) IN ( SELECT UPPER(asin) from gp_data_item where ean = :ean_value )';

    $results = db_query($query, array(
      ':type' => 'node',
      ':ean_value' => '',
      ':authors_value' => '',
      ))->fetchAllAssoc('field_asin_value');;

    return $results;
  }

  /**
   * Retrieve the products of the transactions to process.
   */
  protected function getProducts() {
    $query = 'SELECT asin from gp_data_item where ean = :ean_value AND category IN (:categories)';

    $categories = $this->getAllowedCategories();
    $results = db_query($query, array(':categories' => $categories, ':ean_value' => ''))->fetchAllAssoc('asin');;

    return $results;
  }

  /**
   * List of valid categories to ... .
   *
   * @return array
   *   List of ids.
   */
  protected function getAllowedCategories() {
    return array(
      // "Libri" group.
      14 => 14,
      // "eBook" group.
      351 => 351,
    );
  }
}
