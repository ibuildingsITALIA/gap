<?php
/**
 * @file
 * Handler for stream wrapper fetcher plugin.
 */

/**
 * Class StreamWrapperFetcher
 *
 * This class handles files that are reachable with Drupal stream wrappers.
 */
class StreamWrapperFetcher extends DataFetcher {
  /**
   * @var string
   *   The name of the file to locate.
   */
  protected $fileName;

  /**
   * @var string
   *   The path where the file is located.
   */
  protected $path;

  /**
   * @var object
   *   The managed file entry.
   */
  protected $file;

  /**
   * @var string
   *   The date of the file to locate.
   */
  protected $date;

  /**
   * Override constructor to allow url building.
   */
  public function __construct(array $options = array()) {
    // Ensure all parameters to needed to generate the file name.
    $options += array(
      'prefix' => '',
      'suffix' => '',
    );

    parent::__construct($options);

    // Get date of file to fetch from the variable, if possible.
    $this->date = variable_get($this->options['date_variable'], 20140511);

    // Prepare file names and paths.
    $this->fileName = $this->options['prefix'] . $this->date . $this->options['suffix'] . '.' . $this->options['extension'];
    $this->path = $this->options['path'] . $this->fileName;
  }

  /**
   * Fetch data from the source set.
   */
  public function execute() {
    // Check existence of file.
    if (!file_exists($this->path)) {
      // Failed retrieving file.
      return FALSE;
    }

    if (!$this->saveManaged()) {
      // We failed saving the file into Drupal.
      return FALSE;
    }

    $this->provides['source'] = $this->path;
    $this->provides['file'] = $this->file;
  }

  /**
   * Saves the file to the managed table.
   *
   * @return bool
   *   If the operation was successful.
   */
  protected function saveManaged() {
    global $user;

    // Check if the file has been already saved into the system.
    if ($fid = $this->fileExists()) {
      $this->file = file_load($fid);
      return TRUE;
    }

    // @see file_save_data()
    $file = new stdClass();
    $file->fid = NULL;
    $file->uri = $this->path;
    $file->filename = drupal_basename($file->uri);
    $file->filemime = file_get_mimetype($file->uri);
    $file->uid      = $user->uid;
    $file->status   = FILE_STATUS_PERMANENT;

    return ((bool) $this->file = file_save($file));
  }

  /**
   * Check the existence of a file into the managed table.
   *
   * @return mixed
   *   FALSE if not found, the file id otherwise.
   */
  protected function fileExists() {
    // We use db_query instead of EntityFieldQuery because for simple operations
    // is far more performing.
    // Also the file_load_multiple condition parameter is deprecated, so this
    // is the best approach.
    return db_query('SELECT fid FROM {file_managed} WHERE uri = :uri', array(':uri' => $this->path))->fetchField();
  }
}
