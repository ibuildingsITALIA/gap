<?php
/**
 * @file
 * Handler for S3 Amazon data feed fetcher plugin.
 */

class S3FeedFetcher extends DataFetcher {
  /**
   * @var string
   *   The name of the file to fetch.
   */
  protected $fileName;

  /**
   * @var string
   *   The local path where the file will be saved.
   */
  protected $localPath;

  /**
   * @var string
   *   The remote path where to find the file.
   */
  protected $remotePath;

  /**
   * @var string
   *   The date of the file to fetch.
   */
  protected $date;

  /**
   * Override constructor to allow url building.
   */
  public function __construct(array $options = array()) {
    parent::__construct($options);

    // Prepare directory path.
    file_prepare_directory($this->options['save_path'], FILE_CREATE_DIRECTORY);

    // Get date of file to fetch.
    $this->date = variable_get($this->options['date_variable'], 20140511);

    // Prepare file names and paths.
    $this->fileName = $this->options['prefix'] . $this->date . '.' . $this->options['extension'];
    $this->localPath = $this->options['save_path'] . $this->fileName;

    $this->remotePath = $this->options['url'] . $this->fileName;
    if ($this->options['gzipped']) {
      $this->remotePath .= '.gz';
    }
  }

  /**
   * Fetch data from the source set.
   */
  public function execute() {
    // Check existence of file.
    if (!file_exists($this->localPath)) {
      if (!$this->fetchFile()) {
        // Failed retrieving file.
        return FALSE;
      }
    }

    // Increment date variable.
    variable_set($this->options['date_variable'], date('Ymd', strtotime($this->date . ' + 1 day')));

    // Provide file to next plugin.
    $this->provides['source'] = $this->localPath;

    return TRUE;
  }

  /**
   * Fetch file from S3 servers handling digest authentication.
   *
   * @return bool|string
   *   FALSE on error, filename otherwise.
   */
  protected function fetchFile() {
    $curl_options = array(
      CURLOPT_URL => $this->remotePath,
      CURLOPT_HEADER => FALSE,
      CURLOPT_VERBOSE => FALSE,
      CURLOPT_FOLLOWLOCATION => TRUE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_SSL_VERIFYPEER => FALSE,
      CURLOPT_SSL_VERIFYHOST => FALSE,
      CURLOPT_HTTPAUTH => CURLAUTH_DIGEST,
      CURLOPT_USERPWD => $this->options['user_pwd'],
      CURLOPT_TIMEOUT => 10,
    );
    $ch = curl_init();
    curl_setopt_array($ch, $curl_options);
    $contents = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if (200 !== $status || empty($contents)) {
      // An error has occurred on file retrieval.
      return FALSE;
    }

    // Check if we have zlib.
    if (!function_exists('zlib_decode')) {
      return FALSE;
    }

    // Try to decompress.
    $contents = zlib_decode($contents);
    if (empty($contents)) {
      return FALSE;
    }

    // Save data.
    return file_unmanaged_save_data($contents, $this->localPath, FILE_EXISTS_REPLACE);
  }
}
