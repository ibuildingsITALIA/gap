<?php
/**
 * @file
 * Handler for points processor plugin.
 */

class PointsProcessor extends DataProcessor {
  /**
   * @var string
   *   The last time we checked and set points.
   */
  protected $lastCheck;

  /**
   * Class constructor.
   */
  public function __construct(array $options = array()) {
    parent::__construct($options);

    // Set last check time.
    $this->lastCheck = variable_get('gp_data_points_last_check', 0);
  }

  /**
   * Process data.
   */
  public function execute() {
    // Get subtag of all transactions inserted since last time.
    $subtags = $this->getLastValidSubtags();

    // If there are no subtags, quit but with success.
    if (empty($subtags)) {
      return TRUE;
    }

    // Get all transactions with the subtags involved.
    $transactions = $this->getTransactionsBySubtags(array_keys($subtags));

    // Group by subtags.
    $groups = array();
    foreach ($transactions as $row) {
      $groups[$row->subtag][] = $row;
    }

    // Calculate current points inserted for each subtag group.
    foreach ($groups as $subtag => $items) {
      // Get user uid.
      $uid = $subtags[$subtag]->uid;

      // And the cart id.
      $cid = $subtags[$subtag]->cid;

      // Get points currently assigned.
      $current_points = $this->getCurrentAssignedPoints($uid, $subtag);

      // Calculate total for this subtag.
      $total = 0;
      foreach ($items as $item) {
        // Add total or remove it if it's a refund.
        $total += ($item->refund ? (-$item->total) : $item->total);
      }

      // Multiply total as points calculation acts on numbers without decimals.
      $points = gp_card_calculate_points($total * 100);
      $difference = $points - $current_points;

      if ($difference != 0) {
        // If there's a difference in the points, insert the difference.
        gp_card_points_insert_by_user_uid($difference, $uid, $subtag);

        // Load also the cart entity.
        $cart_entity = entity_load_single('cart', $cid);

        // Allow modules to act on point updates.
        module_invoke_all('gp_data_points_inserted', $cart_entity, $difference, $points, $total);
        if (module_exists('rules')) {
          rules_invoke_event('gp_data_points_inserted', $cart_entity, $difference, $points, $total);
        }
      }
    }

    variable_set('gp_data_points_last_check', REQUEST_TIME);

    return TRUE;
  }

  /**
   * Retrieve the subtags of the transactions inserted since last time.
   *
   * Only subtags present in the gp_amazon_cart table, of
   * non-anonymous users and in the right categories are retrieved.
   */
  protected function getLastValidSubtags() {
    $query = db_select('gp_data_amazon_txn', 't');
    $query->join('gp_amazon_cart', 'c', 't.subtag = c.subtag');
    $query
      ->fields('t', array('subtag'))
      ->fields('c', array('uid', 'cid'))
      ->condition('c.uid', 0, '>')
      ->condition('t.subtag', '', '!=')
      ->condition('t.created', $this->lastCheck, '>')
      ->condition('t.category', $this->getAllowedCategories(), 'IN');
    $results = $query->execute()->fetchAllAssoc('subtag');

    return $results;
  }

  /**
   * Retrieve all the transactions with the subtags provided.
   */
  protected function getTransactionsBySubtags(array $subtags) {
    $results = db_query('SELECT * FROM {gp_data_amazon_txn} WHERE subtag IN (:subtags) AND category IN (:categories)', array(
      ':subtags' => $subtags,
      ':categories' => $this->getAllowedCategories(),
    ))->fetchAllAssoc('tid');

    return $results;
  }

  /**
   * Retrieve the current assegned points from the history.
   */
  protected function getCurrentAssignedPoints($uid, $subtag) {
    $query = db_select('gp_card_history', 'h');
    $query->join('gp_card', 'c');
    $query
      ->condition('c.uid', $uid)
      ->condition('h.import_id', $subtag);
    $query->addExpression('SUM(h.points)', 'total_points');
    $result = $query->execute()->fetchField();

    return is_null($result) ? 0 : $result;
  }

  /**
   * List of valid categories to earn points.
   *
   * @return array
   *   List of ids.
   */
  protected function getAllowedCategories() {
    return array(
      // "Libri" group.
      14 => 14,
      // "eBook Kindle" group.
      351 => 351,
    );
  }
}
