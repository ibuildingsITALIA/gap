<?php
/**
 * @file
 * Base class for fetchers.
 */

abstract class DataFetcher extends DataPlugin {
  /**
   * @var array
   *   Options for the plugin.
   */
  protected $options;

  /**
   * Class constructor.
   *
   * It should be used to set from where to fetch data.
   */
  public function __construct(array $options = array()) {
    $this->options = $options;
  }

  /**
   * Fetch data from the source set.
   */
  abstract public function execute();
}
