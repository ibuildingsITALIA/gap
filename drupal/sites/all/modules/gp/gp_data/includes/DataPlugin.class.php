<?php
/**
 * @file
 * Base class for data plugins.
 */

abstract class DataPlugin {
  /**
   * @var array
   *   A list of provided variables.
   */
  protected $provides = array();

  /**
   * Returns the list of variables provided.
   *
   * @return array
   *   The variables provided by this plugin.
   */
  public function getProvides() {
    return $this->provides;
  }
}
