<?php
/**
 * @file
 * CSV import class.
 */

abstract class DataCSVImporter extends DataImporter {
  /**
   * @var array
   *   Options for the plugin.
   */
  protected $options;

  /**
   * @var mixed
   *   The handle of the opened file.
   */
  protected $handle;

  /**
   * @var bool|array
   *   The csv column names.
   */
  protected $columnNames = FALSE;

  /**
   * Class constructor.
   *
   * Set the source and the options.
   *
   * @param mixed $source
   *   Source of the import.
   *
   * @param array $options
   *   Additional options to configure.
   */
  public function __construct($source, $options) {
    $this->source = $source;
    $options += array(
      'header' => FALSE,
      'length' => 0,
      'delimiter' => ',',
      'enclosure' => '"',
      'escape' => '\\',
      'map' => array(),
    );
    $this->options = $options;
  }

  /**
   * Verify and open the csv file.
   *
   * @return bool
   *   If the operation was successful.
   */
  protected function openFile() {
    // If file doesn't exists or is not a file or can't be opened, quit.
    if (!is_file($this->source->uri) || !($handle = fopen($this->source->uri, 'r'))) {
      return FALSE;
    }

    $this->handle = $handle;

    // Extract the headers if available.
    if ($this->options['header'] && ($headers = $this->nextLine())) {
      $this->columnNames = $headers;
    }

    // Replace the csv header names with custom ones.
    if (!empty($this->columnNames) && !empty($this->options['map'])) {
      foreach ($this->options['map'] as $old => $new) {
        $index = array_search($old, $this->columnNames);
        if (FALSE !== $index) {
          $this->columnNames[$index] = $new;
        }
      }
    }

    return TRUE;
  }

  /**
   * Retrieve the next line from the opened handle.
   */
  protected function nextLine() {
    $line = fgetcsv($this->handle, $this->options['length'], $this->options['delimiter'], $this->options['enclosure'], $this->options['escape']);

    if (FALSE === $line) {
      // EOF reached.
      return FALSE;
    }

    // Use column names as keys if possible.
    return $this->columnNames ? array_combine($this->columnNames, $line) : $line;
  }

  /**
   * Close the opened handle.
   */
  protected function closeFile() {
    fclose($this->handle);
  }
}
