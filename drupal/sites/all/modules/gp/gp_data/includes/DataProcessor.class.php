<?php
/**
 * @file
 * Base class for processors.
 */

abstract class DataProcessor extends DataPlugin {
  /**
   * @var array
   *   Options for the plugin.
   */
  protected $options;

  /**
   * Class constructor.
   *
   * Set options.
   */
  public function __construct(array $options = array()) {
    $this->options = $options;
  }

  /**
   * Process data.
   */
  abstract public function execute();
}
