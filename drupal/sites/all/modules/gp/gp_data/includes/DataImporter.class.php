<?php
/**
 * @file
 * Base class for importers.
 */

abstract class DataImporter extends DataPlugin {
  /**
   * @var mixed
   *   The source of the import.
   *
   * Can be an XML object, a path, everything.
   */
  protected $source;

  /**
   * Class constructor.
   *
   * Set the source.
   *
   * @param mixed $source
   *   Source of the import.
   */
  public function __construct($source) {
    $this->source = $source;
  }

  /**
   * Handle the import of data from the source.
   */
  abstract public function execute();
}
