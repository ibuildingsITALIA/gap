<?php
/**
 * @file
 * Hooks provided by the gp_data module.
 */

/**
 * Act on a points insertion after earnings process.
 *
 * @param CartEntity $cart_entity
 *   The cart associated with the event.
 *
 * @param int $difference
 *   The difference from previous imports.
 *
 * @param int $points
 *   The total points earned by transactions with the same subtag.
 *
 * @param float $total
 *   Total amount earned by transactions with the same subtag.
 */
function hook_gp_data_points_inserted($cart_entity, $difference, $points, $total) {
  // Send mail.
}
