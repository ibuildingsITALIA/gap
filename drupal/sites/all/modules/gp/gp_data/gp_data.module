<?php
/**
 * @file
 * Code for import data module.
 */

// Max amount needed for one point.
define('GP_DATA_POINTS_DIVIDER', 3);

/**
 * Implements hook_permission().
 */
function gp_data_permission() {
  return array(
    'administer transactions' => array(
      'title' => t('Administer transactions'),
      'description' => t('View and administer import transactions.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function gp_data_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'gp_data') {
    return "includes/plugins/$plugin_type";
  }
}

/**
 * Implements hook_ctools_plugin_type().
 */
function gp_data_ctools_plugin_type() {
  $plugins = array();

  // Fetcher plugins retrieve data from a source.
  $plugins['fetcher'] = array(
    'use hooks' => FALSE,
    'classes' => array('handler'),
  );

  // Importer plugins read data fetched and import them somewhere.
  $plugins['importer'] = array(
    'use hooks' => FALSE,
    'classes' => array('handler'),
  );

  // Processor plugins process data imported.
  $plugins['processor'] = array(
    'use hooks' => FALSE,
    'classes' => array('handler'),
  );

  return $plugins;
}

/**
 * Implements hook_views_api().
 */
function gp_data_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_cron().
 */
function gp_data_cron() {
  gp_data_execute_all_sequences();
}

/**
 * Implements hook_menu().
 */
function gp_data_menu() {
  $items = array();

  $items['admin/config/services/sequence_mass_import'] = array(
    'title' => 'Sequence mass import',
    'description' => 'Mass import of sequences that work with a date.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gp_data_sequence_mass_import_form'),
    'access arguments' => array('administer transactions'),
    'file' => 'gp_data.admin.inc',
  );

  $items['admin/config/services/sequence_items_txn_import'] = array(
    'title' => 'Sequence items transactions import',
    'description' => 'Import of items transactions sequences.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gp_data_sequence_items_txn_import_form'),
    'access arguments' => array('administer transactions'),
    'file' => 'gp_data.admin.inc',
  );

  $items['admin/config/services/report_settings'] = array(
    'title' => 'Report settings',
    'description' => 'Configure report settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gp_data_report_export_form'),
    'access arguments' => array('administer transactions'),
    'file' => 'gp_data.admin.inc',
  );

  $items['giunti-data-sequence'] = array(
    'title' => 'Check sequence status',
    'page callback' => 'gp_data_check_sequence',
    'access arguments' => array('access content'),
    'file' => 'gp_data.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_data_sequences().
 */
function gp_data_data_sequences() {
  $sequences = array();

  $sequences['earnings'] = array(
    'fetcher' => array(
      'handler' => 's3amazon',
      'requires' => array('options'),
      'settings' => array(
        'options' => array(
          'url' => 'https://assoc-datafeeds-eu.amazon.com/datafeed/getReport?filename=',
          'user_pwd' => 'moodlestore-21:moogiu1206',
          'prefix' => 'moodlestore-21-earnings-report-',
          'extension' => 'xml',
          'gzipped' => TRUE,
          'save_path' => 'private://earnings/',
          'date_variable' => 'gp_data_s3feed_date',
        ),
      ),
    ),
    'importer' => array(
      'handler' => 'earnings',
      'requires' => array('source'),
    ),
    'processor' => array(
      'handler' => 'points',
    ),
  );

  $sequences['orders'] = array(
    'fetcher' => array(
      'handler' => 's3amazon',
      'requires' => array('options'),
      'settings' => array(
        'options' => array(
          'url' => 'https://assoc-datafeeds-eu.amazon.com/datafeed/getReport?filename=',
          'user_pwd' => 'moodlestore-21:moogiu1206',
          'prefix' => 'moodlestore-21-orders-report-',
          'extension' => 'xml',
          'gzipped' => TRUE,
          'save_path' => 'private://orders/',
          'date_variable' => 'gp_data_s3orders_date',
        ),
      ),
    ),
    'importer' => array(
      'handler' => 'orders',
      'requires' => array('source'),
    ),
  );

  $sequences['customers'] = array(
    'fetcher' => array(
      'handler' => 'streamwrapper',
      'requires' => array('options'),
      'settings' => array(
        'options' => array(
          'path' => 'private://customers/',
          'suffix' => '_feed.tsv_decripted',
          'extension' => 'txt',
          'date_variable' => 'gp_data_s3customers_date',
        ),
      ),
    ),
    'importer' => array(
      'handler' => 'customers',
      'requires' => array('file', 'options'),
      'settings' => array(
        'options' => array(
          'header' => TRUE,
          'delimiter' => "\t",
          'date_variable' => 'gp_data_s3customers_date',
          'map' => array(
            'encrypted_customer_id' => 'customer_id',
            'clear_name' => 'name',
            'clear_lower_email' => 'mail',
            'associate_subtag_number' => 'subtag',
            'Order Id' => 'order_id',
          ),
        ),
      ),
    ),
  );

  $sequences['items'] = array(
    'fetcher' => array(
      'handler' => 'streamwrapper',
      'requires' => array('options'),
      'settings' => array(
        'options' => array(
          'path' => 'private://items-txn/',
          'prefix' => 'moodlestore-21-items-',
          'extension' => 'xml',
          'date_variable' => 'gp_data_s3items_date',
        ),
      ),
    ),
    'importer' => array(
      'handler' => 'items',
      'requires' => array('file', 'options'),
      'settings' => array(
        'options' => array(
          'header' => TRUE,
          'date_variable' => 'gp_data_s3items_date',
        ),
      ),
    ),
    'processor' => array(
      'handler' => 'addaliceproperties',
    ),

  );

  return $sequences;
}

/**
 * Execute all imports configured.
 */
function gp_data_execute_all_sequences() {
  $sequences = module_invoke_all('data_sequences');

  foreach ($sequences as $name => $sequence) {
    gp_data_execute_sequence($name, $sequence);
  }
}

/**
 * Execute a single sequence chain.
 *
 * @param string $name
 *   The name of the sequence.
 *
 * @param array $info
 *   The sequence data.
 *
 * @return bool
 *   Success of the chain.
 */
function gp_data_execute_sequence($name, $info) {
  // A set of variables provided by plugins.
  $provided = array();

  foreach (array('fetcher', 'importer', 'processor') as $type) {
    if (isset($info[$type])) {
      $plugin_id = $info[$type]['handler'];

      $result = _gp_data_execute_plugin($type, $plugin_id, $info[$type], $provided);

      if (FALSE === $result) {
        // Something went wrong, interrupt chain.
        watchdog('gp_data', 'Execute for @name failed, type @type, plugin @plugin failed.', array(
          '@name' => $name,
          '@type' => $type,
          '@plugin' => $plugin_id,
        ), WATCHDOG_CRITICAL);
        return FALSE;
      }

      // If result is an array, merge it with provided variables.
      if (is_array($result)) {
        $provided = array_merge($provided, $result);
      }
    }
  }

  return TRUE;
}

/**
 * Execute a single plugin.
 *
 * @param string $type
 *   The type of the plugin.
 *
 * @param string $id
 *   The id of the plugin.
 *
 * @param array $info
 *   Info about the plugin.
 *
 * @param array $provided
 *   Array of provided variables from other plugins.
 *
 * @return array
 *   The variables provided by the plugin.
 */
function _gp_data_execute_plugin($type, $id, $info, $provided) {
  ctools_include('plugins');
  $class = ctools_plugin_load_class('gp_data', $type, $id, 'handler');

  if (empty($class)) {
    // Class not found, log and skip.
    watchdog('gp_data', 'Plugin class not found: type @type, plugin @plugin.', array(
      '@type' => $type,
      '@plugin' => $id,
    ), WATCHDOG_CRITICAL);
    return FALSE;
  }

  // Empty arguments array.
  $args = array();
  // Check if there are required variables.
  if (!empty($info['requires'])) {
    $requires = $info['requires'];

    // Copy provided variables as we going to insert custom ones.
    $variables = $provided;

    // Add any parameter passed by the plugin info.
    if (!empty($info['settings'])) {
      foreach ($requires as $name) {
        if (isset($info['settings'][$name])) {
          $variables[$name] = $info['settings'][$name];
        }
      }
    }

    // Check missing variables.
    if (array_diff($requires, array_keys($variables))) {
      // Missing variables, skip.
      watchdog('gp_data', 'Missing variables for plugin: type @type, plugin @plugin.', array(
        '@type' => $type,
        '@plugin' => $id,
      ), WATCHDOG_CRITICAL);
      return FALSE;
    }

    // Loop required variables to prepare an ordered array.
    foreach ($requires as $name) {
      $args[] = $variables[$name];
    }
  }

  // Instantiate the class with the required parameters.
  $reflection = new ReflectionClass($class);
  $handler = $reflection->newInstanceArgs($args);

  $result = $handler->execute();

  // Return FALSE on error or the eventually provided variables on success.
  return FALSE === $result ? FALSE : $handler->getProvides();
}

/**
 * Helper to get all the categories used in the earnings xml files.
 *
 * @todo Move somewhere else?
 *
 * @return array
 *   The array of categories.
 */
function gp_data_earnings_categories() {
  return array(
    21 => t('Giocattoli e Giochi'),
    63 => t('Videogiochi'),
    309 => t('Calzature'),
    241 => t('Orologi'),
    198 => t('Valigie'),
    193 => t('Abbigliamento'),
    200 => t("Sport e vita all'aperto"),
    366 => t("Software digitale"),
    351 => t('Kindle e-book'),
    74 => t('DVD'),
    27 => t('VHS'),
    147 => t('Informatica'),
    228 => t('Buono Regalo'),
    75 => t('Bambini'),
    194 => t('Bellezza'),
    14 => t('Books'),
    201 => t('Home'),
    263 => t('Auto e Moto'),
    267 => t('Strumenti musicali'),
    79 => t('Cucina'),
    121 => t('Cura della persona'),
    349 => t('Hardware Kindle'),
    0 => t('Altro'),
    318 => t('Amazon Instant Video'),
    23 => t('Elettronica e altro'),
    364 => t('Cura della persona'),
    65 => t('Software'),
    325 => t('Grocery'),
    197 => t('Gioielli'),
    350 => t('Periodici Kindle'),
    199 => t('Pet Supplies'),
    15 => t('Musica'),
    367 => t('Download giochi'),
    60 => t('Fai da te'),
    265 => t('Grandi elettrodomestici'),
    229 => t("Cancelleria e prodotti per l'ufficio"),
    86 => t('Giardino e giardinaggio'),
    340 => t('Download MP3'),
  );
}

function gp_data_import_content($params) {
  $start = microtime(TRUE);
  $path_info = pathinfo($params['path']);

  if (!GP_DBG_SKIP_FTP) {
    // Getting FTP resource.
    $ftp = ReportApi::connectToFTP($params[GP_API_FTP_URL],
      $params[GP_API_FTP_USER],
      $params[GP_API_FTP_PASSWORD],
      $params[GP_API_FTP_PORT],
      $params[GP_API_FTP_CONNECTION_TIMEOUT],
      $params[GP_API_FTP_MAX_LOGIN_ATTEMPT]
    );

    // Aborting if no FTP resource available.
    if ($ftp === FALSE) {

      watchdog(
        'gp_data_import_content',
        'failed to connect and login after %attempts attempts',
        array(
          '%attempts' => $params[GP_API_FTP_MAX_LOGIN_ATTEMPT],
        ),
        WATCHDOG_CRITICAL
      );
    }

    // Downloading content file, close ftp connection, decompress file and delete original archive.
    ReportApi::downloadFTPFile($ftp, $path_info['dirname'], $path_info['basename'], FALSE, TRUE, TRUE, time() . '_');


  }

  $end = sprintf("%.6f", round(microtime(TRUE) - $start, 6));

  watchdog(
    'gp_data_import_content',
    'Procedure import successfully completed in %seconds seconds.',
    array('%seconds' => $end),
    WATCHDOG_INFO
  );
}


function gp_data_export_content($params) {
  $start = microtime(TRUE);
  $path_info = pathinfo($params['path']);

  if (!GP_DBG_SKIP_FTP) {
    // Getting FTP resource.
    $ftp = ReportApi::connectToFTP($params[GP_API_FTP_URL],
      $params[GP_API_FTP_USER],
      $params[GP_API_FTP_PASSWORD],
      $params[GP_API_FTP_PORT],
      $params[GP_API_FTP_CONNECTION_TIMEOUT],
      $params[GP_API_FTP_MAX_LOGIN_ATTEMPT]
    );

    // Aborting if no FTP resource available.
    if ($ftp === FALSE) {

      watchdog(
        'gp_data_export_content',
        'failed to connect and login after %attempts attempts',
        array(
          '%attempts' => $params[GP_API_FTP_MAX_LOGIN_ATTEMPT],
        ),
        WATCHDOG_CRITICAL
      );
    }

    // Downloading content file, close ftp connection, decompress file and delete original archive.
    ReportApi::uploadFTPFile($ftp, $path_info['dirname'], $path_info['basename'], TRUE, FALSE, time() . '_');


  }

  $end = sprintf("%.6f", round(microtime(TRUE) - $start, 6));

  watchdog(
    'gp_data_export_content',
    'Procedure export successfully completed in %seconds seconds.',
    array('%seconds' => $end),
    WATCHDOG_INFO
  );
}
