<?php
/**
 * @file
 * Drush tasks.
 */
$amazon_import = FALSE;
include('includes/AmazonBookParser.php');
/**
 * Implements hook_drush_help().
 */
define ('alert_example', 'drush import-amazon-xml /abs_path/to/file');
define ('task_done', 'Task done.');
define ('ADD', 'ADD');
define ('REMOVE', 'REMOVE');
function gp_amazon_xml_drush_help($command) {
  switch ($command) {
    case 'drush:import-amazon-xml':
      return dt('Import xml file from amazon');

  }
}

/**
 * Implements hook_drush_command().
 */
function gp_amazon_xml_drush_command() {


  $items = array();
  $items['import-amazon-xml'] = array(
    'description' => dt('Import xml file from amazon'),
    'arguments' => array(
      'arg1' => dt('Mandatory file path'),
    ),
    'examples' => array(
      'Standard example' => alert_example
    ),
  );

  return $items;
}

/**
 * Callback function for drush in order to delete from DB.
 */
function drush_gp_amazon_xml_import_amazon_xml($arg1 = NULL) {
  global $amazon_import;

  // Cancella ogni record sul DB in base al codice isbn.

  if (!isset($arg1)) {
    drush_log('You must enter an absolute file path! exiting...', 'error');
    drush_log(alert_example, 'status');
    return;
  }

  if (!file_exists($arg1)) {
    drush_log('File not exists! exiting...', 'error');
    drush_log(alert_example, 'status');
    return;
  }

  drush_log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
  drush_log("+ DRUPAL: import-amazon-xml on $arg1\n", "status");

  $amazon_import = array(
    'field_current_price' => TRUE,
    'field_asin' => TRUE,
    'field_is_available' => TRUE,
    'field_amazon_rank' => TRUE,
  );
  $time_start = microtime(TRUE);


  $xml_handler = new AmazonBookParser();
  $parser = xml_parser_create("");

  xml_set_object($parser, $xml_handler);
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, FALSE);
  xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
  xml_set_element_handler($parser, "startElement", "endElement");
  xml_set_character_data_handler($parser, "cdata");

  $fp = fopen($arg1, 'r');
  while ($data = fread($fp, 4096)) {
    xml_parse($parser, $data, feof($fp));

    flush();
  }
  fclose($fp);

  // Split eans into small arrays,
  // the huge one causes the "2006 mysql server has gone away" error.
  drush_log(count($xml_handler->getBooks()));
  $chunks = array_chunk($xml_handler->getBooks(), 10000, TRUE);
  $chunk_count = 1;
  $i = 1;
  $total_chunks = count($chunks);
  foreach ($chunks as $single_ean_block) {
    print_r('Processing chunk nr. ' . $chunk_count . 'of ' . $total_chunks);
    $chunk_count++;
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'book')
      ->fieldCondition('field_isbn13', 'value', array_keys($single_ean_block), 'IN');
    $result = $query->execute();

    if (!isset($result['node'])) {
      continue;
    }
    $entities = entity_load('node', array_keys($result['node']));
    $total = count($entities);

    drush_log("+ DRUPAL:  I'm going to update $total books\n", "status");

    foreach ($entities as $entity) {
      $wrapper = entity_metadata_wrapper('node', $entity);
      $isbn = $wrapper->field_isbn13->value();

      $status = $single_ean_block[$isbn . '']['item_delta_status'];
      $price = $single_ean_block[$isbn . '']['price_1'] * 100;
      $title = $single_ean_block[$isbn . '']['item_name'];
      $asin = $single_ean_block[$isbn . '']['item_unique_id'];
      $rank = (intval($single_ean_block[$isbn . '']['item_salesrank']) > 0) ? intval($single_ean_block[$isbn . '']['item_salesrank']) : 2147483647;
      if (empty($status) || $status == '') {
        $status = ADD;
      }
      if (strcasecmp($status, ADD) == 0) {
        print_r("+ DRUPAL: $i    EAN: $isbn \tTITLE: $title with \tPRICE: $price \tRANK: $rank \tSTATUS: $status \n");
        $wrapper->field_current_price->set($price . '');
        $wrapper->field_asin->set($asin);
        $wrapper->field_is_available->set(1);
        $wrapper->title->set($title);
        $wrapper->field_amazon_rank->set($rank);
        $wrapper->save();
      }
      else {
        if (strcasecmp($status, REMOVE) == 0) {
          $wrapper->field_is_available->set(0);
          $wrapper->save();
          print_r("+ DRUPAL: $i   Removed $isbn \tTITLE: $title\n");
        }
      }
      $i++;
      $wrapper = NULL;
    }
    $entities = NULL;
    $query = NULL;
    $result = NULL;
    unset($entities);
    unset($query);
    unset($query);
  }


  $time_end = microtime(TRUE);
  $execution_time = ($time_end - $time_start);
  drush_log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
  drush_log("Finished import $arg1, \n Total Execution Time: $execution_time Secs ", 'status');
  drush_log("TIME: " . date('d-m-Y H:i'), 'status');
  drush_log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 'status');
  $amazon_import = TRUE;
}

