<?php

/**
 * Created by PhpStorm.
 * User: max2thousand
 * Date: 31/10/14
 * Time: 12:18
 */
class AmazonBookParser {

  private $book;
  private $book_elem;
  private $books;
  /**
   * Invoked every time open tag is encountered in the stream.
   *
   * $tag contains the name of the tag
   * $attributes is a key-value array of tag attributes.
   */
  function startElement($parser, $tag, $attributes) {
    switch ($tag) {
      case 'item_data':
        $this->book = array(
          'price_1' => '',
          'item_name' => '',
          'item_ean' => '',
          'item_unique_id' => '',
          'item_delta_status' => '',
          'item_salesrank' => '',
        );
        break;

      case 'price_1':
      case 'item_name':
      case 'item_ean':
      case 'item_unique_id':
      case 'item_delta_status':
      case 'item_salesrank':
        $this->book_elem = $tag;
        break;
    }
  }

  /**
   * Invoked on each closing tag.
   */
  function endElement($parser, $tag) {
    switch ($tag) {
      case 'item_data':
        if ($this->book) {
          $this->handle_product();
          $this->book = NULL;
        }
        break;

      case 'price_1':
      case 'item_name':
      case 'item_ean':
      case 'item_unique_id':
      case 'item_delta_status':
      case 'item_salesrank':
        $this->book_elem = NULL;
        break;

    }
  }

  /**
   * Invoked each time cdata text is processed.
   *
   * note that this may be just a fragment of cdata element
   * so consider that single cdata element can be processed
   * with multiple invocations of this method.
   */
  function cdata($parser, $cdata) {
    if ($this->book_elem) {
      $this->book[$this->book_elem] .= $cdata;
      // print_r('ELEM: ' . $this->book_elem . ' DATA:' . $cdata.'<br />');
    }
  }

  /**
   * Invoked each time a complete product is decoded from the stream.
   */
  function handle_product() {
    $this->books[$this->book['item_ean']] = $this->book;
  }

  public function getBooks() {
    return $this->books;
  }
}

