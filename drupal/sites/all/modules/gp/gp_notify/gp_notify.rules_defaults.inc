<?php
/**
 * @file
 * gp_notify.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function gp_notify_default_rules_configuration() {
  $items = array();
  $items['rules_cart_merged_notification_email'] = entity_import('rules_config', '{ "rules_cart_merged_notification_email" : {
      "LABEL" : "Cart merged notification email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "gp_card", "message_notify", "gp_amazon_cart" ],
      "ON" : { "gp_amazon_cart_merged" : [] },
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "cart-entity:user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        },
        { "gp_card_cart_has_points" : { "amazon_cart" : [ "amazon_cart" ] } }
      ],
      "DO" : [
        { "gp_card_calculate_cart_points" : {
            "USING" : { "amazon_cart" : [ "amazon_cart" ] },
            "PROVIDE" : { "points" : { "points" : "Points that can be earned" } }
          }
        },
        { "entity_create" : {
            "USING" : {
              "type" : "message",
              "param_type" : "amazon_cart_merged_points_email",
              "param_user" : [ "cart-entity:user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:message-cart" ],
            "value" : [ "cart-entity" ]
          }
        },
        { "data_set" : { "data" : [ "entity-created:message-points" ], "value" : [ "points" ] } },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : 1 } },
        { "message_notify_process" : {
            "message" : [ "entity-created" ],
            "save_on_fail" : 0,
            "rendered_subject_field" : "message_rendered_subject",
            "rendered_body_field" : "message_rendered_body"
          }
        }
      ]
    }
  }');
  $items['rules_rules_ebook_oneclickbuy_notification_email'] = entity_import('rules_config', '{ "rules_rules_ebook_oneclickbuy_notification_email" : {
      "LABEL" : "E-book one click buy notification email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "gp_card", "message_notify", "gp_amazon_cart" ],
      "ON" : { "gp_amazon_cart_oneclickbuy" : [] },
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "cart-entity:user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        },
        { "gp_card_item_has_points" : { "item" : [ "item" ] } }
      ],
      "DO" : [
        { "gp_card_calculate_item_points" : {
            "USING" : { "item" : [ "item" ] },
            "PROVIDE" : { "points" : { "points" : "Points that can be earned" } }
          }
        },
        { "entity_create" : {
            "USING" : {
              "type" : "message",
              "param_type" : "ebook_oneclickbuy_points_email",
              "param_user" : [ "cart-entity:user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:message-cart" ],
            "value" : [ "cart-entity" ]
          }
        },
        { "data_set" : { "data" : [ "entity-created:message-points" ], "value" : [ "points" ] } },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : 1 } },
        { "message_notify_process" : {
            "message" : [ "entity-created" ],
            "save_on_fail" : 0,
            "rendered_subject_field" : "message_rendered_subject",
            "rendered_body_field" : "message_rendered_body"
          }
        }
      ]
    }
  }');
  $items['rules_rules_gift_card_notification_email'] = entity_import('rules_config', '{ "rules_rules_gift_card_notification_email" : {
      "LABEL" : "Gift card notification email",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-10",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "gp_promo", "message_notify", "gp_data" ],
      "ON" : { "gp_data_points_inserted" : [] },
      "IF" : [
        { "data_is" : {
            "data" : [ "cart-entity:changed" ],
            "op" : "\\u003C",
            "value" : 1419030000
          }
        },
        { "gp_promo_promo_has_codes_left" : { "promo" : "Z1M7CZ" } },
        { "NOT data_is" : { "data" : [ "total-earned" ], "op" : "\\u003C", "value" : "10" } },
        { "NOT gp_promo_user_has_promo" : { "user" : [ "cart-entity:user" ], "promo" : "Z1M7CZ" } }
      ],
      "DO" : [
        { "gp_promo_get_code" : {
            "USING" : { "user" : [ "cart-entity:user" ], "promo" : "Z1M7CZ" },
            "PROVIDE" : { "code" : { "code" : "The code retrieved" } }
          }
        },
        { "entity_create" : {
            "USING" : {
              "type" : "message",
              "param_type" : "promo_gift_code_email",
              "param_user" : [ "cart-entity:user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : { "data" : [ "entity-created:message-promo-code" ], "value" : [ "code" ] } },
        { "data_set" : { "data" : [ "entity-created:message-points" ], "value" : [ "points" ] } },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : 1 } },
        { "message_notify_process" : {
            "message" : [ "entity-created" ],
            "save_on_fail" : 0,
            "rendered_subject_field" : "message_rendered_subject",
            "rendered_body_field" : "message_rendered_body"
          }
        }
      ]
    }
  }');
  $items['rules_rules_points_earned_notification_email'] = entity_import('rules_config', '{ "rules_rules_points_earned_notification_email" : {
      "LABEL" : "Points earned notification email",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "10",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "gp_promo", "message_notify", "gp_data" ],
      "ON" : { "gp_data_points_inserted" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "points" ], "op" : "\\u003E", "value" : "0" } },
        { "OR" : [
            { "data_is" : { "data" : [ "total-earned" ], "op" : "\\u003C", "value" : "10" } },
            { "gp_promo_user_has_promo" : { "user" : [ "cart-entity:user" ], "promo" : "Z1M7CZ" } },
            { "data_is" : {
                "data" : [ "cart-entity:changed" ],
                "op" : "\\u003E",
                "value" : 1419029999
              }
            }
          ]
        }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "message",
              "param_type" : "earnings_processed_points_email",
              "param_user" : [ "cart-entity:user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : { "data" : [ "entity-created:message-points" ], "value" : [ "points" ] } },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : 1 } },
        { "message_notify_process" : {
            "message" : [ "entity-created" ],
            "save_on_fail" : 0,
            "rendered_subject_field" : "message_rendered_subject",
            "rendered_body_field" : "message_rendered_body"
          }
        }
      ]
    }
  }');
  $items['rules_send_message_entity'] = entity_import('rules_config', '{ "rules_send_message_entity" : {
      "LABEL" : "Send message entity",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "message_notify" ],
      "USES VARIABLES" : { "message" : { "label" : "Message", "type" : "message" } },
      "ACTION SET" : [
        { "message_notify_process" : {
            "message" : [ "message" ],
            "save_on_fail" : 0,
            "rendered_subject_field" : "message_rendered_subject",
            "rendered_body_field" : "message_rendered_body"
          }
        }
      ]
    }
  }');
  return $items;
}
