<?php
/**
 * @file
 * Provides metadata for the cart and line item entity.
 */

class CartMetadataController extends EntityDefaultMetadataController {
  /**
   * Provide properties.
   *
   * @return array
   *   The properties.
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info['cart']['properties'];

    // Cart ID field.
    $properties['cart_id']['title'] = 'ID';
    $properties['cart_id']['description'] = 'The Amazon cart ID.';

    // Cart HMAC field.
    $properties['hmac']['title'] = 'HMAC';
    $properties['hmac']['description'] = 'The Amazon cart HMAC.';

    // Cart url-encoded HMAC field.
    $properties['url_encoded_hmac']['title'] = 'Url-encoded HMAC';
    $properties['url_encoded_hmac']['description'] = 'The Amazon cart url-encoded HMAC.';

    // Cart subtag field.
    $properties['subtag']['title'] = 'Subtag';
    $properties['subtag']['description'] = 'The subtag used for merging this cart.';

    // Unset the uid property, as it is available via the user anyway.
    unset($properties['uid']);

    // Cart owner.
    $properties['user'] = array(
      'type' => 'user',
      'label' => 'Owner',
      'description' => 'The user that owns this cart.',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'schema field' => 'uid',
      'required' => TRUE,
    );

    // Cart subtotal field.
    $properties['subtotal']['description'] = 'The cart subtotal.';

    // Cart currency field.
    $properties['currency']['description'] = 'The currency of the cart subtotal.';

    // Cart status field.
    $properties['status']['title'] = 'Status';
    $properties['status']['description'] = 'The status of the cart.';

    // Cart created field.
    $properties['created']['title'] = 'Created date';
    $properties['created']['description'] = 'The date the cart was created.';
    $properties['created']['type'] = 'date';

    // Cart changed field.
    $properties['changed']['title'] = 'Updated date';
    $properties['changed']['description'] = 'The date the cart was last updated.';
    $properties['changed']['type'] = 'date';

    return $info;
  }
}

class LineItemMetadataController extends EntityDefaultMetadataController {
  /**
   * Provide properties.
   *
   * @return array
   *   The properties.
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info['line_item']['properties'];

    // Unset the cid property, as it is available via the cart anyway.
    unset($properties['cid']);

    // Line item cart.
    $properties['cart'] = array(
      'type' => 'cart',
      'label' => 'Cart',
      'description' => 'The cart this line item belongs to.',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'schema field' => 'cid',
      'required' => TRUE,
    );

    // Line item asin field.
    $properties['asin']['title'] = 'ASIN';
    $properties['asin']['description'] = 'The Amazon item ASIN.';

    // Line item label field.
    $properties['label']['title'] = 'Label';
    $properties['label']['description'] = 'The display label of the line item.';

    // Line item quantity field.
    $properties['quantity']['title'] = 'Quantity';
    $properties['quantity']['description'] = 'The quantity of the line item.';

    // Line item price field.
    $properties['price']['title'] = 'Price';
    $properties['price']['description'] = 'The price for one item.';

    // Line item currency field.
    $properties['currency']['description'] = 'The currency of the line item.';

    // Line item total field.
    $properties['total']['description'] = 'The total price for the line item.';

    // Line item created field.
    $properties['created']['title'] = 'Created date';
    $properties['created']['description'] = 'The date the line item was created.';
    $properties['created']['type'] = 'date';

    // Line item changed field.
    $properties['changed']['title'] = 'Updated date';
    $properties['changed']['description'] = 'The date the Line item was last updated.';
    $properties['changed']['type'] = 'date';

    return $info;
  }
}
