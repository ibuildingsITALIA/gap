<?php
/**
 * @file
 * Amazon cart class wrapper file.
 */

/**
 * Class AmazonCart
 * Wrapper around an Amazon Cart
 */
class AmazonCart {
  /**
   * @var string
   *   The cart id.
   */
  protected $id;

  /**
   * @var string
   *   The cart Hash Message Authentication Code.
   */
  protected $hmac;

  /**
   * @var string
   *   The cart url-encoded Hash Message Authentication Code.
   */
  protected $urlEncodedHmac;


  /**
   * @var string
   *   The url to merge the cart with Amazon.
   */
  protected $purchaseUrl;

  /**
   * @var array
   *   The subtotal cart info.
   */
  protected $subtotal;

  /**
   * @var array
   *   The cart items, if present.
   */
  protected $items;

  /**
   * @var int
   *   The total item count of items, accounting quantity.
   */
  protected $totalItemCount;

  /**
   * @var bool
   *   If the cart is empty or not.
   */
  protected $empty;

  /**
   * @var string
   *   The cart status.
   */
  protected $status;

  /**
   * Build object from xml response.
   *
   * @param SimpleXmlElement $xml_element
   *   The xml returned from amazon.
   */
  public function __construct($xml_element) {
    $this->id = (string) $xml_element->CartId;
    $this->hmac = (string) $xml_element->HMAC;
    $this->urlEncodedHmac = (string) $xml_element->URLEncodedHMAC;
    $this->purchaseUrl = (string) $xml_element->PurchaseURL;
    $this->subtotal = (array) $xml_element->SubTotal;

    // Get cart items.
    $this->items = array();
    $this->totalItemCount = 0;
    if (isset($xml_element->CartItems)) {
      foreach ($xml_element->CartItems->CartItem as $xml_item) {
        $item = $this->xmlItemToArray($xml_item);
        $this->items[] = $item;
        $this->totalItemCount += $item['Quantity'];
      }
    }

    // Inizialize empty variable.
    $this->empty = !count($this->items);

    // Default status.
    $this->status = 'cart';
  }

  /**
   * Converts an xml item object to an array.
   *
   * @param SimpleXmlElement $xml_item
   *   The xml item.
   *
   * @return array
   *   The converted item.
   */
  protected function xmlItemToArray($xml_item) {
    $result = (array) $xml_item;

    foreach ($result as $key => $property) {
      if (is_object($property)) {
        $result[$key] = (array) $property;
      }
    }

    return $result;
  }

  /**
   * Returns the cart id.
   *
   * @return string
   *   The cart id.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Returns the cart HMAC.
   *
   * @return string
   *   The cart HMAC.
   */
  public function getHmac() {
    return $this->hmac;
  }

  /**
   * Returns the cart url-encoded HMAC.
   *
   * @return string
   *   The cart url-encoded HMAC.
   */
  public function getUrlEncodedHmac() {
    return $this->urlEncodedHmac;
  }

  /**
   * Returns the cart purchase url.
   *
   * @return string
   *   The url.
   */
  public function getPurchaseUrl() {
    // Insert the interrogation mark already encoded.
    $subtag = '%26ascsubtag=' . $this->getSubtag();
    // Add the subtag to identify this cart.
    return $this->purchaseUrl . $subtag;
  }

  /**
   * Returns the cart subtotal.
   *
   * @return array
   *   The cart subtotal data.
   */

  /**
   * Returns the cart subtotal array, or a specific value of it.
   *
   * @param null $key
   *   (Optional) The key to retrieve.
   *
   * @return array|string|null
   *   The subtotal array if no key specified,
   *   the key value or null if key not found.
   */
  public function getSubtotal($key = NULL) {
    if (is_null($key)) {
      return $this->subtotal;
    }

    if (isset($this->subtotal[$key])) {
      return $this->subtotal[$key];
    }

    return NULL;
  }

  /**
   * Returns the cart items.
   *
   * @return array
   *   An array of cart items (not AmazonItems).
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * Returns the total item count.
   *
   * @return int
   *   The quantity.
   */
  public function getTotalItemCount() {
    return $this->totalItemCount;
  }

  /**
   * Returns if the cart is empty or not.
   *
   * @return bool
   *   The cart emptiness lol.
   */
  public function isEmpty() {
    return $this->empty;
  }

  /**
   * Returns the cart status.
   *
   * @return string
   *   The cart status.
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Set the cart status.
   *
   * @param string $status
   *   The cart status to set.
   */
  public function setStatus($status) {
    $this->status = (string) $status;
  }


  /**
   * Find an item into the cart by ASIN.
   *
   * @param string $asin
   *   The item ASIN.
   *
   * @return bool|array
   *   False if not found, the item array otherwise.
   */
  public function getItemByASIN($asin) {
    foreach ($this->items as $item) {
      if ($item['ASIN'] === $asin) {
        return $item;
      }
    }

    return FALSE;
  }

  /**
   * Find an item into the cart by ASIN.
   *
   * @param string $id
   *   The cart item id to look for.
   *
   * @return bool|array
   *   False if not found, the item array otherwise.
   */
  public function getItemByCartItemId($id) {
    foreach ($this->items as $item) {
      if ($item['CartItemId'] === $id) {
        return $item;
      }
    }

    return FALSE;
  }

  /**
   * Returns a subtag unique for this cart.
   *
   * @return string
   *   The subtag string.
   */
  public function getSubtag() {
    return gp_amazon_cart_generate_subtag($this->id, $this->hmac);
  }
}

/**
 * Class AmazonEmptyCart
 *
 * Create an empty cart.
 */
class AmazonEmptyCart extends AmazonCart {
  /**
   * Create an empty cart.
   */
  public function __construct() {
    $this->empty = TRUE;

    $this->id = NULL;
    $this->hmac = NULL;
    $this->urlEncodedHmac = NULL;
    $this->purchaseUrl = NULL;
    $this->subtotal = NULL;
    $this->items = array();
    $this->totalItemCount = 0;
  }
}
