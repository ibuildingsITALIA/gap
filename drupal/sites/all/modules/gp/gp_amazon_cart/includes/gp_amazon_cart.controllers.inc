<?php
/**
 * @file
 * Controllers for Giunti al Punto cart entities.
 */

class CartEntityAPIController extends EntityAPIController {
  /**
   * Override save method.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    if (isset($entity->is_new)) {
      $entity->created = REQUEST_TIME;
    }
    $entity->changed = REQUEST_TIME;

    if (empty($entity->hostname)) {
      $entity->hostname = ip_address();
    }

    return parent::save($entity, $transaction);
  }
}

class LineItemEntityAPIController extends EntityAPIController {
  /**
   * Override save method.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    if (isset($entity->is_new)) {
      $entity->created = REQUEST_TIME;
    }
    $entity->changed = REQUEST_TIME;

    return parent::save($entity, $transaction);
  }
}
