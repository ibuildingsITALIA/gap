<?php
/**
 * @file
 * gp_amazon_cart.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function gp_amazon_cart_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cart_items';
  $view->description = 'Show items of a cart.';
  $view->tag = 'default';
  $view->base_table = 'gp_amazon_line_item';
  $view->human_name = 'Cart items';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'asin' => 'asin',
    'label' => 'label',
    'price' => 'price',
    'quantity' => 'quantity',
    'total' => 'total',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'asin' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'label' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'price' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'quantity' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'total' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Line item: Asin */
  $handler->display->display_options['fields']['asin']['id'] = 'asin';
  $handler->display->display_options['fields']['asin']['table'] = 'gp_amazon_line_item';
  $handler->display->display_options['fields']['asin']['field'] = 'asin';
  $handler->display->display_options['fields']['asin']['label'] = 'Product code';
  $handler->display->display_options['fields']['asin']['element_label_colon'] = FALSE;
  /* Field: Line item: Label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'gp_amazon_line_item';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['label'] = 'Title';
  $handler->display->display_options['fields']['label']['element_label_colon'] = FALSE;
  /* Field: Line item: Quantity */
  $handler->display->display_options['fields']['quantity']['id'] = 'quantity';
  $handler->display->display_options['fields']['quantity']['table'] = 'gp_amazon_line_item';
  $handler->display->display_options['fields']['quantity']['field'] = 'quantity';
  $handler->display->display_options['fields']['quantity']['element_label_colon'] = FALSE;
  /* Field: Line item: Price */
  $handler->display->display_options['fields']['price']['id'] = 'price';
  $handler->display->display_options['fields']['price']['table'] = 'gp_amazon_line_item';
  $handler->display->display_options['fields']['price']['field'] = 'price';
  $handler->display->display_options['fields']['price']['label'] = 'Discounted price';
  $handler->display->display_options['fields']['price']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['price']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['price']['precision'] = '2';
  $handler->display->display_options['fields']['price']['prefix'] = '€ ';
  /* Contextual filter: Line item: Cart */
  $handler->display->display_options['arguments']['cart']['id'] = 'cart';
  $handler->display->display_options['arguments']['cart']['table'] = 'gp_amazon_line_item';
  $handler->display->display_options['arguments']['cart']['field'] = 'cart';
  $handler->display->display_options['arguments']['cart']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['cart']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['cart']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['cart']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['cart']['summary_options']['items_per_page'] = '25';
  $translatables['cart_items'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Product code'),
    t('Title'),
    t('Quantity'),
    t('.'),
    t(','),
    t('Discounted price'),
    t('€ '),
    t('All'),
  );

  $export['cart_items'] = $view;

  return $export;
}
