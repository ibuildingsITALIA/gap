<?php
/**
 * @file
 * Theme functions and template preprocess.
 */

/**
 * Renders an empty cart.
 */
function theme_gp_amazon_cart_empty($variables) {
  return '<div class="empty-cart">' . t('Cart is empty') . '</div>';
}

/**
 * Form theme function for gp_amazon_cart_form().
 */
function theme_gp_amazon_cart_form($variables) {
  $form = $variables['form'];
  /* @var AmazonCart $cart */
  $cart = $form['#cart'];

  // Load items from the api.
  $loaded_items = gp_amazon_cart_load_cart_items($cart);

  // Prepare table.
  $header = array(
    t('Item'),
    t('Price-Quantity'),
  );
  $rows = array();

  // If cart-last page get last item only.
  // @todo I don't like this :)
  $items = ($variables['form']['#action'] === '/cart-last') ? array_slice($cart->getItems(), 0, 1) : $cart->getItems();

  foreach ($items as $item) {
    $item_id = $item['CartItemId'];
    $full_item = isset($loaded_items[$item['ASIN']]) ? $loaded_items[$item['ASIN']] : FALSE;

    // Render item in detail view mode, if present.
    if ($full_item) {
      $item_preview = theme('gp_amazon_item', array(
        'item' => $loaded_items[$item['ASIN']],
        'view_mode' => 'detail',
      ));
    }
    else {
      // Fallback to simple title.
      $item_preview = '<h2 class="product-title">' . l($item['Title'], "product/{$item['ASIN']}") . '</h2>';
    }

    // Item price element.
    $item_offer = '<p class="item-price">' . t('Price') . ': <span class="price">';
    $item_offer .= gp_amazon_currency_format($item['Price']['Amount']);
    $item_offer .= '</span></p>';

    // Add the saved amount if possible.
    if ($full_item && $offer_listing = $full_item->getOfferListing()) {
      // If we have the saved amount info, show it.
      if (!empty($offer_listing['AmountSaved']['Amount'])) {
        $item_offer .= '<p class="item-saved">' . t('You save') . ': <span class="price">';
        $item_offer .= gp_amazon_currency_format($offer_listing['AmountSaved']['Amount']);
        $item_offer .= '</span> <span class="perc">(';
        $item_offer .= gp_amazon_discount_percentage($item['Price']['Amount'], $offer_listing['AmountSaved']['Amount']);
        $item_offer .= ')</span></p>';
      }
    }

    // Item quantity element.
    $item_offer .= '<p class="item-quantity">' . t('Quantity') . '</p>';
    $item_offer .= drupal_render($form['items'][$item_id]['quantity']);

    // Render remove button.
    $item_offer .= '<div class="remove-action-wrapper">';
    $item_offer .= drupal_render($form['items'][$item_id]['delete']);
    $item_offer .= '</div>';

    if ($variables['form']['#action'] === '/cart-last') {
      $rows[] = array(
        'data' => array(
          array(
            'data' => $item_preview,
            'class' => array('item-data'),
          ),
        ),
      );
    }
    else {
      $rows[] = array(
        'data' => array(
          array(
            'data' => $item_preview,
            'class' => array('item-data'),
          ),
          array(
            'data' => $item_offer,
            'class' => array('item-price-quantity'),
          ),
        ),
      );
    }
  }

  $subtotal = theme('gp_amazon_cart_subtotal', array('cart' => $cart));

  // Add the subtotal after the submit buttons.
  $actions = & $form['actions'];
  $actions['#sorted'] = FALSE;
  $actions['update']['#weight'] = 2;
  $actions['submit']['#weight'] = 3;
  $actions['edit']['#weight'] = 4;
  $actions['subtotal'] = array(
    '#markup' => $subtotal,
    '#weight' => 5,
  );

  $table_attributes = array('class' => 'tablesaw tablesaw-stack', 'data-mode' => 'stack');

  // Build the title.
  if ($variables['form']['#action'] === '/cart-last') {
    $count_items = $items['0']['Quantity'];
    $title = '<span class="icon-check"></span>';
    $title .= ($count_items === '1') ? t('1 item added to cart') : t('@count items added to cart', array('@count' => $count_items));
  }
  else {
    $title = t('Cart');
  }

  $output = '<div class="cart-wrapper"><h2 class="cart-title">' . $title . '</h2>';
  $output .= drupal_render($form['actions']);

  $output .= '<div class="cart-table">';
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'sticky' => FALSE,
    'attributes' => $table_attributes,
  ));
  $output .= '</div>';

  // Render remaining elements.
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Render a cart subtotal.
 */
function theme_gp_amazon_cart_subtotal($variables) {
  /* @var AmazonCart $cart */
  $cart = $variables['cart'];

  $string = format_plural($cart->getTotalItemCount(), '(%count item in the cart)', '(%count items in the cart)', array(
    '%count' => $cart->getTotalItemCount(),
  ));

  $output = '<div class="subtotal"><span class="label">';
  $output .= t('Order total');
  $output .= ':</span> <span class="article-count">';
  $output .= $string;
  $output .= '</span> <span class="price">';
  $output .= gp_amazon_currency_format($cart->getSubtotal('Amount'));
  $output .= '</span></div>';

  return $output;
}

/**
 * Preprocess function for redirect modal theme.
 */
function template_preprocess_gp_amazon_cart_redirect(&$variables) {
  global $user;

  /* @var AmazonCart $cart */
  $cart = $variables['cart'];
  $variables['cart_url'] = $cart->getPurchaseUrl();

  if (user_is_logged_in()) {
    // Logged in user: prepare username.
    $account = user_load($user->uid);
    $variables['username'] = format_username($account);
    $variables['logout_url'] = url('user/logout');
  }
  else {
    // For register, use a double destination.
    // First, we go to registration page, then login page, then cart.
    $variables['login_url'] = url('signup', array(
      'query' => array(
        'destination' => 'cart',
      ),
    ));
    $variables['register_url'] = url('register', array(
      'query' => array(
        'destination' => ltrim($variables['login_url'], '/'),
      ),
    ));
  }
}

/**
 * Preprocess function for login modal.
 */
function template_preprocess_gp_amazon_cart_login_modal(&$variables) {
  // For register, use a double destination.
  // First, we go to registration page, then login page, then cart.
  $variables['login_url'] = url('signup', array(
    'query' => array(
      'destination' => $variables['destination'],
    ),
  ));
  $variables['register_url'] = url('register', array(
    'query' => array(
      'destination' => $variables['destination'],
    ),
  ));
}
