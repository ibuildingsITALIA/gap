<?php
/**
 * @file
 * Template for redirect message before going to Amazon.
 */
?>
<div class="modal-redirect">
  <p class="notice-card">Per accumulare i punti maturati con questo acquisto devi essere titolare di una GiuntiCard.</p>
  <div class="login-to-card">
    <p class="login">
      Sei già registrato?
    </p>
    <a href="<?php print $login_url; ?>" id="modal-redirect-login">Autenticati</a>
    <p class="register">
      Non hai una GiuntiCard?
    </p>
    <a href="<?php print $register_url; ?>" id="modal-redirect-register">Crea la tua GiuntiCard</a>
  </div>

  <div class="skip-card"><p>Se invece non vuoi accumulare punti e sconti sulla tua GiuntiCard, procedi direttamente con l'acquisto su Amazon</p><a class="btn" id="gp-amazon-cart-redirect" href="<?php print $purchase_url; ?>" target="_blank"><?php print t('Go to Amazon'); ?></a></div>
</div><!-- /.modal-redirect -->
