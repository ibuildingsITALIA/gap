<?php
/**
 * @file
 * Form callbacks.
 */

/**
 * Form callback to add an item to the cart.
 */
function gp_amazon_cart_add_to_cart_form($form, &$form_state, AmazonItem $item, $ajax = false) {
  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_amazon_cart', 'gp_amazon_cart.forms');

  // Add some classes to allow styling.
  $form['#attributes']['class'][] = 'add-to-cart-form';

  // Save item for later.
  $form_state['item'] = $item;

  $form['quantity'] = array(
    '#type' => 'select',
    '#title' => t('Quantity'),
    '#options' => drupal_map_assoc(range(1, GP_AMAZON_CART_MAX_ITEM)),
    '#default_value' => 1,
    '#required' => TRUE,
    // Disable chosen module if active.
    '#chosen' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  if ($ajax) {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add to cart'),
      '#ajax' => array(
        'callback' => 'gp_amazon_cart_add_to_cart_form_submit_ajax',
      ),
    );
  }
  else {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add to cart'),
    );
  }

  return $form;
}

/**
 * Submit callback for gp_amazon_cart_add_to_cart_form().
 */
function gp_amazon_cart_add_to_cart_form_submit($form, &$form_state) {
  /* @var AmazonItem $item */
  $item = $form_state['item'];
  $quantity = $form_state['values']['quantity'];

  // Add the current item to the cart.
  $line_item = gp_amazon_cart_fake_line_item_create($item->ASIN, $quantity, $item->getTitle(), $item);
  if (gp_amazon_cart_add_wrapper(array($line_item))) {
    // If the item has been added successfully, redirect to last item cart page.
    $form_state['redirect'] = 'cart-last';
  }
}

/**
 * Submit callback for gp_amazon_cart_add_to_cart_form_ajax().
 */
function gp_amazon_cart_add_to_cart_form_submit_ajax($form, &$form_state) {
  /* @var AmazonItem $item */
  $item = $form_state['item'];
  $quantity = $form_state['values']['quantity'];

  // Add the current item to the cart.
  $line_item = gp_amazon_cart_fake_line_item_create($item->ASIN, $quantity, $item->getTitle(), $item);
  if (gp_amazon_cart_add_wrapper(array($line_item))) {
    // If the item has been added successfully, update item count.
    $session_cart = gp_amazon_cart_session_cart_data();

    // If we have a session cart, retrieve data from the database.
    if ($session_cart) {
      $cart_id = $session_cart['CartId'];
      $query = db_select('gp_amazon_cart', 'c');
      $query->join('gp_amazon_line_item', 'l', 'c.cid = l.cid');
      $query
        ->fields('l', array('quantity'))
        ->condition('c.cart_id', $cart_id);
      $result = $query->execute()->fetchCol();
      // Array sum is faster than adding a SUM in the query.
      $item_count = array_sum($result);
    }
    else {
      // We have no items.
      $item_count = 0;
    }

    $string = format_plural($item_count, '@count item', '@count items');

    $content = '<div class="block__content">' . l($string, 'cart', array(
        'html' => TRUE,
        'attributes' => array('class' => array('cart-count')),
      )) . '<span class="cart-link">' . l(t('Cart'), 'cart') . '</span></div>';

    $method = 'addTimedText';

    $arguments = array('added-to-cart', 'add-to-cart-box', 'item-added-text', t('Item added to cart'));

    $commands = array(
      ajax_command_replace('#block-gp-amazon-cart-cart > div.block__content', $content),
      ajax_command_invoke('.amazon-item-' . $item->ASIN, $method, $arguments),
    );

    $replace = array('#type' => 'ajax', '#commands' => $commands);

    return $replace;
  }
}

/**
 * Form callback to buy with "one click" an item.
 *
 * Works for everything, but mostly should be used on e-books.
 */
function gp_amazon_cart_oneclick_form($form, &$form_state, AmazonItem $item) {
  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_amazon_cart', 'gp_amazon_cart.forms');
  // Attach our js to the form.
  $form['#attached']['js'][] = drupal_get_path('module', 'gp_amazon_cart') . '/gp_amazon_cart.js';

  // Save item for later.
  $form_state['item'] = $item;

  $form['actions'] = array('#type' => 'actions');

  // If the user is anonymous, send him to the signup page.
  if (user_is_anonymous()) {
    $path = 'signup';
    $options = array(
      'query' => array(
        'destination' => 'one-click-buy/' . $item->ASIN,
        'item' => $item->ASIN,
      ),
      'attributes' => array(
        'class' => array('purchase-submit', 'login-submit'),
      ),
    );
    $form['actions']['submit'] = array(
      '#markup' => l(t('Buy on Amazon.it'), $path, $options),
    );
  }
  // Otherwise to the merge page.
  else {
    $path = 'one-click-buy/' . $item->ASIN;
    $options = array(
      'attributes' => array(
        'class' => array('purchase-submit', 'oneclickbuy-submit'),
        'id' => 'gp-amazon-cart-redirect',
        'target' => '_blank',
        'rel' => 'nofollow',
      ),
    );
    $form['actions']['submit'] = array(
      '#markup' => l(t('Buy on Amazon.it'), $path, $options),
    );
  }

  return $form;
}

/**
 * Submit handler for gp_amazon_cart_oneclick_form.
 *
 * @todo Remove me if not useful!
 */
function gp_amazon_cart_oneclick_form_submit($form, &$form_state) {
  watchdog('gp_amazon_cart', 'Somebody called me! (cit.)');
}

/**
 * Cart form.
 */
function gp_amazon_cart_form($form, &$form_state, AmazonCart $cart, $last_item_only = FALSE) {
  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_amazon_cart', 'gp_amazon_cart.forms');
  // Attach our js to the form.
  $form['#attached']['js'][] = drupal_get_path('module', 'gp_amazon_cart') . '/gp_amazon_cart.js';

  // Since ajax rebuilds form prior to calling the ajax callback,
  // check the form state submitted values and update cart if needed.
  $cart = _gp_amazon_cart_update_quantity_helper($cart, $form_state);

  // Save cart for later.
  $form_state['cart'] = $cart;
  // Save it on $form also as we need it in the theme function.
  $form['#cart'] = $cart;

  // Set a fixed id for replacing with ajax.
  $form['#id'] = 'gp-amazon-cart-form';

  // Set #tree to preserve structure.
  $form['items'] = array(
    '#tree' => TRUE,
  );

  foreach ($cart->getItems() as $item) {
    $item_id = $item['CartItemId'];
    $form['items'][$item_id] = array(
      'quantity' => array(
        '#type' => 'select',
        '#title' => t('Quantity'),
        '#title_display' => 'attribute',
        '#options' => drupal_map_assoc(range(0, GP_AMAZON_CART_MAX_ITEM)),
        '#default_value' => $item['Quantity'],
        '#required' => TRUE,
        '#attributes' => array(
          'class' => array('edit-item-quantity'),
        ),
        '#ajax' => array(
          'callback' => 'gp_amazon_cart_form_ajax_callback',
          'wrapper' => 'gp-amazon-cart-form',
        ),
        // Disable chosen module if active.
        '#chosen' => FALSE,
      ),
      'delete' => array(
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#name' => "delete-{$item_id}",
        '#attributes' => array(
          'class' => array('delete-item'),
        ),
        '#submit' => array('gp_amazon_cart_form_delete_submit'),
      ),
      '#cart_item_id' => $item_id,
    );
  }

  $form['actions'] = array('#type' => 'actions');

  // Add an edit or an update button based on which form version we are.
  if ($last_item_only) {
    $form['actions']['edit'] = array(
      '#markup' => l(t('Edit cart'), 'cart', array(
        'attributes' => array(
          'class' => array('edit-link', 'form-link'),
        ),
      )),
    );
  }
  else {
    $form['actions']['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#attributes' => array(
        'class' => array('update-submit'),
      ),
      '#submit' => array('gp_amazon_cart_form_update_submit'),
    );
  }

  // If the user is anonymous, send him to the signup page.
  if (user_is_anonymous()) {
    $path = 'signup';
    $options = array(
      'query' => array(
        'destination' => 'merge',
      ),
      'attributes' => array(
        'class' => array('purchase-submit', 'login-submit'),
      ),
    );
  }
  // Otherwise to the merge page.
  else {
    $path = 'merge';
    $options = array(
      'attributes' => array(
        'class' => array('purchase-submit', 'merge-submit'),
        'id' => 'gp-amazon-cart-redirect',
        'target' => '_blank',
      ),
    );
  }

  $form['actions']['submit'] = array(
    '#markup' => l(t('Buy on Amazon.it'), $path, $options),
  );

  return $form;
}

/**
 * Helper function to update item quantities based on form state.
 *
 * @param AmazonCart $cart
 *   The current cart.
 *
 * @param array $form_state
 *   The form state to check against.
 *
 * @return AmazonCart
 *   The updated cart.
 */
function _gp_amazon_cart_update_quantity_helper(AmazonCart $cart, &$form_state) {
  if (empty($form_state['values']['items'])) {
    return $cart;
  }

  $item_values = $form_state['values']['items'];
  $line_items = array();

  foreach ($cart->getItems() as $item) {
    $cart_item_id = $item['CartItemId'];

    if (!isset($item_values[$cart_item_id])) {
      // The form was not up to date, skip this line.
      continue;
    }

    // If the quantity of the item differs from submitted one, add it
    // to the line items to update.
    $new_quantity = $item_values[$cart_item_id]['quantity'];
    if ($item['Quantity'] !== $new_quantity) {
      $line_items[] = gp_amazon_cart_fake_line_item_create($item['ASIN'], $new_quantity, $item['Title']);
    }
  }

  if (!empty($line_items)) {
    $new_cart = gp_amazon_cart_remote_cart_modify($line_items);

    // If request succeeded, return the old cart.
    if ($new_cart) {
      $cart = $new_cart;
    }
  }

  return $cart;
}

/**
 * Submit handler for delete buttons in gp_amazon_cart_form().
 */
function gp_amazon_cart_form_delete_submit($form, &$form_state) {
  // Get the triggering element parent.
  $parents = $form_state['triggering_element']['#array_parents'];
  $parents = array_slice($parents, 0, -1);
  $element = drupal_array_get_nested_value($form, $parents);

  /* @var AmazonCart $cart */
  $cart = $form_state['cart'];
  $item = $cart->getItemByCartItemId($element['#cart_item_id']);
  $line_item = gp_amazon_cart_fake_line_item_create($item['ASIN'], 0, $item['Title']);

  // Just call the api, as if will print itself the messages.
  gp_amazon_cart_remote_cart_modify(array($line_item));
}

/**
 * Submit handler for update action in gp_amazon_cart_form().
 *
 * This is mostly needed only for non-js fallback as the form
 * updates automatically as soon as select changes.
 */
function gp_amazon_cart_form_update_submit($form, &$form_state) {
  /* @var AmazonCart $cart */
  $cart = $form_state['cart'];
  $updated_cart = _gp_amazon_cart_update_quantity_helper($cart, $form_state);

  // Update the cart into the form state.
  $form_state['cart'] = $updated_cart;
}

/**
 * Ajax callback to update gp_amazon_cart_form().
 */
function gp_amazon_cart_form_ajax_callback($form, &$form_state) {
  /* @var AmazonCart $cart */
  $cart = $form_state['cart'];

  // If the cart is empty, issue a complete reload of the page.
  if ($cart->isEmpty()) {
    ctools_include('ajax');
    return array(
      '#type' => 'ajax',
      '#commands' => array(ctools_ajax_command_reload()),
    );
  }

  // Since the form has been rebuilt already, return the updated form.
  return $form;
}

/**
 * Common code for rendering modals/links on purchase.
 */
function _gp_amazon_cart_login_modal_button() {
  // Enable ctools ajax and modal.
  ctools_include('ajax');
  ctools_include('modal');
  ctools_add_js('ajax-responder');
  ctools_modal_add_js();
  drupal_add_js(_gp_amazon_cart_modal_settings(), 'setting');

  $element = array(
    '#type' => 'submit',
    '#value' => t('Buy on Amazon.it'),
    '#attributes' => array(
      'class' => array(
        'ctools-use-modal',
        'ctools-modal-giunti-modal-dialog',
        'purchase-submit',
        'login-submit',
      ),
    ),
    '#ajax' => array(
      'callback' => 'gp_amazon_cart_login_modal_ajax_callback',
    ),
  );

  return $element;
}

/**
 * Ajax callback for login modal submit.
 */
function gp_amazon_cart_login_modal_ajax_callback($form, &$form_state) {
  // Include ctools helpers.
  ctools_include('ajax');
  ctools_include('modal');

  $output = theme('gp_amazon_cart_login_modal', array(
    'purchase_url' => $form_state['purchase_url'],
    'destination' => $form_state['links_destination'],
  ));

  ctools_modal_render('Giunti al Punto', $output);
  drupal_exit();
}
