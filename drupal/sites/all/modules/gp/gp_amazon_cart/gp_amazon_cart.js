(function ($) {

  /**
   * Provide the HTML to create the modal dialog.
   */
  Drupal.theme.prototype.GiuntiModalDialog = function () {
    var html = '';

    html += '  <div id="ctools-modal" class="">';
    html += '    <div class="ctools-modal-content modal-dialog giunti-modal-dialog">';
    html += '      <div class="modal-header">';
    html += '        <button type="button" class="close" aria-hidden="true">&times;</button>';
    html += '        <h4 id="modal-title" class="modal-title">&nbsp;</h4>';
    html += '      </div>';
    html += '      <div id="modal-content" class="modal-content modal-body">';
    html += '      </div>';
    html += '      <div class="modal-footer">';
    html += '      </div>';
    html += '    </div>';
    html += '  </div>';

    return html;
  };

  Drupal.behaviors.gpAmazonCart = {
    attach: function (context, settings) {
      // Close modal on backdrop click.
      $('#modalBackdrop').once('dismiss', function () {
        $(this).on('click', function () {
          Drupal.CTools.Modal.dismiss();
        });
      });

      $('#gp-amazon-cart-redirect', context).once('redirect', function () {
        $(this).on('click', function () {
          setTimeout(function() {
            // Redirect to base url.
            window.location = '/';
          }, 300);
        });
      });
    }
  };

})(jQuery);
