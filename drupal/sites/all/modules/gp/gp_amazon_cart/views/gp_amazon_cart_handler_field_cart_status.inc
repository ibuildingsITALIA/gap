<?php
/**
 * @file
 * Definition of gp_amazon_cart_handler_filter_cart_status.
 */

/**
 * Filter handler to filter by booking status.
 */
class gp_amazon_cart_handler_field_cart_status extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);
    $statuses = gp_amazon_cart_cart_statuses();
    return isset($statuses[$value]) ? $statuses[$value] : t('Unknown');
  }
}
