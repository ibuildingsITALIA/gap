<?php
/**
 * @file
 * Rules integration for the gp_amazon_cart module.
 */

/**
 * Implements hook_rules_event_info().
 */
function gp_amazon_cart_rules_event_info() {
  return array(
    'gp_amazon_cart_merged' => array(
      'label' => t('After a cart has been merged with Amazon'),
      'group' => t('Giunti al Punto Amazon Cart'),
      'variables' => array(
        'amazon_cart' => array(
          'type' => 'struct',
          'label' => t('AmazonCart object'),
        ),
        'cart_entity' => array(
          'type' => 'cart',
          'label' => t('Cart entity'),
        ),
      ),
    ),
    'gp_amazon_cart_oneclickbuy' => array(
      'label' => t('After an "one click buy" purchase of an item'),
      'group' => t('Giunti al Punto Amazon Cart'),
      'variables' => array(
        'item' => array(
          'type' => 'struct',
          'label' => t('AmazonItem object'),
        ),
        'cart_entity' => array(
          'type' => 'cart',
          'label' => t('Cart entity'),
        ),
      ),
    ),
  );
}
