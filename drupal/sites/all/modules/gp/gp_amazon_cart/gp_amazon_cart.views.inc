<?php
/**
 * @file
 * Views related hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function gp_amazon_cart_views_data_alter(&$data) {
  if (empty($data['gp_amazon_cart']) || empty($data['gp_amazon_line_item'])) {
    // Our entities, for some strange reasons, aren't available.
    return;
  }

  // Shortcut for our for elements.
  $cart = &$data['gp_amazon_cart'];
  $line_item = &$data['gp_amazon_line_item'];

  // Cart uid field.
  $cart['user']['title'] = 'Owner';
  $cart['user']['field']['handler'] = 'views_handler_field_user';
  $cart['user']['filter']['handler'] = 'views_handler_filter_user_name';
  $cart['user']['relationship']['label'] = 'Owner';
  $cart['user']['relationship']['help'] = 'Relate the cart to the user that owns it.';

  // Cart status field.
  $cart['status']['field']['handler'] = 'gp_amazon_cart_handler_field_cart_status';
  $cart['status']['filter']['handler'] = 'views_handler_filter_in_operator';
  $cart['status']['filter']['options callback'] = 'gp_amazon_cart_cart_statuses';

  // Line item cid field.
  $line_item['cart']['title'] = 'Cart';
  $line_item['cart']['relationship']['help'] = 'Relate the line item to the cart.';
}
