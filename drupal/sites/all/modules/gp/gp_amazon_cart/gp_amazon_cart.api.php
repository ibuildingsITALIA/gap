<?php
/**
 * @file
 * Hooks provided by the gp_amazon_cart module.
 */

/**
 * Act on a cart that has been merged with Amazon.
 *
 * @param AmazonCart $cart
 *   The cart being merged.
 *
 * @param CartEntity $cart_entity
 *   The cart entity related.
 */
function hook_gp_amazon_cart_merged(AmazonCart $cart, CartEntity $cart_entity) {
  // Send mail.
}

/**
 * Act on an "one click buy" purchase of an item.
 *
 * @param AmazonItem $item
 *   The item being purchased..
 *
 * @param CartEntity $cart_entity
 *   The cart entity related.
 */
function hook_gp_amazon_cart_oneclickbuy(AmazonItem $item, CartEntity $cart_entity) {
  // Send mail.
}
