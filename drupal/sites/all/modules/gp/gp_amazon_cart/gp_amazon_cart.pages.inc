<?php
/**
 * @file
 * Page callbacks for the cart.
 */

/**
 * Page callback to render the shopping cart page.
 */
function gp_amazon_cart_view() {
  $cart = gp_amazon_cart_remote_cart_get();

  // If cart is empty, render an empty cart.
  if ($cart->isEmpty()) {
    return theme('gp_amazon_cart_empty', array());
  }

  // Set breadcrumb.
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = t('Cart');
  drupal_set_breadcrumb($breadcrumb);

  // Render form.
  module_load_include('inc', 'gp_amazon_cart', 'gp_amazon_cart.forms');
  return drupal_get_form('gp_amazon_cart_form', $cart);
}

/**
 * Page callback to submit products to Amazon.
 */
function gp_amazon_cart_merge_page() {
  // Get the current cart.
  $cart = gp_amazon_cart_remote_cart_get();

  // If cart is empty, go to homepage.
  if ($cart->isEmpty()) {
    drupal_goto();
  }

  // Persist this last cart to the database.
  gp_amazon_cart_save($cart);

  // Load cart entity from database.
  /* @var CartEntity $cart_entity */
  $cart_entity = gp_amazon_cart_load_by_cart_id($cart->getId());

  // If no cart entity found, quit to home.
  if (!$cart_entity) {
    drupal_goto();
  }

  // Set cart entity status to merge.
  $cart_entity->status = 'merge';
  $cart_entity->save();

  $purchase_url = $cart->getPurchaseUrl();

  // Allow modules to act on merge.
  module_invoke_all('gp_amazon_cart_merged', $cart, $cart_entity);
  if (module_exists('rules')) {
    rules_invoke_event('gp_amazon_cart_merged', $cart, $cart_entity);
  }

  // Delete current session cart.
  gp_amazon_cart_session_cart_delete();

  watchdog('giunti_amazon', 'Redirect to Amazon at %PurchaseUrl.', array(
    '%PurchaseUrl' => $purchase_url,
  ), WATCHDOG_NOTICE);

  drupal_goto($purchase_url);
}

/**
 * Page callback to purchase a product directly on Amazon.
 */
function gp_amazon_cart_oneclickbuy_page(AmazonItem $item) {
  global $user;

  // Prepare the price components defaults.
  $price_components = array(
    'Amount' => 0,
    'CurrencyCode' => 'EUR',
  );
  // Get components from item if possible.
  if ($item_components = $item->getPriceComponents()) {
    $price_components = array_merge($price_components, $item_components);
  }

  // We must track this possible purchase. We do that creating a new cart
  // entity, a single line item for this product and saving this cart as
  // merged. We can't use the normal cart functions as we want to keep the
  // current cart of the user intact.
  /* @var stdClass $cart_entity */
  $cart_entity = entity_create('cart', array());

  // Set cart values.
  $random_key = 'one-click-buy-' . REQUEST_TIME . mt_rand();
  $cart_entity->cart_id = $random_key;
  $cart_entity->hmac = $random_key;
  $cart_entity->url_encoded_hmac = $random_key;
  $cart_entity->subtag = gp_amazon_cart_generate_subtag($random_key, $random_key);
  $cart_entity->uid = $user->uid;
  $cart_entity->subtotal = $price_components['Amount'] / 100;
  $cart_entity->currency = $price_components['CurrencyCode'];
  $cart_entity->status = 'merge';

  // Save the entity.
  try {
    $cart_entity->save();
  }
  catch (Exception $e) {
    watchdog_exception('giunti_amazon', $e);
    throw $e;
  }

  // Create the corrisponding line item.
  $line_item_entity = entity_create('line_item', array());


  // Set line item values.
  $line_item_entity->cid = $cart_entity->cid;
  $line_item_entity->asin = $item->ASIN;
  $line_item_entity->label = $item->getTitle();
  $line_item_entity->quantity = 1;
  $line_item_entity->price = $cart_entity->subtotal;
  $line_item_entity->currency = $cart_entity->currency;
  $line_item_entity->total = $cart_entity->subtotal;

  // Save the entity.
  try {
    $line_item_entity->save();
  }
  catch (Exception $e) {
    watchdog_exception('giunti_amazon', $e);
    throw $e;
  }

  // Generate the detail page url with subtag.
  $detail_url = $item->DetailPageURL . '%26ascsubtag=' . $cart_entity->subtag;

  // Allow modules to act on one click buy.
  module_invoke_all('gp_amazon_cart_oneclickbuy', $item, $cart_entity);
  if (module_exists('rules')) {
    rules_invoke_event('gp_amazon_cart_oneclickbuy', $item, $cart_entity);
  }

  watchdog('giunti_amazon', 'Redirect to Amazon at %DetailUrl.', array(
    '%DetailUrl' => $detail_url,
  ), WATCHDOG_NOTICE);

  // Go to the amazon detail page.
  drupal_goto($detail_url);
}

/**
 * Page callback to render the "last added item in cart" page.
 */
function gp_amazon_cart_last_view() {
  $cart = gp_amazon_cart_remote_cart_get();

  // If cart is empty, render an empty cart.
  if ($cart->isEmpty()) {
    return theme('gp_amazon_cart_empty', array());
  }

  // Set breadcrumb.
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = t('Cart');
  drupal_set_breadcrumb($breadcrumb);

  // Render form.
  module_load_include('inc', 'gp_amazon_cart', 'gp_amazon_cart.forms');
  return drupal_get_form('gp_amazon_cart_form', $cart, TRUE);
}

