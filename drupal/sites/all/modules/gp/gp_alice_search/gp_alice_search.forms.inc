<?php
/**
 * @file
 * Form callbacks.
 */

/**
 * Search form callback.
 */
function gp_alice_search_form($form, &$form_state) {
  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_alice_search', 'gp_alice_search.forms');

  // Change method and disable token.
  $form['#method'] = 'get';
  $form['#token'] = FALSE;

  // Set action to our panel page.
  $form['#action'] = url('search');

  $form['keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#title_display' => 'invisible',
    '#size' => 100,
    '#attributes' => array(
      'title' => t('Search'),
    ),
    '#default_value' => isset($_GET['keywords']) ? $_GET['keywords'] : '',
  );

  $form['actions'] = array('#type' => 'actions', '#weight' => -20);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['#after_build'] = array('gp_alice_search_form_after_build');

  return $form;
}

/**
 * Form #after_build callback for gp_alice_search_search_form.
 */
function gp_alice_search_form_after_build($form, &$form_state) {
  // Prevent stuff from printing.
  $form['form_id']['#printed'] = TRUE;
  $form['form_build_id']['#printed'] = TRUE;
  $form['actions']['submit']['#name'] = '';

  return $form;
}
