<?php
/**
 * @file
 * Plugin to render items based on a configurable Amazon search.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Alice Search block'),
  'description' => t('Configurable Alice search block'),
  'category' => 'Giunti',
);

/**
 * Render the configured search pane.
 */
function gp_alice_search_alice_search_block_content_type_render($subtype, $conf, $panel_args, $context) {
  // Get search string for further processing.
  $search_string = $conf['search_string'];
  $sort_field = $conf['sort_field'];
  $sort_order = $conf['sort_order'];

  $parameters = unserialize($search_string);

  $name = 'alice_search_block';
  $display_id = 'alice_search_box';
  $view = views_get_view($name);
  if (!$view || !$view->access($display_id)) {
    return;
  }
  $view->is_cacheable = FALSE;

  // ToDo: set groups.
  // ToDo: start from current filtersgroup.

//  dpm($view->display_handler->get_option('filter_groups'));
  $groups = array(
    1 => 'AND',
    2 => 'OR',
  );

  foreach ($parameters as $key => $parameter) {
    if ('group' === $parameter['type']) {
      $value = $parameter['value'];
      $operator = $parameter['operator'];
      $groups[$value] = $operator;
    }
  }

  // Setting groups.
  $filter_groups = array(
    'operator' => 'AND',
    'groups' => $groups,
  );
  $view->display_handler->set_option('filter_groups', $filter_groups);

  // Setting sort filter.
  $sort = array();
  $sort[$sort_field] = array(
    'id' => $sort_field,
    'table' => 'search_api_index_books_index',
    'field' => $sort_field,
    'relationship' => 'none',
    'order' => $sort_order,
    'exposed' => FALSE,
  );
  $view->display_handler->set_option('sorts', $sort);

  // Adding filters.
  foreach ($parameters as $key => $parameter) {
    switch ($parameter['type']) {
      case 'title': $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'title',
          array(
            'operator' => $parameter['operator'],
            'value' => $parameter['value'],
            'group' => $parameter['group'],
          )
        );
        break;

      case 'author':
        $value = array();
        $terms = taxonomy_get_term_by_name($parameter['value']);
        foreach ($terms as $term) {
          $value[] = $term->tid;
        }

        $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'field_authors',
          array(
            'operator' => '=',
            'value' => $value,
            'group' => $parameter['group'],
          )
        );
        break;

      case 'publisher':
        $value = array();
        $terms = taxonomy_get_term_by_name($parameter['value']);
        foreach ($terms as $term) {
          $value[] = $term->tid;
        }

        $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'field_publisher',
          array(
            'operator' => $parameter['operator'],
            'value' => $value,
            'group' => $parameter['group'],
          )
        );
        break;

      case 'series_description': $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'field_series_description',
          array(
            'operator' => $parameter['operator'],
            'value' => $parameter['value'],
            'group' => $parameter['group'],
          )
        );
        break;

      case 'bic_subjects':
        $value = array();
        $terms = taxonomy_get_term_by_name($parameter['value']);
        foreach ($terms as $term) {
          $value[] = $term->tid;
        }

        $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'field_bic_subjects',
          array(
            'operator' => $parameter['operator'],
            'value' => $value,
            'group' => $parameter['group'],
          )
        );
        break;

      case 'bic_qualifiers':
        $value = array();
        $terms = taxonomy_get_term_by_name($parameter['value']);
        foreach ($terms as $term) {
          $value[] = $term->tid;
        }

        $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'field_bic_qualifiers',
          array(
            'operator' => $parameter['operator'],
            'value' => $value,
            'group' => $parameter['group'],
          )
        );
        break;

      case 'percentage_discount': $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'percentage_discount',
          array(
            'operator' => $parameter['operator'],
            'value' => $parameter['value'],
            'group' => $parameter['group'],
          )
        );
        break;

      case 'availability_state': $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'field_availability_state',
          array(
            'operator' => '=',
            'value' => $parameter['value'],
            'group' => $parameter['group'],
          )
        );
        break;

      default: $view->add_item(
          $view->current_display,
          'filter',
          'search_api_index_books_index',
          'title',
          array(
            'operator' => $parameter['operator'],
            'value' => $parameter['value'],
            'group' => $parameter['group'],
          )
        );
    }
  }

  $view_content = $view->preview($display_id);

  // Common settings for this block.
  $block = new stdClass();
  $block->title = t('Alice search block');
  $block->module = 'alice_search';
  $block->delta = REQUEST_TIME;
  $block->content = array(
    '#prefix' => '<div class="">',
    '#markup' => $view_content,
    '#suffix' => '</div>',
  );

  return $block;
}

/**
 * Form settings for alice_search.
 */
function gp_alice_search_alice_search_block_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['search_string'] = array(
    '#type' => 'textarea',
    '#title' => t('Search query string'),
    '#description' => t('Enter a valid query param string.'),
    '#default_value' => !empty($conf['search_string']) ? $conf['search_string'] : '',
    '#required' => TRUE,
  );

//  $form['max_results'] = array(
//    '#type' => 'select',
//    '#title' => t('Maximum results to show'),
//    '#options' => drupal_map_assoc(range(1, 10)),
//    '#default_value' => !empty($conf['max_results']) ? $conf['max_results'] : 10,
//    '#states' => array(
//      'visible' => array(
//        ':input[name="show_search_elements"]' => array('checked' => FALSE),
//      ),
//    ),
//  );

  $form['sort_field'] = array(
    '#type' => 'select',
    '#title' => t('Sort field'),
    '#options' => array(
      'title_for_sorting' => t('Title'),
      'field_release_date' => t('Release date'),
      'field_list_price' => t('List price'),
      'field_amazon_rank' => t('Amazon rank'),
    ),
    '#default_value' => !empty($conf['sort_field']) ? $conf['sort_field'] : 'title_for_sorting',
  );

  $form['sort_order'] = array(
    '#type' => 'select',
    '#title' => t('Sort order'),
    '#options' => array(
      'ASC' => t('ASC'),
      'DESC' => t('DESC'),
    ),
    '#default_value' => !empty($conf['sort_order']) ? $conf['sort_order'] : 'ASC',
  );

  return $form;
}
/**
 * Validate handler for settings form.
 */
function gp_alice_search_alice_search_block_content_type_edit_form_validate($form, &$form_state) {
}

/**
 * Submit handler to save settings.
 */
function gp_alice_search_alice_search_block_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['search_string'] = $form_state['values']['search_string'];
  $form_state['conf']['max_results'] = $form_state['values']['max_results'];
  $form_state['conf']['sort_field'] = $form_state['values']['sort_field'];
  $form_state['conf']['sort_order'] = $form_state['values']['sort_order'];
}
