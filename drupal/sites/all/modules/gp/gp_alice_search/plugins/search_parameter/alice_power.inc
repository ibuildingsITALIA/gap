<?php
/**
 * @file
 * Plugin definition for browsenode search parameter.
 */

$plugin = array(
  'title' => 'Alice Power',
);

/**
 * Returns the set of valid terms for a taxonomy field.
 *
 * @param $field
 *   The field definition.
 * @return
 *   The array of valid terms for this field, keyed by term id.
 */
function gp_alice_search_taxonomy_allowed_values($field) {
  $options = array();
  foreach ($field['settings']['allowed_values'] as $tree) {
    if ($vocabulary = taxonomy_vocabulary_machine_name_load($tree['vocabulary'])) {
      if ($terms = taxonomy_get_tree($vocabulary->vid, $tree['parent'])) {
        foreach ($terms as $term) {
          $custom_term = taxonomy_term_load($term->tid);
          $options[$term->tid] = '[' . $custom_term->field_id['und'][0]['value'] . '] ' . $term->name;
        }
      }
    }
  }
  return $options;
}

/**
 * Form callback for this parameter.
 */
function gp_alice_search_alice_power_parameter_form($form, &$form_state) {
  // Remember ajax form to include this file.
  form_load_include($form_state, 'inc', 'gp_alice_search', 'plugins/search_parameter/alice_power');

  drupal_add_css(drupal_get_path('module', 'gp_alice_search') . '/includes/gp_alice_search_style.css');

  $form['parameters']['Power'] = array(
    '#type' => 'fieldset',
    '#title' => t('Power', array(), array('context' => 'alice-search-parameters')),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#attributes' => array('id' => array('power-wrapper')),
    // Use different parents to prevent this group of fields
    // ending up in the parameters tree.
    '#parents' => array('power'),
  );

  // Shortcut to our form element.
  $container = &$form['parameters']['Power'];
  // And to our values.
  $values = !empty($form_state['values']['power']) ? $form_state['values']['power'] : array();
  // Also get all keywords available.
  $power_keywords = gp_alice_search_alice_power_parameter_keywords();

  // Add explanation of operators.
  $container['help'] = array(
    '#markup' => gp_alice_search_alice_power_parameter_help(),
  );

  // Present a select to add keywords.
  $container['choice'] = array(
    '#type' => 'select',
    '#title' => t('Add a field'),
    '#options' => $power_keywords,
  );

  $container['add_keyword'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#name' => 'op_add_keyword',
    '#submit' => array('gp_alice_search_alice_power_parameter_form_add_submit'),
    '#ajax' => array(
      'callback' => 'gp_alice_search_alice_power_parameter_form_add_callback',
      'wrapper' => 'power-wrapper',
    ),
  );

  // Generate value field and operator for every keyword already selected.
  $keywords = !empty($form_state['power']) ? $form_state['power'] : array();

  foreach ($keywords as $delta => $type) {
    $container[$delta] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => array($delta . '-wrapper'),
        'style' => 'border: 1px solid #ccc; padding: 10px; margin-bottom: 5px;',
      ),
    );
    $default_value = !empty($values['power'][$delta]['value']) ? $values['power'][$delta]['value'] : '';

    switch ($power_keywords[$type]) {
      case t('Group'):
        $container[$delta]['value'] = array(
          '#type' => 'select',
          '#options' => array(
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#title' => $power_keywords[$type],
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => drupal_map_assoc(array('AND', 'OR')),
          '#default_value' => $default_value,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('Keywords'):
        $container[$delta]['value'] = array(
          '#type' => 'textfield',
          '#title' => $power_keywords[$type],
          '#size' => 30,
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('Title'):
        $container[$delta]['value'] = array(
          '#type' => 'textfield',
          '#title' => $power_keywords[$type],
          '#size' => 30,
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('Author'):
        $vocabularies = taxonomy_vocabulary_machine_name_load('author');

        $container[$delta]['value'] = array(
          '#type' => 'textfield',
          '#title' => $power_keywords[$type],
          '#size' => 30,
          '#default_value' => $default_value,
          '#autocomplete_path' => 'admin/views/ajax/autocomplete/taxonomy/' . $vocabularies->vid,
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
            'empty' => 'Is empty',
            'not empty' => 'Is not empty',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('Publisher'):
        $vocabularies = taxonomy_vocabulary_machine_name_load('publisher');

        $container[$delta]['value'] = array(
          '#type' => 'textfield',
          '#title' => $power_keywords[$type],
          '#size' => 30,
          '#default_value' => $default_value,
          '#autocomplete_path' => 'admin/views/ajax/autocomplete/taxonomy/' . $vocabularies->vid,
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
            'empty' => 'Is empty',
            'not empty' => 'Is not empty',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('Series'):
        $container[$delta]['value'] = array(
          '#type' => 'textfield',
          '#title' => $power_keywords[$type],
          '#size' => 30,
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('BIC Subject'):
        $container[$delta]['value'] = array(
          '#type' => 'select',
          '#title' => $power_keywords[$type],
          '#multiple' => TRUE,
          '#options' => gp_alice_search_taxonomy_allowed_values(field_info_field('field_bic_subjects')),
          '#attributes' => array(
            'class' => array('chosen-widget'),
          ),
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
            'empty' => 'Is empty',
            'not empty' => 'Is not empty',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('BIC Qualifier'):
        $container[$delta]['value'] = array(
          '#type' => 'select',
          '#title' => $power_keywords[$type],
          '#multiple' => TRUE,
          '#options' => gp_alice_search_taxonomy_allowed_values(field_info_field('field_bic_qualifiers')),
          '#attributes' => array(
            'class' => array('chosen-widget'),
          ),
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
            'empty' => 'Is empty',
            'not empty' => 'Is not empty',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('Percentage Discount'):
        $container[$delta]['value'] = array(
          '#type' => 'textfield',
          '#title' => $power_keywords[$type],
          '#size' => 30,
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      case t('Availability State'):
        $container[$delta]['value'] = array(
          '#type' => 'select',
          '#title' => $power_keywords[$type],
          '#options' => array(
            'C' => 'in commercio',
            'E' => 'disponibilità limitata',
            'F' => 'fuori catalogo',
            'A' => 'prossima pubblicazione',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
        $container[$delta]['operator'] = array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => array(
            '=' => 'Is',
            '<>' => 'Is not',
          ),
          '#default_value' => $default_value,
        );

        $default_value = !empty($values['power'][$delta]['group']) ? $values['power'][$delta]['group'] : '';
        $container[$delta]['group'] = array(
          '#type' => 'select',
          '#title' => t('Group'),
          '#options' => array(
            2 => '-',
            3 => 'A',
            4 => 'B',
            5 => 'C',
            6 => 'D',
            7 => 'E',
            8 => 'F',
            9 => 'G',
            10 => 'H',
          ),
          '#default_value' => $default_value,
          // Show before the value field.
          '#weight' => -1,
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

        break;

      default:
        $container[$delta]['info'] = array(
          '#markup' => $power_keywords[$type] . ' - Block to implement!!!',
        );

        $container[$delta]['remove_keyword'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'op_remove_keyword',
          '#submit' => array('gp_alice_search_alice_power_parameter_form_remove_submit'),
          '#ajax' => array(
            'wrapper' => $delta . '-wrapper',
          ),
        );

    }
  }

  // Add our submit handler to the form generate button.
  // @todo add a validation callback to the plugin declaration.
  array_unshift(
    $form['actions']['submit']['#submit'],
    'gp_alice_search_alice_power_parameter_form_generate_submit'
  );

  return $form;
}

/**
 * Submit handler for the add keyword.
 */
function gp_alice_search_alice_power_parameter_form_add_submit($form, &$form_state) {
  // Add the submitted power parameter to list.
  $form_state['power'][] = $form_state['values']['power']['choice'];

  // Rebuild form.
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback for the add keyword.
 */
function gp_alice_search_alice_power_parameter_form_add_callback($form, &$form_state) {
  return $form['parameters']['Power'];
}

/**
 * Submit handler for the remove keyword.
 */
function gp_alice_search_alice_power_parameter_form_remove_submit($form, &$form_state) {
  $wrapper = $form_state['triggering_element']['#ajax']['wrapper'];
  $key = substr($wrapper, 0, strpos($wrapper, '-wrapper'));

  // Remove the submitted power parameter to list.
  unset($form_state['power'][$key]);

  // Rebuild form.
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the generate button.
 */
function gp_alice_search_alice_power_parameter_form_generate_submit($form, &$form_state) {
  // Get our power values.
  $values = !empty($form_state['values']['power']) ? $form_state['values']['power'] : array();
  // Remove non-keywords values.
  unset($values['choice'], $values['add_keyword']);

  // If values are empty, quit.
  if (empty($values)) {
    return;
  }

  $string = '';
  $object = array();

  // Loop items and generate the power string.
  foreach ($values as $delta => $data) {
    // If no value inserted, skip.
    if (empty($data['value'])) {
      continue;
    }

    // Get the type of this element.
    $type = $form_state['power'][$delta];

    // Add the operator if provided.
    if (isset($data['operator'])) {
      $string .= " {$data['operator']} ";
    }

    // Add the value.
    $string .= "{$type}:{$data['value']}";
    $object[] = array(
      'type' => $type,
      'value' => $data['value'],
      'operator' => $data['operator'],
      'group' => $data['group'],
    );
  }
  $string = serialize($object);

  // If string has been populated, add to values.
  if (strlen($string)) {
    $form_state['values']['parameters']['Power'] = $string;
  }
}

/**
 * Helper to return all keywords of the power search.
 */
function gp_alice_search_alice_power_parameter_keywords() {
  return array(
    'group' => t('Group', array(), array('context' => 'alice-search-power')),
    'keywords' => t('Keywords', array(), array('context' => 'alice-search-power')),
    'title' => t('Title', array(), array('context' => 'alice-search-power')),
    'author' => t('Author', array(), array('context' => 'alice-search-power')),
    'publisher' => t('Publisher', array(), array('context' => 'alice-search-power')),
    'series_description' => t('Series', array(), array('context' => 'alice-search-power')),
    'bic_subjects' => t('BIC Subject', array(), array('context' => 'alice-search-power')),
    'bic_qualifiers' => t('BIC Qualifier', array(), array('context' => 'alice-search-power')),
    'percentage_discount' => t('Percentage Discount', array(), array('context' => 'alice-search-power')),
    'availability_state' => t('Availability State', array(), array('context' => 'alice-search-power')),
  );
}

/**
 * Returns help information about Alice Power parameters.
 */
function gp_alice_search_alice_power_parameter_help() {
  $output = '<div>';

  $output .= t('add help message here');
  $output .= '</div>';

  return $output;
}
