<?php
/**
 * @file
 * Administration forms.
 */

/**
 * Form callback to create an advanced Alice search.
 */
function gp_alice_search_advanced_search_form($form, &$form_state) {
/*
//  $view = &$form_state['view'];
//  $display_id = $form_state['display_id'];
//  $type = $form_state['type'];

  $name = 'alice_search_block';
  $display_id = 'alice_search_box';
  $view = views_get_view($name);
  $type = 'filter';

  $types = views_object_types();
  if (!$view->set_display($display_id)) {
    views_ajax_render(t('Invalid display id @display', array('@display' => $display_id)));
  }
  $display = &$view->display[$display_id];
  $form['#title'] = check_plain($display->display_title) . ': ';
  $form['#title'] .= t('Rearrange @type', array('@type' => $types[$type]['ltitle']));
  $form['#section'] = $display_id . 'rearrange-item';

  if ($display->handler->defaultable_sections($types[$type]['plural'])) {
    $form_state['section'] = $types[$type]['plural'];
//    views_ui_standard_display_dropdown($form, $form_state, $form_state['section']);
  }

  if (!empty($view->form_cache)) {
    $groups = $view->form_cache['groups'];
    $handlers = $view->form_cache['handlers'];
  }
  else {
    $groups = $display->handler->get_option('filter_groups');
    $handlers = $display->handler->get_option($types[$type]['plural']);
  }
  $count = 0;

  // Get relationship labels
  $relationships = array();
  foreach ($display->handler->get_handlers('relationship') as $id => $handler) {
    $relationships[$id] = $handler->label();
  }

  $group_options = array();
*/
  /**
   * Filter groups is an array that contains:
   * array(
   *   'operator' => 'and' || 'or',
   *   'groups' => array(
   *     $group_id => 'and' || 'or',
   *   ),
   * );
   */

/*
  $grouping = count(array_keys($groups['groups'])) > 1;

  $form['filter_groups']['#tree'] = TRUE;
  $form['filter_groups']['operator'] = array(
    '#type' => 'select',
    '#options' => array (
      'AND' => t('And'),
      'OR' => t('Or'),
    ),
    '#default_value' => $groups['operator'],
    '#attributes' => array(
      'class' => array('warning-on-change'),
    ),
    '#title' => t('Operator to use on all groups'),
    '#description' => t('Either "group 0 AND group 1 AND group 2" or "group 0 OR group 1 OR group 2", etc'),
    '#access' => $grouping,
  );

  $form['remove_groups']['#tree'] = TRUE;

  foreach ($groups['groups'] as $id => $group) {
    $form['filter_groups']['groups'][$id] = array(
      '#title' => t('Operator'),
      '#type' => 'select',
      '#options' => array(
        'AND' => t('And'),
        'OR' => t('Or'),
      ),
      '#default_value' => $group,
      '#attributes' => array(
        'class' => array('warning-on-change'),
      ),
    );

    $form['remove_groups'][$id] = array(); // to prevent a notice
    if ($id != 1) {
      $form['remove_groups'][$id] = array(
        '#type' => 'submit',
        '#value' => t('Remove group @group', array('@group' => $id)),
        '#id' => "views-remove-group-$id",
        '#attributes' => array(
          'class' => array('views-remove-group'),
        ),
        '#group' => $id,
      );
    }
    $group_options[$id] = $id == 1 ? t('Default group') : t('Group @group', array('@group' => $id));
    $form['#group_renders'][$id] = array();
  }

  $form['#group_options'] = $group_options;
  $form['#groups'] = $groups;
  // We don't use get_handlers() because we want items without handlers to
  // appear and show up as 'broken' so that the user can see them.
  $form['filters'] = array('#tree' => TRUE);
  foreach ($handlers as $id => $field) {
    // If the group does not exist, move the filters to the default group.
    if (empty($field['group']) || empty($groups['groups'][$field['group']])) {
      $field['group'] = 1;
    }

    $handler = $display->handler->get_handler($type, $id);
    if ($grouping && $handler && !$handler->can_group()) {
      $field['group'] = 'ungroupable';
    }

    // If not grouping and the handler is set ungroupable, move it back to
    // the default group to prevent weird errors from having it be in its
    // own group:
    if (!$grouping && $field['group'] == 'ungroupable') {
      $field['group'] = 1;
    }

    // Place this item into the proper group for rendering.
    $form['#group_renders'][$field['group']][] = $id;

    $form['filters'][$id]['weight'] = array(
      '#type' => 'textfield',
      '#default_value' => ++$count,
      '#size' => 8,
    );
    $form['filters'][$id]['group'] = array(
      '#type' => 'select',
      '#options' => $group_options,
      '#default_value' => $field['group'],
      '#attributes' => array(
        'class' => array('views-region-select', 'views-region-' . $id),
      ),
      '#access' => $field['group'] !== 'ungroupable',
    );

    if ($handler) {
      $name = $handler->ui_name() . ' ' . $handler->admin_summary();
      if (!empty($field['relationship']) && !empty($relationships[$field['relationship']])) {
        $name = '(' . $relationships[$field['relationship']] . ') ' . $name;
      }

      $form['filters'][$id]['name'] = array(
        '#markup' => $name,
      );
    }
    else {
      $form['filters'][$id]['name'] = array('#markup' => t('Broken field @id', array('@id' => $id)));
    }
    $form['filters'][$id]['removed'] = array(
      '#type' => 'checkbox',
      '#id' => 'views-removed-' . $id,
      '#attributes' => array('class' => array('views-remove-checkbox')),
      '#default_value' => 0,
    );
  }

  if (isset($form_state['update_name'])) {
    $name = $form_state['update_name'];
  }

//  views_ui_standard_form_buttons($form, $form_state, 'views_ui_rearrange_filter_form');
  $form['buttons']['add_group'] = array(
    '#type' => 'submit',
    '#value' => t('Create new filter group'),
    '#id' => 'views-add-group',
    '#group' => 'add',
  );

  return $form;
*/


  // Shortcut to access submitted values.
  $values = isset($form_state['values']) ? $form_state['values'] : array();

  // Retrieve all parameters handlers.
  ctools_include('plugins');
  $plugins = ctools_get_plugins('gp_alice_search', 'search_parameter');

  // Add the form actions now, as they will be available to plugins
  // for adding validate/submit handlers on generate button.
  // They won't cause problems as they are rendered at the bottom of the form.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate'),
    '#submit' => array(
      'gp_alice_search_advanced_search_form_rebuild',
      'gp_alice_search_advanced_search_form_generate_submit',
    ),
  );

  // Get all parameters available for this index.
  $available_parameters = array(
    'Alice_Power' => 'Alice_Power',
  );
  // Prepare a wrapper.
  $form['parameters'] = array(
    '#tree' => TRUE,
    '#type' => 'container',
    '#attributes' => array('id' => array('parameters-wrapper')),
  );
  foreach ($available_parameters as $name => $label) {
    // Plugin names don't use capital letters.
    $candidate_plugin = strtolower($name);

    // If there's a plugin that handles this type, use it.
    if (isset($plugins[$candidate_plugin]) && $function = ctools_plugin_get_function($plugins[$candidate_plugin], 'form callback')) {
      $form = $function($form, $form_state);
    }
    // Otherwise fallback as text field.
    else {
      $form['parameters'][$name] = array(
        '#type' => 'textfield',
        '#title' => t($label, array(), array('context' => 'alice-search-parameters')),
        '#default_value' => !empty($values['parameters'][$name]) ? $values['parameters'][$name] : '',
      );
    }
  }

  // Retrieve search string and url from form_state.
  $search_string = !empty($values['generated']['string']) ? $values['generated']['string'] : '';
  $search_url = !empty($values['generated']['url']) ? $values['generated']['url'] : '';

  // Add a container to easily return rendered searches.
  $form['generated'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => array('generated-wrapper')),
    '#tree' => TRUE,
  );

  $form['generated']['string'] = array(
    '#type' => 'textarea',
    '#title' => t('Search string'),
    '#size' => 150,
    '#value' => $search_string,
    '#attributes' => array(
      'readonly' => 'readonly',
    ),
  );

  if ($search_url) {
    $form['generated']['url']['#description'] = t('You can preview the search <a href="@url" target="_blank">here</a>.', array(
      '@url' => $search_url,
    ));
  }

  // Add the ajax capabilities to the main submit button.
  $form['actions']['submit']['#ajax'] = array(
    'callback' => 'gp_alice_search_advanced_search_form_generate_callback',
    'wrapper' => 'generated-wrapper',
  );

  return $form;
}

/**
 * Submit to rebuild the gp_amazon_search_advanced_search_form form.
 */
function gp_alice_search_advanced_search_form_rebuild($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback to render the generated string.
 */
function gp_alice_search_advanced_search_form_generate_callback($form, &$form_state) {
  return $form['generated'];
}

/**
 * Submit handler for gp_alice_search_advanced_search_form.
 */
function gp_alice_search_advanced_search_form_generate_submit($form, &$form_state) {
  $values = $form_state['values'];

  // Generate search string and search url from submitted values.
  $search_string = '';
  if (!empty($values['parameters']) && $parameters = array_filter($values['parameters'])) {
    $search_string = gp_alice_search_implode_parameters($parameters);
  }

  // Set values in the form state.
  form_set_value($form['generated']['string'], $search_string, $form_state);
}
