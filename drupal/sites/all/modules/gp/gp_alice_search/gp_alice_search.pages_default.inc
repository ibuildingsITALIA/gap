<?php
/**
 * @file
 * gp_alice_search.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function gp_alice_search_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'advanced_alice_book_search';
  $page->task = 'page';
  $page->admin_title = 'Ricerca avanzata libri';
  $page->admin_description = '';
  $page->path = 'ricerca-avanzata';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_advanced_alice_book_search_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'advanced_alice_book_search';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Ricerca avanzata libri',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'Amazon BrowseNode',
        'keyword' => 'browsenode',
        'name' => 'browsenode',
        'browsenode_id' => '411663031',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'left-sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Ricerca avanzata libri';
  $display->uuid = '40df0ce7-d9ba-45ba-b13f-5505366c0c64';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2f620f20-a5ab-449e-b12a-25ed29c47f76';
    $pane->panel = 'content';
    $pane->type = 'views_panes';
    $pane->subtype = 'alice_search-advanced_alice_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'items_per_page' => '10',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2f620f20-a5ab-449e-b12a-25ed29c47f76';
    $display->content['new-2f620f20-a5ab-449e-b12a-25ed29c47f76'] = $pane;
    $display->panels['content'][0] = 'new-2f620f20-a5ab-449e-b12a-25ed29c47f76';
    $pane = new stdClass();
    $pane->pid = 'new-c12736aa-9f6d-4447-9ee8-63e256f6f2be';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-gp-menu-block-default';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c12736aa-9f6d-4447-9ee8-63e256f6f2be';
    $display->content['new-c12736aa-9f6d-4447-9ee8-63e256f6f2be'] = $pane;
    $display->panels['sidebar'][0] = 'new-c12736aa-9f6d-4447-9ee8-63e256f6f2be';
    $pane = new stdClass();
    $pane->pid = 'new-496d0066-4d56-44a9-9cc2-b92bbb995fc1';
    $pane->panel = 'sidebar';
    $pane->type = 'browsenode_nav';
    $pane->subtype = 'browsenode_nav';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'context_browsenode_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '496d0066-4d56-44a9-9cc2-b92bbb995fc1';
    $display->content['new-496d0066-4d56-44a9-9cc2-b92bbb995fc1'] = $pane;
    $display->panels['sidebar'][1] = 'new-496d0066-4d56-44a9-9cc2-b92bbb995fc1';
    $pane = new stdClass();
    $pane->pid = 'new-f89adab9-2b8b-4af4-aa43-4b87dbac2ae4';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-sidebar-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'f89adab9-2b8b-4af4-aa43-4b87dbac2ae4';
    $display->content['new-f89adab9-2b8b-4af4-aa43-4b87dbac2ae4'] = $pane;
    $display->panels['sidebar'][2] = 'new-f89adab9-2b8b-4af4-aa43-4b87dbac2ae4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-2f620f20-a5ab-449e-b12a-25ed29c47f76';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['advanced_alice_book_search'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'alice_search_page';
  $page->task = 'page';
  $page->admin_title = 'Alice search page';
  $page->admin_description = '';
  $page->path = 'book_search';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_alice_search_page_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'alice_search_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Ricerca Alice',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'Amazon BrowseNode',
        'keyword' => 'browsenode',
        'name' => 'browsenode',
        'browsenode_id' => '411663031',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'left-sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'f74ffa26-0ff3-477e-aeb1-95c92bd7b96b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5e53beba-c9ba-4a33-b800-9ce49eaea0b4';
    $pane->panel = 'content';
    $pane->type = 'views_panes';
    $pane->subtype = 'alice_search-alice_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'items_per_page' => '10',
      'exposed' => array(
        'keywords' => '',
        'field_bic_subjects_op' => '=',
        'bic_subjects' => array(),
        'field_bic_qualifiers_op' => '=',
        'bic_qualifiers' => array(),
        'discount' => '',
        'field_is_available' => 'All',
        'field_availability_state_op' => '=',
        'field_availability_state' => array(),
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5e53beba-c9ba-4a33-b800-9ce49eaea0b4';
    $display->content['new-5e53beba-c9ba-4a33-b800-9ce49eaea0b4'] = $pane;
    $display->panels['content'][0] = 'new-5e53beba-c9ba-4a33-b800-9ce49eaea0b4';
    $pane = new stdClass();
    $pane->pid = 'new-93fc0e3b-80ca-4273-9313-0e0ba818e923';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-gp-menu-block-default';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '93fc0e3b-80ca-4273-9313-0e0ba818e923';
    $display->content['new-93fc0e3b-80ca-4273-9313-0e0ba818e923'] = $pane;
    $display->panels['sidebar'][0] = 'new-93fc0e3b-80ca-4273-9313-0e0ba818e923';
    $pane = new stdClass();
    $pane->pid = 'new-ebbb0e9a-c3cd-4d5a-9cff-f0c848cb7c64';
    $pane->panel = 'sidebar';
    $pane->type = 'browsenode_nav';
    $pane->subtype = 'browsenode_nav';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'context_browsenode_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ebbb0e9a-c3cd-4d5a-9cff-f0c848cb7c64';
    $display->content['new-ebbb0e9a-c3cd-4d5a-9cff-f0c848cb7c64'] = $pane;
    $display->panels['sidebar'][1] = 'new-ebbb0e9a-c3cd-4d5a-9cff-f0c848cb7c64';
    $pane = new stdClass();
    $pane->pid = 'new-086a7545-ff85-4011-b1d6-e6ea7a1aeb1f';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-sidebar-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '086a7545-ff85-4011-b1d6-e6ea7a1aeb1f';
    $display->content['new-086a7545-ff85-4011-b1d6-e6ea7a1aeb1f'] = $pane;
    $display->panels['sidebar'][2] = 'new-086a7545-ff85-4011-b1d6-e6ea7a1aeb1f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-5e53beba-c9ba-4a33-b800-9ce49eaea0b4';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['alice_search_page'] = $page;

  return $pages;

}
