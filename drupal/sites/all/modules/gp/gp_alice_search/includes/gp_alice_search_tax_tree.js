// Set up a static varibale to remeber the status of the non collapsed.
var $lastOpen = [];
(function ($) {
  Drupal.behaviors.betterExposedFiltersExpandCollapseNested = {
    attach: function (context, settings) {
      var $expander = $('<span class="expander">+</span>');
      // Collapse all.
      $('ul.bef-tree-child > li:has(ul) .form-item').addClass('collapsed');
      // But don't collapse the previous opened.
      if ($lastOpen != 0) {
        $('ul.bef-tree-child > li:has(ul) > div.form-item').each(function (index) {
          $checklist = $(this).attr("class");
          for (var k = 0; k < $lastOpen.length; k++) {
            if ($checklist.indexOf($lastOpen[k]) != -1) {
              if ($checklist.indexOf('collapsed') != -1) {
                $(this).removeClass('collapsed');
              }
            }
          }
        });
      }
      // Give every list parent an expander and change the cursor *of course.
      $('ul.bef-tree-child > li:has(ul) > div.form-item:not(:has(span))').prepend($expander);
      $('.expander').css('cursor', 'pointer');

      // Collapse all on the first pageview.
      $('div.collapsed').next('ul').css('display', 'none');

      // Make it expand on click (only once).
      $('.expander').once($(this), function () {
        $(this).click(function () {
          if ($(this).parent().hasClass('collapsed')) {
            $(this).text('-');
          }
          else {
            $(this).text('+');
          }
          $(this).parent().toggleClass('collapsed');
          // Remember the unfolded for the next ajax call (tid is used for identification).
          var $list = $(this).parent().attr("class");
          var classList = $list.split(' ');
          for (var i = 0; i < classList.length; i++) {
            if (classList[i].indexOf('tid') != -1) {
              if ($lastOpen != null) {
                if ($list.indexOf('collapsed') != -1) {
                  var j = $.inArray(classList[i], $lastOpen)
                  if (j >= 0) {
                    $lastOpen.splice(j, 1);
                  }
                } else {
                  $lastOpen.push(classList[i]);
                }
              } else {
                $lastOpen.push(classList[i]);
              }
            }
          }
          $(this).parents('li:first').children('ul').slideToggle('fast');
        });
      });

      // Select/unselect all childs.
      $('.form-type-bef-checkbox > input').once($(this), function() {
        $(this).click(function() {
          if ($(this).parent().hasClass('highlight')) {
            // Checking all child.
            $(this).parent().next().find('.form-type-bef-checkbox > input').prop('checked', true);
          }
          else {
            // Unchecking all child.
            $(this).parent().next().find('.form-type-bef-checkbox > input').prop('checked', false);
          }
        });
      });

    }
  }

})(jQuery);
