<?php
/**
 * @file
 * gp_alice_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gp_alice_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function gp_alice_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function gp_alice_search_image_default_styles() {
  $styles = array();

  // Exported image style: book_thumb.
  $styles['book_thumb'] = array(
    'name' => 'book_thumb',
    'label' => 'Book thumb (149x170)',
    'effects' => array(
      6 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 149,
          'height' => 170,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_default_search_api_index().
 */
function gp_alice_search_default_search_api_index() {
  $items = array();
  $items['books_index'] = entity_import('search_api_index', '{
    "name" : "Books index",
    "machine_name" : "books_index",
    "description" : null,
    "server" : "solr_server",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "body:value" : { "type" : "text" },
        "created" : { "type" : "date" },
        "field_amazon_rank" : { "type" : "integer" },
        "field_article_sector" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_asin" : { "type" : "string" },
        "field_authors" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_authors:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_availability_state" : { "type" : "string" },
        "field_bic_qualifiers" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_bic_subjects" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_edition_date" : { "type" : "text" },
        "field_format" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_is_available" : { "type" : "boolean" },
        "field_isbn13" : { "type" : "text" },
        "field_list_price" : { "type" : "decimal" },
        "field_publisher" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_publisher:name" : { "type" : "text", "boost" : "0.5" },
        "field_release_date" : { "type" : "date" },
        "field_series" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_series:name" : { "type" : "list\\u003Ctext\\u003E", "boost" : "0.8" },
        "field_series_code" : { "type" : "text" },
        "field_series_description" : { "type" : "text", "boost" : "0.8" },
        "nid" : { "type" : "integer" },
        "percentage_discount" : { "type" : "string" },
        "publication_year" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "integer" },
        "title" : { "type" : "text", "boost" : "3.0" },
        "title_for_sorting" : { "type" : "string" },
        "type" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "book" : "book" } }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "field_isbn13" : true, "field_asin" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "field_isbn13" : true, "field_asin" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : { "title" : true, "field_isbn13" : true, "field_asin" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "field_isbn13" : true, "field_asin" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "field_isbn13" : true, "field_asin" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_sort().
 */
function gp_alice_search_default_search_api_sort() {
  $items = array();
  $items['books_index__field_list_price'] = entity_import('search_api_sort', '{
    "index_id" : "books_index",
    "field" : "field_list_price",
    "name" : "Prezzo",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "books_index__field_list_price",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "asc",
    "options" : { "field_name" : "Prezzo di listino" }
  }');
  $items['books_index__search_api_relevance'] = entity_import('search_api_sort', '{
    "index_id" : "books_index",
    "field" : "search_api_relevance",
    "name" : "Rilevanza",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "books_index__search_api_relevance",
    "default_sort" : "0",
    "default_sort_no_terms" : "1",
    "default_order" : "desc",
    "options" : { "field_name" : "search_api_relevance" }
  }');
  $items['books_index__title_for_sorting'] = entity_import('search_api_sort', '{
    "index_id" : "books_index",
    "field" : "title_for_sorting",
    "name" : "Titolo",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "books_index__title_for_sorting",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Title for sorting purposes" }
  }');
  return $items;
}
