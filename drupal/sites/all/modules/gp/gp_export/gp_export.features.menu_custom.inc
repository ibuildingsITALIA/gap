<?php
/**
 * @file
 * gp_export.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function gp_export_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-categories.
  $menus['menu-categories'] = array(
    'menu_name' => 'menu-categories',
    'title' => 'Categories',
    'description' => 'Links for the Amazon search indexes to show.',
  );
  // Exported menu: menu-footer-menu.
  $menus['menu-footer-menu'] = array(
    'menu_name' => 'menu-footer-menu',
    'title' => 'Footer menu',
    'description' => 'Links for the footer.',
  );
  // Exported menu: menu-giunti-network.
  $menus['menu-giunti-network'] = array(
    'menu_name' => 'menu-giunti-network',
    'title' => 'Giunti Network',
    'description' => 'Collezione di link al network Giunti.',
  );
  // Exported menu: menu-gp-menu-bar-default.
  $menus['menu-gp-menu-bar-default'] = array(
    'menu_name' => 'menu-gp-menu-bar-default',
    'title' => 'Barra menu default',
    'description' => '',
  );
  // Exported menu: menu-gp-menu-block-default.
  $menus['menu-gp-menu-block-default'] = array(
    'menu_name' => 'menu-gp-menu-block-default',
    'title' => 'Blocco menu default',
    'description' => '',
  );
  // Exported menu: menu-sidebar-menu.
  $menus['menu-sidebar-menu'] = array(
    'menu_name' => 'menu-sidebar-menu',
    'title' => 'Sidebar menu',
    'description' => 'Contiene i link per il menu da visualizzare nella barra laterale per i browsenode.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Barra menu default');
  t('Blocco menu default');
  t('Categories');
  t('Collezione di link al network Giunti.');
  t('Contiene i link per il menu da visualizzare nella barra laterale per i browsenode.');
  t('Footer menu');
  t('Giunti Network');
  t('Links for the Amazon search indexes to show.');
  t('Links for the footer.');
  t('Sidebar menu');


  return $menus;
}
