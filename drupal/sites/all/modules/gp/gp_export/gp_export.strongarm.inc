<?php
/**
 * @file
 * gp_export.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gp_export_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_page';
  $strongarm->value = 0;
  $export['comment_anonymous_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_page';
  $strongarm->value = 1;
  $export['comment_default_mode_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_page';
  $strongarm->value = '50';
  $export['comment_default_per_page_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_page';
  $strongarm->value = 1;
  $export['comment_form_location_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_page';
  $strongarm->value = '0';
  $export['comment_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_page';
  $strongarm->value = '1';
  $export['comment_preview_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_page';
  $strongarm->value = 1;
  $export['comment_subject_field_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_big_number';
  $strongarm->value = 'l \\<\\s\\p\\a\\n\\>j\\<\\/\\s\\p\\a\\n\\> F';
  $export['date_format_big_number'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'eu_cookie_compliance_domain';
  $strongarm->value = '';
  $export['eu_cookie_compliance_domain'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'eu_cookie_compliance_en';
  $strongarm->value = array(
    'popup_enabled' => 1,
    'popup_clicking_confirmation' => 0,
    'popup_position' => 0,
    'popup_agree_button_message' => 'OK',
    'popup_disagree_button_message' => 'No, dammi maggiori informazioni',
    'popup_info' => array(
      'value' => '<h2>Giunti al Punto utilizza i cookies per offrirti una navigazione e un servizio migliori.</h2>
',
      'format' => 'full_html',
    ),
    'popup_agreed_enabled' => 0,
    'popup_hide_agreed' => 0,
    'popup_find_more_button_message' => 'More info',
    'popup_hide_button_message' => 'Hide',
    'popup_agreed' => array(
      'value' => '<h2>Thank you for accepting cookies</h2>

<p>You can now hide this message or find out more about cookies.</p>
',
      'format' => 'filtered_html',
    ),
    'popup_link' => 'node/2',
    'popup_link_new_window' => 1,
    'popup_height' => '',
    'popup_width' => '100%',
    'popup_delay' => '1',
    'popup_bg_hex' => 'e74d22',
    'popup_text_hex' => 'ffffff',
    'domains_option' => '1',
    'domains_list' => '',
    'exclude_paths' => 'admin/*',
  );
  $export['eu_cookie_compliance_en'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'eu_cookie_compliance_it';
  $strongarm->value = array(
    'popup_enabled' => 1,
    'popup_clicking_confirmation' => 0,
    'popup_position' => 0,
    'popup_agree_button_message' => 'OK',
    'popup_disagree_button_message' => 'No, dammi maggiori informazioni',
    'popup_info' => array(
      'value' => '<h2>Giunti al Punto utilizza i cookies per offrirti una navigazione e un servizio migliori.</h2>
',
      'format' => 'full_html',
    ),
    'popup_agreed_enabled' => 0,
    'popup_hide_agreed' => 0,
    'popup_find_more_button_message' => 'More info',
    'popup_hide_button_message' => 'Hide',
    'popup_agreed' => array(
      'value' => '<h2>Thank you for accepting cookies</h2>

<p>You can now hide this message or find out more about cookies.</p>
',
      'format' => 'filtered_html',
    ),
    'popup_link' => 'node/2',
    'popup_link_new_window' => 1,
    'popup_height' => '',
    'popup_width' => '100%',
    'popup_delay' => '1',
    'popup_bg_hex' => 'e74d22',
    'popup_text_hex' => 'ffffff',
    'domains_option' => '1',
    'domains_list' => '',
    'exclude_paths' => 'admin/*',
  );
  $export['eu_cookie_compliance_it'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__article';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'image' => array(
        'custom_settings' => FALSE,
      ),
      'detail' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__article'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__page';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'image' => array(
        'custom_settings' => FALSE,
      ),
      'detail' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gp_card_discount_per_point';
  $strongarm->value = '0.2';
  $export['gp_card_discount_per_point'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gp_card_email_merge_books_body';
  $strongarm->value = 'Salve [user:im:fullname],

grazie per aver scelto Giuntialpunto.it per effettuare il tuo acquisto.

Il tuo ordine è passato in gestione ad Amazon.it che si occuperà dell’incasso e della spedizione.

Portando a termine questo acquisto otterrai !points che verranno accreditati sulla tua GiuntiCard solo dopo che Amazon avrà spedito il tuo ordine.

A seguito della spedizione riceverai una nuova mail che ti comunicherà il numero esatto di punti che ti saranno realmente accreditati. Entro 48 ore dalla spedizione i punti saranno presenti sulla 
tua GiuntiCard.

Nel caso il tuo ordine di libri venisse spedito in momenti diversi (succede quando non tutti i libri sono immediatamente disponibili) i punti ti verranno accreditati in momenti diversi.

Ti ricordiamo che i punti GiuntiCard si maturano solo sull’acquisto di libri ed eBook sulla base di 1 punto ogni 3 euro di spesa mentre le altre categorie merceologiche non fanno maturare punti.

Attenzione! 
Se modifichi il tuo ordine su Amazon.it (aggiungendo nuovi libri o togliendo quelli che avevi inserito) i punti GiuntiCard potrebbero non corrispondere a quanto riportato sopra ma saranno calcolati sulla base del nuovo acquisto.

Se hai acquistato eBook, i punti ti verranno accreditati entro 48 ore dall’acquisto. 

I dati della tua GiuntiCard:
Cod. EAN [user:giunti_card:ean]
Password: [user:giunti_card:password]
Scadenza [user:giunti_card:expiration:custom:d/m/Y]
Saldo Punti attuale [user:giunti_card:balance]

Per qualsiasi dubbio scrivici all’indirizzo info@giuntialpunto.it . 

Grazie ancora, ti aspettiamo presto in libreria.

Un saluto dallo staff
Giunti al Punto';
  $export['gp_card_email_merge_books_body'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gp_card_email_merge_books_subject';
  $strongarm->value = 'Giunti al Punto: grazie per aver ordinato da noi!';
  $export['gp_card_email_merge_books_subject'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gp_card_email_oneclickbuy_ebooks_body';
  $strongarm->value = 'Salve [user:im:fullname],

grazie per aver scelto Giuntialpunto.it per effettuare il tuo acquisto di ebook.

Il tuo ordine è passato in gestione ad Amazon.it che si occuperà dell’incasso e dell’invio verso il tuo device.

Portando a termine questo acquisto otterrai !points che verranno accreditati sulla tua GiuntiCard solo dopo che Amazon avrà spedito il tuo ordine.

A seguito della spedizione riceverai una nuova mail che ti comunicherà il numero esatto di punti che ti saranno realmente accreditati. Con questo acquisto di ebook, entro 48 ore dall’ordine i punti saranno presenti sulla tua GiuntiCard.

Ti ricordiamo che i punti GiuntiCard si maturano solo sull’acquisto di libri ed eBook sulla base di 1 punto ogni 3 euro di spesa mentre le altre categorie merceologiche non fanno maturare punti.

Attenzione! 
Se modifichi il tuo ordine su Amazon.it (aggiungendo un nuovo ebook invece di quello che avevi inserito) i punti GiuntiCard potrebbero non corrispondere a quanto riportato sopra ma saranno calcolati sulla base del nuovo acquisto.

I dati della tua GiuntiCard:
Cod. EAN [user:giunti_card:ean]
Password: [user:giunti_card:password]
Scadenza [user:giunti_card:expiration:custom:d/m/Y]
Saldo Punti attuale [user:giunti_card:balance]

Per qualsiasi dubbio scrivici all’indirizzo info@giuntialpunto.it . 

Grazie ancora, ti aspettiamo presto in libreria.

Un saluto dallo staff
Giunti al Punto';
  $export['gp_card_email_oneclickbuy_ebooks_body'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gp_card_email_oneclickbuy_ebooks_subject';
  $strongarm->value = 'Giunti al Punto: grazie per aver ordinato da noi!';
  $export['gp_card_email_oneclickbuy_ebooks_subject'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gp_card_email_points_earned_body';
  $strongarm->value = 'Salve [user:im:fullname],
ti ringraziamo per il tuo recente acquisto su Giuntialpunto.it con Amazon.

Grazie a questo acquisto hai guadagnato !points GiuntiCard, che ti permetteranno di avere tanti sconti per il tuo shopping nelle librerie Giunti al Punto.

Il tuo nuovo saldo punti:
Cod. EAN [user:giunti_card:ean]
Password: [user:giunti_card:password]
Scadenza: [user:giunti_card:expiration:custom:d/m/Y]
Nuovo saldo punti: !new_card_balance

Puoi controllare in ogni momento la scadenza della tua GiuntiCard, verificare i punti accumulati o modificare i tuoi dati collegandoti a www.giuntialpunto.it/giunticard .

I punti accumulati danno diritto a Buoni Sconto che possono essere utilizzati per acquistare qualsiasi libro o prodotto editoriale presente nelle nostre librerie Giunti al Punto.

Attenzione, i Buoni Sconto non possono essere spesi su Giuntialpunto.it ma solo nelle librerie fisiche.

Se hai già raggiunto la soglia di almeno 25 punti e vuoi utilizzare il tuo Buono Sconto, ma non hai la GiuntiCard fisica oppure non hai uno smartphone con installata la app “GiuntiCard”, stampa il pdf della tua GiuntiCard e presentalo alla cassa della libreria Giunti al Punto prima di pagare.

Torna presto a trovarci su Giuntialpunto.it per accumulare tanti altri punti GiuntiCard!

A questo punto non ti resta che venire subito in libreria. Ti aspettiamo! 

Un saluto dallo staff
Giunti al Punto 

Per qualsiasi dubbio scrivici all’indirizzo info@giuntialpunto.it .';
  $export['gp_card_email_points_earned_body'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gp_card_email_points_earned_subject';
  $strongarm->value = 'Giunti al Punto: accredito punti GiuntiCard';
  $export['gp_card_email_points_earned_subject'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gp_card_euro_per_point';
  $strongarm->value = '3';
  $export['gp_card_euro_per_point'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imce_profiles';
  $strongarm->value = array(
    1 => array(
      'name' => 'User-1',
      'usertab' => 1,
      'filesize' => '0',
      'quota' => '0',
      'tuquota' => '0',
      'extensions' => '*',
      'dimensions' => '1200x1200',
      'filenum' => '0',
      'directories' => array(
        0 => array(
          'name' => 'uploads',
          'subnav' => 1,
          'browse' => 1,
          'upload' => 1,
          'thumb' => 1,
          'delete' => 1,
          'resize' => 1,
        ),
      ),
      'thumbnails' => array(
        0 => array(
          'name' => 'Small',
          'dimensions' => '90x90',
          'prefix' => 'small_',
          'suffix' => '',
        ),
        1 => array(
          'name' => 'Medium',
          'dimensions' => '120x120',
          'prefix' => 'medium_',
          'suffix' => '',
        ),
        2 => array(
          'name' => 'Large',
          'dimensions' => '180x180',
          'prefix' => 'large_',
          'suffix' => '',
        ),
      ),
    ),
    2 => array(
      'name' => 'Sample profile',
      'usertab' => 1,
      'filesize' => 1,
      'quota' => 2,
      'tuquota' => 0,
      'extensions' => 'gif png jpg jpeg',
      'dimensions' => '800x600',
      'filenum' => 1,
      'directories' => array(
        0 => array(
          'name' => 'u%uid',
          'subnav' => 0,
          'browse' => 1,
          'upload' => 1,
          'thumb' => 1,
          'delete' => 0,
          'resize' => 0,
        ),
      ),
      'thumbnails' => array(
        0 => array(
          'name' => 'Thumb',
          'dimensions' => '90x90',
          'prefix' => 'thumb_',
          'suffix' => '',
        ),
      ),
    ),
    3 => array(
      'name' => 'Manager profile',
      'usertab' => 1,
      'filesize' => '0',
      'quota' => '0',
      'tuquota' => '0',
      'extensions' => '*',
      'dimensions' => '1200x1200',
      'filenum' => '0',
      'directories' => array(
        0 => array(
          'name' => 'uploads',
          'subnav' => 1,
          'browse' => 1,
          'upload' => 1,
          'thumb' => 1,
          'delete' => 1,
          'resize' => 1,
        ),
      ),
      'thumbnails' => array(
        0 => array(
          'name' => 'Small',
          'dimensions' => '90x90',
          'prefix' => 'small_',
          'suffix' => '',
        ),
        1 => array(
          'name' => 'Medium',
          'dimensions' => '120x120',
          'prefix' => 'medium_',
          'suffix' => '',
        ),
        2 => array(
          'name' => 'Large',
          'dimensions' => '180x180',
          'prefix' => 'large_',
          'suffix' => '',
        ),
      ),
    ),
  );
  $export['imce_profiles'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imce_roles_profiles';
  $strongarm->value = array(
    52310416 => array(
      'weight' => '0',
      's3_pid' => '3',
      'public_pid' => '3',
      'private_pid' => 0,
    ),
    30037204 => array(
      'weight' => '0',
      's3_pid' => 0,
      'public_pid' => 0,
      'private_pid' => 0,
    ),
    2 => array(
      'weight' => 11,
      's3_pid' => 0,
      'public_pid' => 0,
      'private_pid' => 0,
    ),
    1 => array(
      'weight' => 12,
      's3_pid' => 0,
      'public_pid' => 0,
      'private_pid' => 0,
    ),
  );
  $export['imce_roles_profiles'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imce_settings_absurls';
  $strongarm->value = 0;
  $export['imce_settings_absurls'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imce_settings_disable_private';
  $strongarm->value = 1;
  $export['imce_settings_disable_private'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imce_settings_replace';
  $strongarm->value = '0';
  $export['imce_settings_replace'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imce_settings_textarea';
  $strongarm->value = '';
  $export['imce_settings_textarea'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imce_settings_thumb_method';
  $strongarm->value = 'scale_and_crop';
  $export['imce_settings_thumb_method'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_page';
  $strongarm->value = array(
    0 => 'menu-footer-menu',
    1 => 'main-menu',
  );
  $export['menu_options_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_page';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_page';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_page';
  $strongarm->value = '1';
  $export['node_preview_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_page';
  $strongarm->value = 0;
  $export['node_submitted_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_mail_password_reset_body';
  $strongarm->value = 'Salve
abbiamo ricevuto la richiesta di reimpostare la password %source%

Se hai fatto tu questa richiesta, clicca sul link qui sotto ed inserisci una nuova password. Se non hai fatto tu la richiesta, ignora questa email.
 
Link %reset-pwd-url%
 
Se hai altre domande sulla reimpostazione della tua password, scrivici a info@giuntialpunto.it  
 
Cordiali saluti
Lo staff Giunti al Punto';
  $export['user_mail_password_reset_body'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_mail_password_reset_subject';
  $strongarm->value = 'Reimposta la tua password Giunti al Punto';
  $export['user_mail_password_reset_subject'] = $strongarm;

  return $export;
}
