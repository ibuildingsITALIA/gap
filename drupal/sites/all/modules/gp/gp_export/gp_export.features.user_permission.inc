<?php
/**
 * @file
 * gp_export.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function gp_export_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use search_api_sorts'.
  $permissions['use search_api_sorts'] = array(
    'name' => 'use search_api_sorts',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_api_sorts',
  );

  return $permissions;
}
