<?php
/**
 * @file
 * gp_export.features.roles_permissions.inc
 */

/**
 * Implements hook_default_roles_permissions().
 */
function gp_export_default_roles_permissions() {
  $roles = array();

  // Exported role: customer service
  $roles['customer service'] = array(
    'name' => 'customer service',
    'weight' => 4,
    'machine_name' => 'customer_service',
    'permissions' => array(
      'access administration menu' => TRUE,
      'access administration pages' => TRUE,
      'administer carts' => TRUE,
      'administer line items' => TRUE,
      'administer notifications' => TRUE,
      'administer promos' => TRUE,
      'administer transactions' => TRUE,
      'administer virtual cards' => TRUE,
      'change users emails' => TRUE,
      'change users passwords' => TRUE,
      'switch to other user' => TRUE,
      'view the administration theme' => TRUE,
    ),
  );

  // Exported role: manager
  $roles['manager'] = array(
    'name' => 'manager',
    'weight' => 3,
    'machine_name' => 'manager',
    'permissions' => array(
      'access administration menu' => TRUE,
      'access administration pages' => TRUE,
      'access bean overview' => TRUE,
      'access content overview' => TRUE,
      'access contextual links' => TRUE,
      'administer beans' => TRUE,
      'administer carts' => TRUE,
      'administer line items' => TRUE,
      'administer menu items' => TRUE,
      'administer notifications' => TRUE,
      'administer panelizer node page content' => TRUE,
      'administer panelizer node page layout' => TRUE,
      'administer promos' => TRUE,
      'administer redirects' => TRUE,
      'administer simplemeta' => TRUE,
      'administer transactions' => TRUE,
      'administer virtual cards' => TRUE,
      'bypass rh_node' => TRUE,
      'change layouts in place editing' => TRUE,
      'change users emails' => TRUE,
      'change users passwords' => TRUE,
      'create any banner bean' => TRUE,
      'create any box bean' => TRUE,
      'create any slide bean' => TRUE,
      'create bookstore content' => TRUE,
      'create event content' => TRUE,
      'create page content' => TRUE,
      'create url aliases' => TRUE,
      'delete any banner bean' => TRUE,
      'delete any bookstore content' => TRUE,
      'delete any box bean' => TRUE,
      'delete any event content' => TRUE,
      'delete any page content' => TRUE,
      'delete any slide bean' => TRUE,
      'delete own bookstore content' => TRUE,
      'delete own event content' => TRUE,
      'delete own page content' => TRUE,
      'delete revisions' => TRUE,
      'edit any banner bean' => TRUE,
      'edit any book content' => TRUE,
      'edit any bookstore content' => TRUE,
      'edit any box bean' => TRUE,
      'edit any event content' => TRUE,
      'edit any page content' => TRUE,
      'edit any slide bean' => TRUE,
      'edit own bookstore content' => TRUE,
      'edit own event content' => TRUE,
      'edit own page content' => TRUE,
      'popup message administration' => TRUE,
      'revert revisions' => TRUE,
      'switch to other user' => TRUE,
      'use ipe with page manager' => TRUE,
      'use panels in place editing' => TRUE,
      'use text format full_html' => TRUE,
      'use text format plain_html' => TRUE,
      'view any banner bean' => TRUE,
      'view any box bean' => TRUE,
      'view any slide bean' => TRUE,
      'view bean page' => TRUE,
      'view bean revisions' => TRUE,
      'view own unpublished content' => TRUE,
      'view revisions' => TRUE,
      'view the administration theme' => TRUE,
    ),
  );

  return $roles;
}
