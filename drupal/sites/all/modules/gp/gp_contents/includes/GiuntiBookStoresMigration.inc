<?php
/**
 * @file
 * Bookstores migration class.
 */

/**
 * Bookstore node migration.
 */
class GiuntiBookStoresMigration extends Migration {
  /**
   * Initialization of a Migration object.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'CODICELIB' => array(
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->source = new MigrateSourceCSV('private://ELENCO_LIBRERIE_0_7.csv', array(), array(
      'header_rows' => 1,
      'embedded_newlines' => 1,
      'delimiter' => ';',
    ));

    $this->destination = new MigrateDestinationNode('bookstore');

    $this->addFieldMapping('title')->defaultValue('');
    $this->addFieldMapping('field_store_code', 'CODICELIB');
    $this->addFieldMapping('field_store_name', 'NOMELIBRERIA')
      ->callbacks(array($this, 'camelCase'));

    $this->addFieldMapping('field_store_place', 'SONDAGGIO LIBRERIE_presso')
      ->callbacks(array($this, 'camelCase'));

    $this->addFieldMapping('field_store_address', 'INDIRIZZO')
      ->callbacks(array($this, 'camelCase'));

    $this->addFieldMapping('field_store_cap', 'CAP');

    $this->addFieldMapping('field_store_city', 'CITTA')
      ->callbacks(array($this, 'camelCase'));

    $this->addFieldMapping('field_store_province', 'PROV');

    $this->addFieldMapping('field_store_phone', 'tel/fax')
      ->callbacks(array($this, 'cleanPhoneNumber'));

    $this->addFieldMapping('field_store_email', 'indirizzo e-mail')
      ->callbacks(array($this, 'toLower'));

    $this->addFieldMapping('field_location', 'Latitudine');
    $this->addFieldMapping('field_location:lng', 'Longitudine');
    $this->addFieldMapping('field_location:lng_rad', 'Longitudine')
      ->callbacks(array($this, 'toLngRad'));
    $this->addFieldMapping('field_location:lat_sin', 'Latitudine')
      ->callbacks(array($this, 'toLatSine'));
    $this->addFieldMapping('field_location:lat_cos', 'Latitudine')
      ->callbacks(array($this, 'toLatCosine'));

    $this->addFieldMapping('field_store_hours', 'HOURS');

    $this->addFieldMapping('uid')
      ->defaultValue(1);
  }

  /**
   * Override prepareRow().
   */
  public function prepareRow($row) {
    $row->Latitudine = $this->coordDMSToDecimal($row->Latitudine);
    $row->Longitudine = $this->coordDMSToDecimal($row->Longitudine);
    $row->HOURS = $this->buildOpeningTimes($row);

    return parent::prepareRow($row);
  }

  /**
   * Build the opening hours object.
   */
  protected function buildOpeningTimes($row) {

    $data = bookstore_hours_default_values();

    $data->weekly->Mon[] = $this->filterHour($row->lunedì);
    $data->weekly->Tue[] = $this->filterHour($row->martedì);
    $data->weekly->Wed[] = $this->filterHour($row->mercoledì);
    $data->weekly->Thu[] = $this->filterHour($row->giovedì);
    $data->weekly->Fri[] = $this->filterHour($row->venerdì);
    $data->weekly->Sat[] = $this->filterHour($row->sabato);
    $data->weekly->Sun[] = $this->filterHour($row->domenica);

    $special_opening = new stdClass();
    $special_opening->date = $row->{'Aperture straordinarie1'};
    $special_opening->hours = array();
    $data->special_openings[] = $special_opening;

    $special_closure = new stdClass();
    $special_closure->date = $row->{'Chiusure straordinarie1'};
    $special_closure->hours = array();
    $data->special_closures[] = $special_closure;

    return array('data' => $data);
  }

  /**
   * Convert the lat/lng strings in decimal format.
   */
  protected function filterHour($value) {
    $value = trim($value);

    if (!$value || strcasecmp('chiuso', $value) === 0) {
      return NULL;
    }
    else {
      return $value;
    }
  }

  /**
   * Convert the lat/lng strings in decimal format.
   */
  protected function coordDMSToDecimal($coord) {
    $matches = array();
    if (preg_match('/(\d{1,2})°\s*(\d{1,2})\'\s*(\d{1,2}.?\d*)\"+\s*[NE]/', $coord, $matches)) {
      list(, $deg, $min, $sec) = $matches;
      return $deg + ((($min * 60) + ($sec)) / 3600);
    }
    else {
      return 0;
    }
  }

  /**
   * Get longitude radian.
   */
  protected function toLngRad($coord) {
    return deg2rad($coord);
  }

  /**
   * Get the latitude sine.
   */
  protected function toLatSine($coord) {
    return sin(deg2rad($coord));
  }

  /**
   * Get the latitude cosine.
   */
  protected function toLatCosine($coord) {
    return cos(deg2rad($coord));
  }

  /**
   * Trim the string.
   */
  protected function trimValues($value) {
    // Handle single value fields.
    if (!is_array($value)) {
      return trim($value);
    }

    foreach ($value as $key => $val) {
      $value[$key] = trim($val);
    }

    return $value;
  }

  /**
   * Convert the string in camel case.
   */
  protected function camelCase($value) {
    // Handle single value fields.
    if (!is_array($value)) {
      return ucwords(strtolower($value));
    }

    foreach ($value as $key => $val) {
      $value[$key] = ucwords(strtolower($val));
    }

    return $value;
  }

  /**
   * Convert the string in lower case.
   */
  protected function toLower($value) {
    // Handle single value fields.
    if (!is_array($value)) {
      return strtolower($value);
    }

    foreach ($value as $key => $val) {
      $value[$key] = strtolower($val);
    }

    return $value;
  }

  /**
   * Convert the string in lower case.
   */
  protected function cleanPhoneNumber($value) {

    $value = trim($value);
    $value = str_replace('tel. ', '', $value);
    $value = trim($value);

    return $value;
  }
}
