<?php
/**
 * @file
 * Migrate definitions file.
 */

/**
 * Implements hook_migrate_api().
 */
function gp_contents_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'giunti_contents' => array(
        'title' => t('GiuntiContents'),
      ),
    ),
    'migrations' => array(
      'GiuntiBookStores' => array(
        'class_name' => 'GiuntiBookStoresMigration',
        'group_name' => 'giunti_contents',
      ),
    ),
  );

  return $api;
}
