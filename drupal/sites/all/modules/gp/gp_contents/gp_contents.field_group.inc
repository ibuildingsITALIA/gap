<?php
/**
 * @file
 * gp_contents.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function gp_contents_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_address|node|bookstore|default';
  $field_group->group_name = 'group_address';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bookstore';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Indirizzo',
    'weight' => '1',
    'children' => array(
      0 => 'field_store_address',
      1 => 'field_store_cap',
      2 => 'field_store_place',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Indirizzo',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-address',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_address|node|bookstore|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_address|node|bookstore|form';
  $field_group->group_name = 'group_address';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bookstore';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Indirizzo',
    'weight' => '4',
    'children' => array(
      0 => 'field_store_address',
      1 => 'field_store_cap',
      2 => 'field_store_city',
      3 => 'field_store_locality',
      4 => 'field_store_place',
      5 => 'field_store_province',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-address field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_address|node|bookstore|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_other_info|node|bookstore|form';
  $field_group->group_name = 'group_other_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bookstore';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Altre informazioni',
    'weight' => '13',
    'children' => array(
      0 => 'field_store_business_name',
      1 => 'field_store_code',
      2 => 'field_store_name',
      3 => 'field_store_vat_code',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-other-info field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_other_info|node|bookstore|form'] = $field_group;

  return $export;
}
