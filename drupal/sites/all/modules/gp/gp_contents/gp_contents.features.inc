<?php
/**
 * @file
 * gp_contents.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gp_contents_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function gp_contents_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function gp_contents_image_default_styles() {
  $styles = array();

  // Exported image style: event_image_on_bookstore.
  $styles['event_image_on_bookstore'] = array(
    'name' => 'event_image_on_bookstore',
    'label' => 'Event image on bookstore',
    'effects' => array(
      3 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 98,
          'height' => 83,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: event_list_image.
  $styles['event_list_image'] = array(
    'name' => 'event_list_image',
    'label' => 'Event list image (w148)',
    'effects' => array(
      4 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 148,
          'height' => 216,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: store_detail.
  $styles['store_detail'] = array(
    'name' => 'store_detail',
    'label' => 'Store detail (470x350)',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 470,
          'height' => 350,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: store_right_image.
  $styles['store_right_image'] = array(
    'name' => 'store_right_image',
    'label' => 'Store right image (153x138)',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 153,
          'height' => 138,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: store_thumb_100x100.
  $styles['store_thumb_100x100'] = array(
    'name' => 'store_thumb_100x100',
    'label' => 'Store thumb (100x100)',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function gp_contents_node_info() {
  $items = array(
    'bookstore' => array(
      'name' => t('Libreria'),
      'base' => 'node_content',
      'description' => t('Per inserire una nuova libreria di Giunti al Punto.'),
      'has_title' => '1',
      'title_label' => t('Titolo'),
      'help' => '',
    ),
    'event' => array(
      'name' => t('Evento'),
      'base' => 'node_content',
      'description' => t('Usato per tutti gli eventi nelle librerie.'),
      'has_title' => '1',
      'title_label' => t('Titolo'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
