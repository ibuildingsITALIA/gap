<?php

/**
 * Theme function for the bookstore hours field.
 */
function theme_bookstore_hours_formatter($variables) {
  $element = $variables['element'];
  $days = bookstore_hours_get_days();
  $output = '';

  if ($element->weekly != NULL) {

    $weekly_output = '<div class="hours-box weekly-hours">';

    $items = '';

    foreach ($days as $day => $day_label) {

      $day_obj = $element->weekly->{$day};

      $item = array(
        'data' => '<span class="day-name">' . $day_label . '</span>',
        'class' => array('day', 'week-day'),
      );
      $children = array();

      switch (count($day_obj)) {
        case 0:
          // Day closed.
          $children['data'] = t('Closed');
          $item['class'][]  = 'opening-type-closed';
          break;

        case 1:
          // Full time opening.
          $children[]['data'] = $day_obj[0];
          break;

        case 2:
          $children[]['data'] = $day_obj[0] . ' / ' . $day_obj[1];
          break;
      }

      $item['children'] = $children;
      $items[] = $item;
    }

    $weekly_output .= theme('item_list', array(
      'items' => $items,
      'type' => 'ul',
      'title' => 'Orari di apertura settimanali',
    ));
    $weekly_output .= '</div>';

    $output .= $weekly_output;
  }

  if ($element->special_openings != NULL && count($element->special_openings) > 0) {
    $special_openings_output = '<div class="hours-box special-openings">';
    $special_openings_output .= _format_special_days($element->special_openings, 'Aperture straordinarie');
    $special_openings_output .= '</div>';
    $output .= $special_openings_output;
  }

  if ($element->special_closures != NULL && count($element->special_closures) > 0) {
    $special_closures_output = '<div class="hours-box special-closures">';
    $special_closures_output .= _format_special_days($element->special_closures, 'Chiusure straordinarie');
    $special_closures_output .= '</div>';
    $output .= $special_closures_output;
  }

  return $output;
}

/**
 * Return a single item for a special opening or closing.
 *
 * @param array $special_days
 *   The special day object (with date and hours properties).
 * @param string $title
 *   The title of the section.
 *
 * @return array
 *   The rendered output by item_list theme function.
 *
 * @see theme_item_list()
 */
function _format_special_days($special_days, $title) {
  $items = array();

  foreach ($special_days as $special_day) {

    $item = array(
      'data' => '<span class="day-name">' . $special_day->date . '</span>',
      'class' => array('day'),
    );

    if (isset($special_day->hours)) {
      $children = array();
      switch (count($special_day->hours)) {
        case 1:
          // Full time opening.
          $children[]['data'] = $special_day->hours[0];
          break;

        case 2:
          $children[]['data'] = $special_day->hours[0] . ' / ' . $special_day->hours[1];
          break;
      }
      $item['children'] = $children;
    }

    $items[] = $item;
  }

  return theme('item_list', array(
    'items' => $items,
    'type' => 'ul',
    'title' => $title,
  ));
}

/**
 * Theme function for bookstore_hours_field form.
 */
function theme_bookstore_hours_field_form($variables) {
  $element = $variables['element'];
  $days = bookstore_hours_get_days();
  $rows = array();

  foreach ($days as $day => $day_label) {
    $row = array(
      array('data' => $day_label, 'header' => TRUE, 'width' => 100),
      // Form row.
      array('data' => drupal_render($element[$day])),
    );
    $rows[$day] = $row;
  }

  $output = theme('table', array(
    'header' => array(
      array(
        'data' => 'Orari settimanali',
        'colspan' => 2,
      ),
    ),
    'rows' => $rows,
  ));

  $output .= drupal_render($element['special_openings']);
  $output .= drupal_render($element['special_openings_add']);

  $output .= drupal_render($element['special_closures']);
  $output .= drupal_render($element['special_closures_add']);

  // Render remaining elements.
  $output .= drupal_render_children($element);

  return $output;
}

/**
 * Theme function for a single bookstore_hours_field specials.
 *
 * Used for theme opening or closure items set.
 */
function theme_bookstore_hours_specials_field_form($variables) {
  $element = $variables['element'];

  $field_title = $element['#title'];
  unset($element['#title']);

  // Special openings.
  $rows = array();
  for ($i = 0; $i < $element['#items_count']; $i++) {
    $row = array(
      // Form row.
      array('data' => drupal_render($element[$i])),
    );
    $rows[] = $row;
  }

  $output = theme('table', array(
    'header' => array(
      array(
        'data' => $field_title,
        'colspan' => 2,
      ),
    ),
    'rows' => $rows,
    'attributes' => array(
      'id' => $element['#parents'][3] . '-wrapper',
    ),
  ));

  // Render remaining elements.
  $output .= drupal_render_children($element);

  return $output;
}
