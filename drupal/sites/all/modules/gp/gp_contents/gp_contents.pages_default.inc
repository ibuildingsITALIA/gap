<?php
/**
 * @file
 * gp_contents.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function gp_contents_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'eventi';
  $page->task = 'page';
  $page->admin_title = 'Eventi';
  $page->admin_description = '';
  $page->path = 'eventi';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_eventi_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'eventi';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Eventi',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'fullwidth';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Eventi';
  $display->uuid = 'be311781-e558-42f4-9708-94b45469f75d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-dad27995-e3a7-4917-9987-53782303e9b8';
    $pane->panel = 'content';
    $pane->type = 'views_panes';
    $pane->subtype = 'events-panel_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dad27995-e3a7-4917-9987-53782303e9b8';
    $display->content['new-dad27995-e3a7-4917-9987-53782303e9b8'] = $pane;
    $display->panels['content'][0] = 'new-dad27995-e3a7-4917-9987-53782303e9b8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-dad27995-e3a7-4917-9987-53782303e9b8';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['eventi'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'librerie';
  $page->task = 'page';
  $page->admin_title = 'Librerie';
  $page->admin_description = '';
  $page->path = 'librerie';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_librerie_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'librerie';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Librerie',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'fullwidth';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Librerie';
  $display->uuid = '6a107035-5efa-454c-b5db-b94f92d37560';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-09bd5b5c-da63-4cb7-901f-65580ce11bfd';
    $pane->panel = 'content';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<img src="http://giunti-cloud-prod.s3.amazonaws.com/uploads/libreria_meyer.jpg" style="display: block; padding: 0; margin: 0 auto; max-width: 960px !important; width: 100%">',
      'format' => 'plain_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '09bd5b5c-da63-4cb7-901f-65580ce11bfd';
    $display->content['new-09bd5b5c-da63-4cb7-901f-65580ce11bfd'] = $pane;
    $display->panels['content'][0] = 'new-09bd5b5c-da63-4cb7-901f-65580ce11bfd';
    $pane = new stdClass();
    $pane->pid = 'new-294024c6-3f9d-4e9b-8566-d5203af68609';
    $pane->panel = 'content';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<p>Giunti al Punto è la prima catena di librerie per numero di punti vendita, distribuiti su tutto il territorio nazionale.<br />
Aperti con orario continuato: le libraie e i librai sono a tua disposizione, per consigliarti, aiutarti o semplicemente per parlare di libri.<br />
E in più nelle nostre librerie ogni settimana tantissimi eventi: <a href="http://www.giuntialpunto.it/eventi">clicca qui</a> per scoprirli tutti!</p>
',
      'format' => 'filtered_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '294024c6-3f9d-4e9b-8566-d5203af68609';
    $display->content['new-294024c6-3f9d-4e9b-8566-d5203af68609'] = $pane;
    $display->panels['content'][1] = 'new-294024c6-3f9d-4e9b-8566-d5203af68609';
    $pane = new stdClass();
    $pane->pid = 'new-a629650b-8fe0-4221-9c30-a31de6299be6';
    $pane->panel = 'content';
    $pane->type = 'views_panes';
    $pane->subtype = 'bookstores_map-panel_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'a629650b-8fe0-4221-9c30-a31de6299be6';
    $display->content['new-a629650b-8fe0-4221-9c30-a31de6299be6'] = $pane;
    $display->panels['content'][2] = 'new-a629650b-8fe0-4221-9c30-a31de6299be6';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['librerie'] = $page;

  return $pages;

}
