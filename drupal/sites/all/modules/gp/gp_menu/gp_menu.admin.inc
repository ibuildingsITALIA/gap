<?php
/**
 * @file
 * Administration forms.
 */

/**
 * Form to create a menu for blocks and bars.
 */
function gp_menu_create_form($form, &$form_state, AmazonBrowseNode $browsenode, $type) {
  // Normalize type.
  $type = in_array($type, array('bar', 'block')) ? $type : 'block';

  // Save data for later.
  $form_state['browsenode'] = $browsenode;
  $form_state['type'] = $type;

  return confirm_form(
    $form,
    t('Are you sure you want to create a menu @type for browsenode @name?', array('@name' => $browsenode->getName(), '@type' => $type)),
    'browse/' . $browsenode->getId(),
    t('You will have to define links for this menu or it will be empty.')
  );
}

/**
 * Submit handler for positive confirm on menu create form.
 */
function gp_menu_create_form_submit($form, &$form_state) {
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $form_state['browsenode'];
  $type = $form_state['type'];

  // Prepare menu name and title.
  // This string must be in italian as there's no point in translating it.
  $menu_name = "menu-gp-menu-{$type}-{$browsenode->getId()}";
  $menu_title = ($type === 'bar' ? 'Barra' : 'Blocco');
  $menu_title .= " menu per {$browsenode->getName()} ({$browsenode->getId()})";

  // Prepare the menu array.
  $menu = array(
    'menu_name' => $menu_name,
    'title' => $menu_title,
    'description' => '',
  );

  // Create the menu programmatically.
  // @see menu_edit_menu_submit()
  $path = 'admin/structure/menu/manage/';
  $link['link_title'] = $menu_title;
  $link['link_path'] = $path . $menu_name;
  $link['router_path'] = $path . '%';
  $link['module'] = 'menu';
  $link['plid'] = db_query("SELECT mlid FROM {menu_links} WHERE link_path = :link AND module = :module", array(
    ':link' => 'admin/structure/menu',
    ':module' => 'system',
  ))
    ->fetchField();

  menu_link_save($link);
  menu_save($menu);

  drupal_set_message(t('The menu has been created.'));
  $form_state['redirect'] = $path . $menu_name;
}

/**
 * Page callback to show a reduced set of menu and edit links for them.
 */
function gp_menu_overview_page() {
  // Get all user defined menus.
  $menus = menu_get_menus(FALSE);

  // Add the main menu.
  $menus['main-menu'] = t('Main menu');

  // And remove some others.
  unset($menus['devel']);
  unset($menus['features']);

  // Render the menu table.
  // @see menu_overview_page()
  $result = db_query("SELECT * FROM {menu_custom} WHERE menu_name IN (:menus) ORDER BY title", array(
    ':menus' => array_keys($menus),
  ), array('fetch' => PDO::FETCH_ASSOC));
  $header = array(
    t('Title'),
    array('data' => t('Operations'), 'colspan' => '3'),
  );
  $rows = array();
  foreach ($result as $menu) {
    $row = array(
      theme('menu_admin_overview', array(
        'title' => $menu['title'],
        'name' => $menu['menu_name'],
        'description' => $menu['description'],
      )),
    );
    $row[] = array('data' => l(t('list links'), 'admin/structure/menu/manage/' . $menu['menu_name']));
    $row[] = array('data' => l(t('add link'), 'admin/structure/menu/manage/' . $menu['menu_name'] . '/add'));
    $rows[] = $row;
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}
