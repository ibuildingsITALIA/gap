<?php
/**
 * @file
 * Plugin to render a BrowseNode menu block.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Browsenode menu block'),
  'description' => t('Menu block with custom links.'),
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
  'category' => 'Amazon',
);

/**
 * Render the menu pane.
 */
function gp_menu_browsenode_menu_block_content_type_render($subtype, $conf, $panel_args, $context) {
  // Don't render if context is missing.
  if (empty($context) || empty($context->data) || FALSE === $context->data) {
    return array();
  }

  // Retrieve browsenode.
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $context->data->value();

  // Retrieve menu name associated with this browsenode.
  $data = gp_menu_get_menu_data($browsenode, 'block');

  // If we have found a menu, get the tree.
  if ($data) {
    $menu_tree = menu_tree($data['#name']);
  }
  // Otherwise prepare an empty menu.
  else {
    $menu_tree = array();
  }

  // Insert administrative links.
  gp_menu_edit_links($menu_tree, $browsenode, $data, 'block');

  $block = new stdClass();
  $block->title = '';
  $block->module = 'gp_menu';
  $block->delta = 'menu_block';
  $block->content = $menu_tree;
  return $block;
}

/**
 * Empty form callback to allow overriding title.
 */
function gp_menu_browsenode_menu_block_content_type_edit_form($form, &$form_state) {
  return $form;
}
