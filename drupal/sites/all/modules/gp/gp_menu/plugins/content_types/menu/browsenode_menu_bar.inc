<?php
/**
 * @file
 * Plugin to render a BrowseNode menu bar.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Browsenode menu bar'),
  'description' => t('Menu bar with custom links.'),
  'required context' => array(
    new ctools_context_optional(t('Amazon BrowseNode'), 'browsenode'),
    new ctools_context_optional(t('Amazon Item'), 'item'),
  ),
  'category' => 'Amazon',
);

/**
 * Render the menu pane.
 */
function gp_menu_browsenode_menu_bar_content_type_render($subtype, $conf, $panel_args, $context) {
  // Don't render if context is missing.
  if (empty($context) || empty($context->data) || FALSE === $context->data) {
    return array();
  }

  // Retrieve browsenode.
  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();

  // Retrieve menu name associated with this browsenode.
  $menu_name = gp_menu_get_menu_data($browsenode, 'bar');

  // If no menu found, quit.
  if (!$menu_name) {
    return array();
  }

  // Get the menu tree.
  $menu = menu_tree($menu_name);

  $block = new stdClass();
  $block->title = t('Menu bar for @name', array('@name' => $browsenode->getName()));
  $block->module = 'gp_menu';
  $block->delta = 'menu_bar';
  $block->content = $menu;
  return $block;
}

/**
 * Empty form callback to allow overriding title.
 */
function gp_menu_browsenode_menu_bar_content_type_edit_form($form, &$form_state) {
  return $form;
}
