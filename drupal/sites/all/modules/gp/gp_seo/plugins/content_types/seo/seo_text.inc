<?php
/**
 * @file
 * Plugin to render SEO text if the page has one.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('SEO text'),
  'description' => t('Renders the SEO text of the page, if available.'),
  'category' => 'Miscellaneous',
);

/**
 * Render the SEO text pane.
 */
function gp_seo_seo_text_content_type_render($subtype, $conf, $panel_args, $context) {
  global $language;

  $path = $_GET['q'];
  $lang = $language->language;

  // Try to load language specific meta.
  $meta = simplemeta_meta_load_by_path($path, $lang);
  if (!$meta) {
    // Try loading language-neutral.
    $meta = simplemeta_meta_load_by_path($path);
  }

  // If still no meta, don't render anything.
  if (!$meta || empty($meta->data['gp_seo_text'])) {
    return array();
  }

  $block = new stdClass();
  $block->title = '';
  $block->module = 'gp_seo';
  $block->delta = $path;
  $block->content = array(
    '#markup' => $meta->data['gp_seo_text'],
  );
  return $block;
}
