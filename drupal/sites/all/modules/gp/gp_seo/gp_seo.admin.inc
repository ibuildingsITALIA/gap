<?php
/**
 * @file
 *
 * Administration forms.
 */

/**
 * Admin settings form.
 */
function gp_seo_admin_settings_form() {
  $form = array();

  $form['gp_seo_google_merchant_feed_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title of the xml feed'),
    '#required' => TRUE,
    '#default_value' => variable_get('gp_seo_google_merchant_feed_title', ''),
    '#size' => 100,
  );
  $form['gp_seo_google_merchant_feed_filename'] = array(
    '#type' => 'textfield',
    '#title' => t('Filename of the xml feed'),
    '#required' => TRUE,
    '#default_value' => variable_get('gp_seo_google_merchant_feed_filename', ''),
    '#size' => 100,
  );
  $form['gp_seo_google_merchant_feed_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Url of the xml feed'),
    '#required' => FALSE,
    '#disabled' => TRUE,
    '#default_value' => url('sites/default/files/' . variable_get('gp_seo_google_merchant_feed_filename', ''), array('absolute' => TRUE)),
    '#size' => 100,
  );

  return system_settings_form($form);
}
