<?php
/**
 * @file
 * Drush tasks.
 */

/**
 * Implements hook_drush_help().
 */
function gp_seo_drush_help($command) {
  switch ($command) {
    case 'drush:gp-feed':
      return dt('Create Feed file');
  }
}

/**
 * Implements hook_drush_command().
 */
function gp_seo_drush_command() {
  $items = array();
  $items['gp-feed'] = array(
    'description' => dt('Create Feed file'),
  );

  return $items;
}

class SimpleXMLExtended extends SimpleXMLElement {
  /**
   * @param $cdata_text
   */
  public function addCData($cdata_text) {
    $node = dom_import_simplexml($this);
    $no = $node->ownerDocument;
    $node->appendChild($no->createCDATASection($cdata_text));
  }
}

/**
 * Callback function for drush in order to create book feed.
 */
function drush_gp_seo_gp_feed() {
  // All books whit required fields.
  $sql = 'SELECT node.title AS node_title, node.nid AS nid, node.created AS node_created
          FROM {node} node
          LEFT JOIN {field_data_field_asin} field_data_field_asin ON node.nid = field_data_field_asin.entity_id AND (field_data_field_asin.entity_type = \'node\' AND field_data_field_asin.deleted = \'0\')
          WHERE (( (node.status = \'1\') AND (field_data_field_asin.field_asin_value IS NOT NULL ) ))
          ORDER BY node_created DESC
          ';

  // Build xml.
  $xml = new XMLWriter();
  $xml->openMemory();
  $xml->startDocument('1.0');
  $xml->startElement('rss');
  $xml->writeAttribute('version', '2.0');
  $xml->writeAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
  $xml->writeAttribute('xmlns:atom', 'http://www.w3.org/2005/Atom');


  $xml->startElement('channel');

  // Atom.
  $xml->startElement('atom:link');
  $xml->writeAttribute('href', 'http://www.giuntialpunto.it/rss.xml');
  $xml->writeAttribute('rel', 'self');
  $xml->writeAttribute('type', 'application/rss+xml');
  $xml->endElement();

  // Add general elements under "channel" node.
  // Title.
  $xml->startElement('title');
  $xml->writeCdata(variable_get('gp_seo_google_merchant_feed_title', ''));
  $xml->endElement();

  // Link.
  $xml->startElement('link');
  $xml->writeCdata('http://www.giuntialpunto.it');
  $xml->endElement();

  // Description.
  $xml->startElement('description');
  $xml->writeCdata('Items from www.giuntialpunto.it');
  $xml->endElement();


  $feed_filename = variable_get('gp_seo_google_merchant_feed_filename', 'google_merchant.xml');
  $feed_file_tmp = 'sites/default/files/' . $feed_filename;
  $feed_file = 's3://google_merchant/' . $feed_filename;

  $result = db_query($sql);
  // No books.
  if (empty($result)) {
    $xml->endElement();
    // Rss.
    drush_log('No Books.', 'error');
    return;
  }
  file_put_contents($feed_file_tmp, $xml->flush(TRUE));
  drush_log('Start to write on file ' . $feed_file_tmp, 'status');

  $i = 0;
  $j = 0;
  $total = $result->rowCount();
  drush_log($total . ' items to processed', 'status');

  foreach ($result as $node) {
    $j++;
    $book = entity_metadata_wrapper('node', $node->nid);
    if (!_gp_seo_check_mandatory_params($book)) {
      continue;
    }
    $xml = _gp_seo_add_single_book($book, $xml);
    // Flush XML in memory to file every 1000 iterations.
    if (0 == $i % 1000) {
      file_put_contents($feed_file_tmp, $xml->flush(TRUE), FILE_APPEND);
      drupal_static_reset();
      drush_log($j . ' of ' . $total . ' items processed', 'status');
    }
    $i++;
  }
  drush_log('All ' . $total . ' items processed', 'status');
  // Channel.
  $xml->endElement();
  // Rss.
  $xml->endElement();
  $xml->endDocument();
  file_put_contents($feed_file_tmp, $xml->flush(TRUE), FILE_APPEND);
  // Move the temp file on final destination.
  $moved = file_unmanaged_move($feed_file_tmp, $feed_file, FILE_EXISTS_REPLACE);
  if ($moved) {
    drush_log('File moved', 'status');
  }
  else {
    drush_log('File not moved', 'error');
  }
}

/**
 * @param $book
 * @param $xml
 * @return mixed
 */
function _gp_seo_add_single_book($book, $xml) {
  global $base_url;
  $product_url = $base_url . '/product/' . $book->field_asin->value();
  $xml->startElement('item');

  // Title.
  $title = truncate_utf8(strip_tags($book->title->value()), 150, TRUE, TRUE);
  $xml->startElement('title');
  $xml->writeCData($title);
  $xml->endElement();

  // Link.
  $xml->startElement('link');
  $xml->writeCData($product_url);
  $xml->endElement();

  // Description.
  $description = truncate_utf8(strip_tags($book->body->value()['value']), 5000, TRUE, TRUE);
  $xml->startElement('description');
  $xml->writeCData(!empty($description) ? trim($description) : 'See more on ' . $product_url);
  $xml->endElement();

  // Image_link.
  $image_uri = $book->field_image->value()['uri'];
  $image_url = file_stream_wrapper_get_instance_by_uri($image_uri)->getExternalUrl();
  if (!empty($image_url)) {
    $xml->startElementNS('g', 'image_link', 'http://base.google.com/ns/1.0');
    $xml->writeCdata('http:' . preg_replace('/^http:/', '', $image_url));
    $xml->endElement();
  }

  // ID.
  $xml->startElementNS('g', 'id', 'http://base.google.com/ns/1.0');
  $xml->writeCdata($book->field_asin->value());
  $xml->endElement();

  // GTIN.
  $xml->startElementNS('g', 'gtin', 'http://base.google.com/ns/1.0');
  $xml->writeCdata($book->field_isbn13->value());
  $xml->endElement();

  // Price.
  $xml->startElementNS('g', 'price', 'http://base.google.com/ns/1.0');
  $xml->writeCdata(number_format($book->field_current_price->value() / 100, 2, '.', '') . ' EUR');
  $xml->endElement();

  // Condition.
  $xml->startElementNS('g', 'condition', 'http://base.google.com/ns/1.0');
  $xml->writeCdata('new');
  $xml->endElement();

  // Availability.
  $xml->startElementNS('g', 'availability', 'http://base.google.com/ns/1.0');
  $xml->writeCdata('in stock');
  $xml->endElement();

  // Google_product_category.
  $xml->startElementNS('g', 'google_product_category', 'http://base.google.com/ns/1.0');
  $xml->writeCdata('Media > Libri');
  $xml->endElement();


  // Product_type.
  if (!empty($book->field_bic_subjects)) {
    foreach ($book->field_bic_subjects->value() as $bic_subjects) {
      $parent_string = '';
      $parents = array_reverse(taxonomy_get_parents_all($bic_subjects->tid));
      foreach ($parents as $parent) {
        $parent_string .= preg_replace('/[^A-Za-z0-9\. -]/', '', $parent->name);
        $parent_string .= ' > ';
      }
      $parent_string = rtrim($parent_string, ' > ');

      if (!empty($parent_string)) {
        $xml->startElementNS('g', 'product_type', 'http://base.google.com/ns/1.0');
        $xml->writeCdata($parent_string);
        $xml->endElement();
        break;
      }
    }
  }

  // Item.
  $xml->endElement();

  return $xml;
}

/**
 * Function that provides to check if the book has got the mandatory params
 * @param $book , the book to check
 * @returns FALSE, if the mandatory params are not found, TRUE otherwise
 */
function _gp_seo_check_mandatory_params($book) {
  // Price > 0.00 is mandatory.
  $price = $book->field_current_price->value();
  if (empty($price)) {
    // drush_log($book->nid . ' - missing current price', 'status');
    return FALSE;
  }
  // Title is mandatory.
  if (empty($book->title)) {
    // drush_log($book->nid . ' - missing title', 'status');
    return FALSE;
  }
  // Image is mandatory.
  if (empty($book->field_image->value()['uri'])) {
    // drush_log($book->nid . ' - missing image', 'status');
    return FALSE;
  }
  return TRUE;
}

/**
 * Function definition to convert array to xml.
 */
function array_to_xml($array_info, &$xml_info) {
  foreach ($array_info as $key => $value) {
    if (is_array($value)) {
      if (!is_numeric($key)) {
        $subnode = $xml_info->addChild("$key");
        array_to_xml($value, $subnode);
      }
      else {
        $subnode = $xml_info->addChild("item$key");
        array_to_xml($value, $subnode);
      }
    }
    else {
      $xml_info->addChild("$key", htmlspecialchars("$value"));
    }
  }
}
