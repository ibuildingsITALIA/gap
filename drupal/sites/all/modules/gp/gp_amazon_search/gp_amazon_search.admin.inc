<?php
/**
 * @file
 * Administration forms.
 */

/**
 * Form callback to create an advanced Amazon search.
 */
function gp_amazon_search_advanced_search_form($form, &$form_state) {
  // Shortcut to access submitted values.
  $values = isset($form_state['values']) ? $form_state['values'] : array();

  // Retrieve all parameters handlers.
  ctools_include('plugins');
  $plugins = ctools_get_plugins('gp_amazon_search', 'search_parameter');

  $search_indexes = gp_amazon_search_indexes();

  // Prepare search indexes options.
  $indexes_options = array();
  foreach ($search_indexes as $key => $index) {
    $indexes_options[$key] = $index['label'];
  }
  // Get current search index or set a default.
  $current_index = !empty($values['search_index']) ? $values['search_index'] : 'All';

  // Add a search index select.
  $form['search_index'] = array(
    '#type' => 'select',
    '#title' => t('Search index'),
    '#options' => $indexes_options,
    '#default_value' => $current_index,
    '#required' => TRUE,
  );

  $form['search_index_change'] = array(
    '#type' => 'submit',
    '#value' => t('Change'),
    '#name' => 'op_change_index',
    '#submit' => array('gp_amazon_search_advanced_search_form_rebuild'),
    '#ajax' => array(
      'callback' => 'gp_amazon_search_advanced_search_form_change_index_callback',
      'wrapper' => 'parameters-wrapper',
    ),
  );

  // Add the form actions now, as they will be available to plugins
  // for adding validate/submit handlers on generate button.
  // They won't cause problems as they are rendered at the bottom of the form.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate'),
    '#submit' => array(
      'gp_amazon_search_advanced_search_form_rebuild',
      'gp_amazon_search_advanced_search_form_generate_submit',
    ),
  );

  // Get all parameters available for this index.
  $available_parameters = $search_indexes[$current_index]['search_parameters'];
  // Prepare a wrapper.
  $form['parameters'] = array(
    '#tree' => TRUE,
    '#type' => 'container',
    '#attributes' => array('id' => array('parameters-wrapper')),
  );
  foreach ($available_parameters as $name => $label) {
    // Plugin names don't use capital letters.
    $candidate_plugin = strtolower($name);

    // If there's a plugin that handles this type, use it.
    if (isset($plugins[$candidate_plugin]) && $function = ctools_plugin_get_function($plugins[$candidate_plugin], 'form callback')) {
      $form = $function($form, $form_state);
    }
    // Otherwise fallback as text field.
    else {
      $form['parameters'][$name] = array(
        '#type' => 'textfield',
        '#title' => t($label, array(), array('context' => 'amazon-search-parameters')),
        '#default_value' => !empty($values['parameters'][$name]) ? $values['parameters'][$name] : '',
      );
    }
  }

  // Add the possibility to sort items, if available.
  if (!empty($search_indexes[$current_index]['sorts'])) {
    $form['parameters']['Sort'] = array(
      '#type' => 'select',
      '#title' => t('Sort by'),
      '#options' => $search_indexes[$current_index]['sorts'],
    );
  }

  // Add a field to allow entering a custom title for the page.
  $form['page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title', array(), array('context' => 'amazon-search-parameters')),
    '#description' => t('Use this text as the page title for the search.'),
  );

  // Retrieve search string and url from form_state.
  $search_string = !empty($values['generated']['string']) ? $values['generated']['string'] : '';
  $search_url = !empty($values['generated']['url']) ? $values['generated']['url'] : '';

  // Add a container to easily return rendered searches.
  $form['generated'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => array('generated-wrapper')),
    '#tree' => TRUE,
  );

  $form['generated']['string'] = array(
    '#type' => 'textfield',
    '#title' => t('Search string'),
    '#size' => 150,
    '#value' => $search_string,
  );

  $form['generated']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Search page url'),
    '#size' => 150,
    // Remove initial slash as it breaks menus.
    '#value' => ltrim($search_url, '/'),
  );

  if ($search_url) {
    $form['generated']['url']['#description'] = t('You can preview the search <a href="@url" target="_blank">here</a>.', array(
      '@url' => $search_url,
    ));
  }

  // Add the ajax capabilities to the main submit button.
  $form['actions']['submit']['#ajax'] = array(
    'callback' => 'gp_amazon_search_advanced_search_form_generate_callback',
    'wrapper' => 'generated-wrapper',
  );

  return $form;
}

/**
 * Submit to rebuild the gp_amazon_search_advanced_search_form form.
 */
function gp_amazon_search_advanced_search_form_rebuild($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback to change search index.
 */
function gp_amazon_search_advanced_search_form_change_index_callback($form, &$form_state) {
  return $form['parameters'];
}

/**
 * Ajax callback to render the generated string.
 */
function gp_amazon_search_advanced_search_form_generate_callback($form, &$form_state) {
  return $form['generated'];
}

/**
 * Submit handler for gp_amazon_search_advanced_search_form.
 */
function gp_amazon_search_advanced_search_form_generate_submit($form, &$form_state) {
  $values = $form_state['values'];

  // Generate search string and search url from submitted values.
  $search_string = $search_url = '';
  if (!empty($values['parameters']) && $parameters = array_filter($values['parameters'])) {
    $search_string = gp_amazon_implode_parameters($parameters);

    // Add the page title if present.
    if (!empty($values['page_title'])) {
      $parameters['bt'] = $values['page_title'];
    }

    // If the search index is the all one, add a fake browsenode id.
    if ('All' === $values['search_index']) {
      $parameters['BrowseNode'] = 'all';
    }

    // Remove the initial slash as it breaks menu voices.
    $search_url = url('search', array(
      'query' => gp_amazon_search_get_search_query($parameters),
    ));
  }

  // Set values in the form state.
  form_set_value($form['generated']['url'], $search_url, $form_state);
  form_set_value($form['generated']['string'], $search_string, $form_state);
}

/**
 * Form callback to create an Amazon asin search.
 */
function gp_amazon_search_asin_search_form($form, &$form_state) {
  // Shortcut to access submitted values.
  $values = isset($form_state['values']) ? $form_state['values'] : array();

  $form['asin'] = array(
    '#tree' => TRUE,
  );

  // Generate 10 textfields for asins.
  for ($i = 1; $i < 11; $i++) {
    $form['asin'][$i] = array(
      '#type' => 'textfield',
      '#title' => t('ASIN n. @count', array('@count' => $i), array('context' => 'amazon-search-parameters')),
      '#default_value' => !empty($values['asin'][$i]) ? $values['asin'][$i] : '',
    );
  }

  // Add a field to allow entering a custom title for the page.
  $form['page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title', array(), array('context' => 'amazon-search-parameters')),
    '#description' => t('Use this text as the page title for the search.'),
  );

  // Add a container to easily return rendered searches.
  $form['generated'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => array('generated-wrapper')),
    '#tree' => TRUE,
  );

  // Retrieve the search url.
  $search_url = !empty($values['generated']['url']) ? $values['generated']['url'] : '';

  $form['generated']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Search page url'),
    '#size' => 150,
    // Remove initial slash as it breaks menus.
    '#value' => ltrim($search_url, '/'),
  );

  if ($search_url) {
    $form['generated']['url']['#description'] = t('You can preview the search <a href="@url" target="_blank">here</a>.', array(
      '@url' => $search_url,
    ));
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate'),
  );

  return $form;
}

/**
 * Submit handler for gp_amazon_search_asin_search_form.
 */
function gp_amazon_search_asin_search_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $search_url = '';

  if (!empty($values['asin']) && $asins = array_filter($values['asin'])) {
    $query = array(
      'asin' => implode(',', $asins),
    );

    // Add the page title if present.
    if (!empty($values['page_title'])) {
      $query['bt'] = $values['page_title'];
    }

    $search_url = url('search', array('query' => $query));
  }

  // Set values in the form state.
  form_set_value($form['generated']['url'], $search_url, $form_state);

  // Rebuild form.
  $form_state['rebuild'] = TRUE;
}
