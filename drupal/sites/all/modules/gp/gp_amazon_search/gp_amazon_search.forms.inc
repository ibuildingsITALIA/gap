<?php
/**
 * @file
 * Form callbacks.
 */

/**
 * Search form callback.
 */
function gp_amazon_search_form($form, &$form_state) {
  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_amazon_search', 'gp_amazon_search.forms');

  // Change method and disable token.
  $form['#method'] = 'get';
  $form['#token'] = FALSE;

  // Set action to our panel page.
  $form['#action'] = url('search');

  $search_indexes = gp_amazon_search_indexes();
  // Remove some Search Index from search.
  unset($search_indexes['MobileApps']);
  unset($search_indexes['MP3Downloads']);
  unset($search_indexes['Shoes']);

  $options = array();
  foreach ($search_indexes as $index) {
    $options[$index['BrowseNodeId']] = $index['label'];
  }

  if ($browsenode = gp_amazon_load_browsenode_from_url()) {
    $index = $browsenode->getSearchIndex();

    // Only browsenode pages within the Books index use the browsenode id.
    if ('Books' === $index['key'] && drupal_match_path(current_path(), 'browse/*')) {
      $bnid_default = $browsenode->getId();
    }
    else {
      // Use the search index.
      $bnid_default = $index['BrowseNodeId'];
    }
  }
  else {
    // Set fallback.
    $bnid_default = $search_indexes['All']['BrowseNodeId'];
  }

  // Also add this browsenode if not present.
  if (!isset($options[$bnid_default])) {
    // Insert as first element.
    $options = array($bnid_default => $browsenode->getName()) + $options;
  }

  $form['bnid'] = array(
    '#type' => 'select',
    '#title' => t('Index'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => $bnid_default,
    // Disable chosen module if active.
    '#chosen' => FALSE,
  );

  $form['keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#title_display' => 'invisible',
    '#size' => 100,
    '#attributes' => array(
      'title' => t('Search'),
    ),
    '#default_value' => isset($_GET['keywords']) ? $_GET['keywords'] : '',
  );

  $form['actions'] = array('#type' => 'actions', '#weight' => -20);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['#after_build'] = array('gp_amazon_search_form_after_build');

  return $form;
}

/**
 * Form #after_build callback for gp_amazon_search_search_form.
 */
function gp_amazon_search_form_after_build($form, &$form_state) {
  // Prevent stuff from printing.
  $form['form_id']['#printed'] = TRUE;
  $form['form_build_id']['#printed'] = TRUE;
  $form['actions']['submit']['#name'] = '';

  return $form;
}
