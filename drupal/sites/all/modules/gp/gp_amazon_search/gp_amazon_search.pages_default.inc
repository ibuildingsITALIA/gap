<?php
/**
 * @file
 * gp_amazon_search.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function gp_amazon_search_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'search_page';
  $page->task = 'page';
  $page->admin_title = 'Search page';
  $page->admin_description = 'Search page landing.';
  $page->path = 'search';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_search_page_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'search_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'Libri',
        'keyword' => 'browsenode',
        'name' => 'browsenode',
        'browsenode_id' => '411663031',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'left-sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '9a6fbfa6-6d80-4f2f-86d4-28323e241273';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-04f613c2-198a-4e48-abcb-a3138087101d';
  $pane->panel = 'content';
  $pane->type = 'amazon_search_results';
  $pane->subtype = 'amazon_search_results';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'item_view_mode' => 'detail',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '04f613c2-198a-4e48-abcb-a3138087101d';
  $display->content['new-04f613c2-198a-4e48-abcb-a3138087101d'] = $pane;
  $display->panels['content'][0] = 'new-04f613c2-198a-4e48-abcb-a3138087101d';
  $pane = new stdClass();
  $pane->pid = 'new-a6a3daaa-c0d0-4149-a6a5-ab154076b7a8';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-gp-menu-block-default';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a6a3daaa-c0d0-4149-a6a5-ab154076b7a8';
  $display->content['new-a6a3daaa-c0d0-4149-a6a5-ab154076b7a8'] = $pane;
  $display->panels['sidebar'][0] = 'new-a6a3daaa-c0d0-4149-a6a5-ab154076b7a8';
  $pane = new stdClass();
  $pane->pid = 'new-5d46c95c-1bad-4c57-814d-f14f7909f3df';
  $pane->panel = 'sidebar';
  $pane->type = 'browsenode_nav';
  $pane->subtype = 'browsenode_nav';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_browsenode_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '5d46c95c-1bad-4c57-814d-f14f7909f3df';
  $display->content['new-5d46c95c-1bad-4c57-814d-f14f7909f3df'] = $pane;
  $display->panels['sidebar'][1] = 'new-5d46c95c-1bad-4c57-814d-f14f7909f3df';
  $pane = new stdClass();
  $pane->pid = 'new-00a42f1a-04a6-4e0f-bf7e-5a56f3b5faac';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-sidebar-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '00a42f1a-04a6-4e0f-bf7e-5a56f3b5faac';
  $display->content['new-00a42f1a-04a6-4e0f-bf7e-5a56f3b5faac'] = $pane;
  $display->panels['sidebar'][2] = 'new-00a42f1a-04a6-4e0f-bf7e-5a56f3b5faac';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;

  $page->default_handlers[$handler->name] = $handler;
  $pages['search_page'] = $page;

  return $pages;

}
