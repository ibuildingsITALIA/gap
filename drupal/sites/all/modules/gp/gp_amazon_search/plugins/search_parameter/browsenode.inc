<?php
/**
 * @file
 * Plugin definition for browsenode search parameter.
 */

$plugin = array(
  'title' => 'Browsenode',
);

/**
 * Form callback for this parameter.
 */
function gp_amazon_search_browsenode_parameter_form($form, &$form_state) {
  // Remember ajax form to include this file.
  form_load_include($form_state, 'inc', 'gp_amazon_search', 'plugins/search_parameter/browsenode');

  // Shortcut to access submitted values.
  $values = isset($form_state['values']) ? $form_state['values'] : array();

  // Wrap all elements in a common one.
  $form['parameters']['BrowseNode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Browsenode', array(), array('context' => 'amazon-search-parameters')),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#attributes' => array('id' => array('browsenode-wrapper')),
    // Use custom parents.
    '#parents' => array('browsenode'),
  );

  // Use a shortcut to our container element.
  $container = &$form['parameters']['BrowseNode'];

  // If we have a browsenode submitted, use it.
  if (!empty($values['browsenode']['id'])) {
    $browsenode_id = $values['browsenode']['id'];
  }
  // Otherwise use the root browsenode of the index.
  elseif (!empty($values['search_index'])) {
    $search_indexes = gp_amazon_search_indexes();
    $browsenode_id = $search_indexes[$values['search_index']]['BrowseNodeId'];
  }
  // Or quit without doing anything.
  else {
    return $form;
  }

  // Get the browsenode.
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = gp_amazon_get_browsenode($browsenode_id);

  // If a manual entering has been selected, render a textfield.
  if (!empty($values['browsenode']['browsenode_manual'])) {
    $container['id'] = array(
      '#type' => 'textfield',
      '#title' => t('Browsenode'),
    );
  }
  // Otherwise render a select.
  else {
    // Get the options tree.
    $options = gp_amazon_search_get_browsenode_tree($browsenode);

    // The main element.
    $container['id'] = array(
      '#type' => 'select',
      '#title' => t('BrowseNode', array(), array('context' => 'amazon-search-parameters')),
      '#options' => $options,
      '#prefix' => '<div id="browsenode-wrapper">',
      '#suffix' => '</div>',
      // Always use chosen.
      '#chosen' => TRUE,
    );

    // Add a button to retrieve the children of the current browsenode.
    $container['load_children'] = array(
      '#type' => 'submit',
      '#value' => t('Load children'),
      '#name' => 'op_load_children',
      '#submit' => array('gp_amazon_search_advanced_search_form_rebuild'),
      // Change parents to keep this button away in submit.
      '#ajax' => array(
        'callback' => 'gp_amazon_search_browsenode_parameter_callback',
        'wrapper' => 'browsenode-wrapper',
      ),
    );
  }

  // Allow to manually enter a browsenode value.
  $container['browsenode_manual'] = array(
    '#type' => 'checkbox',
    '#title' => t('Manually enter a browsenode id', array(), array('context' => 'amazon-search-parameters')),
    '#default_value' => FALSE,
    '#ajax' => array(
      'callback' => 'gp_amazon_search_browsenode_parameter_callback',
      'wrapper' => 'browsenode-wrapper',
    ),
  );

  // Add our submit handler to the form generate button.
  // @todo add a validation callback to the plugin declaration.
  array_unshift($form['actions']['submit']['#submit'], 'gp_amazon_search_browsenode_parameter_form_generate_submit');

  return $form;
}

/**
 * Ajax callback to load browsenode children.
 */
function gp_amazon_search_browsenode_parameter_callback($form, $form_state) {
  return $form['parameters']['BrowseNode'];
}

/**
 * Submit handler for the generate button.
 */
function gp_amazon_search_browsenode_parameter_form_generate_submit($form, &$form_state) {
  $form_state['values']['parameters']['BrowseNode'] = $form_state['values']['browsenode']['id'];
}

/**
 * Generate the tree of a browsenode.
 *
 * @param AmazonBrowseNode $browsenode
 *   The browsenode itself.
 *
 * @return array
 *   An array suitable for generating options for a select.
 */
function gp_amazon_search_get_browsenode_tree(AmazonBrowseNode $browsenode) {
  $options = array();

  // Render all the ancestors of this node.
  $depth = 0;
  if ($browsenode->hasAncestors()) {
    foreach (array_reverse($browsenode->getAncestors()) as $parent) {
      /* @var AmazonBrowseNode $parent */
      if (!$parent->isRoot()) {
        $options[$parent->getId()] = str_repeat('-', $depth) . $parent->getName();
        $depth++;
      }
    }

    // Add siblings also.
    $ancestor = gp_amazon_get_browsenode($browsenode->getAncestor()->getId());
    if ($ancestor) {
      foreach ($ancestor->getChildren() as $child) {
        /* @var AmazonBrowseNode $child */
        $options[$child->getId()] = str_repeat('-', $depth) . $child->getName();
      }
    }
    else {
      // Add only our browsenode.
      $options[$browsenode->getId()] = str_repeat('-', $depth) . $browsenode->getName();
    }

    // Increase depth for next loops.
    $depth++;
  }
  elseif ($browsenode->isSearchIndex()) {
    // Add the search index as first ancestor.
    $options[$browsenode->getId()] = $browsenode->getName();
    $depth++;
  }

  // Get all children of the main browsenode, if it has any.
  if ($browsenode->hasChildren()) {
    $children = array();
    foreach ($browsenode->getChildren() as $child) {
      /* @var AmazonBrowseNode $child */
      $children[$child->getId()] = str_repeat('-', $depth) . $child->getName();
    }

    // If the browsenode key is already in the options, slice the array
    // and insert the children options after it but before the rest of the
    // siblings.
    if (isset($options[$browsenode->getId()])) {
      $key_pos = array_search($browsenode->getId(), array_keys($options));
      $options = array_slice($options, 0, $key_pos + 1, TRUE) + $children + array_slice($options, $key_pos + 1, -1, TRUE);
    }
    // Otherwise simply add the children directly.
    else {
      $options = $children;
    }
  }

  return $options;
}
