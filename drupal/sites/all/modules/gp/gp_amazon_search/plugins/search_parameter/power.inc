<?php
/**
 * @file
 * Plugin definition for browsenode search parameter.
 */

$plugin = array(
  'title' => 'Power',
);

/**
 * Form callback for this parameter.
 */
function gp_amazon_search_power_parameter_form($form, &$form_state) {
  // Remember ajax form to include this file.
  form_load_include($form_state, 'inc', 'gp_amazon_search', 'plugins/search_parameter/power');

  $form['parameters']['Power'] = array(
    '#type' => 'fieldset',
    '#title' => t('Power', array(), array('context' => 'amazon-search-parameters')),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#attributes' => array('id' => array('power-wrapper')),
    // Use different parents to prevent this group of fields
    // ending up in the parameters tree.
    '#parents' => array('power'),
  );

  // Shortcut to our form element.
  $container = &$form['parameters']['Power'];
  // And to our values.
  $values = !empty($form_state['values']['power']) ? $form_state['values']['power'] : array();
  // Also get all keywords available.
  $power_keywords = gp_amazon_search_power_parameter_keywords();

  // Add explanation of operators.
  $container['help'] = array(
    '#markup' => gp_amazon_search_power_parameter_help(),
  );

  // Present a select to add keywords.
  $container['choice'] = array(
    '#type' => 'select',
    '#title' => t('Add a keyword'),
    '#options' => $power_keywords,
  );

  $container['add_keyword'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#name' => 'op_add_keyword',
    '#submit' => array('gp_amazon_search_power_parameter_form_add_submit'),
    '#ajax' => array(
      'callback' => 'gp_amazon_search_power_parameter_form_add_callback',
      'wrapper' => 'power-wrapper',
    ),
  );

  // Generate value field and operator for every keyword already selected.
  $keywords = !empty($form_state['power']) ? $form_state['power'] : array();

  foreach ($keywords as $delta => $type) {
    $container[$delta] = array(
      '#type' => 'container',
    );
    $default_value = !empty($values['power'][$delta]['value']) ? $values['power'][$delta]['value'] : '';
    $container[$delta]['value'] = array(
      '#type' => 'textfield',
      '#title' => $power_keywords[$type],
      '#default_value' => $default_value,
    );

    // Render an operator for every element but the first.
    if (0 === $delta) {
      continue;
    }

    $default_value = !empty($values['power'][$delta]['operator']) ? $values['power'][$delta]['operator'] : '';
    $container[$delta]['operator'] = array(
      '#type' => 'select',
      '#title' => t('Operator'),
      '#options' => drupal_map_assoc(array('and', 'or')),
      '#default_value' => $default_value,
      // Show before the value field.
      '#weight' => -1,
    );
  }

  // Add our submit handler to the form generate button.
  // @todo add a validation callback to the plugin declaration.
  array_unshift($form['actions']['submit']['#submit'], 'gp_amazon_search_power_parameter_form_generate_submit');

  return $form;
}

/**
 * Submit handler for the add keyword.
 */
function gp_amazon_search_power_parameter_form_add_submit($form, &$form_state) {
  // Add the submitted power parameter to list.
  $form_state['power'][] = $form_state['values']['power']['choice'];

  // Rebuild form.
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback for the add keyword.
 */
function gp_amazon_search_power_parameter_form_add_callback($form, &$form_state) {
  return $form['parameters']['Power'];
}

/**
 * Submit handler for the generate button.
 */
function gp_amazon_search_power_parameter_form_generate_submit($form, &$form_state) {
  // Get our power values.
  $values = !empty($form_state['values']['power']) ? $form_state['values']['power'] : array();
  // Remove non-keywords values.
  unset($values['choice'], $values['add_keyword']);

  // If values are empty, quit.
  if (empty($values)) {
    return;
  }

  $string = '';

  // Loop items and generate the power string.
  foreach ($values as $delta => $data) {
    // If no value inserted, skip.
    if (empty($data['value'])) {
      continue;
    }

    // Get the type of this element.
    $type = $form_state['power'][$delta];

    // Add the operator if provided.
    if (isset($data['operator'])) {
      $string .= " {$data['operator']} ";
    }

    // Add the value.
    $string .= "{$type}:{$data['value']}";
  }

  // If string has been populated, add to values.
  if (strlen($string)) {
    $form_state['values']['parameters']['Power'] = $string;
  }
}

/**
 * Helper to return all keywords of the power search.
 */
function gp_amazon_search_power_parameter_keywords() {
  return array(
    'ASIN' => t('ASIN', array(), array('context' => 'amazon-search-power')),
    'author' => t('Author', array(), array('context' => 'amazon-search-power')),
    'author-begins' => t("Author's name begins with", array(), array('context' => 'amazon-search-power')),
    'author-exact' => t("Author's name is exactly", array(), array('context' => 'amazon-search-power')),
    'binding' => t('Binding', array(), array('context' => 'amazon-search-power')),
    'EISBN' => t('EISBN of the digital book', array(), array('context' => 'amazon-search-power')),
    'ISBN' => t('ISBN of the book', array(), array('context' => 'amazon-search-power')),
    'keywords' => t('Keywords in the title or description', array(), array('context' => 'amazon-search-power')),
    'keywords-begin' => t('Keywords that begin with', array(), array('context' => 'amazon-search-power')),
    'language' => t('Language', array(), array('context' => 'amazon-search-power')),
    'pubdate' => t('Publication date', array(), array('context' => 'amazon-search-power')),
    'publisher' => t('Name of the publisher', array(), array('context' => 'amazon-search-power')),
    'subject' => t('Subject contains these words', array(), array('context' => 'amazon-search-power')),
    'subject-begins' => t('Subject begins with', array(), array('context' => 'amazon-search-power')),
    'subject-words-begin' => t('Subject begins with these words', array(), array('context' => 'amazon-search-power')),
    'title' => t('Title', array(), array('context' => 'amazon-search-power')),
    'title-begins' => t('Title of the book begins exactly with', array(), array('context' => 'amazon-search-power')),
    'title-words-begins' => t('Title of the book begins with these words', array(), array('context' => 'amazon-search-power')),
  );
}

/**
 * Returns help information about Amazon Power parameters.
 */
function gp_amazon_search_power_parameter_help() {
  $output = '<div>';

  $output .= t('You can use the following operators inside every parameter:');
  $output .= '<ul>';
  $output .= '<li><strong>not</strong> : ';
  $output .= t('Excludes the following parameter from the results. For example, <em>subject:history and not military</em> excludes military history in the results.');
  $output .= '</li><li><strong>and</strong> : ';
  $output .= t('Specifies that both values must be true to be selected. For example, <em>subject:history and (Spanish and Mexican)</em> requires that the books selected contain both Spanish and Mexican history.');
  $output .= '</li><li><strong>or</strong> : ';
  $output .= t('Exclusive or which means one of either item but not both. For example <em>subject:history and (Spanish or Mexican)</em> means the subject matter can be about the history of Spain or Mexico, but not both.');
  $output .= '</li></ul></div>';

  return $output;
}
