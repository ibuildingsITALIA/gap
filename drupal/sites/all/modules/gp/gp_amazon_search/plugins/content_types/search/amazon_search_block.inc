<?php
/**
 * @file
 * Plugin to render items based on a configurable Amazon search.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Search block'),
  'description' => t('Configurable Amazon search block'),
  'category' => 'Giunti',
);

/**
 * Render the configured search pane.
 */
function gp_amazon_search_amazon_search_block_content_type_render($subtype, $conf, $panel_args, $context) {
  // Get search string for further processing.
  $search_string = $conf['search_string'];

  // Replace custom tokens in the string.
  $search_string = gp_amazon_search_replace_tokens($search_string);

  $search_parameters = explode('&', $search_string);

  // Explode parameters.
  $parameters = array();
  foreach ($search_parameters as $single) {
    list($key, $value) = explode('=', $single);
    $parameters[$key] = $value;
  }

  // Add (or overwrite) search index.
  $parameters += array(
    'SearchIndex' => $conf['search_index'],
  );

  $search = gp_amazon_search_items($parameters, GP_AMAZON_CACHE_TIME);

  if (!$search || !$search->getCount()) {
    // No results.
    return array();
  }

  $view_mode = !empty($conf['item_view_mode']) ? $conf['item_view_mode'] : 'image';
  $wrapper_attributes_array = array();
  if ('image' === $view_mode && empty($conf['show_search_elements'])) {
    $wrapper_attributes_array['class'][] = 'render-mode-slick';

    // Big image only for image view mode.
    if (!empty($conf['render_mode'])) {
      $wrapper_attributes_array['class'][] = 'big-images-style';
    }
  }

  // Common settings for this block.
  $block = new stdClass();
  $block->title = t('Highlight');
  $block->module = 'amazon_search';
  $block->delta = REQUEST_TIME;
  $block->content = array(
    '#prefix' => '<div' . drupal_attributes($wrapper_attributes_array) . '>',
    '#suffix' => '</div>',
  );

  // Render as search or items based on configuration.
  if (!empty($conf['show_search_elements'])) {
    $block->content['#theme'] = 'gp_amazon_search_results';
    $block->content['#search'] = $search;
    $block->content['#view_mode'] = gp_amazon_get_query_view_mode($view_mode);
  }
  else {
    // Render only the number of items specified in the panel.
    $items = $search->getItems();
    $length = !empty($conf['max_results']) ? $conf['max_results'] : 10;
    $items = array_slice($items, 0, $length, TRUE);

    $block->content['#theme'] = 'gp_amazon_items';
    $block->content['#items'] = $items;
    $block->content['#view_mode'] = $view_mode;
  }

  // If title_link is set, generate an href to a search page.
  if (!empty($conf['title_link'])) {
    switch ($conf['title_link']) {
      case 1:
        $block->title_link = array(
          'href' => 'search',
          'query' => gp_amazon_search_get_search_query($parameters),
        );
        break;

      case 2:
        // Since Panels handle well only links as array, parse the url back
        // into simple components.
        $components = drupal_parse_url($conf['title_link_path']);
        // Again, Panels uses the href key instead of path.
        $components['href'] = $components['path'];
        $block->title_link = $components;
    }
  }

  return $block;
}

/**
 * Form settings for amazon_search.
 */
function gp_amazon_search_amazon_search_block_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Prepare search indexes options.
  $search_indexes = array();
  foreach (gp_amazon_search_indexes() as $key => $index) {
    $search_indexes[$key] = $index['label'];
  }

  $form['search_index'] = array(
    '#type' => 'select',
    '#title' => t('Search index'),
    '#options' => $search_indexes,
    '#default_value' => !empty($conf['search_index']) ? $conf['search_index'] : NULL,
    '#required' => TRUE,
  );

  $form['search_string'] = array(
    '#type' => 'textfield',
    '#title' => t('Search query string'),
    '#description' => t('Enter a valid query string for the ItemLookup API endpoint.'),
    '#default_value' => !empty($conf['search_string']) ? $conf['search_string'] : '',
    '#required' => TRUE,
    '#size' => 150,
  );

  $form['title_link'] = array(
    '#type' => 'radios',
    '#title' => t('Link title'),
    '#options' => array(
      0 => t('No link'),
      1 => t('Link to search'),
      2 => t('Custom path'),
    ),
    '#default_value' => isset($conf['title_link']) ? $conf['title_link'] : 0,
  );

  $form['title_link_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Title link path'),
    '#description' => t('Insert a full path to use as link for the title.'),
    '#default_value' => !empty($conf['title_link_path']) ? $conf['title_link_path'] : '',
    '#states' => array(
      'visible' => array(
        ':input[name="title_link"]' => array('value' => 2),
      ),
      'required' => array(
        ':input[name="title_link"]' => array('value' => 2),
      ),
    ),
  );

  $form['show_search_elements'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show search elements'),
    '#description' => t('Show this block with pager and header elements.'),
    '#default_value' => !empty($conf['show_search_elements']),
  );

  $form['max_results'] = array(
    '#type' => 'select',
    '#title' => t('Maximum results to show'),
    '#options' => drupal_map_assoc(range(1, 10)),
    '#default_value' => !empty($conf['max_results']) ? $conf['max_results'] : 10,
    '#states' => array(
      'visible' => array(
        ':input[name="show_search_elements"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['item_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Default item view mode'),
    '#options' => gp_amazon_item_view_modes(),
    '#default_value' => !empty($conf['item_view_mode']) ? $conf['item_view_mode'] : 'image',
    '#required' => TRUE,
  );

  $form['render_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Big covers'),
    '#default_value' => !empty($conf['render_mode']),
    '#states' => array(
      'visible' => array(
        ':input[name="item_view_mode"]' => array('value' => 'image'),
        ':input[name="show_search_elements"]' => array('checked' => FALSE),
      ),
    ),
  );

  return $form;
}

/**
 * Validate handler for settings form.
 */
function gp_amazon_search_amazon_search_block_content_type_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (2 == $values['title_link'] && !strlen($values['title_link_path'])) {
    form_set_error('title_link_path', t('Path is required'));
  }
}

/**
 * Submit handler to save settings.
 */
function gp_amazon_search_amazon_search_block_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['search_index'] = $form_state['values']['search_index'];
  $form_state['conf']['search_string'] = $form_state['values']['search_string'];
  $form_state['conf']['title_link'] = $form_state['values']['title_link'];
  $form_state['conf']['title_link_path'] = $form_state['values']['title_link_path'];
  $form_state['conf']['show_search_elements'] = $form_state['values']['show_search_elements'];
  $form_state['conf']['max_results'] = $form_state['values']['max_results'];
  $form_state['conf']['item_view_mode'] = $form_state['values']['item_view_mode'];
  $form_state['conf']['render_mode'] = $form_state['values']['render_mode'];
}
