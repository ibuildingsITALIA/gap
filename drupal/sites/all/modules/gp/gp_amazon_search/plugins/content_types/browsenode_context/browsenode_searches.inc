<?php
/**
 * @file
 * Plugin to render top item sets for an Amazon BrowseNode.
 */

$plugin = array(
  'title' => t('BrowseNode searches'),
);

/**
 * Return all content types available.
 */
function gp_amazon_search_browsenode_searches_content_type_content_types($plugin) {
  $list = gp_amazon_search_browsenode_searches_types();

  $types = array();
  foreach ($list as $key => $data) {
    $types[$key] = array(
      'title' => $data['label'],
      'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
      'category' => 'Amazon Browse Node',
    );
  }

  return $types;
}

/**
 * Helper function to get searches.
 *
 * The array is organized this way:
 *   - key: the name of the subtype;
 *   - label: the label to show;
 *   - search: the search string to use. Don't include browsenode and
 *             searchIndex as they will be automatically added.
 *
 * @return array
 *   An array of searches.
 */
function gp_amazon_search_browsenode_searches_types() {
  return array(
    'reviewrank' => array(
      'label' => t('Average customer rating'),
      'search' => 'Sort=reviewrank',
    ),
    'upcomingbooks-2months' => array(
      'label' => t('Next 2 months book releases'),
      'search' => 'Power=[prossime-uscite]',
    ),
  );
}

/**
 * Render top item sets for the provided BrowseNode.
 */
function gp_amazon_search_browsenode_searches_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return array();
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();
  $search_index = $browsenode->getSearchIndex();

  // Add base parameters.
  $parameters = array(
    'BrowseNode' => $browsenode->getId(),
    'SearchIndex' => $search_index['key'],
  );

  // Get current subtype search.
  $types = gp_amazon_search_browsenode_searches_types();

  // Safety check.
  if (!isset($types[$subtype])) {
    return array();
  }

  // Add parameters specified by the search.
  $search_string = gp_amazon_search_replace_tokens($types[$subtype]['search']);
  // Use context tokens too.
  $search_string = ctools_context_keyword_substitute($search_string, array(), $context);
  foreach (explode('&', $search_string) as $single) {
    list($key, $value) = explode('=', $single);
    $parameters[$key] = $value;
  }

  // Perform the search.
  $search = gp_amazon_search_items($parameters, GP_AMAZON_CACHE_TIME);

  if (!$search || !$search->getCount()) {
    // No results.
    return array();
  }

  // Render only the number of items specified in the panel.
  $items = $search->getItems();
  $items = array_slice($items, 0, $conf['max_results']);

  $render = array(
    '#theme' => 'gp_amazon_items',
    '#items' => $items,
    '#view_mode' => 'image',
  );

  // Wrap if render mode is enabled.
  if (!empty($conf['render_mode'])) {
    $render += array(
      '#prefix' => '<div class="render-mode-slick">',
      '#suffix' => '</div>',
    );
  }

  $block = new stdClass();
  $block->title = $types[$subtype]['label'];
  $block->module = 'gp_amazon_search';
  $block->delta = REQUEST_TIME;
  $block->content = $render;

  return $block;
}

/**
 * Form settings for browsenode_topitems.
 */
function gp_amazon_search_browsenode_searches_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['max_results'] = array(
    '#type' => 'select',
    '#title' => t('Maximum results to show'),
    '#options' => drupal_map_assoc(range(1, 10)),
    '#default_value' => !empty($conf['max_results']) ? $conf['max_results'] : 10,
    '#required' => TRUE,
  );

  // Calling it render mode to allow adding new render modes in the future
  // without rename variables.
  $form['render_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Render as carousel'),
    '#default_value' => isset($conf['render_mode']) ? $conf['render_mode'] : TRUE,
  );

  return $form;
}

/**
 * Submit handler to save setting.
 */
function gp_amazon_search_browsenode_searches_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['max_results'] = $form_state['values']['max_results'];
  $form_state['conf']['render_mode'] = $form_state['values']['render_mode'];
}
