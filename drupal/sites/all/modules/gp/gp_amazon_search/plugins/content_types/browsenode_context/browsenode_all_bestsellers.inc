<?php
/**
 * @file
 * Plugin to render bestsellers for all the children categories of a browsenode.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('BrowseNode all categories bestsellers'),
  'description' => t('A block with all the children categories bestsellers'),
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
  'category' => 'Amazon Browse Node',
);

/**
 * Render a menu tree for the provided BrowseNode.
 */
function gp_amazon_search_browsenode_all_bestsellers_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data) || FALSE === $context->data) {
    return array();
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();

  $children = $browsenode->getChildren();

  // Quit if no children.
  if (empty($children)) {
    return array();
  }

  $content = array();
  foreach ($children as $node) {
    /* @var AmazonBrowsenode $node */
    // Load the full node to get the item sets.
    $full_node = gp_amazon_get_browsenode_clean($node->getId());
    if (!$full_node) {
      continue;
    }

    $set = $full_node->getItemSet('TopSellers');
    if (is_null($set)) {
      // Item set not set (lol), do not render.
      continue;
    }

    // Get all asins to load, as the set doesn't return full items.
    $asins = array();
    foreach ($set as $row) {
      $asins[] = $row['ASIN'];
    }

    $items = gp_amazon_get_items($asins);

    if (empty($items)) {
      // No items found or request failed.
      continue;
    }

    $content[$full_node->getId()] = array(
      '#prefix' => '<div class="browsenode-bestseller">',
      'title' => array(
        '#markup' => '<h2 class="pane-title">' . l($full_node->getName(), "bestseller/{$full_node->getId()}") . '</h2>',
      ),
      'items' => array(
        '#theme' => 'gp_amazon_items',
        '#items' => $items,
        '#view_mode' => 'image',
        '#prefix' => '<div class="render-mode-slick">',
        '#suffix' => '</div>',
      ),
      '#suffix' => '</div>',
    );
  }

  $block = new stdClass();
  $block->title = '';
  $block->module = 'browsenode_all_bestsellers';
  $block->delta = $browsenode->getId();
  $block->content = $content;
  return $block;
}

/**
 * Form settings for browsenode_nav.
 */
function gp_amazon_search_browsenode_all_bestsellers_content_type_edit_form($form, &$form_state) {
  return $form;
}
