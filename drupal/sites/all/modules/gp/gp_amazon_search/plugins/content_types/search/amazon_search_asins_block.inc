<?php
/**
 * @file
 * Plugin to render up to ten items by asin.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Items block'),
  'description' => t('Configurable Amazon items block'),
  'category' => 'Giunti',
);

/**
 * Render the configured items pane.
 */
function gp_amazon_search_amazon_search_asins_block_content_type_render($subtype, $conf, $panel_args, $context) {
  $asins = $conf['asins'];

  // If no asins has been specified, quit.
  if (!count($asins)) {
    return array();
  }

  $items = gp_amazon_get_items($asins);

  if (empty($items)) {
    // No items found or request failed.
    return array();
  }

  $view_mode = !empty($conf['item_view_mode']) ? $conf['item_view_mode'] : 'image';
  $style_classes = ($view_mode === 'image') ? 'render-mode-slick' : '';
  $style_classes = $style_classes . (!empty($conf['render_mode']) ? ' big-images-style' : '');

  $block = new stdClass();
  $block->title = t('Items');
  $block->module = 'amazon_search';
  $block->delta = REQUEST_TIME;
  $block->content = array(
    '#theme' => 'gp_amazon_items',
    '#items' => $items,
    '#view_mode' => $view_mode,
    '#prefix' => '<div class="' . $style_classes . '">',
    '#suffix' => '</div>',
  );

  // If title_link is set, generate an href to a search page.
  if (!empty($conf['title_link'])) {
    switch ($conf['title_link']) {
      case 1:
        $query = array(
          'asin' => implode(',', $asins),
        );

        // If we have a title override,
        // pass it to the search page to have a title.
        if ($conf['override_title'] && !empty($conf['override_title_text'])) {
          $query['bt'] = $conf['override_title_text'];
        }

        $block->title_link = array(
          'href' => 'search',
          'query' => $query,
        );
        break;

      case 2:
        // Since Panels handle well only links as array, parse the url back
        // into simple components.
        $components = drupal_parse_url($conf['title_link_path']);
        // Again, Panels uses the href key instead of path.
        $components['href'] = $components['path'];
        $block->title_link = $components;
    }
  }

  return $block;
}

/**
 * Form settings for item block.
 */
function gp_amazon_search_amazon_search_asins_block_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['title_link'] = array(
    '#type' => 'radios',
    '#title' => t('Link title'),
    '#options' => array(
      0 => t('No link'),
      1 => t('Link to search'),
      2 => t('Custom path'),
    ),
    '#default_value' => isset($conf['title_link']) ? $conf['title_link'] : 0,
  );

  $form['title_link_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Title link path'),
    '#description' => t('Insert a full path to use as link for the title.'),
    '#default_value' => !empty($conf['title_link_path']) ? $conf['title_link_path'] : '',
    '#states' => array(
      'visible' => array(
        ':input[name="title_link"]' => array('value' => 2),
      ),
      'required' => array(
        ':input[name="title_link"]' => array('value' => 2),
      ),
    ),
  );

  // Render ten asins textfields.
  $form['asins'] = array(
    '#tree' => TRUE,
  );
  for ($i = 1; $i < 11; $i++) {
    $form['asins'][$i] = array(
      '#type' => 'textfield',
      '#title' => t('ASIN n. @count', array('@count' => $i)),
      '#default_value' => !empty($conf['asins'][$i]) ? $conf['asins'][$i] : '',
    );
  }

  $form['item_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Default item view mode'),
    '#options' => gp_amazon_item_view_modes(),
    '#default_value' => !empty($conf['item_view_mode']) ? $conf['item_view_mode'] : 'image',
    '#required' => TRUE,
  );

  $form['render_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Big covers'),
    '#default_value' => !empty($conf['render_mode']),
    '#states' => array(
      // Hide field when view mode isn't image.
      'visible' => array(
        ':input[name="item_view_mode"]' => array('value' => 'image'),
      ),
    ),
  );

  return $form;
}

/**
 * Validate handler for settings form.
 */
function gp_amazon_search_amazon_search_asins_block_content_type_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (2 == $values['title_link'] && !strlen($values['title_link_path'])) {
    form_set_error('title_link_path', t('Path is required'));
  }
}

/**
 * Submit handler to save settings.
 */
function gp_amazon_search_amazon_search_asins_block_content_type_edit_form_submit($form, &$form_state) {
  for ($i = 1; $i < 11; $i++) {
    if (!empty($form_state['values']['asins'][$i])) {
      $form_state['conf']['asins'][$i] = $form_state['values']['asins'][$i];
    }
  }

  $form_state['conf']['title_link'] = $form_state['values']['title_link'];
  $form_state['conf']['title_link_path'] = $form_state['values']['title_link_path'];
  $form_state['conf']['item_view_mode'] = $form_state['values']['item_view_mode'];
  $form_state['conf']['render_mode'] = $form_state['values']['render_mode'];
}
