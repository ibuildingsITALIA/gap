<?php
/**
 * @file
 * Plugin to render items for an Amazon Keyword search.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Search results'),
  'description' => t('Results of an Amazon search'),
  'category' => 'Amazon Search',
);

/**
 * Render results for the search.
 */
function gp_amazon_search_amazon_search_results_content_type_render($subtype, $conf, $panel_args, $context) {
  $asins = isset($_GET['asin']) ? $_GET['asin'] : FALSE;

  // If the parameter asin has been specified, we are going to make an
  // ItemLookup request and render only those items.
  if ($asins) {
    $block = _gp_amazon_search_asin_search_results($asins, $conf);
  }
  // Otherwise we do a normal search.
  else {
    $block = _gp_amazon_search_normal_search_results($conf);
  }

  return $block;
}

/**
 * Render results for an asin search.
 */
function _gp_amazon_search_asin_search_results($asins, $conf) {
  $block_title = isset($_GET['bt']) ? $_GET['bt'] : FALSE;

  $asins = explode(',', $asins);
  $items = gp_amazon_get_items($asins);

  if (empty($items)) {
    // No results.
    drupal_set_message(t('No results found'), 'warning');
    return array();
  }

  $page_title = empty($block_title) ? t('Search') : $block_title;

  // Render pane.
  $block = new stdClass();
  $block->title = $page_title;
  $block->module = 'search_results';
  $block->delta = REQUEST_TIME;
  $block->content = array(
    '#theme' => 'gp_amazon_items',
    '#items' => $items,
    '#view_mode' => gp_amazon_get_query_view_mode($conf['item_view_mode']),
  );
  return $block;
}

/**
 * Render results for a normal search.
 */
function _gp_amazon_search_normal_search_results($conf) {
  $block_title = isset($_GET['bt']) ? $_GET['bt'] : FALSE;
  $browsenode_id = isset($_GET['bnid']) ? $_GET['bnid'] : FALSE;
  $keywords = isset($_GET['keywords']) ? $_GET['keywords'] : FALSE;
  $advanced = isset($_GET['adv']) ? $_GET['adv'] : FALSE;

  // If no browsenode and no keywords provided, throw a message.
  if (!$browsenode_id && !$keywords) {
    drupal_set_message(t('You must specify a string to search.'));
    return array();
  }

  $parameters = array();

  // If keywords has been provided, add them to the parameters.
  if ($keywords) {
    $parameters['Keywords'] = $keywords;
  }

  // If advanced parameters has been provided, handle them.
  if ($advanced) {
    $parameters += drupal_get_query_array($advanced);
  }

  // Add the correct parameter if we are searching a browsenode or not.
  if ($index = gp_amazon_find_search_index_by_id($browsenode_id)) {
    $parameters += array(
      'SearchIndex' => $index['key'],
    );
  }
  else {
    // Add only BrowseNode, the search method will find the correct index.
    $parameters += array(
      'BrowseNode' => $browsenode_id,
    );
  }

  // Generate a search string based on keywords or other parameters.
  if ($keywords) {
    $search_string = '"' . $keywords . '"';
  }
  elseif ($block_title) {
    $search_string = '"' . $block_title . '"';
  }
  else {
    $search_string = t('Search');
  }

  // Set breadcrumb and title.
  if ('all' === $browsenode_id) {
    // We have the 'all' search index, handle it.
    $breadcrumb = array($search_string);
    $page_title = "{$search_string} in {$index['label']}";
  }
  elseif ($browsenode = gp_amazon_get_browsenode_clean($browsenode_id)) {
    $breadcrumb = gp_amazon_get_breadcrumb($browsenode, $search_string);
    $page_title = "{$search_string} in {$browsenode->getName()}";
  }
  else {
    // Fallback in case of some failure retrieving browsenode.
    $breadcrumb = array($search_string);
    $page_title = $search_string;
  }

  drupal_set_breadcrumb($breadcrumb);
  drupal_set_title($page_title);

  // Do the search.
  $search = gp_amazon_search_items($parameters, GP_AMAZON_CACHE_TIME);

  if (!$search || !$search->getCount()) {
    // No results.
    drupal_set_message(t('No results found'), 'warning');
    return array();
  }

  // Render pane.
  $block = new stdClass();
  $block->title = $page_title;
  $block->module = 'search_results';
  $block->delta = REQUEST_TIME;
  $block->content = array(
    '#theme' => 'gp_amazon_search_results',
    '#search' => $search,
    '#view_mode' => gp_amazon_get_query_view_mode($conf['item_view_mode']),
  );
  return $block;
}

/**
 * Form settings for search_results.
 */
function gp_amazon_search_amazon_search_results_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['item_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Default item view mode'),
    '#options' => gp_amazon_item_view_modes(),
    '#default_value' => !empty($conf['item_view_mode']) ? $conf['item_view_mode'] : 'detail',
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Submit handler to save item_view_mode setting.
 */
function gp_amazon_search_amazon_search_results_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['item_view_mode'] = $form_state['values']['item_view_mode'];
}
