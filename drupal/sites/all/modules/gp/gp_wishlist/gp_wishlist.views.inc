<?php

/**
 * Implements hook_views_data().
 */
function gp_wishlist_views_data() {

  $data = array();

  $data['gp_wishlist']['table']['group'] = t('Wishlist');

  $data['gp_wishlist']['table']['base'] = array(
    'title' => t('Wishlist'),
    'help' => t('Contains the wishlist.'),
  );

  $data['gp_wishlist']['table']['join'] = array(
    'user' => array(
      'left_field' => 'nid',
      'field' => 'uid',
    ),
  );

  // The ASIN field.
  $data['gp_wishlist']['asin'] = array(
    'title' => t('Product ASIN'),
    'help' => t('The Amazon ASIN for the wishlisted product.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // The created field.
  $data['gp_wishlist']['created'] = array(
    'title' => t('Created'),
    'help' => t('The wishlist item created time.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // The User ID field.
  $data['gp_wishlist']['uid'] = array(
    'title' => t('Owner user'),
    'help' => t('The user for the wishlist item.'),
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'user',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user',
      'numeric' => TRUE,
      'validate type' => 'uid',
    ),
  );

  return $data;
}
