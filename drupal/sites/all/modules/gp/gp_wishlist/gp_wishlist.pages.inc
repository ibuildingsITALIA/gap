<?php

/**
 * Page callback to add or remove a product from the user wishlist.
 */
function gp_wishlist_product_toggle_callback($js, $account, $asin, $op) {
  // Anonymous user doesn't have any wishlist.
  if ($account->uid === 0) {
    drupal_access_denied();
    drupal_exit();
  }

  if ($op == 'product_add') {
    gp_wishlist_add_product($asin, $account->uid);
  }
  elseif ($op == 'product_remove') {
    gp_wishlist_remove_product($asin, $account->uid);
  }
  else {
    // WTF?
    drupal_not_found();
    drupal_exit();
  }

  $output = theme('gp_wishlist_product_link', array(
    'asin' => $asin,
  ));

  if (!$js) {
    return $output;
  }

  ctools_include('ajax');

  $commands = array();
  $commands[] = ajax_command_replace('#wishlist-' . $asin, $output);

  print ajax_render($commands);
  exit;
}

/**
 * Page callback to add or remove a product from the user wishlist.
 */
function gp_wishlist_product_remove_listing_callback($js, $account, $asin, $op) {
  // Anonymous user doesn't have any wishlist.
  if ($account->uid === 0) {
    drupal_access_denied();
    drupal_exit();
  }

  gp_wishlist_remove_product($asin, $account->uid);

  if (!$js) {
    return;
  }

  ctools_include('ajax');

  $commands = array();
  $commands[] = ajax_command_remove('#wishlist-item-' . $asin);

  print ajax_render($commands);
  exit;
}

/**
 * Page callback to list the user wishlist.
 */
function gp_wishlist_user_list_page($account) {
  $asins = gp_wishlist_get_by_user($account->uid);
  $products = gp_amazon_get_items($asins);

  return array(
    '#theme_wrappers' => array('gp_wishlist_user_page'),
    '#products' => $products,
  );
}
