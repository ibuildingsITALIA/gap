<?php
/**
 * @file
 * Theme functions and template preprocess.
 */

/**
 * Render a wishlist status for a product.
 */
function theme_gp_wishlist_product_link($variables) {
  global $user;
  $asin = $variables['asin'];
  $status = gp_wishlist_product_is_wishlisted($asin);

  $attributes = array(
    'class' => array('gp-wishlist-link'),
    'id' => 'wishlist-' . $asin,
  );

  if ($status) {
    $attributes['class'][] = 'wishlisted';
    $link_text = t('Remove from wishlist');
    $link_action = 'product_remove';
  }
  else {
    $attributes['class'][] = 'not-wishlisted';
    $link_text = t('Add to wishlist');
    $link_action = 'product_add';
  }

  ctools_include('ajax');
  $url = 'gp_wishlist/nojs/' . $user->uid . '/' . $asin . '/' . $link_action;
  $text = ctools_ajax_text_button($link_text, $url, '', 'gp-wishlist-ajax-link gp-wishlist-' . $asin);

  return '<div' . drupal_attributes($attributes) . '>' . $text . '</div>';
}

/**
 * Render a wishlist remove link for a product.
 */
function theme_gp_wishlist_product_remove_link_listing($variables) {
  global $user;
  $asin = $variables['asin'];

  ctools_include('ajax');

  $link_text = t('Remove');
  $alt_text = t('Remove this product from the wishlist');
  $url = 'gp_wishlist/nojs/' . $user->uid . '/' . $asin . '/product_remove_listing';
  $classes = 'gp-wishlist-ajax-link gp-wishlist-remove-link gp-wishlist-' . $asin;

  $text = ctools_ajax_text_button($link_text, $url, $alt_text, $classes);
  return $text;
}
