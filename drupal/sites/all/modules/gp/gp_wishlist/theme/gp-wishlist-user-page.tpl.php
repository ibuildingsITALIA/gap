

<?php foreach ($items as $asin => $item): ?>
  <div<?php print $item['item_attributes']; ?>>
    <?php print render($item['product']); ?>
    <div class="wishlist-actions">
      <?php print render($item['add_to_cart_form']); ?>
      <?php print render($item['wishlist']); ?>
    </div>
  </div>
<?php endforeach; ?>

<?php print render($pager); ?>
