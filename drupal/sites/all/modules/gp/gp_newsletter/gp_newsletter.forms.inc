<?php
/**
 * @file
 * Form declarations for gp_newsletter module.
 */

/**
 * Newsletter form.
 */
function gp_newsletter_form($form, &$form_state) {
  $form['prefix'] = array(
    '#prefix' => '<div class="prefix">',
    '#markup' => variable_get('gp_newsletter_prefix', ''),
    '#suffix' => '</div>',
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#attributes' => array('placeholder' => t('Name')),
  );

  $form['surname'] = array(
    '#type' => 'textfield',
    '#title' => t('Surname'),
    '#attributes' => array('placeholder' => t('Surname')),
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#attributes' => array('placeholder' => t('E-mail')),
    '#required' => TRUE,
  );

  $form['suffix'] = array(
    '#prefix' => '<div class="suffix">',
    '#markup' => variable_get('gp_newsletter_suffix', ''),
    '#suffix' => '</div>',
  );

  $form['agree'] = array(
    '#type' => 'checkboxes',
    '#options' => array(
      'stats' => t('I authorise the use of my personal data for commercial and research purposes.'),
      'promo' => t('I authorise the use of my personal data for marketing purposes.'),
    ),
  );

  $form['actions'] = array('#type' => 'actions', '#weight' => 19);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register'),
  );

  $form['unsubscribe'] = array(
    '#markup' => l(t('Want to unsubscribe? Click here'), 'newsletter/unsubscribe'),
    '#weight' => 20,
  );
  return $form;
}

/**
 * Validate handler for gp_newsletter_form.
 */
function gp_newsletter_form_validate($form, &$form_state) {
  if ($error = user_validate_mail($form_state['values']['email'])) {
    form_set_error('email', $error);
  }
}

/**
 * Submit handler for gp_newsletter_form.
 */
function gp_newsletter_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  $email = $values['email'];
  $name = isset($values['name']) ? $values['name'] : '';
  $surname = isset($values['surname']) ? $values['surname'] : '';

  $remote = new NewsletterRemoteApi();
  $already_subscribed = $remote->getSubscriptionInfo($email);
  if ($already_subscribed && isset($remote->getData()['active']) && TRUE === $remote->getData()['active']) {
    drupal_set_message(t('You have already registered to the newsletter.', array(), array('context' => 'gp_newsletter')), 'error');
    $form_state['rebuild'] = TRUE;
  }
  else {
    $new_remote = new NewsletterRemoteApi();
    $new_remote->subscribe($email, $name, $surname);

    if (!$new_remote->isValid()) {
      drupal_set_message(t('Sorry, an error has occurred during the process. Please try again.', array(), array('context' => 'gp_newsletter')), 'error');
      $form_state['rebuild'] = TRUE;
    }
    else {
      drupal_set_message(t('Thank you for your registration.'));
      drupal_goto();
    }
  }
}

function gp_newsletter_unsubscribe_form($form, &$form_state) {
  $form['prefix'] = array(
    '#prefix' => '<div class="prefix">',
    '#markup' => t('Insert your email to unsubscribe the newsletter.'),
    '#suffix' => '</div>',
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#attributes' => array('placeholder' => t('E-mail')),
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Unsubscribe'),
  );

  return $form;
}

/**
 * Validate handler for gp_newsletter_unsubscribe_form.
 */
function gp_newsletter_unsubscribe_form_validate($form, &$form_state) {
  if ($error = user_validate_mail($form_state['values']['email'])) {
    form_set_error('email', $error);
  }
}

/**
 * Submit handler for gp_newsletter_unsubscribe_form.
 */
function gp_newsletter_unsubscribe_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  $email = $values['email'];

  $remote = new NewsletterRemoteApi();
  $already_subscribed = $remote->getSubscriptionInfo($email);
  if ($already_subscribed && $remote->isValid() && isset($remote->getData()['active']) && TRUE === $remote->getData()['active']) {
    $remote->unsubscribe($email);
    if ($remote->isValid()) {
      drupal_set_message(t('You have successfully unsubscribed your email from the newsletter.'));
      drupal_goto();
    }
    else {
      drupal_set_message(t('Sorry, an error has occurred during the process. Please try again.', array(), array('context' => 'gp_newsletter')), 'error');
      $form_state['rebuild'] = TRUE;
    }
  }
  else {
    drupal_set_message(t('You are not registered to the newsletter. Impossible to unsubscribe', array(), array('context' => 'gp_newsletter')), 'error');
    $form_state['rebuild'] = TRUE;
  }
}
