<?php
/**
 * @file
 * Class to handle remote API service.
 */

class NewsletterRemoteApi extends UglyGetRemoteApi {
  /**
   * Class constructor.
   *
   * This method has no parameters.
   */
  public function __construct() {
    parent::__construct('GET');

    $this->setBaseUrlFromVariable('gp_newsletter_api_url');
    $this->setParameter('api_key', variable_get('gp_newsletter_api_key', ''));
  }

  /**
   * Subscribe an email to the service.
   *
   * @param string $email
   *   The e-mail of the user.
   *
   * @param string $name
   *   Optional. The name of the user.
   *
   * @param string $surname
   *   Optional. The surname of the user.
   *
   * @return bool
   *   If the operation has been successful.
   */
  public function subscribe($email, $name = '', $surname = '') {
    $this->endpoint = 'newsletter/subscribe';
    $this->addParameters(array(
      'email' => $email,
      'name' => $name,
      'surname' => $surname,
    ));

    return $this->execute();
  }

  /**
   * Unsubscribe an email from the service.
   *
   * @param string $email
   *   The e-mail to unsubscribe.
   *
   * @return bool
   *   If the operation has been successful.
   */
  public function unsubscribe($email) {
    $this->endpoint = 'newsletter/unsubscribe';
    $this->setParameter('email', $email);

    return $this->execute();
  }

  /**
   * Retrieve subscription information.
   *
   * @param string $email
   *   The e-mail to get information for.
   *
   * @return bool
   *   If the operation has been successful.
   */
  public function getSubscriptionInfo($email) {
    $this->endpoint = 'newsletter';
    $this->setParameter('email', $email);

    return $this->execute();
  }
}
