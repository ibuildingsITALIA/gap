<?php
/**
 * @file
 *
 * Administration forms.
 */

/**
 * Admin settings form.
 */
function gp_newsletter_admin_settings_form() {
  $form = array();

  $form['newsletter_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Newsletter API settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['newsletter_api']['gp_newsletter_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Newsletter API url'),
    '#required' => TRUE,
    '#default_value' => variable_get('gp_newsletter_api_url', ''),
    '#size' => 100,
  );

  $form['newsletter_api']['gp_newsletter_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Newsletter API key'),
    '#required' => TRUE,
    '#default_value' => variable_get('gp_newsletter_api_key', ''),
    '#size' => 100,
  );

  $form['newsletter_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Newsletter form configuration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['newsletter_form']['gp_newsletter_prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Prefix'),
    '#description' => t('Text to add on top of the form.'),
    '#default_value' => variable_get('gp_newsletter_prefix', ''),
    '#rows' => 7,
  );

  $form['newsletter_form']['gp_newsletter_suffix'] = array(
    '#type' => 'textarea',
    '#title' => t('Suffix'),
    '#description' => t('Text to add under the form.'),
    '#default_value' => variable_get('gp_newsletter_suffix', ''),
    '#rows' => 7,
  );

  return system_settings_form($form);
}
