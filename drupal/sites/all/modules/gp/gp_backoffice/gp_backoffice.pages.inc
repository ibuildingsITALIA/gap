<?php
/**
 * @file
 * Page callbacks for backoffice module.
 */

/**
 * Page callback to render a message.
 */
function gp_backoffice_message_view_page($js, Message $message) {
  // Render only the fields we are intested in.
  $fields = array(
    'title' => 'message_rendered_subject',
    'body' => 'message_rendered_body',
  );
  $build = array();

  foreach ($fields as $key => $field_name) {
    $field = field_view_field('message', $message, $field_name, array(
      'label' => 'hidden',
      'module' => 'text',
      'settings' => array(),
      'type' => 'text_default',
    ));

    // Render only the field value, without wrappers.
    $build[$key] = drupal_render($field[0]);
  }

  if ($js) {
    // Include modal and ajax helpers.
    ctools_include('modal');

    ctools_modal_render($build['title'], $build['body']);
    drupal_exit();
  }

  // Set page title to the message one and display the body as content.
  drupal_set_title($build['title']);
  return $build['body'];
}

/**
 * Page callback for change user password on IdentityManager.
 */
function gp_backoffice_user_password_change($js, $user) {
  if ($js) {
    // Include modal and ajax helpers.
    ctools_include('modal');
    ctools_include('ajax');

    $sso_uid = gp_user_get_sso_uid($user);

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Change password for user «@user»', array('@user' => $user->realname)),
      'build_info' => array('args' => array($sso_uid, $user)),
    );

    $form_state += array(
      're_render' => FALSE,
      'no_redirect' => !empty($form_state['ajax']),
    );

    $form = drupal_build_form('gp_backoffice_user_password_change_form', $form_state);

    if (!empty($form_state['ajax']) && (!$form_state['executed'] || $form_state['rebuild'])) {
      $commands = ctools_modal_form_render($form_state, $form);
    }

    if ($form_state['executed'] && !$form_state['rebuild']) {
      $commands = array();
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
  }
  else {
    return drupal_get_form('gp_backoffice_user_password_change_form');
  }
}

/**
 * Form for change user password on IdentityManager.
 */
function gp_backoffice_user_password_change_form($form, &$form_state, $sso_uid, $user) {
  if (!isset($form_state['confirm']) || !($form_state['confirm'])) {
    $form_state['confirm'] = FALSE;
    $form = array();

    $form['new_password'] = array(
      '#type' => 'fieldset',
      '#title' => t('New password'),
    );

    $form['new_password']['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#size' => 15,
      '#description' => t('Insert here the new password for this user.'),
    );

    $form['new_password']['password-check'] = array(
      '#type' => 'textfield',
      '#title' => t('Repeat Password'),
      '#required' => TRUE,
      '#size' => 15,
      '#description' => t('Reinsert here the new password.'),
    );

    $form['new_password']['confirm_container'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="confirm-div">',
      '#suffix' => '</div>',
    );

    $form['new_password']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Change password'),
    );

    $form['password_reset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Password reset'),
    );

    $form['password_reset']['url_container'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="url-div">',
      '#suffix' => '</div>',
    );

    if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'gen_password_reset_url_button') {
      // Generate a new password reset URL.
      $url = gp_user_reset_pass_url($sso_uid, $user->data);

      $form['password_reset']['url_container']['password_reset_url'] = array(
        '#type' => 'item',
        '#title' => t('Password reset URL'),
        '#markup' => l($url, $url),
      );
    }

    $form['password_reset']['gen_password_reset_url'] = array(
      '#type' => 'button',
      '#value' => t('Generate password reset URL'),
      '#limit_validation_errors' => array(),
      '#name' => 'gen_password_reset_url_button',
      '#ajax' => array(
        'callback' => 'gp_backoffice_user_gen_password_reset_url',
        'wrapper' => 'url-div',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
  }
  else {
    $password = $form_state['values']['password'];

    $form['password'] = array(
      '#value' => $password,
    );

    $form['confirm_password'] = array(
      '#type' => 'fieldset',
      '#title' => t('Confirm new password'),
    );

    $desc = t('Attenzione. Stai cambiando la password di accesso ai nostri sistemi. Vuoi confermare questa operazione?');
    $form['confirm_password']['description'] = array(
      '#markup' => '<p>' . $desc . '</p>',
    );

    $form['confirm_password']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Change password'),
    );

    $form['confirm_password']['password_cancel'] = array(
      '#type' => 'submit',
      '#title' => t('Cancel'),
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('gp_backoffice_user_password_change_cancel'),
    );
  }

  return $form;
}

/**
 * Form validate for change user password on IdentityManager.
 */
function gp_backoffice_user_password_change_form_validate($form, &$form_state) {
  if ($form_state['confirm']) {
    return;
  }

  $sso_uid = $form_state['build_info']['args'][0];

  if (!isset($sso_uid)) {
    form_error($form, t('User not in Giunti IdentityManager.'));
  }

  if (empty($form_state['values']['password'])) {
    form_error($form, t('Password field is required.'));
  }

  if (empty($form_state['values']['password-check']) || ($form_state['values']['password-check'] != $form_state['values']['password'])) {
    form_error($form, t('Password fields must be equal.'));
  }
}

/**
 * Form submit for change user password on IdentityManager.
 */
function gp_backoffice_user_password_change_form_submit($form, &$form_state) {
  if (!isset($form_state['confirm']) || empty($form_state['confirm'])) {
    // This will cause the form to be rebuilt, entering the confirm part of the form.
    $form_state['confirm'] = TRUE;
    // Along with this.
    $form_state['rebuild'] = TRUE;
  }
  else {
    $sso_uid = $form_state['build_info']['args'][0];

    $password = $form['password']['#value'];
    $status = gp_user_im_user_password_save($sso_uid, $password);

    $form_state['confirm'] = FALSE;

    if (!$status) {
      $form_state['rebuild'] = TRUE;
      form_error($form, t('Error on password change. Identity Manager API returned an error (see logs for details).'));
    }
  }
}

/**
 * Form callback to cancel change user password on IdentityManager.
 */
function gp_backoffice_user_password_change_cancel($form, &$form_state) {
  $form_state['confirm'] = FALSE;
  $form_state['rebuild'] = TRUE;
  return;;
}

/**
 * Form ajax callback for generate passord reset URL.
 */
function gp_backoffice_user_gen_password_reset_url($form, &$form_state) {
  return $form['password_reset']['url_container'];
}

/**
 * Page callback for change user email on IdentityManager.
 */
function gp_backoffice_user_email_change($js, $user) {
  if ($js) {
    // Include modal and ajax helpers.
    ctools_include('modal');
    ctools_include('ajax');

    $sso_uid = gp_user_get_sso_uid($user);

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Change email for user «@user»', array('@user' => $user->realname)),
      'build_info' => array('args' => array($sso_uid, $user)),
    );

    $form_state += array(
      're_render' => FALSE,
      'no_redirect' => !empty($form_state['ajax']),
    );

    $form = drupal_build_form('gp_backoffice_user_email_change_form', $form_state);

    if (!empty($form_state['ajax']) && (!$form_state['executed'] || $form_state['rebuild'])) {
      $commands = ctools_modal_form_render($form_state, $form);
    }

    if ($form_state['executed'] && !$form_state['rebuild']) {
      $commands = array();
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
  }
  else {
    return drupal_get_form('gp_backoffice_user_email_change_form');
  }
}

/**
 * Form for change user email on IdentityManager.
 */
function gp_backoffice_user_email_change_form($form, &$form_state, $sso_uid, $user) {
  if (!isset($form_state['confirm']) || !($form_state['confirm'])) {
    $form_state['confirm'] = FALSE;
    $form = array();

    $form['current_email'] = array(
      '#type' => 'fieldset',
      '#title' => t('Current email'),
    );

    $form['user_email'] = array(
      '#value' => $user->mail,
    );

    // Get user data from IM.
    $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);

    $form['current_email']['drupal_email'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="user-mail">',
      '#markup' => $user->mail,
      '#suffix' => '</div>',
    );

    $form['new_email'] = array(
      '#type' => 'fieldset',
      '#title' => t('New email'),
    );

    $form['new_email']['email'] = array(
      '#type' => 'textfield',
      '#title' => t('email'),
      '#required' => TRUE,
      '#size' => 15,
      '#description' => t('Insert here the new email for this user.'),
      '#attributes' => array(
        'style' => 'width: 100%',
      ),
    );

    $form['new_email']['email-check'] = array(
      '#type' => 'textfield',
      '#title' => t('Repeat email'),
      '#required' => TRUE,
      '#size' => 15,
      '#description' => t('Reinsert here the new email.'),
      '#attributes' => array(
        'style' => 'width: 100%',
      ),
    );

    $form['new_email']['confirm_container'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="confirm-div">',
      '#suffix' => '</div>',
    );

    $form['new_email']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Change email'),
    );
  }
  else {
    $form['user_email'] = array(
      '#value' => $user->mail,
    );

    $email = $form_state['values']['email'];

    $form['email'] = array(
      '#value' => $email,
    );

    $form['confirm_email'] = array(
      '#type' => 'fieldset',
      '#title' => t('Confirm new email'),
    );

    $desc = t('Attenzione. Stai cambiando l\'email di accesso ai nostri sistemi. Vuoi confermare questa operazione?');
    $form['confirm_email']['description'] = array(
      '#markup' => '<p>' . $desc . '</p>',
    );

    $form['confirm_email']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Change email'),
    );

    $form['confirm_email']['email_cancel'] = array(
      '#type' => 'submit',
      '#title' => t('Cancel'),
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('gp_backoffice_user_email_change_cancel'),
    );
  }

  return $form;
}

/**
 * Form validate for change user email on IdentityManager.
 */
function gp_backoffice_user_email_change_form_validate($form, &$form_state) {
  if ($form_state['confirm']) {
    return;
  }

  $sso_uid = $form_state['build_info']['args'][0];

  if (!isset($sso_uid)) {
    form_error($form, t('User not in Giunti IdentityManager.'));
  }

  // Check if email field is filled.
  if (empty($form_state['values']['email'])) {
    form_error($form, t('Email field is required.'));
  }

  // Check if email fields are equal.
  if (empty($form_state['values']['email-check']) || ($form_state['values']['email-check'] != $form_state['values']['email'])) {
    form_error($form, t('Email fields must be equal.'));
  }

  // Check if user with passed email is already in use.
  $account = user_load_by_name($form_state['values']['email']);
  if (!empty($account) && ($form_state['values']['email'] != $form['user_email']['#value'])) {
    form_error($form, t('This email is already in use on the system.'));
    return;
  }

  // Check if cas user with passed email is already in use.
  $account = cas_user_load_by_name($form_state['values']['email']);
  if (!empty($account) && ($form_state['values']['email'] != $form['user_email']['#value'])) {
    form_error($form, t('This email is already in use on the system.'));
    watchdog('User', 'Cas name @cas_name for user @user_id is differ from name @name)', array(
      '@user_id' => $account->uid,
      '@name' => $account->name,
      '@cas_name' => $account->cas_name,
    ));
    return;
  }
}

/**
 * Form submit for change user email on IdentityManager.
 */
function gp_backoffice_user_email_change_form_submit($form, &$form_state) {
  if (!isset($form_state['confirm']) || empty($form_state['confirm'])) {
    // This will cause the form to be rebuilt, entering the confirm part of the form.
    $form_state['confirm'] = TRUE;
    // Along with this.
    $form_state['rebuild'] = TRUE;
  }
  else {
    // The IdentityManager ID.
    $sso_uid = $form_state['build_info']['args'][0];

    // The mail of the drupal user.
    $previous_email = $form['user_email']['#value'];

    // The new email.
    $email = $form['email']['#value'];
    // Save the new mail in Identity manager.
    $status = gp_user_im_user_email_save($sso_uid, $email);

    $form_state['confirm'] = FALSE;

    if (!$status) {
      $form_state['rebuild'] = TRUE;
      form_error($form, t('Error on email change. Identity Manager API returned an error (see logs for details).'));
    }
    else {
      // Update drupal user.
      $account = user_load_by_name($previous_email);
      watchdog('User', 'Changing name/email for user @user_id (cas @cas_name - ssouid @ssouid) from @previous_mail to @new_mail', array(
        '@user_id' => $account->uid,
        '@cas_name' => $account->cas_name,
        '@ssouid' => $sso_uid,
        '@previous_mail' => $previous_email,
        '@new_mail' => $email,
      ));
      user_save($account, array(
        'name' => $email,
        'mail' => $email,
        'cas_name' => $email,
      ));
    }
  }
}

/**
 * Form callback to cancel change user email on IdentityManager.
 */
function gp_backoffice_user_email_change_cancel($form, &$form_state) {
  $form_state['confirm'] = FALSE;
  $form_state['rebuild'] = TRUE;
  return;;
}

/**
 * Page callback for show user info on IdentityManager.
 */
function gp_backoffice_user_giunticard_info($js, $user) {
  $build = array();

  $sso_uid = gp_user_get_sso_uid($user);

  $fieldset_user_title = t('User data from user «@user» [uid: @ssouid]', array(
    '@user' => $user->realname,
    '@ssouid' => $sso_uid,
  ));
  $build['body'] = '<fieldset class="form-wrapper collapsible" id="edit-user-data"><legend><span class="fieldset-legend">' . $fieldset_user_title . '</span></legend><div class="fieldset-wrapper">';

  // Get user data and merge with values retrieved before.
  $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
  if (!$user_data) {
    // API error, return a message.
    $build['body'] .= '<p>' . t('An error has occurred. Please try again later.') . '</p>';
  }
  else {
    // Get schema.
    $schema = gp_user_field_schema_load();
    foreach ($schema as $name => $field) {
      if (empty($user_data[$name])) {
        continue;
      }

      // Get default value.
      $value = empty($user_data[$name]) ? '&nbsp;' : $user_data[$name];

      $build['body'] .= '<div class="form-item" style="padding: 0; clear: left;"><label>' . $name . '</label><div class="value">' . $value . '</div></div>';
    }
    $build['body'] .= '</div></fieldset>';

    // Check if a card is registered with our uuid and get the card info.
    $card_data = gp_card_remote_get_card_info_by_uid($sso_uid);

    // If card retrieved, show info.
    if (FALSE !== $card_data) {
      $fieldset_user_giunticard = t('GiuntiCard from user «@user» [uid: @ssouid] - @ean', array(
        '@user' => $user->realname,
        '@ssouid' => $sso_uid,
        '@ean' => $card_data['ean_code'],
      ));
      $build['body'] .= '<fieldset class="form-wrapper collapsible" id="edit-user-data"><legend><span class="fieldset-legend">' . $fieldset_user_giunticard . '</span></legend><div class="fieldset-wrapper">';

      foreach ($card_data as $name => $field) {
        if ((empty($card_data[$name]) && ('disabled' !== $name)) || (' ' === $card_data[$name]) || ('  ' === $card_data[$name]) || ('.' === $card_data[$name])) {
          continue;
        }

        // Get default value.
        switch ($name) {
          case 'disabled':
            $value = empty($card_data[$name]) ? 'FALSE' : 'TRUE';
            break;

          case 'last_update':
          case 'last_purchase':
            $value = $card_data[$name] . ' (' . gmdate("Y-m-d", $card_data[$name] / 1000) . ')';
            break;

          default:
            $value = empty($card_data[$name]) ? '&nbsp;' : $card_data[$name];
        }

        $build['body'] .= '<div class="form-item" style="padding: 0; clear: left;"><label>' . $name . '</label><div class="value">' . $value . '</div></div>';
      }

      $build['body'] .= '</div></fieldset>';
    }

    // If bind card is not the same retrieved, show bind card info.
    if (!empty($user_data[GP_CARD_IM_FIELD]) && ($user_data[GP_CARD_IM_FIELD] !== $card_data['ean_code'])) {
      $fieldset_bind_giunticard = t('GiuntiCard bind to user «@user» [uid: @ssouid] - @ean', array(
        '@user' => $user->realname,
        '@ssouid' => $sso_uid,
        '@ean' => $user_data[GP_CARD_IM_FIELD],
      ));
      $build['body'] .= '<fieldset class="form-wrapper collapsible" id="edit-user-data"><legend><span class="fieldset-legend">' . $fieldset_bind_giunticard . '</span></legend><div class="fieldset-wrapper">';
      // Get the card info.
      $card_data = gp_card_remote_get_card_info($user_data[GP_CARD_IM_FIELD]);

      // If no card retrieved, prepare a message to show.
      if (FALSE === $card_data) {
        $build['body'] .= '<p>' . t('Sorry, NO card founded retrieving user card info.') . '</p>';
      }
      else {
        $build['body'] .= '<p><strong>' . t('NB: This card is own by another user.') . '</strong></p>';
        foreach ($card_data as $name => $field) {
          if ((empty($card_data[$name]) && ('disabled' !== $name)) || (' ' === $card_data[$name]) || ('  ' === $card_data[$name]) || ('.' === $card_data[$name])) {
            continue;
          }

          // Get default value.
          switch ($name) {
            case 'disabled':
              $value = empty($card_data[$name]) ? 'FALSE' : 'TRUE';
              break;

            case 'last_update':
            case 'last_purchase':
              $value = $card_data[$name] . ' (' . gmdate("Y-m-d", $card_data[$name] / 1000) . ')';
              break;

            default:
              $value = empty($card_data[$name]) ? '&nbsp;' : $card_data[$name];
          }

          $build['body'] .= '<div class="form-item" style="padding: 0; clear: left;"><label>' . $name . '</label><div class="value">' . $value . '</div></div>';
        }
      }
      $build['body'] .= '</div></fieldset>';
    }
  }

  $build['title'] = t('GiuntiCard info - user «@user» [uid: @ssouid]', array(
    '@user' => $user->realname,
    '@ssouid' => $sso_uid,
  ));

  drupal_add_library('system', 'drupal.collapse');

  if ($js) {
    // Include modal and ajax helpers.
    ctools_include('modal');

    ctools_modal_render($build['title'], $build['body']);
    drupal_exit();
  }

  // Set page title to the message one and display the body as content.
  drupal_set_title($build['title']);
  return $build['body'];}

/**
 * Page callback for show GiuntiCard history.
 */
function gp_backoffice_user_giunticard_history($js, $user) {
  $build = array();

  $card_id = gp_card_id_get_by_user_uid($user->uid);

  if (!empty($card_id)) {
    $build['body'] = '<fieldset class="form-wrapper collapsible" id="edit-user-data"><legend><span class="fieldset-legend">Card history (uid:' . $card_id . ')</span></legend><div class="fieldset-wrapper">';

    $build['body'] .= views_embed_view('backoffice_virtual_card_history', 'page_1', $card_id);

    $build['body'] .= '</div></fieldset>';
  }

  $build['title'] = t('GiuntiCard history - user «@user»', array(
    '@user' => $user->realname,
  ));
  drupal_add_library('system', 'drupal.collapse');

  if ($js) {
    // Include modal and ajax helpers.
    ctools_include('modal');

    ctools_modal_render($build['title'], $build['body']);
    drupal_exit();
  }

  // Set page title to the message one and display the body as content.
  drupal_set_title($build['title']);
  return $build['body'];}

/**
 * Page callback for show GiuntiCard points transfer.
 */
function gp_backoffice_user_giunticard_points_transfer($js, $user) {
  if ($js) {
    // Include modal and ajax helpers.
    ctools_include('modal');
    ctools_include('ajax');

    $sso_uid = gp_user_get_sso_uid($user);

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Transfer points from user «@user»', array('@user' => $user->realname)),
      'build_info' => array('args' => array($sso_uid, $user)),
    );

    $form_state += array(
      're_render' => FALSE,
      'no_redirect' => !empty($form_state['ajax']),
    );

    $form = drupal_build_form('gp_backoffice_user_points_transfer_form', $form_state);

    if (!empty($form_state['ajax']) && (!$form_state['executed'] || $form_state['rebuild'])) {
      $commands = ctools_modal_form_render($form_state, $form);
    }

    if ($form_state['executed'] && !$form_state['rebuild']) {
      $commands = array();
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
  }
  else {
    return drupal_get_form('gp_backoffice_user_points_transfer_form');
  }
}

/**
 * Form for points transfer.
 */
function gp_backoffice_user_points_transfer_form($form, &$form_state, $sso_uid, $user) {
  if (isset($form_state['done'])) {
    $form = array();

    // Get user data from identity manager.
    $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
    $ean_from = $user_data[GP_CARD_IM_FIELD];

    $form['info'] = array(
      '#markup' => '<p>' . t('Points are moving from the GiuntiCard [%ean_from].', array(
          '%ean_from' => $ean_from,
        )) . '</p>'
    );

    return $form;
  }
  if (!isset($form_state['confirm']) || !($form_state['confirm'])) {
    $form = array();

    $form['points-transfer'] = array(
      '#type' => 'fieldset',
      '#title' => t('Move points to'),
    );

    // Get user data from identity manager.
    $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
    $ean_from = $user_data[GP_CARD_IM_FIELD];

    $form['points-transfer']['info'] = array(
      '#markup' => '<p>' . t('Points will be moving from the GiuntiCard <strong>[%ean_from]</strong>.', array(
          '%ean_from' => $ean_from,
        )) . '</p>'
    );

    $form['points-transfer']['to-ean'] = array(
      '#type' => 'textfield',
      '#title' => t('Receiver GiuntiCard EAN'),
      '#required' => TRUE,
      '#size' => 13,
      '#description' => t('Insert here the EAN of the GiuntiCard receiver.'),
    );

    $form['points-transfer']['amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Points'),
      '#required' => TRUE,
      '#size' => 10,
      '#description' => t('Insert here the number of points to move.'),
    );

    $form['points-transfer']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Move now'),
    );

    return $form;
  }
  else {
    $amount = $form_state['values']['amount'];

    // Get user data from identity manager.
    $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
    $ean_from = $user_data[GP_CARD_IM_FIELD];

    $ean_to = $form_state['values']['to-ean'];

    $form['to-ean'] = array(
      '#value' => $ean_to,
    );

    $form['amount'] = array(
      '#value' => $amount,
    );

    $form['info'] = array(
      '#markup' => '<p>' . t('Confirm to move %amount points from the GiuntiCard [%ean_from] to GiuntiCard [%ean_to].', array(
          '%amount' => $amount,
          '%ean_from' => $ean_from,
          '%ean_to' => $ean_to,
        )) . '</p>'
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Continue'),
    );

    return $form;
  }
}

/**
 * Form validate for points transfer.
 */
function gp_backoffice_user_points_transfer_form_validate($form, &$form_state) {
  if ($form_state['confirm']) {
    return;
  }

  $sso_uid = $form_state['build_info']['args'][0];
  $to_ean = $form_state['values']['to-ean'];
  $amount = $form_state['values']['amount'];

  if (!isset($sso_uid)) {
    form_error($form, t('User not in Giunti IdentityManager.'));
    return;
  }

  if (empty($amount) || (0 > $amount)) {
    form_error($form, t('Invalid amount of points to move.'));
    return;
  }

  // Get user data from identity manager.
  $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
  if ((!$user_data) || (empty($user_data[GP_CARD_IM_FIELD]))) {
    form_error($form, t('No available GiuntiCard.'));
    return;
  }

  // ToDo: fix it.
  if ($to_ean == empty($user_data[GP_CARD_IM_FIELD])) {
    form_error($form, t('You can\'t move points to the same GiuntiCard.'));
    return;
  }

  // Get the card balance.
  $balance = gp_card_remote_get_card_balance($user_data[GP_CARD_IM_FIELD]);

  if ($amount > $balance) {
    form_error($form, t('You don\'t have yet this points to move.'));
  }

  $to_card = gp_card_remote_get_card_info($to_ean);
  if ((empty($to_card) || ($to_card['disabled']))) {
    form_error($form, t('You can\'t move points to this GiuntiCard.'));
  }
}

/**
 * Form submit for points transfer.
 */
function gp_backoffice_user_points_transfer_form_submit($form, &$form_state) {
  if (!isset($form_state['confirm'])) {
    // This will cause the form to be rebuilt, entering the confirm part of the form.
    $form_state['confirm'] = TRUE;
    // Along with this.
    $form_state['rebuild'] = TRUE;
  }
  else {
    $form_state['confirm'] = FALSE;
    $sso_uid = $form_state['build_info']['args'][0];

    // Get user data from identity manager.
    $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);

    $card_from_id = $user_data[GP_CARD_IM_FIELD];
    $card_to_id = $form['to-ean']['#value'];
    $balance = intval($form['amount']['#value']);

    $status = gp_card_remote_move_card_balance($card_from_id, $card_to_id, $balance);

    if (!$status) {
      $form_state['rebuild'] = TRUE;
      form_error($form, t('Error on points transfer. Virtual Card API returned an error (see logs for details).'));
    }
    else {
      watchdog('GiuntiCard', 'Moved @balance points from @card_from_id to @card_to_id', array(
        '@balance' => $balance,
        '@card_from_id' => $card_from_id,
        '@card_to_id' => $card_to_id,
      ));
      $form_state['done'] = TRUE;
      $form_state['rebuild'] = TRUE;
    }
  }
}

/**
 * Page callback for show GiuntiCard unbind.
 */
function gp_backoffice_user_giunticard_unbind($js, $user) {
  if ($js) {
    // Include modal and ajax helpers.
    ctools_include('modal');
    ctools_include('ajax');

    $sso_uid = gp_user_get_sso_uid($user);

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Unbind card from user «@user»', array('@user' => $user->realname)),
      'build_info' => array('args' => array($sso_uid, $user)),
    );

    $form_state += array(
      're_render' => FALSE,
      'no_redirect' => !empty($form_state['ajax']),
    );

    $form = drupal_build_form('gp_backoffice_user_giunticard_unbind_form', $form_state);

    if (!empty($form_state['ajax']) && (!$form_state['executed'] || $form_state['rebuild'])) {
      $commands = ctools_modal_form_render($form_state, $form);
    }

    if ($form_state['executed'] && !$form_state['rebuild']) {
      $commands = array();
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
  }
  else {
    return drupal_get_form('gp_backoffice_user_giunticard_unbind_form');
  }
}

/**
 * Form for unbind GiuntiCard.
 */
function gp_backoffice_user_giunticard_unbind_form($form, &$form_state, $sso_uid, $user) {
  if (isset($form_state['done'])) {
    $form = array();

    $form['info'] = array(
      '#markup' => '<p>' . t('Card is unbind from the user.') . '</p>'
    );

    return $form;
  }

  $form = array();

  // Get user data from identity manager.
  $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
  $ean_card = $user_data[GP_CARD_IM_FIELD];

  if (empty($ean_card)) {
    return;
  }

  $form['info'] = array(
    '#markup' => '<p>' . t('Pressing the button you confirm to unbind the card <strong>[%ean_card]</strong> from the user.', array(
        '%ean_card' => $ean_card,
      )) . '</p>'
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Unbind card'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#title' => t('Cancel'),
    '#value' => t('Cancel'),
    '#executes_submit_callback' => TRUE,
    '#submit' => array('gp_backoffice_user_giunticard_unbind_form_cancel'),
  );

  return $form;
}

/**
 * Form validate for unbind GiuntiCard.
 */
function gp_backoffice_user_giunticard_unbind_form_validate($form, &$form_state) {
  $sso_uid = $form_state['build_info']['args'][0];

  if (!isset($sso_uid)) {
    form_error($form, t('User not in Giunti IdentityManager.'));
    return;
  }

  // Get user data from identity manager.
  $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
  if ((!$user_data) || (empty($user_data[GP_CARD_IM_FIELD]))) {
    form_error($form, t('No available GiuntiCard.'));
    return;
  }
}

/**
 * Form submit for unbind GiuntiCard.
 */
function gp_backoffice_user_giunticard_unbind_form_submit($form, &$form_state) {
  $sso_uid = $form_state['build_info']['args'][0];

  // Get user data from identity manager.
  $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);

  $ean = $user_data[GP_CARD_IM_FIELD];
  // Get Card info ();
  $card_data = gp_card_remote_get_card_info($ean);
  $password = $card_data[GP_CARD_IM_FIELD_PASSWORD];
  $status = gp_card_unbind_remote_card($ean, $sso_uid, $password);

  if (!$status) {
    $form_state['rebuild'] = TRUE;
    form_error($form, t('Error unbinding card. Virtual Card API returned an error (see logs for details).'));
  }
  else {
    $form_state['done'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Form cancel for unbind GiuntiCard.
 */
function gp_backoffice_user_giunticard_unbind_form_cancel($form, &$form_state) {
  $form_state['done'] = TRUE;
}

/**
 * Page callback for show GiuntiCard reactivate.
 */
function gp_backoffice_user_giunticard_reactivate($js, $user) {
  if ($js) {
    // Include modal and ajax helpers.
    ctools_include('modal');
    ctools_include('ajax');

    $sso_uid = gp_user_get_sso_uid($user);

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Reactivate card to user «@user»', array('@user' => $user->realname)),
      'build_info' => array('args' => array($sso_uid, $user)),
    );

    $form_state += array(
      're_render' => FALSE,
      'no_redirect' => !empty($form_state['ajax']),
    );

    $form = drupal_build_form('gp_backoffice_user_giunticard_reactivate_form', $form_state);

    if (!empty($form_state['ajax']) && (!$form_state['executed'] || $form_state['rebuild'])) {
      $commands = ctools_modal_form_render($form_state, $form);
    }

    if ($form_state['executed'] && !$form_state['rebuild']) {
      $commands = array();
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
  }
  else {
    return drupal_get_form('gp_backoffice_user_giunticard_reactivate_form');
  }
}

/**
 * Form for reactivate GiuntiCard.
 */
function gp_backoffice_user_giunticard_reactivate_form($form, &$form_state, $sso_uid, $user) {
  if(isset($form_state['done'])) {
    $form = array();

    $form['info'] = array(
      '#markup' => '<p>' . t('Card is active.') . '</p>'
    );

    return $form;
  }

  $form = array();

  // Get user data from identity manager.
  $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);
  $ean_card = $user_data[GP_CARD_IM_FIELD];

  $form['info'] = array(
    '#markup' => '<p>' . t('Press the button to reactivate the card <strong>[%ean_card]</strong>.', array(
        '%ean_card' => $ean_card,
      )) . '</p>'
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Reactivate card'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#title' => t('Cancel'),
    '#value' => t('Cancel'),
    '#executes_submit_callback' => TRUE,
    '#submit' => array('gp_backoffice_user_giunticard_reactivate_form_cancel'),
  );

  return $form;
}

/**
 * Form validate for unbind GiuntiCard.
 */
function gp_backoffice_user_giunticard_reactivate_form_validate($form, &$form_state) {
  $sso_uid = $form_state['build_info']['args'][0];

  if (!isset($sso_uid)) {
    form_error($form, t('User not in Giunti IdentityManager.'));
    return;
  }

  // Get user data from identity manager.
  $user_data = gp_user_im_user_data_get_by_account();
  if ((!$user_data) || (empty($user_data[GP_CARD_IM_FIELD]))) {
    form_error($form, t('No available GiuntiCard.'));
    return;
  }
}

/**
 * Form submit for reactivate GiuntiCard.
 */
function gp_backoffice_user_giunticard_reactivate_form_submit($form, &$form_state) {
  $form_state['confirm'] = FALSE;

  $sso_uid = $form_state['build_info']['args'][0];

  // Get user data from identity manager.
  $user_data = gp_user_im_user_data_get_by_sso_uid($sso_uid);

  $ean = $user_data[GP_CARD_IM_FIELD];
  $password = $user_data[GP_CARD_IM_FIELD_PASSWORD];
  $status = gp_card_reactivate_remote_card($ean, $password);

  if (!$status) {
    $form_state['rebuild'] = TRUE;
    form_error($form, t('Error reactivating card. Virtual Card API returned an error (see logs for details).'));
  }
  else {
    $form_state['done'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Form cancel for reactivate GiuntiCard.
 */
function gp_backoffice_user_giunticard_reactivate_form_cancel($form, &$form_state) {
  $form_state['done'] = TRUE;
}

