<?php
/**
 * @file
 * Views related hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function gp_backoffice_views_data_alter(&$data) {
  if (isset($data['message'])) {
    // Expose a new handler to show a link to the rendered fields view.
    $data['message']['message_rendered_fields'] = array(
      'title' => t('Rendered message modal'),
      'help' => t('Link to show the saved rendered fields of the message.'),
      'real field' => 'mid',
      'field' => array(
        'handler' => 'gp_backoffice_handler_field_message_rendered_fields',
      ),
    );
  }

  if (isset($data['users']) && module_exists('gp_user')) {
    // Expose a new handler to show a link to the rendered fields view.
    $data['users']['user_password_change_link'] = array(
      'title' => t('Password change modal'),
      'help' => t('Link to show the password change form.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_backoffice_handler_field_user_password_change_link',
      ),
    );
  }

  if (isset($data['users']) && module_exists('gp_user')) {
    // Expose a new handler to show a link to the rendered fields view.
    $data['users']['user_email_change_link'] = array(
      'title' => t('Email change modal'),
      'help' => t('Link to show the email change form.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_backoffice_handler_field_user_email_change_link',
      ),
    );
  }

  if (isset($data['users']) && module_exists('gp_user')) {
    // Expose a new handler to show a link to the rendered fields view.
    $data['users']['user_giunticard_info_link'] = array(
      'title' => t('Giunticard info modal'),
      'help' => t('Link to show the balance and the user data.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_backoffice_handler_field_user_giunticard_info_link',
      ),
    );
  }

  if (isset($data['users']) && module_exists('gp_user')) {
    // Expose a new handler to show a link to the rendered fields view.
    $data['users']['user_giunticard_history_link'] = array(
      'title' => t('Giunticard history modal'),
      'help' => t('Link to show the history of binded GiuntiCard.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_backoffice_handler_field_user_giunticard_history_link',
      ),
    );
  }

  if (isset($data['users']) && module_exists('gp_user')) {
    // Expose a new handler to show a link to the rendered fields view.
    $data['users']['user_giunticard_points_transfer_link'] = array(
      'title' => t('Giunticard points transfer modal'),
      'help' => t('Link to show the points transfer form.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_backoffice_handler_field_user_giunticard_points_transfer_link',
      ),
    );
  }

  if (isset($data['users']) && module_exists('gp_user')) {
    // Expose a new handler to show a link to the rendered fields view.
    $data['users']['user_giunticard_unbind_link'] = array(
      'title' => t('Giunticard unbind modal'),
      'help' => t('Link to show the unbind GiuntiCard form.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_backoffice_handler_field_user_giunticard_unbind_link',
      ),
    );
  }

  if (isset($data['users']) && module_exists('gp_user')) {
    // Expose a new handler to show a link to the rendered fields view.
    $data['users']['user_giunticard_reactivate_link'] = array(
      'title' => t('Giunticard reactivate modal'),
      'help' => t('Link to show the reactivate GiuntiCard form.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_backoffice_handler_field_user_giunticard_reactivate_link',
      ),
    );
  }
}
