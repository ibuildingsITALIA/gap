<?php
/**
 * @file
 * gp_backoffice.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gp_backoffice_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'views_megarow_override_node_edit';
  $strongarm->value = '0';
  $export['views_megarow_override_node_edit'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'views_megarow_override_user_edit';
  $strongarm->value = '0';
  $export['views_megarow_override_user_edit'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'views_megarow_title';
  $strongarm->value = '';
  $export['views_megarow_title'] = $strongarm;

  return $export;
}
