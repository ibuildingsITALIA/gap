<?php
/**
 * @file
 * Definition of gp_backoffice_handler_field_rendered_message.
 */

class gp_backoffice_handler_field_message_rendered_fields extends views_handler_field {
  function render($values) {
    $mid = $this->get_value($values);
    $label = !empty($this->options['label']) ? $this->options['label'] : 'View message';

    // Include modal and ajax helpers.
    ctools_include('modal');
    ctools_include('ajax');
    ctools_modal_add_js();

    return ctools_modal_text_button(t($label), "admin/giunti/backoffice/message/nojs/$mid", '');
  }
}
