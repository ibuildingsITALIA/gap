<?php
/**
 * @file
 * Definition of gp_backoffice_handler_field_rendered_message.
 */

class gp_backoffice_handler_field_user_giunticard_points_transfer_link extends views_handler_field {
  function render($values) {
    $uid = $this->get_value($values);
    $user = user_load($uid);
    $ssouid = gp_user_get_sso_uid($user);

    // Check if user is connect to an IdentityManager user.
    if ($ssouid !== FALSE) {

      $card = gp_card_remote_get_card_info_by_uid($ssouid);

      // Check if user own a GiuntiCard.
      if (FALSE === $card) {
        return FALSE;
      }

      // Check if GiuntiCard Balance is greater than 0.
      $balance = gp_card_remote_get_card_balance($card['ean_code']);
      if (empty($balance)) {
        return FALSE;
      }

      $label = !empty($this->options['label']) ? $this->options['label'] : 'GiuntiCard Points Transfer';

      // Include modal and ajax helpers.
      ctools_include('modal');
      ctools_include('ajax');
      ctools_modal_add_js();

      return ctools_modal_text_button(t($label), "admin/giunti/backoffice/user/nojs/$uid/giunticard-points-transfer", '');
    }

    return FALSE;
  }
}
