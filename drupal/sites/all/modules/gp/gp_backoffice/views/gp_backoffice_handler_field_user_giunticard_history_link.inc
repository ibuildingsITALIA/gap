<?php
/**
 * @file
 * Definition of gp_backoffice_handler_field_rendered_message.
 */

class gp_backoffice_handler_field_user_giunticard_history_link extends views_handler_field {
  function render($values) {
    $uid = $this->get_value($values);
    $user = user_load($uid);

    if (gp_user_get_sso_uid($user) !== FALSE) {
      $label = !empty($this->options['label']) ? $this->options['label'] : 'GiuntiCard History';

      // Include modal and ajax helpers.
      ctools_include('modal');
      ctools_include('ajax');
      ctools_modal_add_js();

      return ctools_modal_text_button(t($label), "admin/giunti/backoffice/user/nojs/$uid/giunticard-history", '');
    }

    return FALSE;
  }
}
