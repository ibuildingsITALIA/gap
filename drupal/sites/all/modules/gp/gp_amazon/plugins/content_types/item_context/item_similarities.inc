<?php
/**
 * @file
 * Plugin to render an Amazon Item.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Similar items'),
  'description' => t('A list of similar items.'),
  'required context' => new ctools_context_required(t('Amazon Item'), 'item'),
  'category' => 'Amazon Item',
);

/**
 * Render similar Amazon Items.
 */
function gp_amazon_item_similarities_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return array();
  }

  /* @var AmazonItem $item */
  $item = $context->data;

  // If the item is an e-book, search similarities without merchant filter
  // because e-books are actually sold by a different Amazon than Amazon.
  // LOL
  $amazon_only = $item->isKindle() ? FALSE : TRUE;

  // ToDo: Remove after test.
  $amazon_only = FALSE;

  $similarities = gp_amazon_get_similarities($item->ASIN, $amazon_only);

  if (empty($similarities)) {
    // No similarities found or request failed.
    return array();
  }

  // Render only the number of items specified in the panel.
  $similarities = array_slice($similarities, 0, $conf['max_results'], TRUE);

  $block = new stdClass();
  $block->title = t('Similarities');
  $block->module = 'item_similarities';
  $block->delta = $item->ASIN;
  $block->content = array(
    '#theme' => 'gp_amazon_items',
    '#items' => $similarities,
    '#view_mode' => $conf['item_view_mode'],
  );
  return $block;
}

/**
 * Form settings for item_similarities.
 */
function gp_amazon_item_similarities_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['item_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Item view mode'),
    '#options' => gp_amazon_item_view_modes(),
    '#default_value' => !empty($conf['item_view_mode']) ? $conf['item_view_mode'] : 'full',
    '#required' => TRUE,
  );

  $form['max_results'] = array(
    '#type' => 'select',
    '#title' => t('Maximum results to show'),
    '#options' => drupal_map_assoc(range(1, 10)),
    '#default_value' => !empty($conf['max_results']) ? $conf['max_results'] : 10,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Submit handler to save item_view_mode setting.
 */
function gp_amazon_item_similarities_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['item_view_mode'] = $form_state['values']['item_view_mode'];
  $form_state['conf']['max_results'] = $form_state['values']['max_results'];
}
