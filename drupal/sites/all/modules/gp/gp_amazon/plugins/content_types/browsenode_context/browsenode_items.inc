<?php
/**
 * @file
 * Plugin to render items for an Amazon BrowseNode.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('BrowseNode items'),
  'description' => t('Items of a BrowseNode'),
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
  'category' => 'Amazon Browse Node',
);

/**
 * Render items for the provided BrowseNode.
 */
function gp_amazon_browsenode_items_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return array();
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();
  $search_index = $browsenode->getSearchIndex();

  $search = gp_amazon_search_items(array(
    'BrowseNode' => $browsenode->getId(),
    'SearchIndex' => $search_index['key'],
  ), GP_AMAZON_CACHE_TIME);

  if (!$search || !$search->getCount()) {
    // No results.

    // Redirect to homepage.
    drupal_goto();
  }

  $block = new stdClass();
  $block->title = t('Items');
  $block->module = 'browse_items';
  $block->delta = $browsenode->getId();
  $block->content = array(
    '#theme' => 'gp_amazon_search_results',
    '#search' => $search,
    '#view_mode' => gp_amazon_get_query_view_mode($conf['item_view_mode']),
  );
  return $block;
}

/**
 * Form settings for browsenode_items.
 */
function gp_amazon_browsenode_items_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['item_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Default item view mode'),
    '#options' => gp_amazon_item_view_modes(),
    '#default_value' => !empty($conf['item_view_mode']) ? $conf['item_view_mode'] : 'detail',
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Submit handler to save item_view_mode setting.
 */
function gp_amazon_browsenode_items_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['item_view_mode'] = $form_state['values']['item_view_mode'];
}
