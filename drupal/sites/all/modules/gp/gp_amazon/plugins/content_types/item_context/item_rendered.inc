<?php
/**
 * @file
 * Plugin to render an Amazon Item.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Rendered item'),
  'description' => t('A rendered Item'),
  'required context' => new ctools_context_required(t('Amazon Item'), 'item'),
  'category' => 'Amazon Item',
);

/**
 * Render an Amazon Item.
 */
function gp_amazon_item_rendered_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return array();
  }

  /* @var AmazonItem $item */
  $item = $context->data;

  $block = new stdClass();
  $block->title = t('Item');
  $block->module = 'item_render';
  $block->delta = $item->ASIN;
  $block->content = array(
    '#theme' => 'gp_amazon_item',
    '#item' => $item,
    '#view_mode' => $conf['item_view_mode'],
    '#extra' => array(),
  );

  if (!empty($conf['similarities'])) {
    $block->content['#extra'] = array(
      'similarities' => TRUE,
      'similarities_max_results' => $conf['similarities_max_results'],
      'similarities_view_mode' => $conf['similarities_view_mode'],
    );
  }

  return $block;
}

/**
 * Form settings for item_rendered.
 */
function gp_amazon_item_rendered_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['item_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Item view mode'),
    '#options' => gp_amazon_item_view_modes(),
    '#default_value' => !empty($conf['item_view_mode']) ? $conf['item_view_mode'] : 'full',
    '#required' => TRUE,
  );

  $form['similarities'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include item similarities'),
    '#default_value' => !empty($conf['similarities']),
  );

  $form['similarities_max_results'] = array(
    '#type' => 'select',
    '#title' => t('Maximum similar items to show'),
    '#options' => drupal_map_assoc(range(1, 10)),
    '#default_value' => !empty($conf['similarities_max_results']) ? $conf['similarities_max_results'] : 10,
    '#states' => array(
      'visible' => array(
        ':input[name="similarities"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['similarities_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Similar items view mode'),
    '#options' => gp_amazon_item_view_modes(),
    '#default_value' => !empty($conf['similarities_view_mode']) ? $conf['similarities_view_mode'] : 'detail',
    '#states' => array(
      'visible' => array(
        ':input[name="similarities"]' => array('checked' => TRUE),
      ),
    ),
  );

  return $form;
}

/**
 * Submit handler to save item_view_mode setting.
 */
function gp_amazon_item_rendered_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['item_view_mode'] = $form_state['values']['item_view_mode'];
  $form_state['conf']['similarities'] = $form_state['values']['similarities'];
  $form_state['conf']['similarities_max_results'] = $form_state['values']['similarities_max_results'];
  $form_state['conf']['similarities_view_mode'] = $form_state['values']['similarities_view_mode'];
}
