<?php
/**
 * @file
 * Plugin to render an Amazon BrowseNode breadcrumb trail.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('BrowseNode breadcrumb'),
  'description' => t('Breadcrumb for an Amazon BrowseNode'),
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
  'category' => 'Amazon Browse Node',
);

/**
 * Render a breadcrumb for the provided BrowseNode.
 */
function gp_amazon_browsenode_breadcrumb_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data) || FALSE === $context->data) {
    return array();
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();

  $block = new stdClass();
  $block->title = t('Breadcrumb');
  $block->module = 'browse_breadcrumb';
  $block->delta = $browsenode->getId();
  $block->content = array(
    '#theme' => 'gp_amazon_breadcrumb',
    '#browsenode' => $browsenode,
    '#current' => $browsenode->getName(),
  );
  return $block;
}

/**
 * Form settings for browsenode_breadcrumb.
 */
function gp_amazon_browsenode_breadcrumb_content_type_edit_form($form, &$form_state) {
  return $form;
}
