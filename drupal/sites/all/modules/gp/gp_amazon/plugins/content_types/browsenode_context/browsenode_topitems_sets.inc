<?php
/**
 * @file
 * Plugin to render top item sets for an Amazon BrowseNode.
 */

$plugin = array(
  'title' => t('BrowseNode top sets'),
);

/**
 * Return all content types available.
 */
function gp_amazon_browsenode_topitems_sets_content_type_content_types($plugin) {
  $sets = gp_amazon_browsenode_topitems_sets_types();

  $types = array();
  foreach ($sets as $key => $label) {
    $types[$key] = array(
      'title' => 'BrowseNode ' . $label,
      'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
      'category' => 'Amazon Browse Node',
    );
  }

  return $types;
}

/**
 * Helper function to get item sets.
 *
 * @return array
 *   An array of sets.
 */
function gp_amazon_browsenode_topitems_sets_types() {
  return array(
    'TopSellers' => t('Top sellers'),
    'NewReleases' => t('New releases'),
  );
}

/**
 * Render top item sets for the provided BrowseNode.
 */
function gp_amazon_browsenode_topitems_sets_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return array();
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();
  $set = $browsenode->getItemSet($subtype);

  if (is_null($set)) {
    // Item set not set (lol), do not render.
    return array();
  }

  // Get all asins to load, as the set doesn't return full items.
  $asins = array();
  foreach ($set as $row) {
    $asins[] = $row['ASIN'];
  }

  $items = gp_amazon_get_items($asins);

  if (empty($items)) {
    // No items found or request failed.
    return array();
  }

  // Render only the number of items specified in the panel.
  $items = array_slice($items, 0, $conf['max_results'], TRUE);

  $types = gp_amazon_browsenode_topitems_sets_types();

  $render = array(
    '#theme' => 'gp_amazon_items',
    '#items' => $items,
    '#view_mode' => 'image',
  );

  // Wrap if render mode is enabled.
  if (!empty($conf['render_mode'])) {
    $render += array(
      '#prefix' => '<div class="render-mode-slick">',
      '#suffix' => '</div>',
    );
  }

  $block = new stdClass();
  $block->title = $types[$subtype];
  $block->module = 'browse_topitems_sets';
  $block->delta = $browsenode->getId();
  $block->content = $render;

  return $block;
}

/**
 * Form settings for browsenode_topitems.
 */
function gp_amazon_browsenode_topitems_sets_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['max_results'] = array(
    '#type' => 'select',
    '#title' => t('Maximum results to show'),
    '#options' => drupal_map_assoc(range(1, 10)),
    '#default_value' => !empty($conf['max_results']) ? $conf['max_results'] : 10,
    '#required' => TRUE,
  );

  // Calling it render mode to allow adding new render modes in the future
  // without rename variables.
  $form['render_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Render as carousel'),
    '#default_value' => isset($conf['render_mode']) ? $conf['render_mode'] : TRUE,
  );

  return $form;
}

/**
 * Submit handler to save setting.
 */
function gp_amazon_browsenode_topitems_sets_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['max_results'] = $form_state['values']['max_results'];
  $form_state['conf']['render_mode'] = $form_state['values']['render_mode'];
}
