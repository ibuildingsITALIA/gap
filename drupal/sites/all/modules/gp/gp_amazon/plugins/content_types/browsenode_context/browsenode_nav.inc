<?php
/**
 * @file
 * Plugin to render a category block for an Amazon BrowseNode.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('BrowseNode category tree'),
  'description' => t('A category tree block for the BrowseNode'),
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
  'category' => 'Amazon Browse Node',
);

/**
 * Render a menu tree for the provided BrowseNode.
 */
function gp_amazon_browsenode_nav_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data) || FALSE === $context->data) {
    return array();
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();

  $block = new stdClass();
  $block->title = t('Categories');
  $block->module = 'browse_nav';
  $block->delta = $browsenode->getId();
  $block->content = array(
    '#theme' => 'browsenode_nav',
    '#browsenode' => $browsenode,
  );
  return $block;
}

/**
 * Form settings for browsenode_nav.
 */
function gp_amazon_browsenode_nav_content_type_edit_form($form, &$form_state) {
  return $form;
}
