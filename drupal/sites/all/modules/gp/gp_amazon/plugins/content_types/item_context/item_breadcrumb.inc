<?php
/**
 * @file
 * Plugin to render an Amazon Item breadcrumb trail.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Item breadcrumb'),
  'description' => t('Breadcrumb for an Amazon Item'),
  'required context' => new ctools_context_required(t('Amazon Item'), 'item'),
  'category' => 'Amazon Item',
);

/**
 * Render a breadcrumb for the provided Amazon Item.
 */
function gp_amazon_item_breadcrumb_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return array();
  }

  /* @var AmazonItem $item */
  $item = $context->data;

  $block = new stdClass();
  $block->title = t('Breadcrumb');
  $block->module = 'item_breadcrumb';
  $block->delta = $item->ASIN;
  $block->content = array(
    '#theme' => 'gp_amazon_breadcrumb',
    '#browsenode' => $item->getBrowseNode(),
    '#current' => $item->ItemAttributes['Title'],
  );
  return $block;
}

/**
 * Empty form callback to allow overriding title.
 */
function gp_amazon_item_breadcrumb_content_type_edit_form($form, &$form_state) {
  return $form;
}
