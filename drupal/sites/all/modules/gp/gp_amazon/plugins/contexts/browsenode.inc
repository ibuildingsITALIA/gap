<?php
/**
 * @file
 * Plugin to provide a BrowseNode context.
 */

$plugin = array(
  'title' => t('Amazon BrowseNode'),
  'description' => t('An Amazon BrowseNode context'),
  'context' => 'gp_amazon_context_create_browsenode',
  'edit form' => 'gp_amazon_context_browsenode_settings_form',
  'keyword' => 'browsenode',
  'context name' => 'browsenode',
  'convert list' => 'gp_amazon_context_browsenode_convert_list',
  'convert' => 'gp_amazon_context_browsenode_convert',
);

/**
 * Create a browsenode context.
 */
function gp_amazon_context_create_browsenode($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('browsenode');
  $context->plugin = 'browsenode';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    $browsenode_id = $data['browsenode_id'];
  }
  else {
    $browsenode_id = $data;
  }

  $lazy_browsenode = new LazyAmazonBrowseNode($browsenode_id);

  // If the browsenode isn't a search index, load the node immediately.
  if (!gp_amazon_find_search_index_by_id($browsenode_id)) {
    // We can only do argument validation calling the api itself.
    $browsenode = $lazy_browsenode->value();
    if (empty($browsenode)) {
      return FALSE;
    }
  }

  if (empty($lazy_browsenode)) {
    return $context;
  }

  $context->title = 'BrowseNode';
  $context->data = $lazy_browsenode;
  $context->argument = $browsenode_id;

  return $context;
}

/**
 * List of keywords.
 */
function gp_amazon_context_browsenode_convert_list() {
  return array(
    'name' => t('Name'),
    'id' => t('Id'),
    'depth' => t('Depth'),
  );
}

/**
 * Convert keyword to string.
 */
function gp_amazon_context_browsenode_convert($context, $type) {
  if (empty($context->data)) {
    return;
  }

  switch ($type) {
    case 'name':
      return $context->data->value()->getName();

    case 'id':
      return $context->data->value()->getId();

    case 'depth':
      /* @var AmazonBrowsenode $browsenode */
      $browsenode = $context->data->value();
      return count($browsenode->getAncestors());
  }
}

/**
 * Settings form.
 */
function gp_amazon_context_browsenode_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['browsenode_id'] = array(
    '#type' => 'textfield',
    '#title' => t('BrowseNode Id'),
    '#default_value' => !empty($conf['browsenode_id']) ? $conf['browsenode_id'] : '',
    '#required' => TRUE,
    '#weight' => -10,
  );

  // Hide useless description.
  $form['description']['#access'] = FALSE;

  return $form;
}

/**
 * Validate handler for settings form.
 */
function gp_amazon_context_browsenode_settings_form_validate($form, &$form_state) {
  if (empty($form_state['values']['browsenode_id'])) {
    // Default validation will kick in.
    return;
  }

  $browsenode = gp_amazon_get_browsenode($form_state['values']['browsenode_id']);
  if (FALSE === $browsenode) {
    form_set_error('browsenode_id', t('Invalid Browsenode id.'));
  }
}

/**
 * Submit handler to save configuration.
 */
function gp_amazon_context_browsenode_settings_form_submit($form, &$form_state) {
  $form_state['conf']['browsenode_id'] = $form_state['values']['browsenode_id'];
}
