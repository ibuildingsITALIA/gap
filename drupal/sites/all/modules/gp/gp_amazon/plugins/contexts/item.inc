<?php
/**
 * @file
 * Plugin to provide a Item context.
 */

$plugin = array(
  'title' => t('Amazon Item'),
  'description' => t('An Amazon Item context'),
  'context' => 'gp_amazon_context_create_item',
  'keyword' => 'item',
  'context name' => 'item',
  'convert list' => 'gp_amazon_context_item_convert_list',
  'convert' => 'gp_amazon_context_item_convert',
);

/**
 * Create a browsenode context.
 */
function gp_amazon_context_create_item($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('item');
  $context->plugin = 'item';

  if ($empty) {
    return $context;
  }

  $item = gp_amazon_get_single_item($data);

  if (empty($item)) {
    return $context;
  }

  $context->title = 'Item';
  $context->data = $item;
  $context->argument = $data;

  return $context;
}

/**
 * List of keywords.
 */
function gp_amazon_context_item_convert_list() {
  return array(
    'asin' => t('ASIN'),
    'title' => t('Title'),
  );
}

/**
 * Convert keyword to string.
 */
function gp_amazon_context_item_convert($context, $type) {
  if (empty($context->data)) {
    return;
  }

  switch ($type) {
    case 'asin':
      return $context->data->ASIN;

    case 'title':
      return $context->data->ItemAttributes['Title'];
  }
}
