<?php
/**
 * @file
 * Ctools plugin to provide an argument handler for an ASIN.
 */

$plugin = array(
  'title' => t('Amazon ASIN'),
  'keyword' => 'asin',
  'description' => t('Creates a Item context from the ASIN'),
  'context' => 'gp_amazon_argument_asin_context',
  'settings form' => 'gp_amazon_argument_asin_settings_form',
  'breadcrumb' => 'gp_amazon_argument_asin_breadcrumb',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the item ASIN.'),
  ),
);

/**
 * Create a Item context from argument.
 */
function gp_amazon_argument_asin_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  if ($empty) {
    return ctools_context_create_empty('item');
  }

  if (empty($arg)) {
    return FALSE;
  }

  // We can only do argument validation calling the api itself.
  $item = gp_amazon_get_single_item($arg);
  if (empty($item)) {
    return FALSE;
  }

  return ctools_context_create('item', $arg);
}

/**
 * Settings form for the argument.
 */
function gp_amazon_argument_asin_settings_form(&$form, &$form_state, $conf) {
  $form['settings']['breadcrumb'] = array(
    '#title' => t('Inject hierarchy into breadcrumb trail'),
    '#type' => 'checkbox',
    '#default_value' => !empty($conf['breadcrumb']),
    '#description' => t('If checked, the breadcrumb trail will be populated with the categories of the item.'),
  );
}

/**
 * Inject the breadcrumb trail if necessary.
 */
function gp_amazon_argument_asin_breadcrumb($conf, $context) {
  if (empty($conf['breadcrumb'])) {
    return;
  }

  /* @var AmazonItem $item */
  $item = $context->data;
  $breadcrumb = gp_amazon_get_breadcrumb($item->getBrowseNode(), $item->ItemAttributes['Title']);
  drupal_set_breadcrumb($breadcrumb);
}
