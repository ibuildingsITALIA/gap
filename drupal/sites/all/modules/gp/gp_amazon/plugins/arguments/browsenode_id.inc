<?php
/**
 * @file
 * Ctools plugin to provide an argument handler for a BrowseNodeId.
 */

$plugin = array(
  'title' => t('Amazon BrowseNode Id'),
  'keyword' => 'browsenode',
  'description' => t('Creates a BrowseNode context from the Id'),
  'context' => 'gp_amazon_argument_browsenode_id_context',
  'settings form' => 'gp_amazon_argument_browsenode_id_settings_form',
  'breadcrumb' => 'gp_amazon_argument_browsenode_id_breadcrumb',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the BrowseNode Id.'),
  ),
);

/**
 * Create a BrowseNode context from argument.
 */
function gp_amazon_argument_browsenode_id_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  if ($empty) {
    return ctools_context_create_empty('browsenode');
  }

  if (empty($arg)) {
    return FALSE;
  }

  // Return the created context.
  return ctools_context_create('browsenode', $arg);
}

/**
 * Settings form for the argument.
 */
function gp_amazon_argument_browsenode_id_settings_form(&$form, &$form_state, $conf) {
  $form['settings']['breadcrumb'] = array(
    '#title' => t('Inject hierarchy into breadcrumb trail'),
    '#type' => 'checkbox',
    '#default_value' => !empty($conf['breadcrumb']),
    '#description' => t('If checked, the breadcrumb trail will be populated with the ancestors of the BrowseNode.'),
  );
}

/**
 * Inject the breadcrumb trail if necessary.
 */
function gp_amazon_argument_browsenode_id_breadcrumb($conf, $context) {
  if (empty($conf['breadcrumb'])) {
    return;
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();

  // Hide if browsenode is a search index.
  $search_index = $browsenode->getSearchIndex();
  if ($browsenode->getId() == $search_index['BrowseNodeId']) {
    drupal_set_breadcrumb(array());
    return;
  }

  $breadcrumb = gp_amazon_get_breadcrumb($browsenode, $browsenode->getName());
  drupal_set_breadcrumb($breadcrumb);
}
