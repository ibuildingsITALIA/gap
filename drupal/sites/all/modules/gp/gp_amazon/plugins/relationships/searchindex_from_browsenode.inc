<?php
/**
 * @file
 * Plugin to provide a relationship handler for search index from browsenode.
 */

$plugin = array(
  'title' => t('BrowseNode search index'),
  'keyword' => 'searchindex',
  'description' => t('Adds the search index of an browsenode as a context.'),
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
  'context' => 'gp_amazon_searchindex_from_browsenode_context',
);

/**
 * Return a search index browsenode context based on an browsenode context.
 */
function gp_amazon_searchindex_from_browsenode_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('browsenode', NULL);
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();
  $search_index = $browsenode->getSearchIndex();

  if (!$search_index || 'All' === $search_index['key']) {
    // This browsenode has no search indexes associated.
    return ctools_context_create_empty('browsenode', NULL);
  }

  return ctools_context_create('browsenode', $search_index['BrowseNodeId']);
}
