<?php
/**
 * @file
 * Plugin to provide a relationship handler for browsenode from item.
 */

$plugin = array(
  'title' => t('Item BrowseNode'),
  'keyword' => 'browsenode',
  'description' => t('Adds the browsenode of an item as a context.'),
  'required context' => new ctools_context_required(t('Amazon Item'), 'item'),
  'context' => 'gp_amazon_browsenode_from_item_context',
);

/**
 * Return a browsenode context based on an item context.
 */
function gp_amazon_browsenode_from_item_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('browsenode', NULL);
  }

  /* @var AmazonItem $item */
  $item = $context->data;
  $browsenode = $item->getBrowseNode();
  if ($browsenode) {
    return ctools_context_create('browsenode', $browsenode->getId());
  }
}
