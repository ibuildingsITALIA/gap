<?php
/**
 * @file
 * Plugin to provide access control based on browsenode depth.
 */

$plugin = array(
  'title' => t('Browsenode: has depth'),
  'description' => t('Control access by comparing browsenode depth from search index.'),
  'settings form' => 'gs_amazon_browsenode_has_depth_ctools_access_settings',
  'callback' => 'gs_amazon_browsenode_has_depth_ctools_access_check',
  'summary' => 'gs_amazon_browsenode_has_depth_ctools_access_summary',
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
);

/**
 * Settings form.
 */
function gs_amazon_browsenode_has_depth_ctools_access_settings($form, &$form_state, $conf) {
  $form['settings']['depth'] = array(
    '#type' => 'select',
    '#title' => t('Depth'),
    '#description' => t('The depth to compare'),
    '#options' => drupal_map_assoc(range(0, 10)),
    '#default_value' => !empty($conf['depth']) ? $conf['depth'] : 1,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Check for access.
 */
function gs_amazon_browsenode_has_depth_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();

  // Depth is the number of ancestors plus one.
  $depth = count($browsenode->getAncestors()) + 1;

  return $depth == $conf['depth'];
}

/**
 * Provide a summary description for this access control.
 */
function gs_amazon_browsenode_has_depth_ctools_access_summary($conf, $context) {
  return t('"@browsenode" has depth equal to @depth.', array(
    '@depth' => $conf['depth'],
    '@browsenode' => $context->identifier,
  ));
}
