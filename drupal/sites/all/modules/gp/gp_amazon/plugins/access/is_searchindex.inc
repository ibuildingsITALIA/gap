<?php
/**
 * @file
 * Plugin to provide access control if a browsenode is also a search index.
 */

$plugin = array(
  'title' => t('Browsenode: Is SearchIndex'),
  'description' => t('Control access by checking if a BrowseNode is also a SearchIndex.'),
  'callback' => 'gs_amazon_is_searchindex_ctools_access_check',
  'summary' => 'gs_amazon_is_searchindex_ctools_access_summary',
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
);

/**
 * Check for access.
 */
function gs_amazon_is_searchindex_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();
  $search_index = $browsenode->getSearchIndex();

  return $browsenode->getId() == $search_index['BrowseNodeId'];
}

/**
 * Provide a summary description for this access control.
 */
function gs_amazon_is_searchindex_ctools_access_summary($conf, $context) {
  return t('"@browsenode" is also a SearchIndex.', array(
    '@browsenode' => $context->identifier,
  ));
}
