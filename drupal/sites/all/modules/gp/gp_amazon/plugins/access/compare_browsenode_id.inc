<?php
/**
 * @file
 * Plugin to provide access control based on browsenode id.
 */

$plugin = array(
  'title' => t('Browsenode: compare Id'),
  'description' => t('Control access by comparing BrowseNode id.'),
  'settings form' => 'gs_amazon_compare_browsenode_id_ctools_access_settings',
  'callback' => 'gs_amazon_compare_browsenode_id_ctools_access_check',
  'summary' => 'gs_amazon_compare_browsenode_id_ctools_access_summary',
  'required context' => new ctools_context_required(t('Amazon BrowseNode'), 'browsenode'),
);

/**
 * Settings form.
 */
function gs_amazon_compare_browsenode_id_ctools_access_settings($form, &$form_state, $conf) {
  $form['settings']['browsenode_id'] = array(
    '#type' => 'textfield',
    '#title' => t('BrowseNode Id'),
    '#description' => t('Browsenode Id to compare against'),
    '#default_value' => !empty($conf['browsenode_id']) ? $conf['browsenode_id'] : '',
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Check for access.
 */
function gs_amazon_compare_browsenode_id_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  /* @var LazyAmazonBrowseNode $data */
  $data = $context->data;
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $data->value();

  return (string) $browsenode->getId() === (string) $conf['browsenode_id'];
}

/**
 * Provide a summary description for this access control.
 */
function gs_amazon_compare_browsenode_id_ctools_access_summary($conf, $context) {
  return t('"@browsenode" id is equal to @id.', array(
    '@id' => $conf['browsenode_id'],
    '@browsenode' => $context->identifier,
  ));
}
