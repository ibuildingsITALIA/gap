<?php
/**
 * @file
 * Wrappers for Amazon responses: BrowseNodes, Items, SearchIndexes.
 */

/**
 * Class AmazonBrowseNode
 * Wrapper class around an Amazon BrowseNode.
 */
class AmazonBrowseNode {
  /**
   * @var int
   *   The BrowseNode id.
   */
  protected $id;

  /**
   * @var string
   *   The BrowseNode name.
   */
  protected $name;

  /**
   * @var bool
   *   TRUE if element is a root category.
   */
  protected $isRoot;

  /**
   * @var array
   *   The BrowseNode children.
   */
  protected $children;

  /**
   * @var array
   *   The BrowseNode ancestors.
   */
  protected $ancestors;

  /**
   * @var AmazonBrowseNode
   *   The current BrowseNode ancestor.
   */
  protected $ancestor;

  /**
   * @var array
   *   A list of item sets (top sellers, new releases, etc..)
   */
  protected $itemSets;

  /**
   * @var string
   *   The SearchIndex of this BrowseNode.
   *   Loaded only when requested.
   */
  protected $searchIndex;

  /**
   * @var bool
   *   TRUE if the element is a search index.
   */
  protected $isSearchIndex;

  /**
   * Build object from xml response.
   *
   * @param SimpleXmlElement $xml_element
   *   The xml element.
   */
  public function __construct($xml_element) {
    $this->name = (string) $xml_element->Name;
    $this->id = (string) $xml_element->BrowseNodeId;
    $this->isRoot = isset($xml_element->IsCategoryRoot) ? (bool) $xml_element->IsCategoryRoot : FALSE;

    // Set to NULL to be able to retrieve it on first SearchIndex request.
    $this->searchIndex = NULL;

    // Populate the children array.
    $this->children = array();
    if (isset($xml_element->Children)) {
      foreach ($xml_element->Children->BrowseNode as $child) {
        if ($this->isBlacklisted((string) $child->BrowseNodeId)) {
          continue;
        }
        $this->children[] = new AmazonBrowseNode($child);
      }
    }

    // Populate ancestors.
    $this->ancestors = array();
    $this->ancestor = NULL;
    if (isset($xml_element->Ancestors)) {
      $this->ancestor = new AmazonBrowseNode($xml_element->Ancestors->BrowseNode);
      $this->ancestors = $this->flattenAncestors();
    }

    // Populate item sets (best sellers, new releases, etc...).
    $this->itemSets = array();
    if (isset($xml_element->TopItemSet)) {
      foreach ($xml_element->TopItemSet as $set) {
        $type = (string) $set->Type;
        $this->itemSets[$type] = array();
        foreach ($set->TopItem as $item) {
          // This "items" are not real items. We need to load them if we want
          // images or other data.
          $this->itemSets[$type][] = (array) $item;
        }
      }
    }

    // Create url alias.
    gp_amazon_create_url_alias('browse', $this->id, $this->name);
  }

  /**
   * Helper method to flatten current BrowseNode ancestors.
   *
   * @return array
   *   The flattened ancestors array, ordered by increasing depth.
   */
  protected function flattenAncestors() {
    $ancestors = array();
    $current = $this;

    while ($current->hasAncestors()) {
      $ancestors[] = $current->getAncestor();
      $current = $current->getAncestor();
    }

    return $ancestors;
  }

  /**
   * Helper function to check if node has ancestors.
   *
   * @return bool
   *   TRUE is has ancestors.
   */
  public function hasAncestors() {
    return !is_null($this->ancestor);
  }

  /**
   * Return the BrowseNode direct ancestor.
   *
   * @return \AmazonBrowseNode
   *   The ancestor.
   */
  public function getAncestor() {
    return $this->ancestor;
  }

  /**
   * Set the BrowseNode direct ancestor. Use wisely.
   *
   * @param \AmazonBrowseNode $ancestor
   *   The BrowseNode to set as ancestor.
   */
  public function setAncestor($ancestor) {
    $this->ancestor = $ancestor;
  }

  /**
   * Return the BrowseNode children.
   *
   * @return array
   *   The Browsenode children.
   */
  public function getChildren() {
    return $this->children;
  }

  /**
   * Set the BrowseNode children. Use wisely.
   *
   * @param AmazonBrowseNode[] $children
   *   The array of BrowseNodes to set as children.
   */
  public function setChildren($children) {
    $this->children = $children;
  }

  /**
   * Return the Browsenode ancestors, flattened.
   *
   * @return array
   *   The BrowseNode ancestors.
   */
  public function getAncestors() {
    return $this->ancestors;
  }

  /**
   * Set the BrowseNode ancestors. Use wisely.
   *
   * @param array[] $ancestors
   *   An array of (flattened) ancestors.
   */
  public function setAncestors($ancestors) {
    $this->ancestors = $ancestors;
  }

  /**
   * Return the BrowseNode name.
   *
   * @return string
   *   The name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set the BrowseNode name. Use wisely.
   *
   * @param string $name
   *   The name to use.
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Return the item sets of the node.
   *
   * @return array
   *   An array of items sets.
   */
  public function getItemSets() {
    return $this->itemSets;
  }

  /**
   * Helper function to return an item set.
   *
   * @param string $name
   *   The item set name.
   *
   * @return null|string
   *   NULL if item set not found, value otherwise.
   */
  public function getItemSet($name) {
    return isset($this->itemSets[$name]) ? $this->itemSets[$name] : NULL;
  }

  /**
   * Helper function to check if node has children.
   *
   * @return bool
   *   TRUE if has children.
   */
  public function hasChildren() {
    return count($this->children) > 0;
  }

  /**
   * Retrieve SearchIndex info.
   *
   * @return null|array
   *   NULL if no search index, otherwise an array of SearchIndex properties.
   */
  public function getSearchIndex() {
    if (is_null($this->searchIndex)) {
      // Get the farthest ancestor or current id.
      if ($this->hasAncestors()) {
        $node = array_pop($this->ancestors);
      }
      else {
        $node = $this;
      }

      // Loop through known indexes to find the correct one.
      $this->searchIndex = gp_amazon_find_search_index_by_id($node->getId());

      // If search index is still NULL, set to All.
      if (is_null($this->searchIndex)) {
        $search_indexes = gp_amazon_search_indexes();
        $this->searchIndex = $search_indexes['All'];
      }
    }

    return $this->searchIndex;
  }

  /**
   * Set the SearchIndex. Use wisely.
   *
   * @param array $search_index
   *   The search Index to set.
   */
  public function setSearchIndex($search_index) {
    $this->searchIndex = $search_index;
  }

  /**
   * Return the BrowseNodeId.
   *
   * @return int
   *   The BrowseNodeId.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Returns the BrowseNode child that is a root category.
   *
   * @return AmazonBrowseNode|false
   *   The root child if found, false otherwise.
   */
  public function getRootChild() {
    foreach ($this->children as $child) {
      /* @var AmazonBrowseNode $child */
      if ($child->isRoot()) {
        return $child;
      }
    }

    return FALSE;
  }

  /**
   * Return if BrowseNode is root.
   *
   * @return bool
   *   TRUE is root.
   */
  public function isRoot() {
    return $this->isRoot;
  }

  /**
   * Returns if BrowseNode is a search index.
   *
   * @return bool
   *   TRUE if search index.
   */
  public function isSearchIndex() {
    return (bool) gp_amazon_find_search_index_by_id($this->id);
  }

  public function isBlacklisted($browsenode_id) {
    // ToDo: move blacklist outside function and configurable on backend.
    $blacklist = array(
      '489076031' => 'Erotici',
      '489072031' => 'Gay & Lesbo',
    );
    return array_key_exists($browsenode_id, $blacklist);
  }
}

/**
 * Class AmazonItem
 * Wrapper around an Amazon Item.
 */
class AmazonItem {
  /**
   * @var string
   *   The item title.
   */
  protected $title;

  /**
   * @var array
   *   List of attributes of the Item.
   */
  protected $attributes;

  /**
   * @var AmazonBrowseNode
   *   The BrowseNode the item belongs to.
   */
  protected $browseNode;

  /**
   * @var string
   *   The original "serialized" XML of this item.
   */
  protected $xml;

  /**
   * Build object from xml response.
   *
   * @param SimpleXmlElement $xml_element
   *   The xml element.
   */
  public function __construct($xml_element) {
    $this->attributes = $this->childrenToArray($xml_element);

    // Help finding title.
    $this->title = $this->attributes['ItemAttributes']['Title'];

    // Normalize AlternateVersions.
    // @todo maybe find better solutions.
    $this->normalizeAlternateVersions();

    $this->browseNode = NULL;
    // If we have a BrowseNode for the item, set it,
    if (isset($xml_element->BrowseNodes)) {
      $browsenode_xml = $xml_element->BrowseNodes->BrowseNode;
      // Use the last BrowseNode item gave us.
      $this->browseNode = new AmazonBrowseNode($browsenode_xml);
    }

    // Create url alias.
    gp_amazon_create_url_alias('product', $this->ASIN, $this->getNiceName());

    $this->xml = $xml_element->asXML();
  }

  /**
   * Recursive function to transform a xml node into array.
   *
   * @todo fix string to array conversions for some elements like author.
   *
   * @param SimpleXmlElement $xml_element
   *   The element to loop.
   *
   * @return array
   *   The array of attributes.
   */
  protected function childrenToArray($xml_element) {
    $attributes = array();

    foreach ((array) $xml_element as $index => $node) {
      if (is_object($node)) {
        $attributes[$index] = $this->childrenToArray($node);
      }
      elseif (is_array($node)) {
        $attributes[$index] = array();
        foreach ($node as $leaf) {
          $attributes[$index][] = $this->childrenToArray($leaf);
        }
      }
      else {
        $attributes[$index] = $node;
      }
    }

    return $attributes;
  }

  /**
   * Normalize AlternateVersions to be always an array.
   */
  protected function normalizeAlternateVersions() {
    if (empty($this->attributes['AlternateVersions']['AlternateVersion'])) {
      // No alternate versions, quit.
      return;
    }

    $versions = $this->attributes['AlternateVersions']['AlternateVersion'];
    // Get the first key of the array.
    $first_key = key($versions);
    // If the first element key is a string, we assume that all these elements
    // are strings, and rewrap them as array.
    if (is_string($first_key)) {
      $versions = array($versions);
    }

    // Shorten access to alternate versions.
    $this->attributes['AlternateVersions'] = $versions;
  }

  /**
   * Returns the item title.
   *
   * @return string
   *   The title.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Helper function to return url of the node.
   *
   * @param array $options
   *  The url options.
   *
   * @return string
   *   The url.
   */
  public function getUrl(array $options = array()) {
    return url("product/{$this->ASIN}", $options);
  }

  /**
   * Helper function to return absolute url of the node.
   *
   * @param array $options
   *  The url options.
   *
   * @return string
   *   The url.
   */
  public function getAbsoluteUrl(array $options = array()) {
    $options['absolute'] = TRUE;
    return url("product/{$this->ASIN}", $options);
  }

  /**
   * Return the Amazon BrowseNode for the item.
   *
   * @return null|\AmazonBrowseNode
   *   Null if no browsenode set, the browsenode otherwise.
   */
  public function getBrowseNode() {
    return $this->browseNode;
  }

  /**
   * Returns all the attributes.
   *
   * @return array
   *   The attributes of the item.
   */
  public function getAttributes() {
    return $this->attributes;
  }

  /**
   * Returns a string to use in nice urls.
   */
  public function getNiceName() {
    if (is_null($this->getBrowseNode()) || is_null($this->getBrowseNode()->getSearchIndex())) {
      // Cannot find browseNode or searchIndex, return just the title.
      return $this->title;
    }

    $index = $this->getBrowseNode()->getSearchIndex();
    $name = "{$index['label']}-{$this->title}";

    // Check if we have an author.
    if (!empty($this->attributes['ItemAttributes']['Author'])) {
      $author = $this->attributes['ItemAttributes']['Author'];
      // Normalize to string.
      // @todo fix childrenToArray function!
      while (is_array($author)) {
        $author = reset($author);
      }

      $name .= "-{$author}";
    }

    return $name;
  }

  /**
   * Returns the schema.org type for this item.
   *
   * @todo If it grows too much, maybe try to map types from search indexes?
   *   Two parameters in the search_indexes function with the product group
   *   and the schema type. Then loop all search_indexes to find the type.
   *
   * @return string
   *   The full schema.org itemtype property.
   */
  public function getSchemaType() {
    $type = 'Product';

    if (!empty($this->attributes['ItemAttributes']['ProductGroup'])) {
      switch ($this->attributes['ItemAttributes']['ProductGroup']) {
        case 'Libro':
          $type = 'Book';
          break;
      }
    }

    return $type;
  }

  /**
   * Returns the OfferListing for the item.
   *
   * @return bool|string
   *   FALSE if not found, the OfferListing otherwise.
   */
  public function getOfferListing() {
    if (!empty($this->attributes['Offers']['Offer']['OfferListing'])) {
      return $this->attributes['Offers']['Offer']['OfferListing'];
    }

    return FALSE;
  }

  /**
   * Returns the OfferListingId for the item.
   *
   * @return bool|string
   *   FALSE if not found, the id otherwise.
   */
  public function getOfferListingId() {
    $offer_listing = $this->getOfferListing();
    if ($offer_listing && !empty($offer_listing['OfferListingId'])) {
      return $offer_listing['OfferListingId'];
    }

    return FALSE;
  }

  /**
   * Returns the price components of the current offer listing.
   *
   * @return bool|array
   *   FALSE if no offer found, the price components
   *   array (Amount, CurrencyCode, FormattedPrice) otherwise.
   */
  public function getPriceComponents() {
    $offer_listing = $this->getOfferListing();
    if ($offer_listing && !empty($offer_listing['Price'])) {
      return $offer_listing['Price'];
    }

    return FALSE;
  }

  /**
   * Returns the original XML element.
   *
   * @return SimpleXMLElement
   *   The xml.
   */
  public function getXml() {
    return simplexml_load_string($this->xml);
  }

  /**
   * Returns if the item is an eBook.
   *
   * @return bool
   *   If the item is an eBook or not.
   */
  public function isKindle() {
    if (!empty($this->attributes['ItemAttributes']['Format'])) {
      return ('eBook Kindle' === $this->attributes['ItemAttributes']['Format']);
    }

    return FALSE;
  }

  /**
   * Returns if the item is a book.
   */
  public function isBook() {
    if (!empty($this->attributes['ItemAttributes']['ProductTypeName'])) {
      $book_types = array('ABIS_BOOK', 'BOOKS_1973_AND_LATER');
      return (in_array($this->attributes['ItemAttributes']['ProductTypeName'], $book_types));
    }

    return FALSE;
  }

  /**
   * Returns if the item is a MP3.
   */
  public function isMp3() {
    if (!empty($this->attributes['ItemAttributes']['ProductTypeName'])) {
      $mp3_types = array('DOWNLOADABLE_MUSIC_TRACK', 'DOWNLOADABLE_MUSIC_ALBUM');
      return (in_array($this->attributes['ItemAttributes']['ProductTypeName'], $mp3_types));
    }

    return FALSE;
  }

  /**
   * Returns if the item is a videogame.
   *
   * @return bool
   *   If the item is a videogame or not.
   */
  public function isVideogame() {
    if (!empty($this->attributes['ItemAttributes']['ProductTypeName'])) {
      $videogame_types = array(
        'ABIS_VIDEO_GAMES',
        'VIDEO_GAME_HARDWARE',
        'CONSOLE_VIDEO_GAMES',
      );
      return (in_array($this->attributes['ItemAttributes']['ProductTypeName'], $videogame_types));
    }

    return FALSE;
  }

  /**
   * Returns if the item is a toy.
   *
   * @return bool
   *   If the item is a toy or not.
   */
  public function isToy() {
    if (!empty($this->attributes['ItemAttributes']['ProductTypeName'])) {
      $toy_types = array('TOYS_AND_GAMES');
      return (in_array($this->attributes['ItemAttributes']['ProductTypeName'], $toy_types));
    }

    return FALSE;
  }

  /**
   * Returns if the item is a baby product.
   *
   * @return bool
   *   If the item is a baby product or not.
   */
  public function isBaby() {
    if (!empty($this->attributes['ItemAttributes']['ProductTypeName'])) {
      $baby_types = array('BABY_PRODUCT');
      return (in_array($this->attributes['ItemAttributes']['ProductTypeName'], $baby_types));
    }

    return FALSE;
  }

  /**
   * Returns if the item is an app.
   *
   * @return bool
   *   If the item is an app or not.
   */
  public function isApp() {
    if (!empty($this->attributes['ItemAttributes']['ProductTypeName'])) {
      $app_types = array('MOBILE_APPLICATION');
      return (in_array($this->attributes['ItemAttributes']['ProductTypeName'], $app_types));
    }

    return FALSE;
  }

  /**
   * Returns if an item need warnings.
   */
  public function needProductWarnings() {
    return $this->isToy() || $this->isVideogame() || $this->isBaby();
  }

  /**
   * Returns if an item is suitable for one click buy.
   */
  public function isOneClickBuy() {
    return $this->isKindle() || $this->isMp3() || $this->isApp();
  }

  /**
   * Magic getter for Item attributes.
   *
   * @param string $name
   *   The attribute name.
   *
   * @return null|mixed
   *   NULL if attribute not found, the parameter otherwise.
   */
  public function __get($name) {
    if (isset($this->attributes[$name])) {
      return $this->attributes[$name];
    }

    return NULL;
  }

  /**
   * Magic isset method.
   *
   * @param string $name
   *   The attribute name.
   *
   * @return bool
   *   True or false.
   */
  public function __isset($name) {
    return isset($this->attributes[$name]);
  }

  /**
   * Converts an Item to a string.
   *
   * @return string
   *   The ASIN of the item.
   */
  public function __toString() {
    return $this->attributes['ASIN'];
  }

  /**
   * Return Item dimensions
   *
   * @param string $unit
   *
   * @return array
   */
  public function getItemDimensions($unit = 'cm') {
    $raw_dimensions = $this->attributes['ItemAttributes']['ItemDimensions'];
    if (!(in_array($unit, ['cm', 'Centesimi pollici']))) {
      return;
    }

    if (empty($raw_dimensions)) {
      return;
    }

    $dimensions = array();

    $elem_keys = array('Height', 'Length', 'Width');
    foreach ($elem_keys as $elem_key) {
      $elem_dim = $raw_dimensions[$elem_key];
      $elem_unit = $this->getXml()->ItemAttributes->ItemDimensions->{$elem_key}->attributes()['Units'];
      if (!empty($elem_dim) && !empty($elem_unit)) {
        $elem_key_unit = $this->getXml()->ItemAttributes->ItemDimensions->{$elem_key}->attributes()['Units']->__toString();

        $multiplier = 1;
        switch ($elem_key_unit) {
          case 'Centesimi pollici':
            if ($unit === 'cm') {
              $multiplier = (0.01 / 0.39370);
            }
            break;

          case 'cm':
            if ($unit === 'Centesimi pollici') {
              $multiplier = 0.01 * 0.39370;
            }
            break;
        }
        $dimensions[$elem_key] = round($raw_dimensions[$elem_key] * $multiplier);
      }
    }

    $dimensions['Unit'] = $unit;

    return $dimensions;
  }

  /**
   * Return Package dimensions
   *
   * @param string $unit
   *
   * @return array
   */
  public function getPackageDimensions($unit = 'cm') {
    $raw_dimensions = $this->attributes['ItemAttributes']['PackageDimensions'];
    if (!(in_array($unit, ['cm', 'Centesimi pollici']))) {
      return;
    }

    if (empty($raw_dimensions)) {
      return;
    }

    $dimensions = array();

    $elem_keys = array('Height', 'Length', 'Width');
    foreach ($elem_keys as $elem_key) {
      $elem_dim = $raw_dimensions[$elem_key];
      $elem_unit = $this->getXml()->ItemAttributes->PackageDimensions->{$elem_key}->attributes()['Units'];
      if (!empty($elem_dim) && !empty($elem_unit)) {
        $elem_key_unit = $this->getXml()->ItemAttributes->PackageDimensions->{$elem_key}->attributes()['Units']->__toString();

        $multiplier = 1;
        switch ($elem_key_unit) {
          case 'Centesimi pollici':
            if ($unit === 'cm') {
              $multiplier = (0.01 / 0.39370);
            }
            break;

          case 'cm':
            if ($unit === 'Centesimi pollici') {
              $multiplier = 0.01 * 0.39370;
            }
            break;
        }
        $dimensions[$elem_key] = round($raw_dimensions[$elem_key] * $multiplier);
      }
    }

    $dimensions['Unit'] = $unit;

    return $dimensions;
  }

  /**
   * Return Item weight
   *
   * @param string $unit
   *
   * @return array
   */
  public function getItemWeight($unit = 'g') {
    $raw_dimensions = $this->attributes['ItemAttributes']['ItemDimensions'];
    if (!(in_array($unit, ['g', 'Centesimi libbre']))) {
      return;
    }

    if (empty($raw_dimensions)) {
      return;
    }

    $dimensions = array();

    $elem_keys = array('Weight');
    foreach ($elem_keys as $elem_key) {
      $elem_dim = $raw_dimensions[$elem_key];
      $elem_unit = $this->getXml()->ItemAttributes->ItemDimensions->{$elem_key}->attributes()['Units'];
      if (!empty($elem_dim) && !empty($elem_unit)) {
        $elem_key_unit = $this->getXml()->ItemAttributes->ItemDimensions->{$elem_key}->attributes()['Units']->__toString();

        $multiplier = 1;
        switch ($elem_key_unit) {
          case 'Centesimi libbre':
            if ($unit === 'g') {
              $multiplier = (0.01 / 0.0022046);
            }
            break;

          case 'g':
            if ($unit === 'Centesimi libbre') {
              $multiplier = 0.01 * 0.0022046;
            }
            break;
        }
        $dimensions[$elem_key] = round($raw_dimensions[$elem_key] * $multiplier);

        if (($unit === 'g') && ($dimensions[$elem_key] > 2000)) {
          $dimensions[$elem_key] = round($dimensions[$elem_key] / 1000, 2);
          $unit = 'Kg';
        }
      }
    }

    $dimensions['Unit'] = $unit;

    return $dimensions;
  }

  /**
   * Return Package weight
   *
   * @param string $unit
   *
   * @return array
   */
  public function getPackageWeight($unit = 'g') {
    $raw_dimensions = $this->attributes['ItemAttributes']['PackageDimensions'];

    if (!(in_array($unit, ['g', 'Centesimi libbre']))) {
      return;
    }

    if (empty($raw_dimensions)) {
      return;
    }

    $dimensions = array();

    $elem_keys = array('Weight');

    foreach ($elem_keys as $elem_key) {
      $elem_dim = $raw_dimensions[$elem_key];
      $elem_unit = $this->getXml()->ItemAttributes->PackageDimensions->{$elem_key}->attributes()['Units'];
      if (!empty($elem_dim) && !empty($elem_unit)) {
        $elem_key_unit = $this->getXml()->ItemAttributes->PackageDimensions->{$elem_key}->attributes()['Units']->__toString();

        $multiplier = 1;
        switch ($elem_key_unit) {
          case 'Centesimi libbre':
            if ($unit === 'g') {
              $multiplier = (0.01 / 0.0022046);
            }
            break;

          case 'g':
            if ($unit === 'Centesimi libbre') {
              $multiplier = 0.01 * 0.0022046;
            }
            break;
        }
        $dimensions[$elem_key] = round($raw_dimensions[$elem_key] * $multiplier);

        if (($unit === 'g') && ($dimensions[$elem_key] > 2000)) {
          $dimensions[$elem_key] = round($dimensions[$elem_key] / 1000, 2);
          $unit = 'Kg';
        }
      }
    }

    $dimensions['Unit'] = $unit;

    return $dimensions;
  }

  public function getRunningTime() {
    $raw_time = $this->attributes['ItemAttributes']['RunningTime'];
    $time_unit = $this->getXml()->ItemAttributes->RunningTime->attributes()['Units'];
    $time = $raw_time . ' ' . $time_unit->__toString();
    return $time;
  }
}

/**
 * Class AmazonSearch
 * Wrapper around an Amazon ItemSearch.
 */
class AmazonSearch {
  /**
   * @var array
   *   Holds the parameters of the search.
   */
  protected $parameters;

  /**
   * @var array
   *   The items of this search.
   */
  protected $items;

  /**
   * @var int
   *   Number of pages of this search.
   */
  protected $pages;

  /**
   * @var int
   *   The total items for this search.
   */
  protected $count;

  /**
   * Build object from xml response.
   *
   * @param SimpleXmlElement $xml_element
   *   The xml element.
   */
  public function __construct($xml_element) {
    $this->count = (int) $xml_element->TotalResults;
    $this->pages = (int) $xml_element->TotalPages;

    // Save parameters.
    $this->parameters = array();
    foreach ($xml_element->Request->ItemSearchRequest->children() as $name => $value) {
      $this->parameters[$name] = (string) $value;
    }

    foreach ($xml_element->Item as $item) {
      $this->items[] = new AmazonItem($item);
    }
  }

  /**
   * Returns the total items of this search.
   *
   * @return int
   *   The count sent by Amazon.
   */
  public function getCount() {
    return $this->count;
  }

  /**
   * Returns the items of the current page of this search.
   *
   * @return array
   *   An array of AmazonItem objects.
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * Returns the numbers of pages.
   *
   * @return int
   *   The number of pages by Amazon.
   */
  public function getPages() {
    return $this->pages;
  }

  /**
   * Returns the parameters set for this search.
   *
   * @return array
   *   An array of ItemSearch parameters.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Returns a search parameter if found.
   *
   * @param string $name
   *   The parameter name.
   *
   * @return null|string
   *   NULL if parameter not found, value otherwise.
   */
  public function getParameter($name) {
    return isset($this->parameters[$name]) ? $this->parameters[$name] : NULL;
  }
}

/**
 * Class LazyAmazonBrowseNode
 * This class is needed to prevent api queries to Amazon if there is
 * no real need for the data.
 */
class LazyAmazonBrowseNode {
  /**
   * @var int
   *   The browsenode id.
   */
  protected $browsenodeId;

  /**
   * @var null|false|AmazonBrowseNode
   *   The loaded browsenode.
   */
  protected $data;

  /**
   * Class constructor.
   *
   * @param int $browsenode_id
   *   The Amazon BrowseNode id.
   */
  public function __construct($browsenode_id) {
    $this->browsenodeId = $browsenode_id;

    // Set data to NULL to allow lazy load.
    $this->data = NULL;
  }

  /**
   * Loads the actual BrowseNode making the api call.
   *
   * @return false|AmazonBrowseNode
   *   Returns the AmazonBrowseNode or false if query failed.
   */
  public function value() {
    if (is_null($this->data)) {
      $this->data = gp_amazon_get_browsenode_clean($this->browsenodeId);
    }

    return $this->data;
  }

  /**
   * Returns the BrowseNode id.
   *
   * @return int
   *   The Amazon browsenode id.
   */
  public function getBrowsenodeId() {
    return $this->browsenodeId;
  }
}
