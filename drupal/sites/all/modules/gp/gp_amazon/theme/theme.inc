<?php
/**
 * @file
 * Theme related functions.
 */

/**
 * Theme function to render a browsenode navigation list.
 */
function theme_browsenode_nav($variables) {
  /* @var AmazonBrowseNode $browsenode */
  $browsenode = $variables['browsenode'];

  $items = array();
  // Level represents the current level in the array of item_list elements.
  $level = & $items;

  // Set search index first.
  $search_index = $browsenode->getSearchIndex();
  if ('All' !== $search_index['key'] && $search_index['BrowseNodeId'] != $browsenode->getId()) {
    $level[$search_index['key']] = array(
      'data' => l($search_index['label'], "browse/{$search_index['BrowseNodeId']}"),
      'children' => array(),
    );
    $level = & $level[$search_index['key']]['children'];
  }

  if ($browsenode->hasAncestors()) {
    $ancestors = $browsenode->getAncestors();
    while ($node = array_pop($ancestors)) {
      /* @var AmazonBrowseNode $node */
      // Skip ancestor if it's a root browsenode.
      if ($node->isRoot()) {
        continue;
      }

      $level[$node->getId()] = array(
        'data' => l($node->getName(), "browse/{$node->getId()}"),
        'children' => array(),
      );
      $level = & $level[$node->getId()]['children'];
    }
  }

  $level[$browsenode->getId()] = array(
    'data' => '<span class="current">' . $browsenode->getName() . '</span>',
  );

  // Add children of current browsenode.
  if ($browsenode->hasChildren()) {
    $level = & $level[$browsenode->getId()];
    $level['children'] = array();

    foreach ($browsenode->getChildren() as $child) {
      /* @var AmazonBrowseNode $child */
      $level['children'][] = array(
        'data' => l($child->getName(), "browse/{$child->getId()}"),
      );
    }
  }

  return theme('item_list', array('items' => $items));
}

/**
 * Preprocess function for amazon_search_items.
 */
function template_preprocess_gp_amazon_search_results(&$variables) {
  /* @var AmazonSearch $search */
  $search = $variables['search'];
  $items = $search->getItems();
  $view_mode = is_null($variables['view_mode']) ? 'detail' : $variables['view_mode'];

  // We need view mode for the items wrapper, set it back so it always
  // has a value.
  $variables['view_mode'] = $view_mode;

  $variables['results'] = array(
    '#theme' => 'gp_amazon_items',
    '#items' => $items,
    '#view_mode' => $view_mode,
  );

  // Add results numbers.
  $start = 1 + ($search->getParameter('ItemPage') - 1) * 10;
  $end = $start + count($items) - 1;
  $variables['results_count'] = array(
    '#prefix' => '<div class="result-count">',
    '#markup' => t('@start-@end of @total results', array(
      '@start' => $start,
      '@end' => $end,
      '@total' => $search->getCount(),
    )),
    '#suffix' => '</div>',
  );

  // Render pager, if needed.
  if ($search->getPages() > 1) {
    $variables['pager'] = array(
      '#theme' => 'gp_amazon_search_pager',
      '#current_page' => $search->getParameter('ItemPage'),
      '#total_pages' => $search->getPages(),
      '#search_index' => $search->getParameter('SearchIndex'),
    );
  }

  // Render sort, if needed.
  $indexes = gp_amazon_search_indexes();
  $current_index = $search->getParameter('SearchIndex');
  if (isset($indexes[$current_index]) && count($indexes[$current_index]['sorts'])) {
    $variables['sort'] = array(
      '#theme' => 'gp_amazon_search_sort',
      '#search_index' => $search->getParameter('SearchIndex'),
      '#current_sort' => $search->getParameter('Sort'),
    );
  }

  // Render view mode switch.
  $view_modes = array();
  $current_view_mode = gp_amazon_get_query_view_mode();
  foreach (gp_amazon_item_view_modes() as $slug => $name) {
    // Skip full view mode.
    if ('full' === $slug) {
      continue;
    }
    // Prepare attributes for the link.
    $attributes = array();
    if ($current_view_mode === $slug) {
      $attributes['class'] = array('active');
    }
    $view_modes[] = array(
      'data' => gp_amazon_make_link($name, NULL, array('amode' => $slug), $attributes),
      'class' => array('view-mode-' . $slug),
    );
  }
  $variables['view_modes'] = array(
    '#theme' => 'item_list',
    '#items' => $view_modes,
    '#attributes' => array(
      'class' => array('view-mode-switcher'),
    ),
  );
}

/**
 * Preprocess function for gp_amazon_items. Render a list of amazon items.
 */
function template_preprocess_gp_amazon_items(&$variables) {
  $items = $variables['items'];
  $view_mode = is_null($variables['view_mode']) ? 'detail' : $variables['view_mode'];

  // Prepare rows.
  $rows = array();
  $row_attributes_array = array();
  $count = 0;
  $max = count($items);

  // Reset keys as we want delta and not ASINs.
  $items = array_values($items);

  foreach ($items as $delta => $item) {
    $count++;
    $rows[$delta] = array(
      '#theme' => 'gp_amazon_item',
      '#item' => $item,
      '#view_mode' => $view_mode,
      '#extra' => array('delta' => $delta),
    );
    $row_attributes_array[$delta]['class'] = array(
      'amazon-search-row',
      'amazon-search-row-' . $count,
      'amazon-search-row-' . ($count % 2 ? 'odd' : 'even'),
    );
    if (1 === $count) {
      $row_attributes_array[$delta]['class'][] = 'amazon-search-row-first';
    }
    if ($max === $count) {
      $row_attributes_array[$delta]['class'][] = 'amazon-search-row-last';
    }
  }

  $variables['rows'] = $rows;
  $variables['row_attributes_array'] = $row_attributes_array;
}

/**
 * Process function for amazon_items.
 */
function template_process_gp_amazon_items(&$variables) {
  foreach ($variables['rows'] as $delta => $row) {
    $variables['row_attributes'][$delta] = drupal_attributes($variables['row_attributes_array'][$delta]);
  }
}

/**
 * Preprocess function for amazon item template.
 */
function template_preprocess_gp_amazon_item(&$variables) {
  /* @var AmazonItem $item */
  $item = $variables['item'];
  $view_mode = $variables['view_mode'];
  $extra = $variables['extra'];

  $variables['classes_array'] = array(
    'amazon-item',
    'amazon-item-' . $item->ASIN,
    'amazon-item--' . $view_mode,
  );

  // Always add suggestion (even if template is different from default).
  $variables['theme_hook_suggestions'][] = 'gp_amazon_item__' . $view_mode;

  // Prepare variables.
  $variables['url'] = url("product/{$item->ASIN}");
  $variables['asin'] = $item->ASIN;
  $variables['title'] = $item->ItemAttributes['Title'];
  $variables['delta'] = !empty($extra['delta']) ? $extra['delta'] : FALSE;

  // Add extra elements.
  $variables['similarities'] = FALSE;
  if (!empty($extra['similarities'])) {
    // Fake panel rendering. Magic!
    // @todo is this cool?
    $pane = ctools_content_render(
      'item_similarities',
      'item_similarities',
      array(
        'item_view_mode' => $extra['similarities_view_mode'],
        'max_results' => $extra['similarities_max_results'],
        'context' => 'argument_asin_1',
        'override_title' => 1,
        'override_title_text' => '',
      ),
      array(),
      array($item->ASIN),
      array(
        'argument_asin_1' => ctools_context_create('item', $item->ASIN),
      )
    );

    if (!empty($pane)) {
      $variables['similarities'] = array(
        // Render this block as slide.
        '#prefix' => '<div class="render-mode-slick">',
        '#suffix' => '</div>',
        'content' => $pane->content,
      );
    }
  }

  // Add schema.org integration.
  $variables['attributes_array'] += array(
    'itemscope' => '',
    'itemtype' => 'http://schema.org/' . $item->getSchemaType(),
  );

  // Prepare attributes.
  $variables['item_attributes'] = [];
  foreach ($item->ItemAttributes as $key => $item_attribute) {
    // Item attributes only for full view mode.
    if ($view_mode != 'full') {
      break;
    }

    // Empty attribute.
    if (empty($item_attribute)) {
      continue;
    }

    $item_markup = '<label>' . t($key) . ':</label>&nbsp;';
    $access = FALSE;

    switch ($key) {
      case 'EANList':
      case 'UPCList':
        if (!(is_array($item_attribute))) {
          break;
        }
        foreach ($item_attribute as $elem) {
          if (!(is_array($elem))) {
            $item_markup .= '<span class="field__item">' . $elem . '</span>&nbsp;';
          }
        }
        break;

      case 'Director':
        if (!(is_array($item_attribute))) {
          break;
        }
        $prefix = '';
        foreach ($item_attribute as $elem) {
          $item_markup .= $prefix . '<span class="field__item">' . $elem['0'] . '</span>';
          $prefix = ', ';
        }
        break;

      case 'Feature':
        if (!(is_array($item_attribute))) {
          break;
        }
        $prefix = '';
        foreach ($item_attribute as $elem) {
          $item_markup .= $prefix . '<span class="field__item">' . $elem['0'] . '</span>';
          $prefix = '. ';
        }
        break;

      case 'ItemDimensions':
      case 'PackageDimensions':
        if (!(is_array($item_attribute))) {
          break;
        }
        if ($key === 'ItemDimensions') {
          $dimensions = $item->getItemDimensions();
          $weight = $item->getItemWeight();
        }
        elseif ($key === 'PackageDimensions') {
          $dimensions = $item->getPackageDimensions();
          $weight = $item->getPackageWeight();
        }
        else {
          break;
        }

        $access = TRUE;
        $item_markup .= '<span class="field__item">';
        $elem_key = 'Height';
        if (!empty($dimensions[$elem_key])) {
          $elem = $dimensions[$elem_key];
          $item_markup .= $elem;
        }
        $elem_key = 'Width';
        if (!empty($dimensions[$elem_key])) {
          $elem = $dimensions[$elem_key];
          $item_markup .= ' x ' . $elem;
        }
        $elem_key = 'Length';
        if (!empty($dimensions[$elem_key])) {
          $elem = $dimensions[$elem_key];
          $item_markup .= ' x ' . $elem;
        }

        // Print correct unit.
        if (!empty($dimensions['Unit'])) {
          $item_markup .= '&nbsp;' . $dimensions['Unit'] . ' ';
        }

        $elem_key = 'Weight';
        if (!empty($weight[$elem_key])) {
          $item_markup .= '</span>&nbsp;-&nbsp;';
          $item_markup .= '<span class="field__item">( ';

          $elem = $weight[$elem_key];
          $item_markup .= t($elem_key) . ': ' . $elem;

          // Print correct unit.
          $item_markup .= '&nbsp;' . $weight['Unit'] . ' ';
          $item_markup .= ')</span>&nbsp;';
        }
        else {
          $item_markup .= '</span>&nbsp;';
        }
        break;

      case 'ListPrice':
        if (!(is_array($item_attribute))) {
          break;
        }
        foreach ($item_attribute as $elem_key => $elem) {
          $item_markup .= '<span class="field__item">' . $elem . '(' . t($elem_key) . ')</span>&nbsp;';
        }
        break;

      case 'Languages':
        if (!(is_array($item_attribute))) {
          break;
        }
        if ((isset($item_attribute['Language']['Name'])) && (isset($item_attribute['Language']['Type']))) {
          $access = TRUE;
          $item_markup .= '<span class="field__item">' . $item_attribute['Language']['Name'] . ' (' . $item_attribute['Language']['Type'] . ')</span>';
        }
        elseif (isset($item_attribute['Language'])) {
          $access = TRUE;
          $prefix = '';
          foreach ($item_attribute['Language'] as $elem) {
            $item_markup .= $prefix . '<span class="field__item">' . $elem['Name'] . ' (' . $elem['Type'] . ')</span>';
          }
        }
        break;

      case 'ReleaseDate':
        if (is_array($item_attribute)) {
          break;
        }
        $release_date = DateTime::createFromFormat('Y-m-d', $item_attribute)->getTimestamp();
        $access = TRUE;
        $item_markup .= '<span class="field__item">' . format_date($release_date, 'custom', 'd/m/Y') . '</span>';
        break;

      case 'RunningTime':
        if (is_array($item_attribute)) {
          break;
        }
        $access = TRUE;

        $running_time = $item->getRunningTime();
        $item_markup .= '<span class="field__item">' . $running_time . '</span>';
        break;

      case 'Brand':
      case 'Color':
      case 'Department':
      case 'EAN':
      case 'Format':
      case 'Genre':
      case 'HardwarePlatform':
      case 'Model':
      case 'NumberOfDiscs':
      case 'OperatingSystem':
      case 'PackageQuantity':
      case 'PartNumber':
      case 'PictureFormat':
      case 'Platform':
      case 'Warranty':
        if (is_array($item_attribute)) {
          break;
        }
        $access = TRUE;
        $item_markup .= '<span class="field__item">' . $item_attribute . '</span>';
        break;

      default:
        if (is_array($item_attribute)) {
          $item_markup .= '<span class="field__item"> - </span>';
          break;
        }
        $item_markup .= '<span class="field__item">' . $item_attribute . '</span>';
        break;
    }

    $variables['item_attributes'][$key] = array(
      'label' => $key,
      'value' => $item_attribute,
      '#prefix' => '<div class="field field--name-' . $key . ' field--label-inline inline">',
      '#markup' => $item_markup,
      '#suffix' => '</div>',
      '#access' => $access,
    );
  }
}

/**
 * Theme function to render a pager for Amazon searches.
 *
 * @see omega_pager()
 */
function theme_gp_amazon_search_pager($variables) {
  $search_index = $variables['search_index'];
  $current_page = $variables['current_page'];
  $total_pages = $variables['total_pages'];

  if ($total_pages == 1) {
    // Only one page, do not render.
    return FALSE;
  }

  // Calculate various markers within this pager piece:
  // Current is the page we are currently paged to.
  $pager_current = $current_page;
  // First is the first page listed by this pager piece (re quantity).
  $pager_first = $pager_current - 1;
  // Last is the last page listed by this pager piece (re quantity).
  $pager_last = $pager_current + 1;
  // Max is the maximum page number.
  $search_indexes = gp_amazon_search_indexes();
  if ((isset($search_indexes[$search_index]['max_pages']))) {
    $max_pages = $search_indexes[$search_index]['max_pages'];
  }
  $pager_max = ($total_pages > $max_pages) ? $max_pages : $total_pages;
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }

  $items = array();

  // Add previous button.
  $previous_item = t('Previous page');
  if ($pager_current > 1) {
    $previous_item = gp_amazon_make_link($previous_item, NULL, array('apage' => $pager_current - 1));
  }
  $items[] = array(
    'class' => array('pager__item', 'pager__item--previous'),
    'data' => $previous_item,
  );

  if ($i > 1) {
    $items[] = array(
      'class' => array('pager__item', 'pager__item--first'),
      'data' => gp_amazon_make_link(1, NULL, array('apage' => NULL)),
    );
  }

  if ($i > 2) {
    $items[] = array(
      'class' => array('pager__item', 'pager__item--ellipsis'),
      'data' => '…',
    );
  }

  // Generate middle pagers.
  for (; $i <= $pager_last && $i <= $pager_max; $i++) {
    if ($i == $pager_current) {
      // Current page is a simple element.
      $items[] = array(
        'class' => array('pager__item', 'pager__item--current'),
        'data' => $i,
      );
    }
    else {
      $items[] = array(
        'class' => array('pager__item'),
        'data' => gp_amazon_make_link($i, NULL, array('apage' => $i)),
      );
    }
  }

  if ($i < $pager_max) {
    $items[] = array(
      'class' => array('pager__item', 'pager__item--ellipsis'),
      'data' => '…',
    );
  }

  if ($i <= $pager_max) {
    $items[] = array(
      'class' => array('pager__item', 'pager__item--last'),
      'data' => gp_amazon_make_link($pager_max, NULL, array('apage' => $pager_max)),
    );
  }

  $next_item = t('Next page');
  if ($pager_current < $pager_max) {
    $next_item = gp_amazon_make_link($next_item, NULL, array('apage' => $pager_current + 1));
  }
  $items[] = array(
    'class' => array('pager__item', 'pager__item--next'),
    'data' => $next_item,
  );

  return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
    'items' => $items,
    'attributes' => array('class' => array('pager')),
  ));
}

/**
 * Theme function to render a sort form for Amazon searches.
 */
function theme_gp_amazon_search_sort($variables) {
  $current_index = $variables['search_index'];
  $current_sort = $variables['current_sort'];

  $indexes = gp_amazon_search_indexes();

  if (!isset($indexes[$current_index]) || empty($indexes[$current_index]['sorts'])) {
    // Index not found/without sorts, do not render anything.
    return '';
  }

  $sorts = $indexes[$current_index]['sorts'];
  $items = array();

  foreach ($sorts as $value => $label) {
    // Next line is marked as warning but we are passing
    // the sort label for translation, so it's correct.
    $sort_label = t($label, array(), array('context' => 'amazon-sort'));
    $items[$value] = array(
      'data' => gp_amazon_make_link($sort_label, NULL, array('asort' => $value, 'apage' => NULL)),
    );
  }

  // Add current sort as first element.
  if (!is_null($current_sort) && isset($items[$current_sort])) {
    $first = $items[$current_sort];
    $first['class'] = 'active-sort';
    unset($items[$current_sort]);
    array_unshift($items, $first);
  }

  return theme('item_list', array('items' => $items));
}

/**
 * Helper function to make a link.
 *
 * @see http://drupal.org/node/1410574
 *
 * @param string $text
 *   The link text.
 *
 * @param null|string $url
 *   An url to use, or NULL to use current page url.
 *
 * @param array $parameters
 *   A list of query parameters to add/replace/remove from current ones.
 *
 * @param array $attributes
 *   An array of attributes to add to the link.
 *
 * @return string
 *   An HTML link.
 */
function gp_amazon_make_link($text, $url = NULL, $parameters = array(), $attributes = array()) {
  // Get query parameters, excluding ours.
  $query = drupal_get_query_parameters(NULL, array_merge(array('q'), array_keys($parameters)));

  // Add our parameter.
  foreach ($parameters as $key => $value) {
    // If value is set to NULL, we are only removing it so skip.
    if (!is_null($value)) {
      $query[$key] = $value;
    }
  }

  // If $url is NULL, get current one.
  if (is_null($url)) {
    $url = $_GET['q'];
  }

  // We use our href here.
  $attributes['href'] = url($url, array('query' => $query));

  // Add active class if current page url matches requested one.
  // This is different from Drupal implementation as it takes in account
  // matching of query parameters.
  if ($_GET['q'] == $url) {
    // Verify that query parameters match too.
    $current_query = drupal_get_query_parameters();

    // Continue only if number of parameters match.
    if (count($current_query) === count($query)) {

      // Loop each parameter and verify that both key and value match.
      $equal = TRUE;
      foreach ($current_query as $key => $param) {
        if (!isset($query[$key]) || $query[$key] !== $param) {
          $equal = FALSE;
        }
      }

      if ($equal) {
        $attributes['class'][] = 'active';
      }
    }
  }

  return '<a' . drupal_attributes($attributes) . '>' . check_plain($text) . '</a>';
}

/**
 * Returns HTML for an Amazon breadcrumb.
 */
function theme_gp_amazon_breadcrumb($variables) {
  $browsenode = $variables['browsenode'];
  $current = $variables['current'];

  $breadcrumb = gp_amazon_get_breadcrumb($browsenode, $current);

  return theme('breadcrumb', array('breadcrumb' => $breadcrumb));
}
