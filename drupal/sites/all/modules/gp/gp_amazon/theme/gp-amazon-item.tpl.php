<?php
/**
 * @file
 * Template to render an amazon item in detail mode.
 *
 * @var string $attributes
 *   List of HTML attributes.
 *
 * @var string $url
 *   The url of the product.
 *
 * @var AmazonItem $item
 *   An AmazonItem object.
 */
?>
<article<?php print $attributes; ?>>
  <h1><a href="<?php print $url; ?>"><?php print $item->ItemAttributes['Title']; ?></a></h1>
  <h3><?php print $item->ASIN; ?></h3>
  <div><img src="<?php print $item->SmallImage['URL']; ?>" alt=""/></div>
</article>
