<?php
/**
 * @file
 * Template to render a list of Amazon items.
 */
?>
<?php foreach ($rows as $delta => $row): ?>
  <div<?php print $row_attributes[$delta]; ?>>
    <?php print render($row); ?>
  </div>
<?php endforeach; ?>
