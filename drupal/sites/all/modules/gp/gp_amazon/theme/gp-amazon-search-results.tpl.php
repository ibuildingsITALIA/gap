<?php
/**
 * @file
 * Template to render a list of Amazon items.
 */
?>

<div class="search-header-bar">
  <?php print render($results_count); ?>
  <?php print render($view_modes); ?>
  <?php if (isset($sort)): ?>
    <div class="sort">
      <span class="sort-label"><?php print t('Order by'); ?></span>
      <div class="sort-button">
        <?php print render($sort); ?>
        <a href="#" class="sort-arrow"><span>▼</span></a>
      </div>
    </div>
  <?php endif; ?>
</div>

<div class="search-results search-results--amode-<?php print $view_mode; ?>">
  <?php print render($results); ?>
</div>

<?php if (isset($pager)): ?>
  <div class="pager amazon-search-pager">
    <?php print render($pager); ?>
  </div>
<?php endif; ?>
