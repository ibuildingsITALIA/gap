<?php
/**
 * @file
 * gp_panels_page.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gp_panels_page_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array(
    0 => array(
      'identifier' => 'Books',
      'keyword' => 'books',
      'name' => 'browsenode',
      'browsenode_id' => '411663031',
      'id' => 1,
    ),
    1 => array(
      'identifier' => 'E-books',
      'keyword' => 'ebooks',
      'name' => 'browsenode',
      'browsenode_id' => '827182031',
      'id' => 2,
    ),
  );
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'fullwidth';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '517c5c47-1779-43c7-a4a1-b51be896214f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-64e70719-1e3d-4a15-9576-9803497a314e';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '64e70719-1e3d-4a15-9576-9803497a314e';
    $display->content['new-64e70719-1e3d-4a15-9576-9803497a314e'] = $pane;
    $display->panels['content'][0] = 'new-64e70719-1e3d-4a15-9576-9803497a314e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:default'] = $panelizer;

  return $export;
}
