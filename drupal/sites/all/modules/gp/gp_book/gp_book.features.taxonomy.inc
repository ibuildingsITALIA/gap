<?php
/**
 * @file
 * gp_book.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function gp_book_taxonomy_default_vocabularies() {
  return array(
    'author' => array(
      'name' => 'Autore',
      'machine_name' => 'author',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'bic_qualifier' => array(
      'name' => 'Qualificatori BIC',
      'machine_name' => 'bic_qualifier',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'bic_subject' => array(
      'name' => 'Soggetti BIC',
      'machine_name' => 'bic_subject',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'format' => array(
      'name' => 'Formato',
      'machine_name' => 'format',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'publisher' => array(
      'name' => 'Editore',
      'machine_name' => 'publisher',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'series' => array(
      'name' => 'Collana',
      'machine_name' => 'series',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'settori_article' => array(
      'name' => 'Settori Article',
      'machine_name' => 'settori_article',
      'description' => 'Settori Article',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
