<?php
/**
 * Created by PhpStorm.
 * User: max2thousand
 * Date: 04/03/15
 * Time: 10:11
 */

/**
 * Implements hook_drush_help().
 */
function gp_sector_import_drush_help($command) {
  switch ($command) {
    case 'drush:gpsector-import':
      return dt('Import drush gp sectors.');
    case 'drush:gpsector-taxonomy':
      return dt('Import drush gp sectors taxonomies.');

  }
}

/**
 * Implements hook_drush_command().
 */
function gp_sector_import_drush_command() {
  $items = array();
  $items['gpsector-import'] = array(
    'description' => dt('Import drush gp sectors.'),
  );
  $items['gpsector-taxonomy'] = array(
    'description' => dt('Import drush gp sectors taxonomies.'),
  );
  return $items;
}

function drush_gp_sector_import_gpsector_import() {
  global $amazon_import;
  //Fill array
  $db_data = array();
  $file_data = array();
  $array_diff = array();
  $url = variable_get('gap_microsoft_sql_server_url', '192.168.0.198');
  $port = variable_get('gap_microsoft_sql_server_port', 3306);
  $username = variable_get('gap_microsoft_sql_server_username', 'gapstore_web');
  $password = variable_get('gap_microsoft_sql_server_password', 'EaNWebGaP2o!5$');
  $database = variable_get('gap_microsoft_sql_server_database', 'statistiche');
  $db_connector = new MSSqlServerConnector($url, $port, $username, $password, $database);
  $db_connector->connectToDb();

  $q = <<<EOF
select * from gapstore_eansettori
EOF;
  $result = db_query($q);
  $terms_to_save = array();
  while ($record = $result->fetchAssoc()) {
    $db_data[$record['EAN']] = $record['CODSETTORE'];
    $term = new stdClass();
    $term->name = $record['DESCRIZIONE'];
    $term->field_id_fake = $record['CODSETTORE'];
    $terms_to_save[$term->field_id_fake] = $term;
  }

  $db_connector->close();
  $vocabulary = taxonomy_vocabulary_machine_name_load('settori_article');
  $terms = array();
  foreach ($terms_to_save as $single_term) {
    $single_term->vid = $vocabulary->vid;
    if (empty($single_term->name) || empty($single_term->field_id_fake)) {
      continue;
    }
    $term = _gp_sector_import_get_taxonomy_by_field_id($single_term->field_id_fake, $vocabulary->vid);
    if (!$term) {
      //E' un padre, non faccio nulla
      if (strlen($single_term->field_id_fake) <= 1) {
        $single_term->parent = 0;
      }
      //ALTRIMENTI, vado a prendere il padre.
      $codice_padre = substr($single_term->field_id_fake, 0, -1);
      $padre = _gp_sector_import_get_taxonomy_by_field_id($codice_padre, $vocabulary->vid);
      if (!empty($padre)) {
        $single_term->parent = $padre->tid;
      }
      else {
        if (strlen($single_term->field_id_fake) == 4) {
          $codice_padre = substr($single_term->field_id_fake, 0, -2);
          $padre = _gp_sector_import_get_taxonomy_by_field_id($codice_padre, $vocabulary->vid);
          if (!empty($padre)) {
            $single_term->parent = $padre->tid;
          }
        }
      }
      taxonomy_term_save($single_term);
      $term_wrapper = entity_metadata_wrapper('taxonomy_term', $single_term);
      $term_wrapper->field_id->set($single_term->field_id_fake);
      $term_wrapper->save();
      $terms[$single_term->field_id_fake] = $single_term;
    }
    else {
      $terms[$single_term->field_id_fake] = $term;
    }
  }

  $first_time = FALSE;
  if (!file_exists('/tmp/settori_article.txt')) {
    $first_time = TRUE;
  }
  else {
    $handle = fopen('/tmp/settori_article.txt', "r");
    if ($handle) {
      while (($line = fgets($handle)) !== FALSE) {
        $parse = explode('|', trim($line));
        $file_data[$parse[0]] = $parse[1];
      }
      fclose($handle);
    }
  }


  if ($first_time) {
    $array_diff = $db_data;
  }
  else {
    foreach ($db_data as $key => $value) {
      $found = $file_data[$key];
      if (!empty($found) && $value != $found) {
        $array_diff[$key] = $found;
      }
    }
  }


  //Load node and process it
  $i = 0;
  $chunks = array_chunk($array_diff, 10000, TRUE);
  foreach ($chunks as $single_ean_block) {
    //Process books in chunks
    $start = time();
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'book')
      ->fieldCondition('field_isbn13', 'value', array_keys($single_ean_block), 'IN');
    $result = $query->execute();
    if (!isset($result['node'])) {
      continue;
    }
    drush_print('Query to fetch book executed in: ' . (time() - $start));


    $entities = entity_load('node', array_keys($result['node']));
    foreach ($entities as $entity) {
      $start = time();
      drush_print('Query to fetch taxonomy executed in: ' . (time() - $start));
      $start = time();
      $i++;
      $amazon_import = array(
        'field_article_sector' => TRUE,
      );
      $wrapper = entity_metadata_wrapper('node', $entity);
      //LOAD TAXONOMY BY FIELD_ID AND THEN
      $term_to_assoc = $terms[$wrapper->field_isbn13->value()]; //_gp_sector_import_get_taxonomy_by_field_id($value, $vocabulary->vid);

      $wrapper->field_article_sector->set($term_to_assoc->tid);
      $wrapper->save();
      drush_print('Query to associate taxonomy executed in: ' . (time() - $start));
      drush_print('Processed ' . $i . ' chunks');
    }

  }

  unlink('/tmp/settori_article.txt');

  //Write the file only when all the data are updated
  foreach ($db_data as $key => $value) {
    file_put_contents('/tmp/settori_article.txt', $key . '|' . $value . "\n", FILE_APPEND);
  }
  $amazon_import = TRUE;
}


function drush_gp_sector_import_gpsector_taxonomy() {
  $url = variable_get('gap_microsoft_sql_server_url', '192.168.0.198');
  $port = variable_get('gap_microsoft_sql_server_port', 3306);
  $username = variable_get('gap_microsoft_sql_server_username', 'gapstore_web');
  $password = variable_get('gap_microsoft_sql_server_password', 'EaNWebGaP2o!5$');
  $database = variable_get('gap_microsoft_sql_server_database', 'statistiche');
  $db_connector = new MSSqlServerConnector($url, $port, $username, $password, $database);
  $db_connector->connectToDb();

  $q = <<<EOF
SELECT * FROM gapstore_settori ORDER BY codice ASC
EOF;
  $result = db_query($q);
  $db_connector->close();
  $vocabulary = taxonomy_vocabulary_machine_name_load('settori_article');
  while ($record = $result->fetchAssoc()) {
    $term = _gp_sector_import_get_taxonomy_by_field_id($record['codice'], $vocabulary->vid);
    if (!$term) {
      $term_class = new stdClass();

      //E' un padre, non faccio nulla
      if (strlen($record['codice']) <= 1) {
        $term_class->parent = 0;
      }
      //ALTRIMENTI, vado a prendere il padre.
      $codice_padre = substr($record['codice'], 0, -1);
      $padre = _gp_sector_import_get_taxonomy_by_field_id($codice_padre, $vocabulary->vid);
      if (!empty($padre)) {
        $term_class->parent = $padre->tid;
      }
      else {
        if (strlen($record['codice']) == 4) {
          $codice_padre = substr($record['codice'], 0, -2);
          $padre = _gp_sector_import_get_taxonomy_by_field_id($codice_padre, $vocabulary->vid);
          if (!empty($padre)) {
            $term_class->parent = $padre->tid;
          }
        }
      }

      $term_class->vid = $vocabulary->vid;
      $term_class->name = $record['descrizione'];
      taxonomy_term_save($term_class);
      $term_wrapper = entity_metadata_wrapper('taxonomy_term', $term_class);
      $term_wrapper->field_id->set($record['codice']);
      $term_wrapper->save();
    }
    else {
      $term_wrapper = entity_metadata_wrapper('taxonomy_term', $term);
      $term_wrapper->name->set($record['descrizione']);
      $term_wrapper->save();
    }
  }
}

function _gp_sector_import_get_taxonomy_by_field_id($field_id, $vocabulary_id) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'taxonomy_term')
    ->propertyCondition('vid', $vocabulary_id, '=')
    ->fieldCondition('field_id', 'value', $field_id, '=');
  $result = $query->execute();
  if (!empty($result) && is_array($result)) {
    $first_value = reset($result['taxonomy_term']);
    return $first_value;
  }
  return $result;

}
