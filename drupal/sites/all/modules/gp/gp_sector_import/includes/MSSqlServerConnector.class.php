<?php

/**
 * Created by PhpStorm.
 * User: max2thousand
 * Date: 03/03/15
 * Time: 16:09
 */
class MSSqlServerConnector {

  private $url;
  private $port;
  private $username;
  private $password;
  private $database;

  public function __construct($url, $port, $username, $password, $database) {
    $this->url = $url;
    $this->port = $port;
    $this->username = $username;
    $this->password = $password;
    $this->database = $database;
  }


  public function connect() {
    $article_db_data = array(
      'database' => $this->database,
      'username' => $this->username,
      'password' => $this->password,
      'host' => $this->url,
      'driver' => 'mysql',
    );
    Database::addConnectionInfo('gapstore_article', 'default', $article_db_data);
  }

  public function connectToDb() {
    $link = $this->connect();
    db_set_active('gapstore_article');
  }

  public function close() {
    db_set_active();
  }

}