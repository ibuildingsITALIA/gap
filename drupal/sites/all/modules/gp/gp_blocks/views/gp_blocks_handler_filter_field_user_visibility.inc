<?php

/**
 * @file
 * Definition of gp_blocks_handler_filter_field_user_visibility.
 */

/**
 * Filter handler which uses list-fields as options.
 *
 * @ingroup views_filter_handlers
 */
class gp_blocks_handler_filter_field_user_visibility extends views_handler_filter {
  var $no_operator = TRUE;
  var $always_multiple = TRUE;

  function can_expose() {
    return FALSE;
  }

  function operator_options() {
    return array(
      'IN' => t('Is one of'),
    );
  }

  function options_form(&$form, &$form_state) {
    // No configuration form for us.
    $form['noconf'] = array(
      '#markup' => t('This filter has no configuration.'),
    );

    // Default operator.
    $form['operator'] = array(
      '#type' => 'value',
      '#value' => 'IN',
    );
  }

  function admin_summary() {
    return t('Depends on current user logged status');
  }

  function query() {
    $values = array();
    // Add the current status.
    $values[] = (int) user_is_logged_in();
    // And the all status.
    $values[] = -1;

    $this->ensure_my_table();
    $this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field", $values, $this->operator);
  }
}
