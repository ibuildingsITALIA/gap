<?php
/**
 * @file
 * gp_blocks.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function gp_blocks_taxonomy_default_vocabularies() {
  return array(
    'block_group' => array(
      'name' => 'Gruppo blocco',
      'machine_name' => 'block_group',
      'description' => 'Raggruppa i blocchi per la visualizzazione.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
