<?php
/**
 * @file
 * gp_blocks.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function gp_blocks_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'banner';
  $bean_type->label = 'Banner';
  $bean_type->options = '';
  $bean_type->description = 'Banner da visualizzare nella testata del sito.';
  $export['banner'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'box';
  $bean_type->label = 'Box';
  $bean_type->options = '';
  $bean_type->description = 'Un semplice box con immagine e link.';
  $export['box'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'slide';
  $bean_type->label = 'Slide';
  $bean_type->options = '';
  $bean_type->description = 'Blocchi usati per la generazione di slideshow.';
  $export['slide'] = $bean_type;

  return $export;
}
