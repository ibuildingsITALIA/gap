<?php
/**
 * @file
 * gp_blocks.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function gp_blocks_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'administration_beans';
  $view->description = 'Find and manage bean blocks.';
  $view->tag = 'default';
  $view->base_table = 'bean';
  $view->human_name = 'Administration: Beans';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Blocks';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'label' => 'label',
    'type' => 'type',
    'field_blocks' => 'field_blocks',
    'field_sort_weight' => 'field_sort_weight',
    'edit_link' => 'edit_link',
    'delete_link' => 'edit_link',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'label' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_blocks' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_sort_weight' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_link' => array(
      'align' => '',
      'separator' => '&emsp;',
      'empty_column' => 0,
    ),
    'delete_link' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'No results found.';
  /* Field: Block: Label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'bean';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  /* Field: Block: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'bean';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: Block: Blocchi */
  $handler->display->display_options['fields']['field_blocks']['id'] = 'field_blocks';
  $handler->display->display_options['fields']['field_blocks']['table'] = 'field_data_field_blocks';
  $handler->display->display_options['fields']['field_blocks']['field'] = 'field_blocks';
  $handler->display->display_options['fields']['field_blocks']['label'] = 'Gruppo';
  $handler->display->display_options['fields']['field_blocks']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_blocks']['delta_offset'] = '0';
  /* Field: Block: Peso */
  $handler->display->display_options['fields']['field_sort_weight']['id'] = 'field_sort_weight';
  $handler->display->display_options['fields']['field_sort_weight']['table'] = 'field_data_field_sort_weight';
  $handler->display->display_options['fields']['field_sort_weight']['field'] = 'field_sort_weight';
  /* Field: Block: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'bean';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['label'] = 'Operations';
  /* Field: Block: Delete link */
  $handler->display->display_options['fields']['delete_link']['id'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['table'] = 'bean';
  $handler->display->display_options['fields']['delete_link']['field'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['label'] = '';
  $handler->display->display_options['fields']['delete_link']['element_label_colon'] = FALSE;
  /* Sort criterion: Block: Changed */
  $handler->display->display_options['sorts']['changed']['id'] = 'changed';
  $handler->display->display_options['sorts']['changed']['table'] = 'bean';
  $handler->display->display_options['sorts']['changed']['field'] = 'changed';
  $handler->display->display_options['sorts']['changed']['order'] = 'DESC';
  /* Sort criterion: Block: Created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'bean';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Block: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'bean';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    52310416 => 0,
  );
  /* Filter criterion: Block: Label */
  $handler->display->display_options['filters']['label']['id'] = 'label';
  $handler->display->display_options['filters']['label']['table'] = 'bean';
  $handler->display->display_options['filters']['label']['field'] = 'label';
  $handler->display->display_options['filters']['label']['operator'] = 'contains';
  $handler->display->display_options['filters']['label']['group'] = 1;
  $handler->display->display_options['filters']['label']['exposed'] = TRUE;
  $handler->display->display_options['filters']['label']['expose']['operator_id'] = 'label_op';
  $handler->display->display_options['filters']['label']['expose']['label'] = 'Label';
  $handler->display->display_options['filters']['label']['expose']['operator'] = 'label_op';
  $handler->display->display_options['filters']['label']['expose']['identifier'] = 'label';
  $handler->display->display_options['filters']['label']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    52310416 => 0,
  );
  /* Filter criterion: Block: Blocchi (field_blocks) */
  $handler->display->display_options['filters']['field_blocks_tid']['id'] = 'field_blocks_tid';
  $handler->display->display_options['filters']['field_blocks_tid']['table'] = 'field_data_field_blocks';
  $handler->display->display_options['filters']['field_blocks_tid']['field'] = 'field_blocks_tid';
  $handler->display->display_options['filters']['field_blocks_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_blocks_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_blocks_tid']['expose']['operator_id'] = 'field_blocks_tid_op';
  $handler->display->display_options['filters']['field_blocks_tid']['expose']['label'] = 'Blocchi';
  $handler->display->display_options['filters']['field_blocks_tid']['expose']['operator'] = 'field_blocks_tid_op';
  $handler->display->display_options['filters']['field_blocks_tid']['expose']['identifier'] = 'field_blocks_tid';
  $handler->display->display_options['filters']['field_blocks_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    52310416 => 0,
  );
  $handler->display->display_options['filters']['field_blocks_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_blocks_tid']['vocabulary'] = 'block_group';
  $handler->display->display_options['filters']['field_blocks_tid']['hierarchy'] = 1;

  /* Display: System */
  $handler = $view->new_display('system', 'System', 'system_1');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['path'] = 'admin/content/blocks';
  $translatables['administration_beans'] = array(
    t('Master'),
    t('Blocks'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('No results found.'),
    t('Label'),
    t('Type'),
    t('Gruppo'),
    t('Peso'),
    t('Operations'),
    t('Blocchi'),
    t('System'),
  );
  $export['administration_beans'] = $view;

  $view = new view();
  $view->name = 'banners_top';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'bean';
  $view->human_name = 'Banners top';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  /* Field: Block: Internal, numeric block ID */
  $handler->display->display_options['fields']['bid']['id'] = 'bid';
  $handler->display->display_options['fields']['bid']['table'] = 'bean';
  $handler->display->display_options['fields']['bid']['field'] = 'bid';
  /* Sort criterion: Block: Peso (field_sort_weight) */
  $handler->display->display_options['sorts']['field_sort_weight_value']['id'] = 'field_sort_weight_value';
  $handler->display->display_options['sorts']['field_sort_weight_value']['table'] = 'field_data_field_sort_weight';
  $handler->display->display_options['sorts']['field_sort_weight_value']['field'] = 'field_sort_weight_value';
  /* Filter criterion: Block: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'bean';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'banner' => 'banner',
  );
  /* Filter criterion: Block: User visibility (field_user_visibility) */
  $handler->display->display_options['filters']['field_user_visibility_value']['id'] = 'field_user_visibility_value';
  $handler->display->display_options['filters']['field_user_visibility_value']['table'] = 'field_data_field_user_visibility';
  $handler->display->display_options['filters']['field_user_visibility_value']['field'] = 'field_user_visibility_value';
  $handler->display->display_options['filters']['field_user_visibility_value']['operator'] = 'IN';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'banners_top_block');
  $translatables['banners_top'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Internal, numeric block ID'),
    t('.'),
    t(','),
    t('Block'),
  );
  $export['banners_top'] = $view;

  $view = new view();
  $view->name = 'blurb_block';
  $view->description = 'Mostra un gruppo di slide in un blocco con visualizzazione blurb (immagine/link).';
  $view->tag = 'default';
  $view->base_table = 'bean';
  $view->human_name = 'Blurb block';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Field: Immagine */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['type'] = 'image_link_formatter';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => '',
    'image_link' => 'field_link',
  );
  $handler->display->display_options['fields']['field_image']['field_api_classes'] = TRUE;
  /* Field: Block: Link */
  $handler->display->display_options['fields']['field_link']['id'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
  $handler->display->display_options['fields']['field_link']['field'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['label'] = '';
  $handler->display->display_options['fields']['field_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_link']['field_api_classes'] = TRUE;
  /* Sort criterion: Block: Peso (field_sort_weight) */
  $handler->display->display_options['sorts']['field_sort_weight_value']['id'] = 'field_sort_weight_value';
  $handler->display->display_options['sorts']['field_sort_weight_value']['table'] = 'field_data_field_sort_weight';
  $handler->display->display_options['sorts']['field_sort_weight_value']['field'] = 'field_sort_weight_value';
  /* Contextual filter: Block: Blocchi (field_blocks) */
  $handler->display->display_options['arguments']['field_blocks_tid']['id'] = 'field_blocks_tid';
  $handler->display->display_options['arguments']['field_blocks_tid']['table'] = 'field_data_field_blocks';
  $handler->display->display_options['arguments']['field_blocks_tid']['field'] = 'field_blocks_tid';
  $handler->display->display_options['arguments']['field_blocks_tid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_blocks_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_blocks_tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['field_blocks_tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['field_blocks_tid']['validate_options']['vocabularies'] = array(
    'block_group' => 'block_group',
  );
  /* Filter criterion: Block: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'bean';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'slide' => 'slide',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'blurb_box');
  $handler->display->display_options['pane_category']['name'] = 'Giunti';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'field_blocks_tid' => array(
      'type' => 'user',
      'context' => 'browsenode.id',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Blocco',
    ),
  );
  $translatables['blurb_block'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('All'),
    t('Content pane'),
    t('Giunti'),
  );
  $export['blurb_block'] = $view;

  $view = new view();
  $view->name = 'hero_block';
  $view->description = 'Mostra un gruppo di slide in un blocco con visualizzazione hero.';
  $view->tag = 'default';
  $view->base_table = 'bean';
  $view->human_name = 'Hero block';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Field: Immagine */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['type'] = 'image_link_formatter';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => '',
    'image_link' => 'field_link',
  );
  $handler->display->display_options['fields']['field_image']['field_api_classes'] = TRUE;
  /* Sort criterion: Block: Peso (field_sort_weight) */
  $handler->display->display_options['sorts']['field_sort_weight_value']['id'] = 'field_sort_weight_value';
  $handler->display->display_options['sorts']['field_sort_weight_value']['table'] = 'field_data_field_sort_weight';
  $handler->display->display_options['sorts']['field_sort_weight_value']['field'] = 'field_sort_weight_value';
  /* Contextual filter: Block: Blocchi (field_blocks) */
  $handler->display->display_options['arguments']['field_blocks_tid']['id'] = 'field_blocks_tid';
  $handler->display->display_options['arguments']['field_blocks_tid']['table'] = 'field_data_field_blocks';
  $handler->display->display_options['arguments']['field_blocks_tid']['field'] = 'field_blocks_tid';
  $handler->display->display_options['arguments']['field_blocks_tid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_blocks_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_blocks_tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['field_blocks_tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['field_blocks_tid']['validate_options']['vocabularies'] = array(
    'block_group' => 'block_group',
  );
  /* Filter criterion: Block: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'bean';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'slide' => 'slide',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'hero_pane');
  $handler->display->display_options['pane_category']['name'] = 'Giunti';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'field_blocks_tid' => array(
      'type' => 'user',
      'context' => 'browsenode.id',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Blocco',
    ),
  );

  /* Display: Pager */
  $handler = $view->new_display('attachment', 'Pager', 'hero_pager');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Block: Anteprima */
  $handler->display->display_options['fields']['field_thumbnail']['id'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['field'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_thumbnail']['element_type'] = '0';
  $handler->display->display_options['fields']['field_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_thumbnail']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_thumbnail']['settings'] = array(
    'image_style' => 'hero_box_thumb_42x42',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_thumbnail']['field_api_classes'] = TRUE;
  /* Field: Block: Link */
  $handler->display->display_options['fields']['field_link']['id'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
  $handler->display->display_options['fields']['field_link']['field'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['label'] = '';
  $handler->display->display_options['fields']['field_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_link']['field_api_classes'] = TRUE;
  /* Field: Block: Sottotitolo */
  $handler->display->display_options['fields']['field_subtitle']['id'] = 'field_subtitle';
  $handler->display->display_options['fields']['field_subtitle']['table'] = 'field_data_field_subtitle';
  $handler->display->display_options['fields']['field_subtitle']['field'] = 'field_subtitle';
  $handler->display->display_options['fields']['field_subtitle']['label'] = '';
  $handler->display->display_options['fields']['field_subtitle']['element_type'] = '0';
  $handler->display->display_options['fields']['field_subtitle']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_subtitle']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_subtitle']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_subtitle']['field_api_classes'] = TRUE;
  $handler->display->display_options['displays'] = array(
    'hero_pane' => 'hero_pane',
    'default' => 0,
  );
  $handler->display->display_options['attachment_position'] = 'after';
  $translatables['hero_block'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('All'),
    t('Content pane'),
    t('Giunti'),
    t('Pager'),
  );
  $export['hero_block'] = $view;

  $view = new view();
  $view->name = 'slideshow';
  $view->description = 'Mostra un gruppo di slide in un box con visualizzazione slideshow.';
  $view->tag = 'default';
  $view->base_table = 'bean';
  $view->human_name = 'Slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Field: Immagine */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['type'] = 'image_link_formatter';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => '',
    'image_link' => 'field_link',
  );
  $handler->display->display_options['fields']['field_image']['field_api_classes'] = TRUE;
  /* Sort criterion: Block: Peso (field_sort_weight) */
  $handler->display->display_options['sorts']['field_sort_weight_value']['id'] = 'field_sort_weight_value';
  $handler->display->display_options['sorts']['field_sort_weight_value']['table'] = 'field_data_field_sort_weight';
  $handler->display->display_options['sorts']['field_sort_weight_value']['field'] = 'field_sort_weight_value';
  /* Contextual filter: Block: Blocchi (field_blocks) */
  $handler->display->display_options['arguments']['field_blocks_tid']['id'] = 'field_blocks_tid';
  $handler->display->display_options['arguments']['field_blocks_tid']['table'] = 'field_data_field_blocks';
  $handler->display->display_options['arguments']['field_blocks_tid']['field'] = 'field_blocks_tid';
  $handler->display->display_options['arguments']['field_blocks_tid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_blocks_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_blocks_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_blocks_tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['field_blocks_tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['field_blocks_tid']['validate_options']['vocabularies'] = array(
    'block_group' => 'block_group',
  );
  /* Filter criterion: Block: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'bean';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'slide' => 'slide',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'slideshow_pane');
  $handler->display->display_options['pane_category']['name'] = 'Giunti';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'field_blocks_tid' => array(
      'type' => 'user',
      'context' => 'browsenode.id',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Blocco',
    ),
  );

  /* Display: Pager */
  $handler = $view->new_display('attachment', 'Pager', 'slideshow_pager');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Block: Label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'bean';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['label'] = '';
  $handler->display->display_options['fields']['label']['element_label_colon'] = FALSE;
  $handler->display->display_options['displays'] = array(
    'slideshow_pane' => 'slideshow_pane',
    'default' => 0,
  );
  $translatables['slideshow'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('All'),
    t('Content pane'),
    t('Giunti'),
    t('Pager'),
  );
  $export['slideshow'] = $view;

  return $export;
}
