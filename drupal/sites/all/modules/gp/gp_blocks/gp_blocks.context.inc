<?php
/**
 * @file
 * gp_blocks.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function gp_blocks_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'browsenode_menu_bar';
  $context->description = 'Browsenode menu bar in pages.';
  $context->tag = 'theme';
  $context->conditions = array();
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'gp_menu-menu_bar' => array(
          'module' => 'gp_menu',
          'delta' => 'menu_bar',
          'region' => 'navigation',
          'weight' => '-10',
        ),
        'menu-menu-gp-menu-bar-default' => array(
          'module' => 'menu',
          'delta' => 'menu-gp-menu-bar-default',
          'region' => 'navigation',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Browsenode menu bar in pages.');
  t('theme');
  $export['browsenode_menu_bar'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cart_page';
  $context->description = 'Blocks in cart pages';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cart' => 'cart',
        'cart-last' => 'cart-last',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'gp_blocks-last_item_suggest_items' => array(
          'module' => 'gp_blocks',
          'delta' => 'last_item_suggest_items',
          'region' => 'content',
          'weight' => '9',
        ),
        'gp_blocks-cart_suggest_items' => array(
          'module' => 'gp_blocks',
          'delta' => 'cart_suggest_items',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks in cart pages');
  t('theme');
  $export['cart_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'default_menu_bar';
  $context->description = 'Default menu bar in pages.';
  $context->tag = 'theme';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'article' => 'article',
        'book' => 'book',
        'bookstore' => 'bookstore',
        'event' => 'event',
        'page' => 'page',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'search*' => 'search*',
        'browse/*' => 'browse/*',
        'product/*' => 'product/*',
        'bestseller/*' => 'bestseller/*',
        'classifiche' => 'classifiche',
        'offerte' => 'offerte',
        'librerie' => 'librerie',
        'librerie/*' => 'librerie/*',
        'event' => 'event',
        'signup' => 'signup',
        'register' => 'register',
        'users/*' => 'users/*',
        'cart' => 'cart',
        'cart-last' => 'cart-last',
        'eventi' => 'eventi',
        'eventi/*' => 'eventi/*',
        'ricerca-avanzata' => 'ricerca-avanzata',
        'book_search' => 'book_search',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-gp-menu-bar-home' => array(
          'module' => 'menu',
          'delta' => 'menu-gp-menu-bar-home',
          'region' => 'navigation',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Default menu bar in pages.');
  t('theme');
  $export['default_menu_bar'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front_page';
  $context->description = 'Blocks in frontpage';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-gp-menu-bar-home' => array(
          'module' => 'menu',
          'delta' => 'menu-gp-menu-bar-home',
          'region' => 'navigation',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks in frontpage');
  t('theme');
  $export['front_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_page';
  $context->description = 'Blocks in product pages';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'product/*' => 'product/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sharethis-sharethis_block' => array(
          'module' => 'sharethis',
          'delta' => 'sharethis_block',
          'region' => 'navigation',
          'weight' => '0',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks in product pages');
  t('theme');
  $export['product_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_blocks';
  $context->description = 'All default blocks across entire site.';
  $context->tag = 'theme';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-banners_top-banners_top_block' => array(
          'module' => 'views',
          'delta' => 'banners_top-banners_top_block',
          'region' => 'branding',
          'weight' => '-10',
        ),
        'gp_amazon_cart-cart' => array(
          'module' => 'gp_amazon_cart',
          'delta' => 'cart',
          'region' => 'header',
          'weight' => '-10',
        ),
        'menu-menu-categories' => array(
          'module' => 'menu',
          'delta' => 'menu-categories',
          'region' => 'header',
          'weight' => '-9',
        ),
        'gp_blocks-user' => array(
          'module' => 'gp_blocks',
          'delta' => 'user',
          'region' => 'header',
          'weight' => '-8',
        ),
        'gp_amazon_search-form' => array(
          'module' => 'gp_amazon_search',
          'delta' => 'form',
          'region' => 'header',
          'weight' => '-7',
        ),
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-10',
        ),
        'menu-menu-footer-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-footer-menu',
          'region' => 'footer_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('All default blocks across entire site.');
  t('theme');
  $export['site_blocks'] = $context;

  return $export;
}
