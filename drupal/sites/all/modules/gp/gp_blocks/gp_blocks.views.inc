<?php
/**
 * @file
 * Views related hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function gp_blocks_views_data_alter(&$data) {
  if (isset($data['field_data_field_user_visibility'])) {
    $data['field_data_field_user_visibility']['field_user_visibility_value']['filter']['handler'] = 'gp_blocks_handler_filter_field_user_visibility';
  }

  if (isset($data['bean']['type'])) {
    $data['bean']['type']['sort']['handler'] = 'views_handler_sort';
    $data['bean']['type']['field']['click sortable'] = TRUE;
  }
}
