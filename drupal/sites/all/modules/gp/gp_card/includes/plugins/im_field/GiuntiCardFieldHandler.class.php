<?php
/**
 * @file
 * Handler for Giunticard plugin.
 */

/**
 * Class GiuntiCardFieldHandler
 */
class GiuntiCardFieldHandler extends StringTypeFieldHandler {
  /**
   * Return form elements for this field.
   */
  public function getForm($form, &$form_state) {
    // Do not allow to edit this field.
    return $form;
  }
}
