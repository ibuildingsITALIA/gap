<?php
/**
 * @file
 * Giunticard field plugin.
 */

$plugin = array(
  'title' => 'Giunticard mode handler',
  'handler' => 'GiuntiCardModeFieldHandler',
);
