<?php
/**
 * @file
 * Giunticard field plugin.
 */

$plugin = array(
  'title' => 'Giunticard handler',
  'handler' => 'GiuntiCardFieldHandler',
);
