<?php
/**
 * @file
 * Form callbacks.
 */

/**
 * Form callback to add a card to an user profile.
 */
function gp_card_bind_giunticard_form($form, &$form_state, $sso_uid = NULL) {
  global $user;

  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_card', 'gp_card.forms');

  // Try to retrieve the sso_ui from the form_state if not already passed
  // as parameter. This is needed to handle the ctools modal version.
  if (is_null($sso_uid)) {
    if (!empty($form_state['sso_uid'])) {
      $sso_uid = $form_state['sso_uid'];
    }
    // Otherwise fallback to current user sso_uid.
    else {
      $account = user_load($user->uid);
      $sso_uid = gp_user_get_sso_uid($account);
    }
  }

  // Now save account to form_state.
  $form_state['sso_uid'] = $sso_uid;

  $message = t('Do you own already a GiuntiCard?');
  $message .= '<br />' . t('Bind it and start earning points with your purchases.');
  $message .= '<br />' . t('Enter the 13-digit EAN and password printed on the back of your GiuntiCard.');

  $form['text'] = array(
    '#prefix' => '<div class="message">',
    '#markup' => $message,
    '#suffix' => '</div>',
  );

  $form['ean'] = array(
    '#type' => 'textfield',
    '#title' => t('EAN code'),
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#maxlength' => 13,
    '#attributes' => array('placeholder' => t('EAN code')),
  );

  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#attributes' => array('placeholder' => t('Password of your GiuntiCard in UPPERCASE')),
  );

  $notice_oldgiunticard = '<p class="notice">Attenzione: sulle vecchie GiuntiCard (di colore arancione) la password non è stampata sul retro della carta ma veniva consegnata su un foglio a parte. Nel caso tu non ne fossi in possesso puoi richiederla contattando la tua libreria preferita (elenco librerie) oppure scrivendo a online@giuntialpunto.it</p>';
  $form['actions'] = array(
    '#type' => 'actions',
    '#suffix' => '<div class="giunticard-data"><p>' . t('Where do I find these data?') . '</p></div>' . $notice_oldgiunticard,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Bind'),
  );

  return $form;
}

/**
 * Validate callback for gp_card_bind_giunticard_form.
 */
function gp_card_bind_giunticard_form_validate($form, &$form_state) {
  $values = !empty($form_state['values']) ? $form_state['values'] : array();

  // If no values provided, standard validation of the required field
  // will occur, so simply return.
  if (empty($values['ean'])) {
    return;
  }

  // Retrieve info about the EAN inserted.
  $info = gp_card_remote_get_card_info($values['ean']);

  // If we didn't find the card, warn the user and quit here.
  if (!$info) {
    form_set_error('ean', t('No card with this EAN has been found.'));
    return;
  }
  // Form is forced to be valid.
  if ('force_validate' == $info) {
    return;
  }
  // Manage Wrong Parameter Value errors.
  if (!empty($info['error_code']) && ('0003' === $info['error_code'])) {
    form_set_error('ean', t('This card cannot be managed.'));
    return;
  }
  // We can set a card as master only if the sso_uid is empty or the same
  // as the current user.
  if (!empty($info['master_uid']) && $info['master_uid'] !== $form_state['sso_uid']) {
    form_set_error('ean', t('This card is already bind to another user.'));
    return;
  }

  // Check the password provided against the card one.
  $card_password = !empty($info['password']) ? $info['password'] : '';
  if ($card_password !== $values['password']) {
    form_set_error('password', t('You have provided a wrong password.'));
  }
}

/**
 * Submit callback for gp_card_bind_giunticard_form.
 */
function gp_card_bind_giunticard_form_submit($form, &$form_state) {
  // Bind the card to the user.
  try {
    gp_card_bind_remote_card($form_state['values']['ean'], $form_state['sso_uid'],GP_CARD_MODE_MASTER,$form_state['values']['password']);
  }
  catch (Exception $e) {
    // Exception is already logged.
  }

  $path = user_is_anonymous() ? 'signup' : 'user';
  $form_state['redirect'] = $path;
}

/**
 * Form callback to add a card to an user profile.
 *
 * @todo delete, not used anymore
 */
function gp_card_create_giunticard_form($form, &$form_state, $sso_uid = NULL) {
  global $user;

  // Ensure this include file is loaded when processing this form.
  form_load_include($form_state, 'inc', 'gp_card', 'gp_card.forms');

  // If no sso_uid provided, fallback to current user sso_uid.
  if (is_null($sso_uid)) {
    $account = user_load($user->uid);
    $sso_uid = gp_user_get_sso_uid($account);
  }

  // Now save account to form_state.
  $form_state['sso_uid'] = $sso_uid;

  // Provide a wrapper for all the form.
  $form['#prefix'] = '<div id="card-create-form-wrapper">';
  $form['#suffix'] = '</div>';

  // Get the form state step.
  $step = empty($form_state['step']) ? 1 : $form_state['step'];
  $form_state['step'] = $step;

  // Default code for button.
  $form['actions'] = array('#type' => 'actions');

  if (1 === $step) {
    $form['message'] = array(
      '#prefix' => '<div class="message">',
      '#markup' => t("You don't own a GiuntiCard yet? Create it online in few seconds!"),
      '#suffix' => '</div>',
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create your GiuntiCard'),
      '#submit' => array('gp_card_create_giunticard_form_create_submit'),
    );

    return $form;
  }

  // Get info about the card.
  $card_ean = $form_state['ean'];
  $info = gp_card_remote_get_card_info($card_ean);
  $form['card'] = array(
    '#theme' => 'giunti_card_info',
    '#card' => $info,
    '#ean' => $card_ean,
  );

  // Replace the button with a link.
  if (isset($_GET['destination']) && !url_is_external($_GET['destination'])) {
    $path = $_GET['destination'];
  }
  else {
    $path = 'user';
  }

  // Handle different destination urls.
  switch ($path) {
    case 'cart':
      $label = t('Return to your cart');
      break;

    case 'user':
      $label = t('Go to your profile');
      break;

    default:
      $label = t('Go ahead');
  }

  $form['actions']['submit'] = array(
    '#markup' => l($label, $path),
  );

  return $form;
}

/**
 * Submit callback for gp_card_bind_giunticard_form.
 */
function gp_card_create_giunticard_form_create_submit($form, &$form_state) {
  $sso_uid = $form_state['sso_uid'];

  // Create the new card.
  try {
    $card_ean = gp_card_create_remote_card($sso_uid);
  }
  catch (Exception $e) {
    // Show a warning to the user.
    drupal_set_message($e->getMessage(), 'warning');
    return;
  }

  // This form will be always rebuild with this submit handler.
  $form_state['rebuild'] = TRUE;

  // Save EAN for later.
  $form_state['ean'] = $card_ean;

  // Ready for the next step.
  $form_state['step']++;
}
