<?php
/**
 * @file
 * Template file to show info about a Giunti card.
 *
 * Available variables:
 * - $balance: the balance of the card.
 * - $expiration: the expiration date of the card.
 */
?>

<div class="giunticard-info">
  <div class="giunticard-item giunticard-item-image">
    <div class="card-image"></div>
  </div>
  <div class="giunticard-item giunticard-item-card-ean">
    <label for="card-ean">EAN </label>
    <div class="card-ean"><?php print $ean; ?></div>
  </div>
  <div class="giunticard-item giunticard-item-card-password">
    <label for="card-password">Password </label>
    <div class="card-password"><?php print $password; ?></div>
  </div>
  <div class="giunticard-item giunticard-item-card-balance">
    <label for="card-balance">Saldo punti </label>
    <div class="card-balance"><?php print $balance; ?></div>
  </div>
  <div class="giunticard-item giunticard-item-expiration-date">
    <label for="expiration-date">Scadenza </label>
    <div class="expiration-date"><?php print $expiration; ?></div>
  </div>
  <div class="giunticard-item giunticard-item-last-update">
    <label for="last-update">Data ultimo acquisto </label>
    <div class="last-update"><?php print $last_update; ?></div>
  </div>
  <div class="giunticard-item giunticard-item-link-pdf">
    <?php print $pdf_link; ?>
  </div>
  <div class="giunticard-item giunticard-item-app">
    <p><a href="/content/app-giunticard">Scarica la nostra app e porta la tua GiuntiCard sempre sul tuo smartphone</a></p>
  </div>
</div>
