<?php
/**
 * @file
 * Theme functions and template preprocess.
 */

/**
 * Preprocess function for rendering the missing links block.
 */
function template_preprocess_giunti_card_missing_block(&$variables) {
  $account = $variables['account'];

  // Respect destination if needed.
  $options = array();
  if (isset($_GET['destination'])) {
    $options['query']['destination'] = $_GET['destination'];
  }
  else {
    // Otherwise redirect to user page.
    $options['query']['destination'] = 'user';
  }

  $variables['bind'] = l(t('Bind GiuntiCard'), 'giunti-card/bind', $options);
  $variables['create'] = l(t('Create a GiuntiCard'), 'giunti-card/create', $options);
}

/**
 * Preprocess function for user card info.
 */
function template_preprocess_giunti_card_info(&$variables) {
  $card = $variables['card'];
  $ean = $variables['ean'];

  // Prepare a list of data to show.
  $variables['ean'] = $ean;
  $variables['password'] = !empty($card['password']) ? $card['password'] : '';
  $variables['balance'] = !empty($card['card_balance']) ? $card['card_balance'] : 0;
  if (!empty ($card['expiration_date'])) {
    $expiration = DateTime::createFromFormat('Y-m-d', $card['expiration_date'])->getTimestamp();
    $variables['expiration'] = format_date($expiration, 'custom', 'd/m/Y');
  }
  else {
    $variables['expiration'] = '-';
  }
  $expiration = DateTime::createFromFormat('Y-m-d', $card['expiration_date'])->getTimestamp();
  $variables['expiration'] = format_date($expiration, 'custom', 'd/m/Y');
  if (!empty ($card['renewal_date'])) {
    $last_update = DateTime::createFromFormat('Y-m-d', $card['renewal_date'])->getTimestamp();;
    $variables['last_update'] = format_date($last_update, 'custom', 'd/m/Y');
  }
  else {
    $variables['last_update'] = '-';
  }

  // Render the link for the pdf.
  $variables['pdf_link'] = l(t('Show the printable version'), 'giunti-card/download', array(
    'attributes' => array('target' => '_blank'),
  ));
}

/**
 * Theme function to render the points a user earns with a cart.
 */
function theme_gp_card_earn_points($variables) {
  global $user;
  $points = $variables['points'];

  // Do not display 0 point message.
  if (0 == $points) {
    return '';
  }

  $output = '<div class="block-giunti-card"><p class="block-content"><span class="text1">';
  $output .= t('With this purchase you gain <strong>@points points</strong>', array('@points' => $points));
  $output .= '</span><br /><span class="text2">';
  $output .= t('equal to <strong>@amount discount</strong> in our book stores', array(
    '@amount' => gp_card_calculate_points_value($points),
  ));
  $output .= '</span></p></div>';
  if (user_is_logged_in()) {
    $output .= '<div class="goto-wishlist">' . l(t('Go to the wishlist'), 'user/' . $user->uid . '/wishlist') . '</div>';
  }
  return $output;
}

/**
 * Preprocess function for missing giunti card page.
 */
function template_preprocess_giunti_card_missing_page(&$variables) {
  $build = $variables['build'];

  $variables['points'] = $variables['purchase_url'] = FALSE;

  foreach (element_children($build) as $key) {
    $variables[$key] = render($build[$key]);
  }
}
