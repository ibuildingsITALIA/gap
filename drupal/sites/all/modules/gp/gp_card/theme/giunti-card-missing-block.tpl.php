<?php
/**
 * @file
 * Template for redirect message before going to Amazon.
 */
?>
<div class="bind-create-card">
  <p class="bind">
    <?php print t('Do you already own a Giunti Card?') . ' '; ?>
  </p>
  <?php print $bind; ?>

  <p class="create">
    <?php print t("You don't own a Giunti Card yet?") . ' '; ?>
  </p>
  <?php print $create; ?>
</div>
