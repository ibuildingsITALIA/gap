<?php
/**
 * @file
 * Template for missing Giunticard page.
 */
?>
<?php print $points; ?>

<?php if (!empty($purchase_url)) : ?>
  <p class="notice-card">Per accumulare i punti maturati con questo acquisto abbina o crea la tua GiuntiCard.</p>

  <div class="skip-card">
    <p>Se invece non vuoi accumulare punti e sconti sulla tua GiuntiCard, procedi direttamente con l'acquisto su Amazon</p>
    <a class="btn" id="gp-amazon-cart-redirect" href="<?php print $purchase_url; ?>" target="_blank"><?php print t('Go to checkout on Amazon.it'); ?></a>
  </div>
<?php endif; ?>

<div class="bind-create-card-wrapper">
  <?php print $links; ?>
</div>
