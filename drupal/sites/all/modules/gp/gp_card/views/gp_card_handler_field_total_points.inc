<?php
/**
 * @file
 * Definition of gp_card_handler_field_total_points.
 */

class gp_card_handler_field_total_points extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);

    // Render the total points for this cart.
    $result = db_query('SELECT points FROM {gp_card_history} WHERE import_id = :subtag', array(
      ':subtag' => $value,
    ))->fetchCol();

    return array_sum($result);
  }
}
