<?php
/**
 * @file
 * Definition of gp_card_handler_field_giunti_card.
 */

class gp_card_handler_field_giunti_card extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();

    $options['card_field'] = array('default' => 'ean');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['card_field'] = array(
      '#type' => 'select',
      '#title' => t('Field to show'),
      '#options' => array(
        'ean' => t('EAN'),
        'password' => t('Password'),
        'balance' => t('Balance'),
        'expiration' => t('Expiration date'),
      ),
      '#default_value' => $this->options['card_field'],
      '#description' => t('Which field of the card must be shown.'),
    );

    parent::options_form($form, $form_state);
  }

  function render($values) {
    $uid = $this->get_value($values);

    if (empty($uid) || empty($this->options['card_field'])) {
      return '';
    }

    $account = user_load($uid);
    if (empty($account)) {
      return '';
    }

    $field = $this->options['card_field'];
    // We use tokens to DRY the code. Probably this is not good on performance.
    // @todo implement a class to handle cards!
    $value = token_replace("[user:giunti_card:$field]", array('user' => $account), array('clear' => TRUE));

    return $value;
  }
}
