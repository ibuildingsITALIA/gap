<?php
/**
 * @file
 * Views related hooks.
 */

/**
 * Implements hook_views_data().
 */
function gp_card_views_data() {
  $data = array();

  // If date_views module is enabled, use its filter implementation
  // for date fields.
  $date_filter_handler = module_exists('date_views') ? 'date_views_filter_handler_simple' : 'views_handler_filter_date';

  // Virtual card table.
  $data['gp_card']['table']['group'] = t('Virtual card');

  // Advertise this table as possible base table.
  $data['gp_card']['table']['base'] = array(
    'field' => 'card_id',
    'title' => t('Virtual cards'),
  );

  // Expose card_id field.
  $data['gp_card']['card_id'] = array(
    'title' => t('ID'),
    'help' => t('The card ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose uid field as user.
  $data['gp_card']['user'] = array(
    'title' => t('Owner'),
    'help' => t('The user this card belongs to.'),
    'real field' => 'uid',
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'title' => t('Owner'),
      'help' => t('Relate card with the its owner.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'relationship field' => 'uid',
      'label' => t('Owner'),
    ),
  );

  // Expose balance field.
  $data['gp_card']['balance'] = array(
    'title' => t('Balance'),
    'help' => t('The current points balance.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose deposited field.
  $data['gp_card']['deposited'] = array(
    'title' => t('Deposited date'),
    'help' => t('The date the card was deposited to remote service.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  // Expose changed field.
  $data['gp_card']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the card was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  // Card history table.
  $data['gp_card_history']['table']['group'] = t('Virtual card history log');

  // Advertise this table as possible base table.
  $data['gp_card_history']['table']['base'] = array(
    'field' => 'card_id',
    'title' => t('Virtual card history log'),
  );

  // Expose card_id field.
  $data['gp_card_history']['card_id'] = array(
    'title' => t('Card ID'),
    'help' => t('The card ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'title' => t('Card'),
      'help' => t('Relate history log with the card.'),
      'handler' => 'views_handler_relationship',
      'base' => 'gp_card',
      'base field' => 'card_id',
      'relationship field' => 'card_id',
      'label' => t('Card'),
    ),
  );

  // Expose points field.
  $data['gp_card_history']['points'] = array(
    'title' => t('Balance'),
    'help' => t('The points involved in this entry.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Expose created field.
  $data['gp_card_history']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the entry was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => $date_filter_handler,
    ),
  );

  // Expose pseudo cart field.
  $data['gp_card_history']['cart'] = array(
    'title' => t('Subtag'),
    'help' => t('The cart the entry refers to.'),
    'real field' => 'import_id',
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'title' => t('Subtag'),
      'help' => t('Relate history log with the cart.'),
      'handler' => 'views_handler_relationship',
      'base' => 'gp_amazon_cart',
      'base field' => 'subtag',
      'relationship field' => 'import_id',
      'label' => t('Cart'),
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function gp_card_views_data_alter(&$data) {
  if (isset($data['gp_amazon_cart'])) {
    $data['gp_amazon_cart']['card_history'] = array(
      'relationship' => array(
        'title' => t('Card history'),
        'help' => t('Relate cart with log entries.'),
        'handler' => 'views_handler_relationship',
        'base' => 'gp_card_history',
        'base field' => 'import_id',
        'relationship field' => 'subtag',
        'label' => t('Card history'),
      ),
    );

    // Expose a new handler with the total points earned by the cart.
    $data['gp_amazon_cart']['total_points'] = array(
      'title' => t('Total points'),
      'help' => t('The total points earned with the cart.'),
      'real field' => 'subtag',
      'field' => array(
        'handler' => 'gp_card_handler_field_total_points',
      ),
    );
  }

  if (isset($data['users'])) {
    $data['users']['giunti_card'] = array(
      'title' => t('GiuntiCard data'),
      'help' => t('Data about the Giunticard of the user.'),
      'real field' => 'uid',
      'field' => array(
        'handler' => 'gp_card_handler_field_giunti_card',
      ),
    );
  }
}
