<?php
/**
 * @file
 * Token module integration.
 */

/**
 * Implements hook_token_info().
 */
function gp_card_token_info() {
  $types = array();
  $types['gp_card'] = array(
    'name' => t('Giunti virtual card'),
    'description' => t('Tokens related to Giunti al Punto virtual cards.'),
    'needs-data' => 'gp_card',
  );
  $types['giunti_card'] = array(
    'name' => t('Giunti real card'),
    'description' => t('Tokens related to Giunti cards.'),
    'needs-data' => 'giunti_card',
  );

  $gp_card = array();
  $gp_card['id'] = array(
    'name' => t('Card ID'),
    'description' => t('The card identifier.'),
  );
  $gp_card['owner'] = array(
    'name' => t('Card owner'),
    'description' => t('The user that owns this card.'),
    'type' => 'user',
  );
  $gp_card['balance'] = array(
    'name' => t('Card balance'),
    'description' => t('The current balance of the card.'),
  );
  $gp_card['deposited'] = array(
    'name' => t('Card last deposit time'),
    'description' => t('The last time the card has points deposited.'),
    'type' => 'date',
  );
  $gp_card['changed'] = array(
    'name' => t('Card changed'),
    'description' => t('The date the card was most recently updated.'),
    'type' => 'date',
  );

  $giunti_card = array();
  $giunti_card['ean'] = array(
    'name' => t('Card EAN'),
    'description' => t('The card EAN code.'),
  );
  $giunti_card['password'] = array(
    'name' => t('Card password'),
    'description' => t('The password of the card.'),
  );
  $giunti_card['balance'] = array(
    'name' => t('Card balance'),
    'description' => t('The current balance of the card.'),
  );
  $giunti_card['expiration'] = array(
    'name' => t('Card expiration time'),
    'description' => t('The date the card will expire.'),
    'type' => 'date',
  );

  return array(
    'types' => $types,
    'tokens' => array('gp_card' => $gp_card, 'giunti_card' => $giunti_card),
  );
}

/**
 * Implements hook_token_info_alter().
 */
function gp_card_token_info_alter(&$data) {
  $data['tokens']['user']['gp_card'] = array(
    'name' => t('Giunti virtual card'),
    'description' => t('Tokens related to Giunti al Punto user virtual card.'),
    'type' => 'gp_card',
  );
  $data['tokens']['user']['giunti_card'] = array(
    'name' => t('Giunti real card'),
    'description' => t('Tokens related to Giunti cards.'),
    'type' => 'giunti_card',
  );
}

/**
 * Implements hook_tokens().
 */
function gp_card_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $language_code = isset($options['language']) ? $options['language']->language : NULL;
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'user' && !empty($data['user'])) {
    $account = $data['user'];

    // No remote card for default.
    $remote_card = FALSE;
    // Load the giunti card if needed.
    $giunti_card_tokens = token_find_with_prefix($tokens, 'giunti_card');
    if ($giunti_card_tokens || isset($tokens['giunti_card'])) {
      // Get user data from identity manager.
      $data = gp_user_im_user_data_get_by_account($account);
      if (!empty($data[GP_CARD_IM_FIELD])) {
        $remote_card = gp_card_remote_get_card_info($data[GP_CARD_IM_FIELD]);
      }
    }

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'gp_card':
          // Return the card id.
          $replacements[$original] = gp_card_id_get_by_user_uid($account->uid);
          break;

        case 'giunti_card':
          $replacements[$original] = !empty($remote_card['ean_code']) ? check_plain($remote_card['ean_code']) : '';
          break;
      }
    }

    if ($gp_card_tokens = token_find_with_prefix($tokens, 'gp_card')) {
      $db_card = gp_card_get_by_user_uid($account->uid);
      $replacements += token_generate('gp_card', $gp_card_tokens, array('gp_card' => $db_card), $options);
    }

    // Replace sub-tokens only if we have the remote card.
    if ($giunti_card_tokens && $remote_card) {
      $replacements += token_generate('giunti_card', $giunti_card_tokens, array('giunti_card' => $remote_card), $options);
    }
  }

  if ($type == 'gp_card' && !empty($data['gp_card'])) {
    $gp_card = $data['gp_card'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          $replacements[$original] = $gp_card->card_id;
          break;

        case 'balance':
          $replacements[$original] = $gp_card->balance;
          break;

        // Default values for the chained tokens handled below.
        case 'owner':
          $owner = user_load($gp_card->uid);
          $name = format_username($owner);
          $replacements[$original] = $sanitize ? check_plain($name) : $name;
          break;

        case 'deposited':
          $replacements[$original] = format_date($gp_card->deposited, 'medium', '', NULL, $language_code);
          break;

        case 'changed':
          $replacements[$original] = format_date($gp_card->changed, 'medium', '', NULL, $language_code);
          break;
      }
    }

    if ($owner_tokens = token_find_with_prefix($tokens, 'owner')) {
      $owner = user_load($gp_card->uid);
      $replacements += token_generate('user', $owner_tokens, array('user' => $owner), $options);
    }

    if ($deposited_tokens = token_find_with_prefix($tokens, 'deposited')) {
      $replacements += token_generate('date', $deposited_tokens, array('date' => $gp_card->deposited), $options);
    }

    if ($changed_tokens = token_find_with_prefix($tokens, 'changed')) {
      $replacements += token_generate('date', $changed_tokens, array('date' => $gp_card->changed), $options);
    }
  }

  if ($type == 'giunti_card' && !empty($data['giunti_card'])) {
    $giunti_card = $data['giunti_card'];
    $expiration = DateTime::createFromFormat('Y-m-d', $giunti_card['expiration_date'])->getTimestamp();

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'ean':
          $replacements[$original] = !empty($giunti_card['ean_code']) ? check_plain($giunti_card['ean_code']) : '';
          break;

        case 'password':
          $replacements[$original] = !empty($giunti_card['password']) ? check_plain($giunti_card['password']) : '';
          break;

        case 'balance':
          $replacements[$original] = !empty($giunti_card['card_balance']) ? check_plain($giunti_card['card_balance']) : '0';
          break;

        // Default values for the chained tokens handled below.
        case 'expiration':
          $expiration = DateTime::createFromFormat('Y-m-d', $giunti_card['expiration_date'])->getTimestamp();
          $replacements[$original] = format_date($expiration, 'medium', '', NULL, $language_code);
          break;
      }
    }

    if ($expiration_tokens = token_find_with_prefix($tokens, 'expiration')) {
      $replacements += token_generate('date', $expiration_tokens, array('date' => $expiration), $options);
    }
  }

  return $replacements;
}
