<?php
/**
 * @file
 * Hooks provided by the gp_card module.
 */

/**
 * Act on balance submission of earned points of an account.
 *
 * @param object $account
 *   The account that earned points.
 *
 * @param string $ean
 *   The ean of the card.
 *
 * @param int $balance
 *   The points earned.
 */
function hook_gp_card_balance_submitted($account, $ean, $balance) {
  // Send mail.
}
