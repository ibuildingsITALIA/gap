<?php
/**
 * @file
 * Rules integration for the gp_card module.
 */

/**
 * Implements hook_rules_event_info().
 */
function gp_card_rules_event_info() {
  $events = array();

  $events['gp_card_fan_on_fb'] = array(
    'label' => t('User has become fan on facebook'),
    'group' => t('Giunti al Punto card'),
    'variables' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
    ),
  );

  $events['gp_card_survey_complete'] = array(
    'label' => t('User has complete the survey'),
    'group' => t('Giunti al Punto card'),
    'variables' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
    ),
  );

  return $events;
}

/**
 * Implements hook_rules_condition_info().
 */
function gp_card_rules_condition_info() {
  $conditions = array();

  $conditions['gp_card_cart_has_points'] = array(
    'label' => t('Amazon cart is suitable to earn points'),
    'parameter' => array(
      'amazon_cart' => array(
        'type' => 'struct',
        'label' => t('AmazonCart object'),
      ),
    ),
    'group' => t('Giunti al Punto card'),
    'callbacks' => array(
      'execute' => 'gp_card_rules_cart_has_points',
    ),
  );

  $conditions['gp_card_item_has_points'] = array(
    'label' => t('Amazon item is suitable to earn points'),
    'parameter' => array(
      'item' => array(
        'type' => 'struct',
        'label' => t('AmazonItem object'),
      ),
    ),
    'group' => t('Giunti al Punto card'),
    'callbacks' => array(
      'execute' => 'gp_card_rules_item_has_points',
    ),
  );

  $conditions['gp_card_user_has_giunticard'] = array(
    'label' => t('User has a GiuntiCard'),
    'parameter' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
    ),
    'group' => t('Giunti al Punto card'),
    'callbacks' => array(
      'execute' => 'gp_card_rules_user_has_giunticard',
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function gp_card_rules_action_info() {
  $actions = array();

  $actions['gp_card_calculate_cart_points'] = array(
    'label' => t('Calculate Amazon cart points'),
    'parameter' => array(
      'amazon_cart' => array(
        'type' => 'struct',
        'label' => t('AmazonCart object'),
      ),
    ),
    'provides' => array(
      'points' => array(
        'type' => 'integer',
        'label' => t('Points that can be earned'),
      ),
    ),
    'group' => t('Giunti al Punto card'),
    'callbacks' => array(
      'execute' => 'gp_card_rules_calculate_cart_points',
    ),
  );

  $actions['gp_card_calculate_item_points'] = array(
    'label' => t('Calculate Amazon item points'),
    'parameter' => array(
      'item' => array(
        'type' => 'struct',
        'label' => t('AmazonItem object'),
      ),
    ),
    'provides' => array(
      'points' => array(
        'type' => 'integer',
        'label' => t('Points that can be earned'),
      ),
    ),
    'group' => t('Giunti al Punto card'),
    'callbacks' => array(
      'execute' => 'gp_card_rules_calculate_item_points',
    ),
  );

  return $actions;
}

/**
 * Rules condition: checks if an Amazon Cart is suitable to earn points.
 */
function gp_card_rules_cart_has_points($amazon_cart) {
  return gp_card_calculate_cart_points($amazon_cart);
}

/**
 * Rules condition: checks if an Amazon Cart is suitable to earn points.
 */
function gp_card_rules_item_has_points($item) {
  return gp_card_calculate_item_points($item);
}

/**
 * Rules condition: checks if an user has a Giunticard bound.
 */
function gp_card_rules_user_has_giunticard($user) {
  $data = gp_user_im_user_data_get_by_account($user);
  return !empty($data[GP_CARD_IM_FIELD]);
}

/**
 * Rules action: returns the points of an Amazon cart.
 */
function gp_card_rules_calculate_cart_points($amazon_cart) {
  $points = gp_card_calculate_cart_points($amazon_cart);

  return array('points' => $points);
}

/**
 * Rules action: returns the points of an Amazon item.
 */
function gp_card_rules_calculate_item_points($item) {
  $points = gp_card_calculate_item_points($item);

  return array('points' => $points);
}
