<?php
/**
 * @file
 * Administration forms.
 */

/**
 * Admin settings form.
 */
function gp_card_admin_settings_form() {
  $form = array();

  $form['card_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Card API settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['card_api']['gp_card_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Card API url'),
    '#required' => TRUE,
    '#default_value' => variable_get('gp_card_api_url', ''),
    '#size' => 100,
  );

  $form['card_api']['gp_card_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Card API key'),
    '#required' => TRUE,
    '#default_value' => variable_get('gp_card_api_key', ''),
    '#size' => 100,
  );

  $form['card_points'] = array(
    '#type' => 'fieldset',
    '#title' => t('Card points settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['card_points']['gp_card_euro_per_point'] = array(
    '#type' => 'textfield',
    '#title' => t('How much euro a point is worth'),
    '#required' => TRUE,
    '#default_value' => variable_get('gp_card_euro_per_point', 3),
    '#size' => 20,
    '#field_prefix' => '&euro;',
  );

  $form['card_points']['gp_card_discount_per_point'] = array(
    '#type' => 'textfield',
    '#title' => t('How much euro of discount a point gives'),
    '#required' => TRUE,
    '#default_value' => variable_get('gp_card_discount_per_point', 0.60),
    '#size' => 20,
    '#field_prefix' => '&euro;',
  );

  $form['email_title'] = array(
    '#type' => 'item',
    '#title' => t('E-mails'),
  );
  $form['email'] = array(
    '#type' => 'vertical_tabs',
  );

  // Add the token tree UI.
  $form['email']['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('user'),
    '#show_restricted' => TRUE,
    '#dialog' => TRUE,
    '#weight' => 90,
  );

  $form['email_merge_books'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cart merged (with books)'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'email',
  );

  $form['email_merge_books']['gp_card_email_merge_books_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('gp_card_email_merge_books_subject', ''),
    '#maxlength' => 180,
  );

  $form['email_merge_books']['gp_card_email_merge_books_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => variable_get('gp_card_email_merge_books_body', ''),
    '#rows' => 15,
  );

  $form['email_oneclickbuy_ebooks'] = array(
    '#type' => 'fieldset',
    '#title' => t('One click buy (for e-books)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'email',
  );

  $form['email_oneclickbuy_ebooks']['gp_card_email_oneclickbuy_ebooks_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('gp_card_email_oneclickbuy_ebooks_subject', ''),
    '#maxlength' => 180,
  );

  $form['email_oneclickbuy_ebooks']['gp_card_email_oneclickbuy_ebooks_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => variable_get('gp_card_email_oneclickbuy_ebooks_body', ''),
    '#rows' => 15,
  );

  $form['email_points_earned'] = array(
    '#type' => 'fieldset',
    '#title' => t('Points earned'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'email',
  );

  $form['email_points_earned']['gp_card_email_points_earned_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('gp_card_email_points_earned_subject', ''),
    '#maxlength' => 180,
  );

  $form['email_points_earned']['gp_card_email_points_earned_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => variable_get('gp_card_email_points_earned_body', ''),
    '#rows' => 15,
  );

  return system_settings_form($form);
}
