<?php

/**
 * Implements hook_drush_help().
 */
function gp_card_drush_help($command) {
  switch ($command) {
    case 'drush:gpcard-sync-points':
      return dt('Syncs points with Giunti card api');

  }
}

/**
 * Implements hook_drush_command().
 */
function gp_card_drush_command() {
  $items = array();
  $items['gpcard-sync-points'] = array(
    'description' => dt('Syncs points'),
    'arguments' => array(),
  );
  return $items;
}

/**
 * Callback function for drush in order to update points to cards
 */
function drush_gp_card_gpcard_sync_points() {
  // Get the last time we prepared sync queue.
  $last_sync = variable_get('gp_card_last_sync', 0);
  variable_set('gp_card_last_sync_start', time());
  variable_set('gp_card_last_deposited', time());
  drush_log('Request time: '.REQUEST_TIME);
  if (REQUEST_TIME >= ($last_sync + GP_CARD_SYNC_INTERVAL)) {
    // Create the points array to allow cron calls to process it.
    $items = gp_card_create_points_array();

    foreach ($items as $item) {
      gp_card_commit_user_balance_worker($item);
      
      $last_deposited = gp_card_check_last_deposited($item);
      if ((0 < $last_deposited) && (variable_get('gp_card_last_deposited') > $last_deposited)) {
        variable_set('gp_card_last_deposited', $last_deposited);
      }
    }
    // Update last sync time.
    variable_set('gp_card_last_sync', REQUEST_TIME);
  }
}