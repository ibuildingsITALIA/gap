<?php
/**
 * @file
 * Page callbacks for card module.
 */

/**
 * Page callback to add (bind) a card to the current user.
 */
function gp_card_bind_giunticard_page() {
  global $user;

  // Get the user data to check if he owns already a card.
  $account = user_load($user->uid);
  $data = gp_user_im_user_data_get_by_account($account);
  if (FALSE === $data || !empty($data[GP_CARD_IM_FIELD])) {
    drupal_access_denied();
    drupal_exit();
  }

  // Get the user sso_uid.
  $sso_uid = gp_user_get_sso_uid($account);

  if (FALSE === $sso_uid) {
    // Failed retrieving sso_uid.
    drupal_set_message(t('An error has occurred retrieving your profile. Please try again later.'), 'warning');
    drupal_exit();
  }

  // Include the forms file.
  module_load_include('inc', 'gp_card', 'gp_card.forms');

  return drupal_get_form('gp_card_bind_giunticard_form', $sso_uid);
}

/**
 * Page callback to create a new card to the current user.
 */
function gp_card_create_giunticard_page() {
  global $user;

  // Get the user data to check if he owns already a card.
  $account = user_load($user->uid);
  $data = gp_user_im_user_data_get_by_account($account);
  if (FALSE === $data) {
    drupal_access_denied();
    drupal_exit();
  }

  if (!empty($data[GP_CARD_IM_FIELD])) {
    drupal_goto('user');
  }

  // Get the user sso_uid.
  $sso_uid = gp_user_get_sso_uid($account);

  if (FALSE === $sso_uid) {
    // Failed retrieving sso_uid.
    drupal_set_message(t('An error has occurred retrieving your profile. Please try again later.'), 'warning');
    drupal_exit();
  }

  // Create the new card.
  try {
    gp_card_create_remote_card($sso_uid);
  }
  catch (Exception $e) {
    // Show a warning and return to user page
    // (or the destination page if provided).
    drupal_set_message($e->getMessage(), 'warning');
    drupal_goto('user');
  }

  // Empty default.
  $options = array(
    'query' => array(),
  );

  // If a destination is already provided, use it.
  if (isset($_GET['destination'])) {
    $options['query'] = drupal_get_destination();
  }

  // Show the successful creation message.
  $options['query']['card_created'] = 1;

  // Ignore destination in this redirect.
  unset($_GET['destination']);
  // Always send the user to the profile page because it handles the creation
  // message.
  drupal_goto('user', $options);
}

/**
 * Junction page to show links to card binding/creation.
 */
function gp_card_missing_page() {
  global $user;

  // Get the user data to check if he owns already a card.
  $account = user_load($user->uid);
  $data = gp_user_im_user_data_get_by_account($account);
  if (FALSE === $data || !empty($data[GP_CARD_IM_FIELD])) {
    drupal_access_denied();
    drupal_exit();
  }

  $build = array(
    'info' => array(
      '#theme' => 'giunti_card_missing_page',
    ),
  );

  $build['info']['links'] = array(
    '#theme' => 'giunti_card_missing_block',
    '#account' => $account,
  );

  // If possible, show points and purchase link
  // if a cart or an item is available.
  if ($context = gp_card_points_context_info()) {
    $build['info']['points'] = theme('gp_card_earn_points', array('points' => $context['points']));
    $build['info']['purchase_url'] = $context['purchase_url'];
  }

  return $build;
}

/**
 * Page callback to serve the Giunti card pdf.
 */
function gp_card_download_giunticard_page() {
  global $user;

  // Get the user data.
  $account = user_load($user->uid);
  $data = gp_user_im_user_data_get_by_account($account);

  // Access denied on data error.
  if (FALSE === $data) {
    drupal_access_denied();
    drupal_exit();
  }

  if (empty($data[GP_CARD_IM_FIELD])) {
    drupal_goto('giunti-card-missing');
  }

  $url = trim(variable_get('gp_card_api_url'), '/');
  $endpoint = url($url . '/get_card_pdf', array(
    'query' => array(
      'access_key' => variable_get('gp_card_api_key', ''),
      'ean' => $data[GP_CARD_IM_FIELD],
    ),
    'absolute' => TRUE,
  ));

  // Use curl to retrieve the file.
  $ch = curl_init();
  curl_setopt_array($ch, array(
    CURLOPT_URL => $endpoint,
    CURLOPT_HEADER => FALSE,
    CURLOPT_VERBOSE => FALSE,
    CURLOPT_FOLLOWLOCATION => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_TIMEOUT => 10,
  ));
  $contents = curl_exec($ch);
  $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);

  if (200 !== $status || empty($contents)) {
    // An error has occurred on file retrieval.
    drupal_set_message(t('An error has occurred while retrieving the file'), 'error');
    return '';
  }

  // Prevent this page from being cached.
  drupal_page_is_cacheable(FALSE);

  // Set content type.
  drupal_add_http_header('Content-Type', 'application/pdf; charset=utf-8');

  // Return the file.
  print $contents;
  drupal_exit();
}

function gp_card_check_sync() {
  $last_sync_start = variable_get('gp_card_last_sync_start', 0);
  $last_sync = variable_get('gp_card_last_sync', 0);
  $last_deposited = variable_get('gp_card_last_deposited', 0);

  if (time() >= ($last_sync_start + GP_CARD_SYNC_INTERVAL)) {
    print 'ERROR: too long time from last sync start - ' . $last_sync_start;
    exit;
  }
  if (time() >= ($last_sync + GP_CARD_SYNC_INTERVAL+(2*60*60))) {
    print 'ERROR: too long time from last sync end - ' . $last_sync;
    exit;
  }
  if (time() >= ($last_deposited + (2*GP_CARD_SYNC_INTERVAL+(1*60*60)))) {
    print 'ERROR: pending cards - ' . date('Y-m-d h:i', $last_deposited);
    exit;
  }
  print 'OK';
  exit;
}
