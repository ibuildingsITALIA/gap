#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${SCRIPT_DIR}/import_commons.sh"

if [[ $# -gt 0 ]]; then
		while [[ $# -gt 0 ]]; do
			parseArg $1 
			shift
		done
	else
		echo "Script launched only in download mode and no logging"
		echo "Usage: -i (launch import) -l (store output in a log)"
fi

sleep 3


colorLog2 $LINE 
colorLog2 "Procedura di scaricamento customer feed da Amazon: START"
colorLog2 "Data ${TIMESTAMP}" 
colorLog2 $LINE


cd $SCRIPT_DIR
java -jar giunti-amazon-cert-1.1-SNAPSHOT-jar-with-dependencies.jar --downloadDir ../../private/customers --keyDir amazon_feed_keys

if [ $? -ne 0 ]; then
   colorErr "Command returned with error"
fi

colorLog2 $LINE 
colorLog2 "Procedura di scaricamento customer feed da Amazon: END"
colorLog2 "Data ${TIMESTAMP}"
colorLog2 $LINE
colorLog2 ""
colorLog2 ""