#!/bin/bash
#set -x
#set -e

SCRIPT_DIR="/var/www/gsamazon/drupal/scripts"
source "${SCRIPT_DIR}/import_commons.sh"




## CONTROLLI
function checkFileExistsInLocal() {
	FILE_NAME=$1
	FILE_PATH=$DESTINATION_DIR/$FILE_NAME
	if [ ! -e "$FILE_PATH" ]; then
  		colorLog "+ Il file ${FILE_PATH} non esiste"
  		colorLog "+ Provvedo a scaricarlo"
  		downloadFtpFile $FILE_NAME;
  	else	
  		colorLog "+ Il file ${FILE_PATH} è gia presente"
  	
	fi
}
function checkFileExistsInFtp() {
	FILE_NAME=$1
	URLtoCheck="ftp://$GIUNTI_USR:$GIUNTI_PASSWD@ftp.ie-online.it/pub/$FILE_NAME"
	wget -O/dev/null -q $URLtoCheck && return 1 || return 0
}




##  DOWNLOAD
function downloadFtpFile() {
	FILE_NAME=$1
	if checkFileExistsInFtp $FILE_NAME ; then
    	colorErr "+ Il file ${FILE_NAME} non esiste  nel file server remoto ftp.ie-online.it/pub/"
    	exit 0;
	else
		colorLog "+ Download di ${FILE_NAME}"
		wget "ftp://$GIUNTI_USR:$GIUNTI_PASSWD@ftp.ie-online.it/pub/$FILE_NAME"	
	fi
}

checkDirectories

### 1) CONTROLLO DEGLI ARGOMENTI 
if [[ $# -gt 0 ]]; then
	while [[ $# -gt 0 ]]; do
		parseArg $1 
		shift
	done
else
	echo "Script launched only in download mode and no logging"
	echo "Usage: -i (launch import) -l (store output in a log)"
fi
sleep 3








colorLog $LINE 
colorLog "Procedura di import/aggiornameno dati da Alice: START"
colorLog "Data ${TIMESTAMP}" 
colorLog $LINE

colorLog "+ Preparazione procedura per l'Update del db"
##############
### FILE *.NEW
colorLog "+ cd ${DESTINATION_DIR}"
cd $DESTINATION_DIR

FILE_NAME="${FILENAME_PREFIX}${DATE_SUFFIX}.new"
checkFileExistsInLocal $FILE_NAME

colorLog "+ Modifiche preliminari per inserimento ${FILE_NAME} -> ${MIGRATE_FILE}"
`cp $FILE_NAME $MIGRATE_FILE`;
colorLog "+ cd ${DRUPAL_DIR}"
cd $DRUPAL_DIR

colorLog "+ update ==> ${COMMAND_UPDATE} &>> ${LOG_FILE} <=="
$COMMAND_UPDATE &>> "${LOG_FILE}";

##############
### FILE *.VAR
colorLog "+ cd ${DESTINATION_DIR}"
cd $DESTINATION_DIR

FILE_NAME="${FILENAME_PREFIX}${DATE_SUFFIX}.var"
checkFileExistsInLocal $FILE_NAME

colorLog "+ Modifiche preliminari per update ${FILE_NAME} -> ${MIGRATE_FILE}"
colorLog "+ cp $FILE_NAME $MIGRATE_FILE"
`cp $FILE_NAME $MIGRATE_FILE`;

colorLog "+ cd ${DRUPAL_DIR}"
cd $DRUPAL_DIR

colorLog "+ update ==> ${COMMAND_UPDATE} &>> ${LOG_FILE} <=="
$COMMAND_UPDATE &>> "${LOG_FILE}";


##############
### FILE *.DEL
colorLog "+ cd ${DESTINATION_DIR}"
cd $DESTINATION_DIR

FILE_NAME="${FILENAME_PREFIX}${DATE_SUFFIX}.del"
checkFileExistsInLocal $FILE_NAME

colorLog "+ Modifiche preliminari per cancellazione ${FILE_NAME} -> ${MIGRATE_FILE}"

colorLog "+ cd ${DRUPAL_DIR}"
cd $DRUPAL_DIR
colorLog "+ delete ==> ${COMMAND_DELETE} ${DESTINATION_DIR}/${FILE_NAME} &>> ${LOG_FILE} <=="

if [ "$LOG" != "log" ]; then
	$COMMAND_DELETE ${DESTINATION_DIR}/${FILE_NAME} &>> "${LOG_FILE}";
else 
	$COMMAND_DELETE ${DESTINATION_DIR}/${FILE_NAME} &>> "${LOG_FILE}";
fi

## lancio gli script di aggiornamento

colorLog $LINE
colorLog "Procedura di import/aggiornameno dati da Alice: END"
colorLog $LINE
colorLog ""
colorLog ""
