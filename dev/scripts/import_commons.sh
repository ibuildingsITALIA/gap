#!/bin/bash
## VARIABILI in comune
LINE="++++++++++++++++++++++++++++++++++++++++++++++++++++++"
DESTINATION_DIR="/var/www/gsamazon/private"
LOG_PATH="/home/susanna/log"
LOG_FILE="${LOG_PATH}/imports.log"
DRUPAL_DIR="/var/www/gsamazon/drupal"
LOG="off"
IMPORT="off"

## VARIABILI AMAZON IMPORT
DATAFEEDAMAZON_URL="https://assoc-datafeeds-eu.amazon.com/datafeed/getFeed?filename="
DATAFEEDAMAZON_PASS="moogiu1206"
DATAFEEDAMAZON_USER="moodlestore-21"
DATAFEEAMAZON_PROGRESSIVE_NUMBER="${DESTINATION_DIR}/progressive_number.log"
DATAFEEAMAZON_COMMAND="drush import-amazon-xml"
DATAFEEDAMAZON_DATE_SUFFIX=`date -d 'last Friday' '+%Y%m%d'`
DATAFEEAMAZON_FILEPREFIX="it_retail_books_delta_${DATAFEEDAMAZON_DATE_SUFFIX}"

## VARIABILI ALICE IMPORT
MIGRATE_FILE="migrate.csv"
FILENAME_PREFIX="giuntialpunto_"
GIUNTI_USR="giunti"
GIUNTI_PASSWD="ods616"
DATE_SUFFIX=`date -d 'yesterday'  '+%Y%m%d'`
TIMESTAMP=`date '+%d-%m-%Y %H:%M'`
COMMAND_UPDATE="drush mi AliceBooks --update --instrument  --strict=0"
COMMAND_DELETE="drush delete-isbn-db"


function colorErr() {
	if [ "$LOG" != "log" ]; then
		echo -e "\e[0;41m" $1 "\e[0m"
	else 
		printf "$1\n" &>> "${LOG_FILE}"
	fi
}

function colorLog2() 
{
	if [ "$LOG" != "log" ]; then
		echo -e "\e[0;46m" $1 "\e[0m"
	else 
		printf "$1\n" &>> "${LOG_FILE}"
	fi
}

function colorLog() {

    if [ "$LOG" != "log" ]; then

		echo -e "\e[0;42m" $1 "\e[0m"
	else 
		printf "$1\n" &>> "${LOG_FILE}"
	fi
}

function setLogFile() 
{
	if [ "$1" != "log" ]; then	
		LOG_FILE="/dev/null"
	fi
}

function parseArg() 
{

	if [ "$1" = "-l" ]; then	
    	LOG="log"
    	setLogFile $LOG
    	printf "Logging ON $1\n" &>> "${LOG_FILE}"
    	echo "Logging ON $1"
    else 
    	if [ "$1" = "-i" ]; then	
    		IMPORT="import"
    		printf "Import ON $1\n" &>> "${LOG_FILE}"
    		echo "Import ON $1"
    	else
    		printf "Invalid argument $1\n" &>> "${LOG_FILE}" 
    		echo "Invalid argument $1"
    		printf "Usage: -i (launch import) -l (store output in a log)\n" &>> "${LOG_FILE}" 
    		echo "Usage: -i (launch import) -l (store output in a log)"
    		exit 0;
    	fi
    fi
}

function checkDirectories() {
	## controllo che la dir di destinazione esista
	if [ ! -d "$DESTINATION_DIR" ]; then
  		colorErr "La directory ${DESTINATION_DIR} non esiste"
  		exit 0;
	fi

	## e che sia scrivibile dall'utente che lancia lo script
	perm=$(stat -c %a "$DESTINATION_DIR")
	if [ $perm != "777" ]; then
	  colorErr "La directory ${DESTINATION_DIR} non ha i permessi 777"
	  exit 0;
	fi

	## controllo che la dir di destinazione esista
	if [ ! -d "$DRUPAL_DIR" ]; then
		colorErr "La directory ${DRUPAL_DIR} non esiste"
		exit 0;
	fi

	## e che sia scrivibile dall'utente che lancia lo script
	perm=$(stat -c %a "$DRUPAL_DIR")
	if [ $perm != "777" ]; then
		colorErr "La directory ${DRUPAL_DIR} non ha i permessi 777"
		exit 0;
	fi

	## controllo che la dir di destinazione esista
	if [ ! -d "$LOG_PATH" ]; then
		colorErr "La directory ${LOG_PATH} non esiste"
		exit 0;
	fi

	## e che sia scrivibile dall'utente che lancia lo script
	perm=$(stat -c %a "$LOG_PATH")
	if [ $perm != "777" ]; then
		colorErr "La directory ${LOG_PATH} non ha i permessi 777"
		exit 0;
	fi

	
}



	