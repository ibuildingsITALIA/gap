#!/bin/bash

SCRIPT_DIR="/var/www/gsamazon/dev/scripts"
source "${SCRIPT_DIR}/import_commons.sh"
TIMESTAMP=`date '+%d-%m-%Y %H:%M'`



#Util Functions
function checkFileExistsInFeed() {
	FILE="$DESTINATION_DIR/$1"
	if [[ $(find $FILE -type f -size +51200c 2>/dev/null) ]]; then
		return 1
    else 
    	colorLog2 "+ File ${FILE} does not exist in Amazon Feed. Exiting..."
    	return 0
	fi
}

function removeFile() {

	FILE="$DESTINATION_DIR/$1"
	colorLog2 "+ Removing fake file ${FILE}"
	rm $FILE
}

function downloadDataFeedFile() {
	FILE=$1
	colorLog2 "+ Download di ${FILE}"
	colorLog2 `wget --http-user=$DATAFEEDAMAZON_USER --http-password=$DATAFEEDAMAZON_PASS -O $DESTINATION_DIR/$FILE $DATAFEEDAMAZON_URL$FILE`
}


checkDirectories


if [[ $# -gt 0 ]]; then
		while [[ $# -gt 0 ]]; do
			parseArg $1 
			shift
		done
	else
		echo "Script launched only in download mode and no logging"
		echo "Usage: -i (launch import) -l (store output in a log)"
fi

sleep 3





colorLog2 $LINE 
colorLog2 "Procedura di allineamento dati da AMAZON FEED: START"
colorLog2 "Data ${TIMESTAMP}" 
colorLog2 $LINE

### 3) controllo l'esistenza del file che indica il progressivo dei download
if [ ! -e "$DATAFEEAMAZON_PROGRESSIVE_NUMBER" ]; then
	PROG=1
	colorLog2 "+ No ${DATAFEEAMAZON_PROGRESSIVE_NUMBER} file found!!"
else
	PROG=$(sed -n "1p" $DATAFEEAMAZON_PROGRESSIVE_NUMBER)
fi

colorLog2 "+ Progressive number starts from ${PROG}!"
FILE_NAME="${DATAFEEAMAZON_FILEPREFIX}_${PROG}.delta.xml"
FILE_NAME_GZ="${FILE_NAME}.gz" 

while : ; 
do
	downloadDataFeedFile $FILE_NAME_GZ
	checkFileExistsInFeed $FILE_NAME_GZ

	if [ $? -eq 0 ]; then
		removeFile $FILE_NAME_GZ
		break
	fi	
	
	gunzip -f $DESTINATION_DIR/$FILE_NAME_GZ

	colorLog2 "+ cd ${DRUPAL_DIR}"
 	cd $DRUPAL_DIR

	

	if [ "$IMPORT" == "import" ]; then
		colorLog2 "+ ==> $DATAFEEAMAZON_COMMAND ${DESTINATION_DIR}/${FILE_NAME} &>> ${LOG_FILE}"
		if [ "$LOG" != "log" ]; then
	 		$DATAFEEAMAZON_COMMAND ${DESTINATION_DIR}/${FILE_NAME} &>> "${LOG_FILE}";
		else 
	 		$DATAFEEAMAZON_COMMAND ${DESTINATION_DIR}/${FILE_NAME} &>> "${LOG_FILE}";
		fi
	fi

	sleep 3
	colorLog2 "Rimuovo il file ${FILE_NAME}"
	removeFile $FILE_NAME

	PROG=$(( $PROG + 1 ))
	FILE_NAME="${DATAFEEAMAZON_FILEPREFIX}_${PROG}.delta.xml"
	FILE_NAME_GZ="${FILE_NAME}.gz"
done
printf $PROG > "${DATAFEEAMAZON_PROGRESSIVE_NUMBER}"

colorLog2 $LINE 
colorLog2 "Procedura di allineamento dati da AMAZON FEED: END"
colorLog2 "Data ${TIMESTAMP}"
colorLog2 $LINE
colorLog2 ""
colorLog2 ""